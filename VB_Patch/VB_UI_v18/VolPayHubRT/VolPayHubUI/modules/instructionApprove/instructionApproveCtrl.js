VolpayApp.controller("instructionApproveCtrl", function($scope,$http){
 $scope.tableDisp = false;
   //date
  $http.get( BASEURL + '/rest/v2/payments/date').success(function (data, status) {
   $scope.date=data;   
 }).error(function (data, status, headers, config) {
  $scope.alerts = [{
    type: 'danger',
    msg: data.responseMessage
  }
  ];
});

  //session
  $scope.getSession = function(sessionType){

    if(sessionType == 'cheque'){
      $http.get( BASEURL + '/rest/v2/session/readall').success(function (data, status) {
        $scope.sessions=data;
        $scope.value.SettlementSession =data[0];
      }).error(function (data, status, headers, config) {
        $scope.alerts = [{
          type: 'danger',
          msg: data.responseMessage
        }
        ];
      });
    }
    else {
      if(sessionType == 'bulk'){
        $scope.value.SettlementSession='';
        $http.get( BASEURL + '/rest/v2/bpsui/schedule').success(function (data, status) {
          $scope.sessions=data; 

        }).error(function (data, status, headers, config) {
          $scope.alerts = [{
            type: 'danger',
            msg: data.responseMessage
          }
          ];
        });
      }
    }
  }
//partycode
$scope.getParticipant = function(partyType){
  $http.get( BASEURL + '/rest/v2/partycode/readall?schemeNm='+partyType).success(function (data, status) {
   $scope.participantData=data;   
   triggerSelectDrops();
 }).error(function (data, status, headers, config) {
  $scope.alerts = [{
    type: 'danger',
    msg:data.responseMessage
  }
  ];
});
}



$scope.schemeChange = function(schemename){
if($scope.value!=undefined){
    delete $scope.value.Participant;
  }
  $scope.getSession(schemename);
  $scope.getParticipant(schemename);

}



$scope.schemeChange('cheque');


function triggerSelectDrops() {
    $scope.select2Arr = ["IsForceReset", "Participant" , "Participant1"]
    $(document).ready(function () {

      for (var i in $scope.select2Arr) {
        // console.log("dssgf", $scope.select2Arr[i])
        $("select[name=" + $scope.select2Arr[i] + "]").select2({
          placeholder: 'Select an option',
          minimumInputLength: 0,
          allowClear: true

        })

        if ($scope.select2Arr[i] == 'Participant') {


          for (var jk in $scope.participantData) {
              $('#Participant').append('<option value=' + $scope.participantData[jk].actualvalue + '>' + $scope.participantData[jk].displayvalue + '</option>');
          }

        }
        if ($scope.select2Arr[i] == 'Participant1') {


          for (var jk in $scope.participantData) {
              $('#Participant1').append('<option value=' + $scope.participantData[jk].actualvalue + '>' + $scope.participantData[jk].displayvalue + '</option>');
          }

        }

      }
    })

  }
  setTimeout(function () {
    triggerSelectDrops();
  }, 1000);

  $(window).resize(function () {

    $scope.$apply(function () {
      $scope.alertWidth = $('.tab-content').width();

    });

  });


$scope.intial=function(){

  $scope.tableData=[]
  $scope.tableDisp=false;
  tableData1=[]
  $scope.value.Scheme='cheque';
  $scope.value.Currency='SOL';
  $scope.value.SettlementDate='';
  delete $scope.value.Participant;  
  delete $scope.items;
  $scope.schemeChange('cheque'); 
}




$scope.instructionsearch =function(){
  
var val=angular.copy($scope.value);   
  
  if($scope.value.SettlementDate!=undefined){
    val.SettlementDate = $scope.value.SettlementDate.actualvalue;
  }

  if($scope.value.SettlementSession!=undefined){
    val.SettlementSession = $scope.value.SettlementSession.actualvalue;  
  }
 
  
   $http.post( BASEURL + '/rest/v2/payments/search',val).success(function (data, status) {
         if(!$scope.tableDisp &&  data!=undefined){
    $scope.tableDisp=true;
     }

        }).error(function (data, status, headers, config) {
          $scope.alerts = [{
            type: 'danger',
            msg: data.responseMessage
          }
          ];
        });
      }


$scope.fields=
    [{
      "field_name" : "SettlementDate",
      "label" : "Date",
      "type" : "select"
    },{
      "field_name" : "SettlementSession",
      "label" : "Session",
      "type" : "select",
      },{
      "field_name" : "Currency",
      "label" : "Currency",
      "type" : "select",
      "options" : ['SOL' , 'USD']
    },{
      "field_name" : "Participant",
      "label" : "Participant",
      "type" : "select",
    }]; 


$scope.tableFields=[
						{
							"FieldName":"MakerID",
							"Label":"Maker ID",
						},{
							"FieldName":"Scheme",
							"Label":"Scheme",
						},{
							"FieldName":"Date",
							"Label":"Date",
						},{
							"FieldName":"Currency",
							"Label":"Currency",
						},{
							"FieldName":"Session",
							"Label":"Session",
						},{
							"FieldName":"Participant",
							"Label":"Participant",
						},
						{
							"FieldName":"Action",
							"Label":"Action"
						}
						]
	$scope.tableData=[]
	
$scope.itemsPerPage = 2;
$scope.currentPage = 0;  
$scope.items=$scope.tableData;
$scope.range = function() {
    if($scope.items.length <= $scope.itemsPerPage){
      var rangeSize =1;

    }
    else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
      var rangeSize =2;
    }
    else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
      var rangeSize =3;
    }
    else{
      var rangeSize =4;
    }

    var ret = [];
    var start;

    start = $scope.currentPage;
    if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
    }

    for (var i=start; i<start+rangeSize; i++) {
      ret.push(i);
    }
    return ret;
  };
  $scope.navigate = function(n){
    $scope.currentPage=n-1;
  }
  $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
      $scope.currentPage--;
    }

  };
  $scope.prevRange = function() {

    if ($scope.currentPage >= 4) {

      $scope.currentPage=$scope.currentPage-4;
    }

  };
  $scope.nextRange = function() {
    var len =$scope.items.length/$scope.itemsPerPage;
    if (len-$scope.currentPage > 4) {
      $scope.currentPage=$scope.currentPage+4;
    }

  };
  $scope.prevPageDisabled = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
  };
  $scope.prevRangeDisabled = function() {
    return $scope.currentPage < 4 ? "disabled" : "";
  };
  $scope.nextRangeDisabled = function() {
    var len =$scope.items.length/$scope.itemsPerPage;


    return len-$scope.currentPage < 4 ? "disabled" : "";
  };

  $scope.pageCount = function() {
    return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
  };

  $scope.nextPage = function() {
    if ($scope.currentPage < $scope.pageCount()) {
      $scope.currentPage++;
    }

  };

  $scope.nextPageDisabled = function() {
    
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
  };
});