VolpayApp.controller("searchCtrl", function($scope,$http){
	$scope.num_and_text="Only letters and numbers are valid";
	$scope.num="Only numbers are valid";
	$scope.search="Search";
	$scope.value={};
	$scope.resultTable=false;
	$scope.Transaction_data={
		"transaction_details" :
		[{
			"field_name" : "TransactionID",
			"label" : "Transaction ID",
			"placeholder" : "Enter ID",
			"type" : "text",
			"regex" : "^[a-zA-Z0-9]+$"
		},{
			"field_name" : "Scheme",
			"label" : "Scheme",
			"select" : "true",
			"options" : ["Bulk","Cheque"]
		},{
			"field_name" : "TransactionType",
			"label" : "Transaction type",
			"select" : "true"
		},{
			"field_name" : "OriginatingBankAccountNumber",
			"label" : "Originating Bank Account Number",
			"placeholder" : "Enter bank account",
			"type" : "text",
			"regex" : "^[a-zA-Z0-9]+$"
		},{
			"field_name" : "Session",
			"label" : "Session",
			"select" : "true"
		},{
			"field_name" : "CreditCardNumber",
			"label" : "Credit Card Number",
			"placeholder" : "Enter card number",
			"type" : "text",
			"regex" : "^[0-9]+$"
		},{
			"field_name" : "DestinationBankAccountNumber",
			"label" : "Destination Bank Account Number",
			"placeholder" : "Enter bank Account",
			"type" : "text",
			"regex" : "^[a-zA-Z0-9]+$"
		},{
			"field_name" : "Schedule",
			"label" : "Schedule",
			"select" : "true"
		},{
			"field_name" : "TransactionDate",
			"label" : "Transaction range",
			"placeholder" : "dd/mm/yyyy - dd/mm/yyyy"
		}],
		"currency": 
		[{
			"field_name" : "Currency",
			"label" : "Currency",
			"select" : "true",
			"options" : ["USD" , "PEN"]
		},{
			"field_name" : "AmountFrom",
			"label" : "Amount from",
			"placeholder" : "Enter Amount",
			"type" : "number",
			"regex" : "^[0-9]*(([.]?[0-9]+)|([0-9][.])|([.]))$"
		},{
			"field_name" : "AmountTo",
			"label" : "Amount to",
			"placeholder" : "Enter Amount",
			"type" : "number",
			"regex" : "^[0-9]*(([.]?[0-9]+)|([0-9][.])|([.]))$"
		}]
	}
	$scope.Datahead=[
					{
						"field" : "TransactionID",
						"label" : "Transaction ID"
					},{
						"field" : "OriginatingParticipantID",
						"label" : "Originating Participant ID"
					},{
						"field" : "DateTime",
						"label" : "Date/Time"
					},{
						"field" : "Status",
						"label" : "Status"
					},{
						"field" : "Amount",
						"label" : "Amount"
					}
					]
	/*select box data call*/
	$scope.get_select =function (){
		$http.get( BASEURL + '/rest/v2/bpsui/schedule').success(function (data, status) {
			$scope.Schedule=data;
		}).error(function (data, status, headers, config) {
			$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
			];
		});
		$http.get( BASEURL + '/rest/v2/bpsui/session').success(function (data, status) {
			$scope.Session=data;
		}).error(function (data, status, headers, config) {
			$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
			];
		});
		$http.get( BASEURL + '/rest/v2/bpsui/transactiontype').success(function (data, status) {
			$scope.TransactionType=data;
		}).error(function (data, status, headers, config) {
			$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
			];
		});
	}
	$scope.get_select();
	/*search call*/
	$scope.initial=function(req){
		$http.post( BASEURL + '/rest/v2/bpsui/transactionsearch',req).success(function (data, status) {
			if(!data.Details){
				delete $scope.searchResult;
				$scope.alerts = [{
					type: 'danger',
					msg: "Your search returned no results"
				}]
				$scope.resultMsg="You have no results found";
			}
			if(data.count){
				$scope.SearchResultLen=data.count;	
			}
			if($scope.hide&&$scope.dataReq.start!=0){
				for (var i=0; i<data.Details.length; i++){
					$scope.searchResult.push(data.Details[i]);
				} 
				$scope.hide=false;
			} 
			else {
				$scope.searchResult=data.Details;
				if($scope.SearchResultLen){
					$scope.resultMsg="You have "+$scope.SearchResultLen+" results found";	
				}
			}
			$scope.len=$scope.searchResult.length;
			$scope.itemsPerPage = 5;
			if($scope.currentPage&&$scope.dataReq.start!=0){
          		$scope.currentPage=$scope.currentPage+1;  
        	} else {
          		$scope.currentPage = 0;  
        	}
			$scope.items=$scope.searchResult;
		}).error(function (data, status, headers, config) {
			$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
			];		   
		});
	}
	/*search function*/
	$scope.searchfn=function(){
		if($scope.value.AmountFrom=="."){
			delete $scope.value.AmountFrom;
		}
		$scope.alerts = [];
		$scope.resultMsg=undefined;
		if($scope.searchResult){
			delete $scope.searchResult;
		}
		if($scope.value.Schedule){
			$scope.value.Schedule=$scope.value.Schedule.actualvalue;
		}
		else if($scope.value.Session){
			$scope.value.Session=$scope.value.Session.actualvalue;	
		}
		else if($scope.value.TransactionType){
			$scope.value.TransactionType=$scope.value.TransactionType.actualvalue;	
		}
		Object.keys($scope.value).forEach(k =>(!$scope.value[k]&& $scope.value[k]!==undefined)&&delete $scope.value[k]);
		if(Object.keys($scope.value).length === 0){
			$scope.resultTable=true;
			$scope.resultMsg="Your search returned no results.";
			$scope.alerts = [{
				type: 'danger',
				msg: 'Your search returned no results.Please use the above fields to enter the search criteria'
			}
			]
		}
		else{
			$scope.resultTable=true;
			$scope._value = [];
			$scope._qArray = []
			$scope._requestArray=[];
			$scope._key=[];
			let check=false;
			for(let key in $scope.value){
				$scope._key.push({"fieldName":key});
				if($scope.value[key] != null && $scope.value[key] !=""){
					$scope._value.push({"fieldName":key,"value":$scope.value[key]});
				}
			}
			for(let value in $scope.value){
				if($scope.value[value].length>=1){
					
						if(value=="AmountFrom"){
						operator=">=";
						}
						else if(value=="AmountTo"){
						operator="<=";
						}
						else{
							operator="=";
						}

					$scope._qArray.push({
						"columnName": value,
						"operator": operator,
						"value": $scope.value[value]
					});
					check=true;
				}
			}
			if(check==true){
				for(i=0;i<$scope._qArray.length;i++){
					$scope._requestArray.push({
						"logicalOperator": "AND",
						"clauses":[$scope._qArray[i]]
					})		
				}
			}
			$scope.dataReq={
				"filters": {
					"logicalOperator": "AND",
					"groupLvl1": [{
						"logicalOperator": "AND",
						"groupLvl2": [{
							"logicalOperator": "AND",
							"groupLvl3": $scope._requestArray
						}
						]
					}
					]
				},
				"sorts": [
				],
				"start": 0,
				"count": 20
			};
			$scope.initial($scope.dataReq);
		}
	}
	/*show details call*/
	$scope.get_details = function(index,TransactionID){
		$http.post( BASEURL + '/rest/v2/bpsui/transactionsearch/detail',{"TransactionID" : TransactionID }).success(function (data, status) {
			$scope.searchResult[index].showdetails=data.DetailTransacEntry;
		}).error(function (data, status, headers, config) {
			$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
			];
		});
	}
	/*pagination functions*/
	$scope.getmore = function(){
		$scope.dataReq.start = $scope.dataReq.start+20;
		$scope.dataReq.count = $scope.dataReq.count;
		if($scope.dataReq.start < $scope.SearchResultLen){
			$scope.initial($scope.dataReq);
		}
	}
	$scope.navigate = function(n){
		$scope.currentPage=n-1;
	}
	$scope.range = function() {
		if($scope.items.length <= $scope.itemsPerPage){
			var rangeSize =1;
		}
		else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
			var rangeSize =2;
		}
		else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
			var rangeSize =3;
		}
		else{
			var rangeSize =4;
		}
		var ret = [];
		var start;
		start = $scope.currentPage;
		if ( start > $scope.pageCount()-rangeSize ) {
			start = $scope.pageCount()-rangeSize+1;
		}
		for (var i=start; i<start+rangeSize; i++) {
			ret.push(i);
		}
		return ret;
	};
	$scope.prevPage = function() {
		if ($scope.currentPage > 0) {
			$scope.currentPage--;
		}
	};
	$scope.prevRange = function() {
		if ($scope.currentPage >= 4) {
			$scope.currentPage=$scope.currentPage-4;
		}
	};
	$scope.nextRange = function() {
		var len =$scope.items.length/$scope.itemsPerPage;
		if (len-$scope.currentPage > 4) {
			$scope.currentPage=$scope.currentPage+4;
		}
	};
	$scope.prevPageDisabled = function() {
		return $scope.currentPage === 0 ? "disabled" : "";
	};
	$scope.prevRangeDisabled = function() {
		return $scope.currentPage < 4 ? "disabled" : "";
	};
	$scope.nextRangeDisabled = function() {
		var len =$scope.items.length/$scope.itemsPerPage;
		return len-$scope.currentPage <= 4 ? "disabled" : "";
	};
	$scope.pageCount = function() {
		return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
	};
	$scope.nextPage = function() {
		if ($scope.currentPage < $scope.pageCount()) {
			$scope.currentPage++;
		}
	};
	$scope.nextPageDisabled = function() {
		if($scope.currentPage === $scope.pageCount()){
			if($scope.dataReq.start>=$scope.SearchResultLen){
				$scope.hide=false;  
			}
			else if($scope.SearchResultLen<$scope.dataReq.count){
				$scope.hide=false;
			}
			else{
				$scope.hide=true; 
			}
		}
		else{
			$scope.hide=false; 
		}
		return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	};
	/*get card value*/
	$scope.getccv = function(){
		$scope.value.CreditCardNumber=document.getElementById("CreditCardNumber").value.replace(/ /g, '');
		$scope.value.CreditCardNumber=$scope.value.CreditCardNumber.replace(/_/g, '');
	}
	/*date range picker plugin config*/
	$(document).ready(function(){
		if (window.matchMedia('(max-width: 950px)').matches) {
			$('input[name="TransactionDate"]').daterangepicker({
				opens: 'right',
				drops: 'up',
				showDropdowns: true,
				autoUpdateInput:false,
				autoApply: true,
				"locale": {
					"format": "DD/MM/YYYY",
					cancelLabel: 'Clear'
				}
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
			});
		} else {
			$('input[name="TransactionDate"]').daterangepicker({
				opens: 'right',
				drops: 'down',
				showDropdowns: true,
				autoApply: true,
				autoUpdateInput:false,
				"locale": {
					"format": "DD/MM/YYYY",
					cancelLabel: 'Clear'
				}
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
			}); 
		}
		$('.TransactionDate').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			$scope.value.TransactionDate=picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY');
		});
		$('.TransactionDate').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
	});
	$scope.removeDate=function(){
		$scope.value.TransactionDate='';
	}
})