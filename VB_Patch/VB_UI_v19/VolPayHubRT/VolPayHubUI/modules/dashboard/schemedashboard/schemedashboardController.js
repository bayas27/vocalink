VolpayApp.controller('SchemeController', function ($scope, $http, $filter, $timeout, $state, $translate, $location, $window, PersonService1, AllPaymentsGlobalData, GlobalService, LogoutService, DashboardService , $interval) {
  $scope.toggle1 = false;
  $scope.toggle2 = false;
  /*pagination function for both*/
  $scope.paginationfun = function(num,data,exp,type){
    if(type=='bulk'){
      $scope.itemsPerPage =num ;
      if(exp=='false'){
        if($scope.currentPage&&$scope.restInputData.start!=0){
          $scope.currentPage=$scope.currentPage+1;  
        }
        else{
          $scope.currentPage = 0;  
        }  
      }
      else{
        if(num==10){
          $scope.currentPage = Math.ceil(($scope.currentPage+1)/2)-1;
        }
        else{
          $scope.currentPage = Math.ceil(($scope.currentPage)*2);
        }
      }
      
      
      $scope.items = data;    
    }
    else{
      $scope.itemsPerPageChe =num ;
      if(exp=='false'){
        if($scope.currentPageChe&&$scope.restInputCheque.start!=0){
          $scope.currentPageChe=$scope.currentPageChe+1;  
        }
        else{
          $scope.currentPageChe = 0;  
        }  
      }
      else{
        if(num==10){
          $scope.currentPageChe = Math.ceil(($scope.currentPageChe+1)/2)-1;
        }
        else{
          $scope.currentPageChe = Math.ceil(($scope.currentPageChe)*2);
        }
      }
      $scope.itemsChe = data;
    }
    
  }
  $scope.navigate = function(n){
    $scope.currentPage=n-1;

  }
  $scope.range = function() {
    if($scope.items.length <= $scope.itemsPerPage){
      var rangeSize =1;
    }
    else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
      var rangeSize =2;
    }
    else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
      var rangeSize =3;
    }
    else{      
      var rangeSize =4;
    }
    var ret = [];
    var start;
    start = $scope.currentPage;
    if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
    }
    for (var i=start; i<start+rangeSize; i++) {
      ret.push(i);
    }
    return ret;
  };

  $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
      $scope.currentPage--;
    }

  };


  $scope.prevPageDisabled = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
  };

  $scope.pageCount = function() {
    return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
  };

  $scope.nextPage = function() {
    if ($scope.currentPage < $scope.pageCount()) {
      $scope.currentPage++;
    }
  };

  $scope.nextPageDisabled = function() {
    if($scope.currentPage === $scope.pageCount()){
      if($scope.restInputData.start>=$scope.bulkLen){
        $scope.hide=false;	
      }
      else if($scope.bulkLen<$scope.restInputData.count){
        $scope.hide=false;
      }
      else{
        $scope.hide=true; 
      }
      
    }
    else{
      $scope.hide=false; 
    }
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
  };

  /*cheque*/
  
  $scope.navigateChe = function(n){
    $scope.currentPageChe=n-1;

  }

  $scope.rangeChe = function() {
    if($scope.itemsChe.length <= $scope.itemsPerPageChe){
      var rangeSizeChe =1;

    }
    else if($scope.itemsChe.length > $scope.itemsPerPageChe && $scope.itemsChe.length < 2*$scope.itemsPerPageChe ){
      var rangeSizeChe =2;
    }
    else if($scope.itemsChe.length > 2*$scope.itemsPerPageChe && $scope.itemsChe.length < 3*$scope.itemsPerPageChe ){
      var rangeSizeChe =3;
    }
    else{      
      var rangeSizeChe =4;
    }

    var retChe = [];
    var startChe;

    startChe = $scope.currentPageChe;
    if ( startChe > $scope.pageCountChe()-rangeSizeChe ) {
      startChe = $scope.pageCountChe()-rangeSizeChe+1;
    }

    for (var i=startChe; i<startChe+rangeSizeChe; i++) {
      retChe.push(i);
    }
    return retChe;
  };

  $scope.prevPageChe = function() {
    if ($scope.currentPageChe > 0) {
      $scope.currentPageChe--;
    }

  };
  


  $scope.prevPageDisabledChe = function() {
    return $scope.currentPageChe === 0 ? "disabled" : "";
  };

  $scope.pageCountChe = function() {
    return Math.ceil($scope.itemsChe.length/$scope.itemsPerPageChe)-1;
  };

  $scope.nextPageChe = function() {
    if ($scope.currentPageChe < $scope.pageCountChe()) {
      $scope.currentPageChe++;
    }
  };

  $scope.nextPageDisabledChe = function() {
    if($scope.currentPageChe === $scope.pageCountChe()){
      if($scope.restInputCheque.start>=$scope.chequeBankDataLen){
        $scope.hideChe=false;  
      }
      else if($scope.chequeBankDataLen<$scope.restInputCheque.count){
        $scope.hideChe=false;
      }
      else{
        $scope.hideChe=true; 
      }
      
    }
    else{
      $scope.hideChe=false; 
    }
    return $scope.currentPageChe === $scope.pageCountChe() ? "disabled" : "";
  };

  $scope.restInputData={"sorts":[],"start":0,"count":20}
  $scope.restInputCheque={"sorts":[],"start":0,"count":20}
  /*table keys*/
  $scope.tableFields={  "bulkTableKey":
  [  

  {
    'FieldName' : "ID",
    'Label'    : "ID"
  },
  {
    'FieldName' : "bank_name",
    'Label'    : "Bank Name"
  },
  {
    'FieldName' : "Submitted",
    'Label'    : "Submitted"
  },
  {
    'FieldName' : "Acknowledged",
    'Label'    : "Acknowledged"
  },
  {
    'FieldName' : "Rejections",
    'Label'    : "Rejections"
  },
  {
    'FieldName' : "",
    'Label'    : ""
  }
  ],
  "chequeTableKey":
  [  
  {
    'FieldName' : "ID",
    'Label'    : "ID"
  },
  {
    'FieldName' : "bank_name",
    'Label'    : "Bank Name"
  },
  {
    'FieldName' : "Submitted",
    'Label'    : "Submitted"
  },
  {
    'FieldName' : "Acknowledged",
    'Label'    : "Acknowledged"
  },
  {
    'FieldName' : "Rejections",
    'Label'    : "Rejections"
  },
  {
    'FieldName' : "",
    'Label'    : ""
  }      

  ],
  "partictionKey":[
  {
    'FieldName' : "PEN",
    'Label'    : "PEN"
  },
  {
    'FieldName' : "USD",
    'Label'    : "USD"
  }
  ]
}

$scope.initial=function(){
  $http.get( BASEURL + '/rest/v2/bpsui/schedule').success(function (data, status) {
    $scope.BulkSchedule=data;
      $scope.getBulk($scope.BulkSchedule[0].actualvalue);
      $scope.selectedSchedule=$scope.BulkSchedule[0];  
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
  $http.get( BASEURL + '/rest/v2/bpsui/session').success(function (data, status) {
    $scope.ChequeSession=data;
    $scope.getCheque($scope.ChequeSession[1].actualvalue);
    $scope.selectedSession=$scope.ChequeSession[1];  
  }).error(function (data, status, headers, config) {
    alert(data.error.message)
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
}


$scope.initial();  
$scope.getBulk = function(n){
  $http.post( BASEURL + '/rest/v2/bpsui/dash/bulk',{"schemeNm":n,"dashboardType":"scheme"}).success(function (data) {
    $scope.bulkData=data;
    $scope.sendRest(n,$scope.restInputData,'bulk');    
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
}
$scope.getmore = function(schedule){

  $scope.restInputData.start=$scope.restInputData.start+20;
  $scope.restInputData.count=$scope.restInputData.count;
  if($scope.restInputData.start<$scope.bulkLen){
  	$scope.sendRest(schedule,$scope.restInputData,'bulk');	
  }
}
$scope.getmoreChe = function(session){
  $scope.restInputCheque.start=$scope.restInputCheque.start+20;
  $scope.restInputCheque.count=$scope.restInputCheque.count;
  if($scope.restInputCheque.start<$scope.chequeBankDataLen){
  	$scope.sendRest(session,$scope.restInputCheque,'cheque');	
  }
  
}
$scope.sendRest = function(n,restInputData,type){
  $http.post( BASEURL + '/rest/v2/bpsui/dash/details/'+n,restInputData).success(function (bankData) {
    if(type=='bulk'){
      if($scope.restInputData.start!=0){
        for (var i=0; i<bankData.Details.length; i++){
          $scope.bankData.push(bankData.Details[i]);
        } 
        $scope.hide=false;
      }
      else{
        $scope.bankData=bankData.Details;
      }
      if($scope.toggle1==false){
        var num=5;  
      }
      else{
        var num=10;
      }
      $scope.bulkLen=bankData.count;
      $scope.paginationfun(num,$scope.bankData,'false','bulk');  
    }
    else{
      if($scope.restInputCheque.start!=0){
        for (var i=0; i<bankData.Details.length; i++){
          $scope.chequeBankData.push(bankData.Details[i]);
        } 
        $scope.hideChe=false;
      }
      else{
        $scope.chequeBankData=bankData.Details;  
      }
      
      if($scope.toggle2==false){
        var numChe=5;  
      }
      else{
        var numChe=10;
      }
      $scope.chequeBankDataLen=bankData.count;
      $scope.paginationfun(numChe,$scope.chequeBankData,'false','cheque');  
    }

  }).error(function (data, status, headers, config) {
    $scope.hide=false;
    
  });
}
/*sorting for both bulk and cheque*/
$scope.gotoSorting = function(dat,type){

  if(type=='bulk'){
    $scope.hide=false;
    $scope.loadMorecalled = false;
    $scope.restInputData.start = 0;
    $scope.restInputData.count = 20;
    var orderFlag = true;
    if(dat!=""){
      if('sorts' in $scope.restInputData && $scope.restInputData.sorts.length){
        for(k in $scope.restInputData.sorts){
          if($scope.restInputData.sorts[k].columnName == dat){
            if($scope.restInputData.sorts[k].sortOrder == 'Asc'){
              $('#'+dat+'_icon').attr('class','fa fa-long-arrow-down')
              $('#'+dat+'_Icon').attr('class','fa fa-caret-down')
              $scope.restInputData.sorts[k].sortOrder = 'Desc'
              orderFlag = false;
              break;
            }
            else{
              $scope.restInputData.sorts.splice(k,1);
              orderFlag = false;
              $('#'+dat+'_icon').attr('class','fa fa-minus fa-sm')
              $('#'+dat+'_Icon').removeAttr('class')
            //console.log('Remove',$scope.restInputData.QueryOrder)
            $timeout(function(){
              $(".alert-danger").hide();
            },1000)
            break;
          }        
        }
      }
      if(orderFlag){
        $('#'+dat+'_icon').attr('class','fa fa-long-arrow-up')
        $('#'+dat+'_Icon').attr('class','fa fa-caret-up')
        $scope.restInputData.sorts.push({
          "columnName": dat,
          "sortOrder": 'Asc'
        })
        //console.log('Add',$scope.restInputData.QueryOrder)
      }
    }
    else{
      $('#'+dat+'_icon').attr('class','fa fa-long-arrow-up')
      $('#'+dat+'_Icon').attr('class','fa fa-caret-up')
      $scope.restInputData.sorts.push({
        "columnName": dat,
        "sortOrder": 'Asc'
      })
      //console.log('initial',$scope.restInputData.QueryOrder)
    }   
    $scope.sendRest($scope.selectedSchedule.actualvalue,$scope.restInputData,'bulk');      
  }

  
}
else if(dat!="")
{
  $scope.loadMorecalled = false;
  $scope.restInputCheque.start = 0;
  $scope.restInputCheque.count = 20;

  var orderFlag = true;
  if('sorts' in $scope.restInputCheque && $scope.restInputCheque.sorts.length){
    for(k in $scope.restInputCheque.sorts){
      if($scope.restInputCheque.sorts[k].columnName == dat){
        if($scope.restInputCheque.sorts[k].sortOrder == 'Asc'){
          $('#'+dat+'_icon1').attr('class','fa fa-long-arrow-down')
          $('#'+dat+'_Icon1').attr('class','fa fa-caret-down')
          $scope.restInputCheque.sorts[k].sortOrder = 'Desc'
          orderFlag = false;
            //console.log('Desc',$scope.restInputCheque.QueryOrder)
            break;
          }
          else{
            $scope.restInputCheque.sorts.splice(k,1);
            orderFlag = false;
            $('#'+dat+'_icon1').attr('class','fa fa-minus fa-sm')
            $('#'+dat+'_Icon1').removeAttr('class')
            //console.log('Remove',$scope.restInputCheque.QueryOrder)
            $timeout(function(){
              $(".alert-danger").hide();
            },1000)
            break;
          }        
        }
      }
      if(orderFlag){
        $('#'+dat+'_icon1').attr('class','fa fa-long-arrow-up')
        $('#'+dat+'_Icon1').attr('class','fa fa-caret-up')
        $scope.restInputCheque.sorts.push({
          "columnName": dat,
          "sortOrder": 'Asc'
        })
      }
    }
    else{
      $('#'+dat+'_icon1').attr('class','fa fa-long-arrow-up')
      $('#'+dat+'_Icon1').attr('class','fa fa-caret-up')
      $scope.restInputCheque.sorts.push({
        "columnName": dat,
        "sortOrder": 'Asc'
      })
    }
    
    $scope.sendRest($scope.selectedSession.actualvalue,$scope.restInputCheque,'cheque');      
  }

}

$scope.getCheque = function(session){
  $http.post( BASEURL + '/rest/v2/bpsui/dash/cheque',{"schemeNm":session,"dashboardType":"scheme"}).success(function (data) {
    $scope.chequeData=data;
    $scope.sendRest(session,$scope.restInputCheque,'cheque');    
    
  }).error(function (data, status, headers, config) {

    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];

  });
}
/*print function*/
$scope.dashboardPrint = function () {
  $('[data-toggle="tooltip"]').tooltip('hide');
  window.print()
}
/*expand collapse of both bulk and check*/
var num=5;
$scope.expand = function(){
  if(num==5){
    num=10;
    $scope.toggle1 = true;
    $scope.paginationfun(num,$scope.bankData,'true','bulk');  
  }
  else{
    num=5;
    $scope.toggle1 = false;
    $scope.paginationfun(num,$scope.bankData,'true','bulk');  
  }
}
var numChe=5;
$scope.expandChe = function(){
  if(numChe==5){
    numChe=10;
    $scope.toggle2 = true;
    $scope.paginationfun(numChe,$scope.chequeBankData,'true','cheque');
  }
  else{
    numChe=5;
    $scope.toggle2 = false;
    $scope.paginationfun(numChe,$scope.chequeBankData,'true','cheque');
  }
}

/*else{
      $scope.toggle2 = false;
      $scope.paginationfun(num,$scope.chequeBankData,'true','cheque');
    }
 else{
      $scope.toggle2 = true;
      $scope.paginationfun(num,$scope.chequeBankData,'true','cheque');
    }*/


  });