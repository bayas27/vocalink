set folderdatetime=target
echo %folderdatetime%
mkdir %folderdatetime%

REM Packaging VolPay Channel into war
cd VolPayChannel
jar -cvf ..\%folderdatetime%\VolPayChannel.war .
cd ..

REM Packaging VolPay Core into war
cd VolPayHub
jar -cvf ..\%folderdatetime%\VolPayHub.war .
cd ..

REM Packaging VolPayHubBankSimulator into war
cd VolPayHubBankSimulator
jar -cvf ..\%folderdatetime%\VolPayHubBankSimulator.war .
cd ..

REM Packaging NotiGate into war
cd NotiGate
jar -cvf ..\%folderdatetime%\NotiGate.war .
cd ..

REM Packaging VolPayRest into war
cd VolPayRest
jar -cvf ..\%folderdatetime%\VolPayRest.war .
cd ..

REM Packaging VolPay UI into war
cd VolPayHubUI
jar -cvf ..\%folderdatetime%\VolPayHubUI.war .
cd ..

REM Packaging VolPay UI into war
cd SiteminderSimulator
jar -cvf ..\%folderdatetime%\SiteminderSimulator.war .
cd ..

pause
