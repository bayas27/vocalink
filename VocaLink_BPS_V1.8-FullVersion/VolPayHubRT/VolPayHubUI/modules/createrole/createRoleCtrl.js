VolpayApp.controller('createRoleCtrl', function ($scope, $stateParams, $rootScope, $http, $location, $state, $timeout, GlobalService, editservice) {
$scope.initial = function(){
	$scope.request={"RoleName": "",
	"Dashboard": {
		"schemeDashboard": false,
		"participantDashboard": false
	},
	"FileManagement": {
		"upload": false,
		"download": false,
		"viewListSubmitFile": false,
		"searchSubmitFile": false
	},
	"ReportsAccess": {
		"listOfReports": false,
		"downloadReports": false
	},
	"Search": {
		"searchTransaction": false,
		"viewListTransaction": false
	}}
$scope.blocknames={"Dashboard":[{
	"FieldName":"dashboard",
	"label":"Dashboard",
	"checked":false
}],
"FileManagement":[{
	"FieldName":"fileManagement",
	"label":"File Management",
	"checked":false
}],
"ReportsAccess":[{
	"FieldName":"reportAccess",
	"label":"Report Access",
	"checked":false
}],
"Search":[{
	"FieldName":"search",
	"label":"Search",
	"checked":false
}]
}
}
$scope.initial();
$scope.keys={
"role":[{
	"FieldName":"RoleName",
	"label":"Role Name",
	"type":"text"
}],
"Dashboard":[{
	"FieldName":"schemeDashboard",
	"label":"Scheme Dashboard"
},
{
	"FieldName":"participantDashboard",
	"label":"Participant Dashboard"
}],
"FileManagement":[{
	"FieldName":"upload",
	"label":"Upload Payment File"
	},{
	"FieldName":"download",
	"label":"Download Payment File"
	},{
	"FieldName":"searchSubmitFile",
	"label":"Search submitted files"
	},{
	"FieldName":"viewListSubmitFile",
	"label":"View the list of submitted files",
	
}],
"ReportsAccess":[{
	"FieldName":"listOfReports",
	"label":"View list of Reports",
	},{
	"FieldName":"downloadReports",
	"label":"Download Reports"
}],
"Search":[{
	"FieldName":"searchTransaction",
	"label":"Search for transaction",
	},{
	"FieldName":"viewListTransaction",
	"label":"View Transaction Details"
}]
}

$scope.selectAll  = function(data,boolean){
		for(var i=0;i<$scope.keys[data].length;i++){
			var field=$scope.keys[data][i].FieldName;
				$scope.request[data][field]=boolean;
		}

		}
		
		$scope.selectAllChild = function(data,boolean){
			if(boolean){
				for(var i=0;i<$scope.keys[data].length;i++){
					var field=$scope.keys[data][i].FieldName;
					if($scope.request[data][field]!=boolean){
						break;
					}
				}
				if(i==$scope.keys[data].length){
					$scope.blocknames[data][0].checked=boolean;
				}
			}
			else{
				$scope.blocknames[data][0].checked=boolean;
			}
		}
$scope.cancel= function(){
	$state.go('app.bpsroles');
}
/*save operation*/
$scope.saveRole = function(data){
 $http.post( BASEURL + '/rest/v2/bpsui/role',data).success(function (data, status) {
 	$scope.initial();
    $scope.alerts = [{
      	type: 'success',
     	msg: data.responseMessage
    }];
    
  }).error(function (data, status, headers, config) {
  	if(data.responseMessage){
  		$scope.alerts = [{
      type: 'danger',
      msg: data.responseMessage
    }
    ];
  	}
  	else{
  		$scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];	
  	}
    
  });
}

});
