VolpayApp.controller('viewRoleCtrl', function ($scope, $stateParams, $rootScope, $http, $location, $state, $timeout, GlobalService, editservice) {
	$scope.roleName=$stateParams.RoleName;
$http.post( BASEURL + '/rest/v2/bpsui/role/read',{"RoleName":$scope.roleName}).success(function (data, status) {
	$scope.RoleDetail=data;
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  }); 
$scope.values={"label":"fileManagement",
"dashboard":[{
	"FieldName":"schemeDashboard",
	"label":"Scheme Dashboard",
	"parent":"Dashboard"
},{
	"FieldName":"participantDashboard",
	"label":"Participant Dashboard",
	"parent":"Dashboard"
}],
"fileManagement":[{
	"FieldName":"upload",
	"label":"Upload Payment File",
	"parent":"FileManagement"
	},{
	"FieldName":"download",
	"label":"Download Payment File",
	"parent":"FileManagement"
	},{
	"FieldName":"searchSubmitFile",
	"label":"Search submitted files",
	"parent":"FileManagement"
	},{
	"FieldName":"viewListSubmitFile",
	"label":"View the list of submitted files",
	"parent":"FileManagement"
}],
"reportAccess":[{
	"FieldName":"listOfReports",
	"label":"View list of Reports",
	"parent":"ReportsAccess"
	},{
	"FieldName":"downloadReports",
	"label":"Download Reports",
	"parent":"ReportsAccess"
}],
"search":[{
	"FieldName":"searchTransaction",
	"label":"Search for transaction",
	"parent":"Search"
	},{
	"FieldName":"viewListTransaction",
	"label":"View Transaction Details",
	"parent":"Search"
}]
}
$scope.label="Dashboard";
$scope.selected = 'Dashboard';
$scope.features = $scope.values.dashboard;	

/*click side bar */

$scope.roleMenu =function(data)
	{
		$scope.label=data;
		$scope.selected = data;
	if (data=='Dashboard') {
	$scope.features = $scope.values.dashboard;	
	}
	else if(data=='File Management')  {
$scope.features = $scope.values.fileManagement;
	}
	else if(data=='Report Access')  {
$scope.features = $scope.values.reportAccess;
	}
	else
	{
		$scope.features = $scope.values.search;	}
}


});