VolpayApp.controller('paymentDetailCtrl', function ($scope, $http, $location, $state, $timeout, $filter, $rootScope , GlobalService,AllPaymentsGlobalData,RefService,bankData) {	//console.log($state.params.input)
	
	$scope.backupObj = {
		value:'',
		ind:-1
	};

	$scope.rfclicked = false;
	
	$scope.Pconfirm={}
	$http.get(BASEURL + '/rest/v2/confirmation/status').success(function (data, status, headers, config) {

		$scope.confirmationStatus=data;
	}).error(function (data, status, headers, config) {

	});
	
	$scope.ManualConfirmationStatusArr=["COMPLETED","DELIVERED","ACCEPTED","PROCESSING","PENDING","PARTIALLYACCEPTED","PARTIALLYCOMPLETED","COMPLETED WITH AMENDMENTS"]
	
	

	if($state.params.input == '')
	{
		$state.go('app.payments')

	}

	$scope.uor = $state.params.input.uor;
	//$scope.refId = $state.params.input.nav.UIR;
	$scope.UniqueRefID = $state.params.input.nav.PID;
	$scope.attachMgsFunc = $state.params.input.nav.AttachMsg;
	$scope.PaymentID = $state.params.input.nav.PaymentID;
	$scope.attachparentID =  $state.params.input.parentID;
	$scope.attachrootpaymentID =  $state.params.input.rootPaymentID;
	$scope.AttachFunc = $state.params.input.Attach;
	$scope.refId = '';

	$scope.fromPage = $state.params.input.from;
	//console.log("$scope.fromPage", $scope.fromPage,$scope.PaymentID)
	$scope.instructionType = $state.params.input.InstructionType;

	$scope.isCollapsed = false;
	$scope.isPaymentCollapsed = false;
	$scope.isPaymentInfoCollapsed = true;
	$scope.isPaymentInfoDebitPartyCollapsed = true;
	$scope.isPaymentInfo1173editPartyCollapsed = true;
	$scope.isPaymentInfoRoutingCollapsed = false; 


	$scope.DebtorAgentCollapsed = true;
	$scope.InstructingReimbursementAgentCollapse = true;
	$scope.senderCollapsed = true;
	$scope.CreditorAgentCollapsed = true;
	$scope.IntermediaryAgent1Collapsed = true;
	$scope.IntermediaryAgent2Collapsed = true;
	$scope.ThirdReimbursementAgentCollapsed = true;
	$scope.ReceiversCorrespondentCollapsed = true;
	$scope.DebitSideDetailsCollapsed = true;
	$scope.CreditSideDetailsCollapsed = true;

	$scope.isFileInfoCollapsed = true;
	$scope.morefileInfoCollapsed = false;

    $scope.debitGroup = false;
    $scope.iscreditorGroup = false;
    $scope.additionalPaymntInfoCollapsed = false;
    $scope.additionalProcessingInfoCollapsed = false;
    $scope.fxDetailsCollapsed = false;




	$scope.loading=false;
	$scope.AuditTableLoaded=false;
	$scope.BIDLoaded=false;
	$scope.RelatedMSGLoaded=false;	
	$scope.pagination = false;
	
	$scope.paymentObj = {};
	$scope.dropdownVal = [];
	var payObj = {};
	$scope.valCheck=0;
	$scope.count = 0;


	$scope.IsApproveUser = sessionStorage.ROLE_ID;
	$scope.AccountPostingTab=false;
	$scope.ErrorInfoTab=false;
		
	$scope.paymentRepaired = GlobalService.paymentRepaired;

		if($scope.paymentRepaired)
		{

			$scope.alerts = [{
						type : 'success',
						msg : "Payment Repair details have been forwarded to bank"
					}
				];
			GlobalService.paymentRepaired = false;
		}



	
	
	function fillData(){

	
	
		if($scope.dropdownVal.length > 1){
			
		
			$scope.pagination = true;
		}
		else{
	
			$scope.pagination = false;
		}
		
		for (var i = 0; i < $scope.dropdownVal.length; i++) { 
			if ($scope.dropdownVal[i]["PaymentID"] == $scope.UniqueRefID) {
				$scope.valCheck = angular.copy(i);	
				if($scope.dropdownVal.length>1)
				{
				  if(($scope.valCheck == ($scope.dropdownVal.length - 1))&&($scope.fromPage != "filedetail")){

                    $scope.count += 20;
					RefService.dropDownLoadMore($scope.count).then(function (items) {
					   
						if(items.data.length){
						    $scope.dropdownVal = $scope.dropdownVal.concat(items.data);
						 }
                    });

				}
			  }

			}
		}

		
		$scope.paymentObj.PaymentID = $scope.UniqueRefID;
		var aa, attachedRestcalls;

		if($scope.paymentObj.PaymentID == $scope.PaymentID )
		{
			console.log("equal")
			attachedRestcalls = BASEURL + '/rest/v2/payments/read/' + $scope.paymentObj.PaymentID;
		}
		
		else if($scope.paymentObj.PaymentID != $scope.PaymentID){
			console.log("not-equal")
				 if($scope.attachrootpaymentID){
					console.log("RootPaymentID")
					attachedRestcalls = BASEURL + '/rest/v2/payments/read/' + $scope.paymentObj.PaymentID;
				}
			
		}
		
		
		if ($scope.instructionType || $scope.attachMgsFunc || $scope.AttachFunc) {
			
			
			
			aa = ($scope.instructionType == 'RESPONSE') ? (BASEURL + '/rest/v2/payments/readpcdforconf/' + $scope.paymentObj.PaymentID) : (($scope.attachMgsFunc != "Customer Credit Transfer" && $scope.attachMgsFunc != "Request for Payment" && $scope.attachMgsFunc != "Payment Instruction" && $scope.attachMgsFunc != "RESPONSE" && !$scope.AttachFunc) ? (attachedRestcalls) : (($scope.AttachFunc) ? (BASEURL + '/rest/v2/payments/attachmsg/' + $scope.paymentObj.PaymentID) : (BASEURL + RESTCALL.AllPaymentListSpecificREST)));

		} else {
			aa = BASEURL + RESTCALL.AllPaymentListSpecificREST;
		}

		var methods;
		if ($scope.instructionType || $scope.attachMgsFunc || $scope.AttachFunc) {

			methods = ($scope.instructionType == 'RESPONSE') || ($scope.attachMgsFunc != "Customer Credit Transfer" && $scope.attachMgsFunc != "Request for Payment" && $scope.attachMgsFunc != "Payment Instruction" && $scope.attachMgsFunc != "RESPONSE") || ($scope.AttachFunc);

		}

		$http({
			method: methods ? 'GET' : 'POST',
			url: aa,
			data: $scope.paymentObj

		}).then(function (allPayment) {
			
			$scope.cData = allPayment.data;

			$scope.refId = $scope.cData.InstructionID;
			$scope.UniqueRefID = $scope.cData.PaymentID;	
			$scope.MOPForConfirm = $scope.cData.MethodOfPayment;

			$scope.showOverride = false;
			$scope.statusCheck = $scope.cData.Status.split('_');

			


			

			$scope.statusREST = {
				"Queryfield": [{
						"ColumnName": "WorkFlowCode",
						"ColumnOperation": "=",
						"ColumnValue": 'PAYMENT'
					},
					{
						"ColumnName": "ProcessStatus",
						"ColumnOperation": "=",
						"ColumnValue": $scope.cData.Status
					}
				]
			}
		
			$scope.statusREST = constructQuery($scope.statusREST)
			
			$scope.selectedClr = {};
			
				$http.post(BASEURL + RESTCALL.StatusDefnColors,$scope.statusREST).success(function(data){
					
					if(data.length)
					{

					
					if (data[0].ColourB)
					{
						$scope.selectedClr.ColourB = data[0].ColourB;
						$scope.selectedClr.Grandient = true;
					}
					else
					{
						$scope.selectedClr.Grandient = false;
					}
					$scope.selectedClr.ColourA = data[0].ColourA;
					$scope.selectedClr.Opacity = data[0].Opacity/100;
					

					$scope.appliedStyle = (!$scope.selectedClr.Grandient) ? {'color':$scope.selectedClr.ColourA,'opacity':$scope.selectedClr.Opacity} :{'background':'-webkit-linear-gradient('+$scope.selectedClr.ColourA+','+$scope.selectedClr.ColourB+')'}
				}

				
				}).error(function(){
			
				}) 
		


			//$scope.appliedStyle = (!$scope.selectedClr.Grandient) ? "{'color':"+$scope.selectedClr.ColourA+",'opacity':"+$scope.selectedClr.Opacity+"} :{'background':'-webkit-linear-gradient("+$scope.selectedClr.ColourA+','+$scope.selectedClr.ColourB+")}"
			




			/* if($scope.statusCheck[0].toUpperCase() == 'WAITING')
			   {
				for(var i=0;i<$scope.statusCheck.length;i++)
				{
				 if($scope.statusCheck[i].indexOf('RESPONSE') != -1)
				 {
				  $scope.showOverride = true;
				  if($scope.cData.Status.toUpperCase() == 'WAITING_RFPRESPONSE')
				  {
				   $scope.showOverride = false;
				  }
				 }
				}
			   }

				if($scope.statusCheck[0].toUpperCase() == 'TECHNICALFAILURE')
				{
					$scope.showOverride = true;
				}*/
			


			//$scope.uor = $state.params.input.uor;
				$http.post(BASEURL+RESTCALL.PaymentRelatedMsgREST,{'PaymentID':$scope.UniqueRefID}).then(function (paymentRelatedMsg) {
				$scope.relatedMsgs = paymentRelatedMsg.data;
				$scope.rowSpan = $scope.relatedMsgs.length;
				$scope.RelatedMSGLoaded=true;
				$scope.loading=true;
					}, function (err) {
						console.error('ERR', err);
					})

		
			//$("#refreshBtn").focus();

				
				
				$http.post(BASEURL+RESTCALL.PaymentAuditREST,{'PaymentID':$scope.UniqueRefID}).then(function (paymentAudit) {
			
		    $scope.cDataAudit = paymentAudit.data;			
			$scope.AuditTableLoaded=true;
		}, function (err) {
			
			$scope.AuditTableLoaded=true;
		})

			
			$http.post(BASEURL+RESTCALL.PaymentBIDREST,{'PaymentID':$scope.UniqueRefID}).then(function (paymentBID) {
			$scope.cDataBID = paymentBID.data;
			$scope.BIDLoaded=true;
		}, function (err) {
			console.error('ERR', err);
		})
			



				
		$http.post(BASEURL+'/rest/v2/payments/accountposting/readall',{'PaymentID':$scope.UniqueRefID}).success(function (data, status, headers, config) {
			$scope.AccountPostingTab=true;
			$scope.accountPostingDetails=data;
		
		}).error(function (data, status, headers, config) {
		
		})
		
		
		$http.post(BASEURL+'/rest/v2/payments/errorinformation/readall',{'PaymentID':$scope.UniqueRefID}).success(function (data, status, headers, config) {
			$scope.ErrorInfoTab=true;
			$scope.errorInformationData=data;
		}).error(function (data, status, headers, config) {
		
		})
	

		$http.post(BASEURL+RESTCALL.linkedPayements,{'PaymentID':$scope.UniqueRefID}).success(function (data, status, headers, config) {
			$scope.linkedPayments=data;
		
		}).error(function (data, status, headers, config) {
		
		})
					getForceAction()
				

		}, function (err) {
			$scope.alerts = [{
					type : 'danger',
					msg : err.data.error.message
				}
			];
		})


		/* $http.post(BASEURL+RESTCALL.PaymentAuditREST,$scope.paymentObj).then(function (paymentAudit) {
			console.log("paymentAudit", paymentAudit,$scope.paymentObj)
		    $scope.cDataAudit = paymentAudit.data;			
			$scope.AuditTableLoaded=true;
		}, function (err) {
			console.error('ERR', err);
			$scope.AuditTableLoaded=true;
		})
 */
		/* $http.post(BASEURL+RESTCALL.PaymentBIDREST,$scope.paymentObj).then(function (paymentBID) {
			$scope.cDataBID = paymentBID.data;
			$scope.BIDLoaded=true;
		}, function (err) {
			console.error('ERR', err);
		})
 */
		

			
		

	/*var payTransaction = {};
	payTransaction.UserId = sessionStorage.UserID;
	payTransaction.Data = btoa(JSON.stringify({'UIR':$scope.refId,'URI':$scope.UniqueRefID}));*/

	/*$http.post(BASEURL+RESTCALL.PaymentTransaction,{'PaymentID':$scope.UniqueRefID,'InstructionID':$scope.refId}).then(function (response) {
            $scope.transactionalData = response.data;
        }, function (err) {
			console.error('ERR', err);
		})*/
/* 
		$http.post(BASEURL+'/rest/v2/payments/accountposting/readall',$scope.paymentObj).success(function (data, status, headers, config) {
			$scope.AccountPostingTab=true;
			$scope.accountPostingDetails=data;
		
		}).error(function (data, status, headers, config) {
		
		})
		
		
		$http.post(BASEURL+'/rest/v2/payments/errorinformation/readall',$scope.paymentObj).success(function (data, status, headers, config) {
			$scope.ErrorInfoTab=true;
			$scope.errorInformationData=data;
		}).error(function (data, status, headers, config) {
		
		})
	

		$http.post(BASEURL+RESTCALL.linkedPayements,$scope.paymentObj).success(function (data, status, headers, config) {
			$scope.linkedPayments=data;
		
		}).error(function (data, status, headers, config) {
		
		})

		//console.log($state.params.input)

		

		

        /*** Payment Process Data ***/
		/* $http.post(BASEURL+RESTCALL.PaymentProcess,payObj).then(function (response) {

			$scope.formatedRawData = response.data.formattedRawData

		//	console.log($scope.formatedRawData)

		}, function (err) {
			console.error('ERR', err);
		}) */

		
    }


	/*$scope.gotoPaymentSummary = function(uor)
	{
		console.log($state.params.input)
		$state.go('app.outputpaymentsummary',{input:{'uor':uor,'from':$state.params.input.from}})
	}*/

	$scope.textDocDownload = function(data)
	 {
			bankData.textDownload($filter('hex2a')(data.OutputMessage), data.UniqueOutputReference);
	 }



	$scope.fetchDataAgain = function()
	{
		$scope.UniqueRefID =$scope.paymentObj.PaymentID;
		fillData()
	}


     if(sessionStorage.InstructionPaymentNotes==undefined){
        $scope.NotesArr=[];
    }else{
        $scope.NotesArr=JSON.parse(sessionStorage.InstructionPaymentNotes);
    }

	$scope.data={};
	$scope.addNotes = function(notes,toDetails){
		$scope.Notes = {
							"InstructionID": toDetails.InstructionID,
							"PaymentID": toDetails.PaymentID,
							"Notes": notes.notes
						}
		$http.post(BASEURL+'/rest/v2/payments/notes',$scope.Notes).then(function (notes) {
			     $scope.alerts = [{
            					type : 'success',
            					msg : notes.data.responseMessage
            				}
            			];

            	$timeout(function(){
            	    $('.alert-success').hide;
            	},5000)

			$scope.data.notes = ''
			$('.modal').modal('hide')

                $http.post(BASEURL+RESTCALL.PaymentAuditREST,$scope.paymentObj).then(function (paymentAudit) {
					$scope.cDataAudit = paymentAudit.data;
					$scope.AuditTableLoaded=true;
				}, function (err) {
					console.error('ERR', err);
				})
		}, function (err) {
			$scope.alerts = [{
					type : 'danger',
					msg : err.data.error.message
				}
			];
		})
	}
/* 	
    $scope.findExistingObj = function(data,PaymentID){
        $scope.fileObjExistingFlag=false;
        for(i=0;i<$scope.NotesArr.length;i++){
            if($scope.NotesArr[i].PaymentID==PaymentID){
               $scope.fileObjExistingFlag=true;
               delete data.$$hashKey;
               $scope.NotesArr[i].notes=data.notes;
               $scope.NotesArr[i].NoteBy=sessionStorage.ROLE_ID;
               $scope.NotesArr[i].Datetime=new Date();
            }
        }
        if($scope.fileObjExistingFlag==false){
		 data.NoteBy=sessionStorage.ROLE_ID;
		 data.Datetime=new Date();	
         $scope.NotesArr.push(data)
        }

    }
	

    $scope.ExistingPaymentNodes=false;
    $scope.addPaymentNotes = function(data,PaymentID)
    {
        data.PaymentID=PaymentID;
        data=$scope.findExistingObj(data,PaymentID)		
        sessionStorage.InstructionPaymentNotes = JSON.stringify($scope.NotesArr);


    }
	
	
	//$scope.addNotes($state.params.input.data);

    

    $scope.getExistingPaymentNotes=function(PaymentID){
         $scope.ExistingPaymentNodes=false;
            for(i=0;i<$scope.NotesArr.length;i++){
                    if($scope.NotesArr[i].PaymentID==PaymentID){
                        $scope.data.notes=$scope.NotesArr[i].notes;
						$scope.data.NoteBy=$scope.NotesArr[i].NoteBy;
						$scope.data.Datetime=$scope.NotesArr[i].Datetime;
                        $scope.ExistingPaymentNodes=true;
                    }
                }
            if(!$scope.ExistingPaymentNodes)
            {
             $scope.data.notes="";
            }
    }

    $scope.getExistingPaymentNotes($scope.UniqueRefID)

 */

	
	
	if($scope.fromPage == "filedetail" || $scope.fromPage == "AllConfirmaion")
	{

        $scope.dropdownVal = GlobalService.allFileListDetails;
		fillData()

	}
    else{


		$scope.dropdownVal = [];
		$scope.dropdownVal = (AllPaymentsGlobalData.allPaymentDetails)?AllPaymentsGlobalData.allPaymentDetails:[];
		fillData();
	}

	$scope.NrP = function(val){
		
		if(val != ''){
			$("#SelectedUniqueReferenceId :selected")[val]().prop("selected", true);
		}
		$scope.UniqueRefID = $("#SelectedUniqueReferenceId :selected").val();
		$scope.paymentID = $scope.UniqueRefID;
		$('.panel').addClass('fade-in-up');
		if($scope.UniqueRefID != ''){
			fillData();			
		}
		$timeout(function() {
			$('.panel').removeClass('fade-in-up');
		}, 500);
	}
	

$scope.goToRepair = function(val) {
        GlobalService.fileListId = val.data.InstructionID;
		GlobalService.UniqueRefID = val.data.PaymentID;
		GlobalService.fromPage = val.fromPage;

		$state.go('app.payment-repair',{input:val})
	}

    $scope.goToWaitforApproval = function(URI,RepairID){
        GlobalService.repairURI = URI;
        GlobalService.repairId = RepairID;
        $location.path('app/waitforpaymentapproval')
    }

	$scope.gotoFiledetail = function (id) {
		GlobalService.fileListId = id;
		$state.go('app.filedetail', {
			input: $state.params
		})

		//$location.path('app/filedetail')

	}

	$scope.multipleEmptySpace = function (e) {
            if($.trim($(e.currentTarget).val()).length == 0)
            {
            $(e.currentTarget).val('');
            }
    }


	$scope.exportToDoc = function(msg)
	{
	//console.log($filter('hex2a')(msg.MessageContents))
    bankData.textDownload($filter('hex2a')(msg.MessageContents),msg.GroupInteractionUniqueID+"_"+msg.MessageInteractionUniqueID);
	}


    $scope.ExportForIE = function()
    {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
        var table_html = ($('#pymntDetailTable').html()).concat($('#fileInfoTable').html()).concat($('#pymntInfoTable').html()).concat($('#tableExport').html());
       // var content2 = $('#tableExport').html();
        bankData.exportToExcelHtml(table_html, $scope.cData.InstructionID+"_"+$scope.cData.OriginalPaymentReference);
        }
    }

	



    var tablesToExcel = (function() {

        var uri = 'data:application/vnd.ms-excel;base64,'
        , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
          + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
          + '<Styles>'
          + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
          + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
          + '</Styles>'
          + '{worksheets}</Workbook>'
        , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
        , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(tables, wsnames, wbname, appname) {

		  var ctx = "";
          var workbookXML = "";
          var worksheetsXML = "";
          var rowsXML = "";

          for (var i = 0; i < tables.length; i++) {
            if (!tables[i].nodeType) tables[i] = document.getElementById(tables[i]);
            for (var j = 0; j < tables[i].rows.length; j++) {
              rowsXML += '<Row>'
              for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                dataValue = (dataValue)?dataValue:tables[i].rows[j].cells[k].innerHTML.trim();
                var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                dataFormula = (dataFormula)?dataFormula:(appname=='Calc' && dataType=='DateTime')?dataValue:null;
                ctx = {  attributeStyleID: (dataStyle=='Currency' || dataStyle=='Date')?' ss:StyleID="'+dataStyle+'"':''
                       , nameType: (dataType=='Number' || dataType=='DateTime' || dataType=='Boolean' || dataType=='Error')?dataType:'String'
                       , data: (dataFormula)?'':dataValue
                       , attributeFormula: (dataFormula)?' ss:Formula="'+dataFormula+'"':''
                      };
                rowsXML += format(tmplCellXML, ctx);
              }
              rowsXML += '</Row>'
            }
            ctx = {rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i};
            worksheetsXML += format(tmplWorksheetXML, ctx);
            rowsXML = "";
          }

          ctx = {created: (new Date()).getTime(), worksheets: worksheetsXML};
          workbookXML = format(tmplWorkbookXML, ctx);


         wbname = $scope.cData.InstructionID+"_"+$scope.cData.OriginalPaymentReference+".xls"
		  

          var link = document.createElement("A");
          link.href = uri + base64(workbookXML);
          link.download = wbname || 'Workbook.xls';
          link.target = '_blank';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      })();


      $('#exportBtn').click(function(){

         var ua = window.navigator.userAgent;
         var msie = ua.indexOf("MSIE");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
            $scope.ExportForIE();
        }
        else
        {
			tablesToExcel(['val1','flInfo','val2','tab1dummy','tab2','tab3','tab4','tab5','tab6'], ['Payment Details','File Information','Payment Information','Payment Event Log','System Interaction','External Communications','Account Posting','Issue Information','Linked Messages'], 'TestBook.xls', 'Excel');

		//tablesToExcel(['val1','flInfo','val2','tab1'], ['Payment Details','File Information','Payment Information','aaa'], 'TestBook.xls', 'Excel');
        }
    })

    function objectFindByKey(array, key, value) {
		var findArr=[];
		for (var i = 0; i < array.length; i++) {
				//console.log(array[i])
			if (array[i][key] === value) {				
				findArr.push(array[i]);
			}
		}
		if(findArr.length>0){
			return findArr;
		}else{
			return null;
		}
	}


	function getForceAction() {

		$scope.nActionBtns = [];
		//console.log("cdata",$scope.cData,$scope.cData.InstructionData.PartyServiceAssociationCode)

		$http.post(BASEURL + RESTCALL.ActionREST, {
			"ProcessStatus" : $scope.cData.Status,
			"WorkFlowCode" : "PAYMENT",
			"PartyServiceAssociationCode" : $scope.cData.InstructionData.PartyServiceAssociationCode
		}).then(function (response) {
			$scope.aBtns = response.data;
			if(response.data.length>0){

				
				//$scope.aBtns.reverse();

				$scope.enableActionbuttons=response.data;
				$scope.enableActionbuttons.reverse() // Buttons arrangements are reversed in HTML view due to pull-right calss

				$scope.rfObj = {};
				$scope.empObj = [];
				for(var i in $scope.aBtns)
				{
					$scope.empObj.push({'ActionName':$scope.aBtns[i].ActionName})
				}
				$scope.PayId = $scope.cData.PaymentID;
				
				$http.post(BASEURL+RESTCALL.PaymentActionDetails,{PaymentID:$scope.PayId,ActionName:$scope.empObj}).success(function(data){
					$scope.chkObj = {};

						/*for(var i in data)
						{	if(data[i].Applicability == 'Enable')	
							{
								$scope.chkObj[data[i].ActionName] = false;
							}
							else if(data[i].Applicability == 'Not Applicable')
							{
								$scope.chkObj[data[i].ActionName] = "notapplicable";
							}
							else{
								$scope.chkObj[data[i].ActionName] = true;
							}
						}*/


						$scope.nActionBtns = [];

						for(var i in data)
						{
							if(data[i].Applicability != 'Not Applicable')
							{	
								for(var j in $scope.aBtns)
								{
									if($scope.aBtns[j].ActionName == data[i].ActionName)
									{
										$scope.aBtns[j].show = data[i].Applicability
										$scope.nActionBtns.push($scope.aBtns[j])
									}
								}
							}
						}

						//console.log("action",$scope.nActionBtns)

						

					}).error(function(data){
						$scope.nActionBtns=[];
					})
			}
			else{
				
				$scope.enableActionbuttons = response.data;
				$scope.enableActionbuttons.reverse() // Buttons arrangements are reversed in HTML view due to pull-right calss
				$scope.nActionBtns=[];
			}
			
		}, function (err) {
			$scope.enableActionbuttons = [];
			$scope.nActionBtns=[];
			//console.error('ERR', err);
		})
	}
	
	//getForceAction()
	
/* 	var enableActionbuttons='';
	if(sessionStorage.ColpData!=undefined){
	$scope.ColpData=JSON.parse(atob(sessionStorage.ColpData));
	console.log($scope.ColpData);
	if($scope.ColpData.length>0)
	{
		var thisPageNewActions=objectFindByKey($scope.ColpData,'Page',$location.path());
			if(thisPageNewActions.Page==$location.path()){
				$scope.enableActionbuttons=thisPageNewActions.CurrentState;
				//console.log($scope.enableActionbuttons)
			}
		}
	}
	 */
	
	function getProcessCode(val){			
			 $http.post(BASEURL+'/rest/v2/partyserviceassociations/read',{'PartyServiceAssociationCode':val}).then(function (response) {
				$scope.ProcessCode = response.data.ProcessCode;
				return response.data.ProcessCode;
			}, function (err) {
				//console.error('ERR', err);
			})
		}
	
		
	$scope.forceAction=function(items,actions){
		
		actions.show = 'Disable';

		var obj1={};
		obj1.payments=[{"PaymentID": items.PaymentID}];		
		
		var method=actions.RestMethod;
		var REST_URL='/rest'+actions.RestURL;
		
		$http({
			url : BASEURL + REST_URL,
			method :method,
			data : obj1,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			/*$scope.alerts = [{
				type : 'success',
				msg : 'Success'
			}];*/
			$rootScope.bulkOverride = data.responseMessage;
			if((actions.SuccessURL!='')&&(actions.SuccessURL!=undefined)){
				
				$location.path(actions.SuccessURL);
			}else{
				
				$location.path('app/allpayments')
			}
			
		}).error(function (data, status, headers, config) { 

			actions.show = 'Enable';

			if((actions.failureURL!='')&&(actions.failureURL!=undefined)){
				$location.path(actions.failureURL);
			}else{
				$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}];
				
				//$location.path('app/allpayments')
			}
			
		});  
		 
	}	

	
	
		$scope.resendAction=function(values){
			//console.log(values)
			
			var obj1={};
			obj1.GrpReferenceId=values.GrpReferenceId;
			obj1.InvocationPoint=values.InvocationPoint;
			obj1.Relationship=values.Relationship;
			obj1.Status=values.Status;
			//console.log(obj1)
			//console.log(BASEURL+'/rest/v2/interface/request/resend')
			
			
			 $http({
				url : BASEURL+'/rest/v2/interface/request/resend',
				method :'POST',
				data : obj1,
				headers : {
					'Content-Type' : 'application/json'
				}
				}).success(function (data, status, headers, config) {
					
					$http.post(BASEURL+RESTCALL.PaymentBIDREST,$scope.paymentObj).then(function (paymentBID) {
						$scope.cDataBID = paymentBID.data;
						$scope.BIDLoaded=true;
					}, function (err) {
						
					})


				}).error(function (data, status, headers, config) {
					$scope.alerts = [{
						type : 'danger',
						msg : data.error.message
					}];
					//$location.path(actions.failureURL);
				}); 
			}

	$scope.fetchedData = {};

	$scope.fetchData = function(addButtons)
	{
		var Inputdata={}
		Inputdata[addButtons.InputObjKey]=$scope.UniqueRefID;
		
		
		var method=addButtons.REST_Method;
		var REST_URL='/rest'+addButtons.REST;
		
		$http({
			url : BASEURL + REST_URL,
			method :method,
			data : Inputdata,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function(data){
	    $scope.fetchedData = data;
	   // console.log($scope.fetchedData)
			if((addButtons.SuccessURL!='')&&(addButtons.SuccessURL!=undefined)){
				$location.path(addButtons.SuccessURL);
			}

	    }).error(function(){
			if((addButtons.failureURL!='')&&(addButtons.failureURL!=undefined)){
				$location.path(addButtons.failureURL);
			}	
	    })
	}

	
	var modalPopButton='';
	if(sessionStorage.PopUpAddonData!=undefined){
	$scope.PopUpAddonData=JSON.parse(atob(sessionStorage.PopUpAddonData));
	
	if($scope.PopUpAddonData.length>0)
	{
	
		var thisPageNewActions=objectFindByKey($scope.PopUpAddonData,'Page',$location.path());
	
			if(thisPageNewActions[0].Page==$location.path()){
				$scope.modalPopButton=thisPageNewActions[0].CurrentState;
	
			}
		}
	}

	$scope.paymentConfirm = function (confirm1, MOPForConfirm) {
	confirm1.InstructionID = $scope.refId;
		//$scope.confirmObj = angular.copy(confirm1)
		//$scope.confirmObj.PaymentID = $scope.cData.DistributionID;

		//console.log(confirm1,$scope.refId)
		//console.log("cc",$scope.cData)
		confirm1 = cleantheinputdata(confirm1);
		$http.post(BASEURL + '/rest/v2/confirmation/' + MOPForConfirm, confirm1).success(function (data, status, headers, config) {
			
			$scope.alerts = [{
					type : 'success',
					msg : data.responseMessage
				}
			];

		$('.modal').modal('hide')
		}).error(function (data, status, headers, config) {
			//console.log(data)
			$scope.alerts = [{
						type : 'danger',
						msg : data.error.message
					}];
		});
	}
	
	
	
	$scope.clickReferenceID = function (val) {
		
		//console.log(val)
		/*val.data.PaymentID=val.LinkedMsgID;
        GlobalService.fileListId = val.InstructionID;
		GlobalService.UniqueRefID = val.LinkedMsgID;
		GlobalService.fromPage = val.fromPage;

		$state.go('app.paymentdetail',{input:val}) */


		GlobalService.fileListId = val.data.InstructionID;
		GlobalService.UniqueRefID = val.LinkedMsgID;
		GlobalService.fromPage = val.fromPage;
       


        $scope.Obj = {
		'uor':(val.data.OutputInstructionID)?val.data.OutputInstructionID:'',
		'nav':{
			  'UIR':val.data.InstructionID,
			  'PID':val.LinkedMsgID
				},
		'from': val.fromPage
	    }

	

		$state.go('app.paymentdetail', {
			
			input: $scope.Obj
		})

	}

	$scope.goToPaymentSummary = function(val)
{
	
	//$state.go('app.outputpaymentsummary',{input:{'uor':val}})

	$scope.Obj = {
		'uor':val,
		'nav':{
			  'UIR':$scope.refId,
			  'PID':$scope.UniqueRefID
				},
		'from': $state.params.input.from
		//'from': ($scope.fromPage == 'allpayments')?'allpayments':'filedetail'
	}

	$state.go('app.outputpaymentsummary',{input:$scope.Obj})
}
	
/*$scope.paymentOverride = function()
{
	$scope.pVal = {
		'PID':$scope.UniqueRefID,
		'UIR':$state.params.input.nav.UIR,
		'from':$state.params.input.from,
		'uor':$state.params.input.uor
		
	}
		
	$state.go('app.interfaceoverride',{input:$scope.pVal})
}*/

	
	//console.log($state.params)
    $scope.iteratedObj = {};
	$scope.fieldDetails = {
		section:[],
		subSection:[]
	};
	
	function iterateSubObj(argu,group,flag)
	{
			$scope.section = {};
				
				$scope.section = {

						'name':group.name,
						'type':group.type,
						'showsectionheader':group.fieldGroup1.webformsectiongroup.showsectionheader,
						'sectionheader':group.fieldGroup1.webformsectiongroup.sectionheader,
						'indentsubfields':group.fieldGroup1.webformsectiongroup.indentsubfields,
						'customsectionlayout':group.fieldGroup1.webformsectiongroup.customsectionlayout,
						'minoccurs':group.fieldGroup1.webformsectiongroup.minoccurs,
						'maxoccurs':group.fieldGroup1.webformsectiongroup.maxoccurs,
						'subArr': []


				}
		
					var iArr = [];
		
				for(var i in argu)
				{
					
					if(argu[i].type != 'Section')	
					{
						$scope.section.subArr.push($scope.objectIttration(argu[i]))
					}
					else{
						$scope.section =  subsectionIterate($scope.section,argu[i],argu[i].fieldGroup1.webformsectiongroup.fields.field)
					}
					
					$scope.iteratedObj = {}
				}
				return $scope.section; 
	}

	function iterateSubArr(argu,group,flag)
	{

		

		$scope.section  = {};
	
		$scope.section = {
						'name':group.name,
						'type':group.type,
						'showsectionheader':group.fieldGroup1.webformsectiongroup.showsectionheader,
						'sectionheader':group.fieldGroup1.webformsectiongroup.sectionheader,
						'indentsubfields':group.fieldGroup1.webformsectiongroup.indentsubfields,
						'customsectionlayout':group.fieldGroup1.webformsectiongroup.customsectionlayout,
						'minoccurs':group.fieldGroup1.webformsectiongroup.minoccurs,
						'maxoccurs':group.fieldGroup1.webformsectiongroup.maxoccurs,
						'subArr':[]
		}


		var iArr = [];
		for(var i in argu)
		{
			
			if(argu[i].type != 'Section')	
			{

				iArr.push($scope.objectIttration(argu[i]))
			}
			else{
				$scope.section.subArr.push({'fields':iArr});
				subsectionIterateArr($scope.section,argu[i],argu[i].fieldGroup1.webformsectiongroup.fields.field)
			}
			
			$scope.iteratedObj = {}
		}

		

			//console.log($scope.fieldDetails,$scope.globalObj.CData[$scope.fieldDetails.subSection[0].name][group.name])

			//console.log("aa")

			/*for(var x in $scope.fieldDetails)
			{
				//console.log($scope.section.subArr, iArr)
				//$scope.section.subArr.push({'fields':iArr});
			}*/
			$scope.section.subArr.push({'fields':iArr});
			iArr = [];
			return $scope.section;

			
				
	}


	function subsectionIterate(obj,arg1,arg2)
	{

		$scope.subArr = {
				'name':arg1.name,
				'type':arg1.type,
				'showsectionheader':arg1.fieldGroup1.webformsectiongroup.showsectionheader,
				'sectionheader':arg1.fieldGroup1.webformsectiongroup.sectionheader,
				'indentsubfields':arg1.fieldGroup1.webformsectiongroup.indentsubfields,
				'customsectionlayout':arg1.fieldGroup1.webformsectiongroup.customsectionlayout,
				'minoccurs':arg1.fieldGroup1.webformsectiongroup.minoccurs,
				'maxoccurs':arg1.fieldGroup1.webformsectiongroup.maxoccurs,
				'subArr':[]
		};

			//console.log($scope.subArr)
			$scope.iteratedObj = {}	
		for(var i in arg2)
		{
			
			//console.log(i,arg2[i])
			$scope.iteratedObj = {};
			if(arg2[i].type != 'Section')
			{
				$scope.subArr.subArr.push($scope.objectIttration(arg2[i]))
			}
			else{
				//console.log("aa",arg2[i])
				$scope.ssg = {
								'name':arg2[i].name,
								'type':arg2[i].type,
								'showsectionheader':arg2[i].fieldGroup1.webformsectiongroup.showsectionheader,
								'sectionheader':arg2[i].fieldGroup1.webformsectiongroup.sectionheader,
								'indentsubfields':arg2[i].fieldGroup1.webformsectiongroup.indentsubfields,
								'customsectionlayout':arg2[i].fieldGroup1.webformsectiongroup.customsectionlayout,
								'minoccurs':arg2[i].fieldGroup1.webformsectiongroup.minoccurs,
								'maxoccurs':arg2[i].fieldGroup1.webformsectiongroup.maxoccurs,
								'subArr': []
				}
				

				for(var k in arg2[i].fieldGroup1.webformsectiongroup.fields.field)
				{
					

					if(arg2[i].fieldGroup1.webformsectiongroup.fields.field[k].type != 'Section')
					{
						$scope.iteratedObj = {};
						$scope.ssg.subArr.push($scope.objectIttration(arg2[i].fieldGroup1.webformsectiongroup.fields.field[k]))
					}
					else{
						var nd = arg2[i].fieldGroup1.webformsectiongroup.fields.field[k];
						$scope.ssg1 = {
							'name':nd.name,
							'type':nd.type,
							'showsectionheader':nd.fieldGroup1.webformsectiongroup.showsectionheader,
							'sectionheader':nd.fieldGroup1.webformsectiongroup.sectionheader,
							'indentsubfields':nd.fieldGroup1.webformsectiongroup.indentsubfields,
							'customsectionlayout':nd.fieldGroup1.webformsectiongroup.customsectionlayout,
							'minoccurs':nd.fieldGroup1.webformsectiongroup.minoccurs,
							'maxoccurs':nd.fieldGroup1.webformsectiongroup.maxoccurs,
							'subArr': []
						}
						for(var l in arg2[i].fieldGroup1.webformsectiongroup.fields.field[k].fieldGroup1.webformsectiongroup.fields.field)
						{
							$scope.iteratedObj = {};
							$scope.ssg1.subArr.push($scope.objectIttration(arg2[i].fieldGroup1.webformsectiongroup.fields.field[k].fieldGroup1.webformsectiongroup.fields.field[l]))
						}

						$scope.ssg.subArr.push($scope.ssg1)
					}
					
					
				}
				$scope.subArr.subArr.push($scope.ssg)
				

				
			}
			
		}
		
		//console.log(arg2, $scope.subArr.subArr)
		obj.subArr.push($scope.subArr)
		
		return obj;
	}

	function subsectionIterateArr(obj,arg1,arg2)
	{
		//console.log("section",obj,arg1,arg2)
		$scope.subArr = {
				'name':arg1.name,
				'type':arg1.type,
				'showsectionheader':arg1.fieldGroup1.webformsectiongroup.showsectionheader,
				'sectionheader':arg1.fieldGroup1.webformsectiongroup.sectionheader,
				'indentsubfields':arg1.fieldGroup1.webformsectiongroup.indentsubfields,
				'customsectionlayout':arg1.fieldGroup1.webformsectiongroup.customsectionlayout,
				'minoccurs':arg1.fieldGroup1.webformsectiongroup.minoccurs,
				'maxoccurs':arg1.fieldGroup1.webformsectiongroup.maxoccurs,
				'subArr':[]
		};

		var iArr1 = [];
		for(var i in arg2)
		{
			if(arg2[i].type != 'Section')
			{
				iArr1.push($scope.objectIttration(arg2[i]))
			}
			else{
					//console.log(arg2[i])

					var nd = arg2[i].fieldGroup1.webformsectiongroup.fields.field;
					
					$scope.sn = {
						'name':arg2[i].name,
						'type':arg2[i].type,
						'showsectionheader':arg2[i].fieldGroup1.webformsectiongroup.showsectionheader,
						'sectionheader':arg2[i].fieldGroup1.webformsectiongroup.sectionheader,
						'indentsubfields':arg2[i].fieldGroup1.webformsectiongroup.indentsubfields,
						'customsectionlayout':arg2[i].fieldGroup1.webformsectiongroup.customsectionlayout,
						'minoccurs':arg2[i].fieldGroup1.webformsectiongroup.minoccurs,
						'maxoccurs':arg2[i].fieldGroup1.webformsectiongroup.maxoccurs,
						'subArr': []
					}
					

					for(var x in nd)
					{
						$scope.sn.subArr.push($scope.objectIttration(nd[x]))
						$scope.iteratedObj = {}
					}

					iArr1.push($scope.sn)
					
			}
			
			$scope.iteratedObj = {}
		}
		
		$scope.subArr.subArr = iArr1
		iArr1 = [];
		obj.subArr[0].fields.push($scope.subArr);
		obj.subArr.splice(0,1)
		return obj;
	}

	function webformIttration(argu){
		
		 $scope.iteratedObj = {};
		 $scope.fieldDetails = {
			 section:[],
			subSection:[]
		 };
		var obtainedFields = argu.webformuiformat.fields.field;
		$scope.obtainThisKeys = ['name','type','columnspan','rowspan','enabled','label','labelposition','newrow','notnull','visible','width','renderer','customsectionlayout','indentsubfields','maxoccurs','minoccurs','sectionheader','showsectionheader','dateformat','property','choiceOptions']
			var k ='';
			var j = '';
		
			
				

		for(j in obtainedFields){

			
			 if('webformfieldgroup' in obtainedFields[j].fieldGroup1)
			 {
				  $scope.iteratedObj = {};
				  //console.log(obtainedFields[j])
				  $scope.fieldDetails.section.push($scope.objectIttration(obtainedFields[j]))
				 
			}
			 else{
				 
				 	if(obtainedFields[j].type != 'Section'){
						//console.log(obtainedFields[j].type,obtainedFields[j])
							
						$scope.fieldDetails.section.push($scope.objectIttration(obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field))
					}
					else if(obtainedFields[j].type == 'Section'){
					
						$scope.iteratedObj['sectionlabel'] = obtainedFields[j].name;
						$scope.fieldDetails.subSection.push({
							'name':('name' in obtainedFields[j] ? obtainedFields[j].name: ''),
							'type':('type' in obtainedFields[j] ? obtainedFields[j].type: ''),
							'showsectionheader':('showsectionheader' in obtainedFields[j].fieldGroup1.webformsectiongroup ? obtainedFields[j].fieldGroup1.webformsectiongroup.showsectionheader:''),
							'sectionheader':('sectionheader' in obtainedFields[j].fieldGroup1.webformsectiongroup ? obtainedFields[j].fieldGroup1.webformsectiongroup.sectionheader:''),
							'indentsubfields':('indentsubfields' in obtainedFields[j].fieldGroup1.webformsectiongroup ? obtainedFields[j].fieldGroup1.webformsectiongroup.indentsubfields:''),
							'customsectionlayout':('customsectionlayout' in obtainedFields[j].fieldGroup1.webformsectiongroup ?obtainedFields[j].fieldGroup1.webformsectiongroup.customsectionlayout:''),
							'minoccurs':('minoccurs' in obtainedFields[j].fieldGroup1.webformsectiongroup ? obtainedFields[j].fieldGroup1.webformsectiongroup.minoccurs:''),
							'maxoccurs':('maxoccurs' in obtainedFields[j].fieldGroup1.webformsectiongroup ? obtainedFields[j].fieldGroup1.webformsectiongroup.maxoccurs:''),
							'subArr':[]
						})

						

						for(k in obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field){
							if(obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k].type == "Section")
							{	
								
									if(obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k].fieldGroup1.webformsectiongroup.maxoccurs == -1)
									{
										$scope.fieldDetails.subSection[j]['subArr'].push(iterateSubArr(obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k].fieldGroup1.webformsectiongroup.fields.field, obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k],false))	
									}
									else
									{
										$scope.fieldDetails.subSection[j]['subArr'].push(iterateSubObj(obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k].fieldGroup1.webformsectiongroup.fields.field, obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k],false))	
									}
							}
							else
							{
								$scope.fieldDetails.subSection[j].subArr.push($scope.objectIttration(obtainedFields[j].fieldGroup1.webformsectiongroup.fields.field[k]))
							
							}

							for(l in Object.keys(obtainedFields[j].fieldGroup1.webformsectiongroup)){
								if(($scope.obtainThisKeys).indexOf(Object.keys(obtainedFields[j].fieldGroup1.webformsectiongroup)[l]) != -1){
									
									$scope.iteratedObj[Object.keys(obtainedFields[j].fieldGroup1.webformsectiongroup)[l]] = Object.values(obtainedFields[j].fieldGroup1.webformsectiongroup)[l]
								}
							}							
							$scope.iteratedObj = {}
						}
					}	
					
					}
				
				}


				for(var x in $scope.fieldDetails.section)
				{
					if('Choice' in $scope.fieldDetails.section[x].renderer)
					{
						if($scope.fieldDetails.section[x].renderer.Choice.choiceOptions[0].actualvalue == 'REST')
						{
							var prop =  $scope.fieldDetails.section[x].renderer.Choice.customattributes.property[$scope.fieldDetails.section[x].renderer.Choice.customattributes.property.length-1].value

							
							
							if($scope.fieldDetails.section[x].renderer.Choice.customattributes.property[$scope.fieldDetails.section[x].renderer.Choice.customattributes.property.length-1].value.indexOf('{') != -1)
							{
								$scope.restPath = $scope.fieldDetails.section[x].renderer.Choice.customattributes.property[$scope.fieldDetails.section[x].renderer.Choice.customattributes.property.length-1].value.split('{')[0] + $scope[$scope.fieldDetails.section[x].renderer.Choice.customattributes.property[0].value];
							}
							else{
								$scope.restPath = $scope.fieldDetails.section[x].renderer.Choice.customattributes.property[$scope.fieldDetails.section[x].renderer.Choice.customattributes.property.length-1].value
							}

							

							

							getChoiceOption('GET', $scope.restPath,x)
						}
					}
				}

				$scope.bDataForFields = angular.copy($scope.fieldDetails)
				$scope.backupObj.value = $scope.bDataForFields.section;
				
			return $scope.fieldDetails
	}


	function getChoiceOption(_method,url,x)
	{
		return $http({
			method : _method,
			url : BASEURL+'/rest/v2/'+url
		}).then(function (response) {
			
			$scope.fieldDetails.section[x].renderer.Choice.choiceOptions = response.data;
			return response.data;
			
		},function (error) {


		})
	}

    $scope.objectIttration = function(argu,k,l){

		for (var key in argu) {
			
			if (argu.hasOwnProperty(key)) {				
				if(typeof(argu[key]) == 'object'){
								
					$scope.objectIttration(argu[key],k,l);						
					if(($scope.obtainThisKeys).indexOf(key) != -1 && !(key in $scope.iteratedObj)){
						$scope.iteratedObj[key] = argu[key]	
					}							
				}
				else{
					//console.log('else',key,obj[key])						
					if(($scope.obtainThisKeys).indexOf(key) != -1 && !(key in $scope.iteratedObj)){
						$scope.iteratedObj[key] = argu[key]	
					}
				}
			}
		}
		return $scope.iteratedObj
	}
	
	$scope.fieldData = {};

	$scope.PayId = $scope.UniqueRefID;

	$scope.paymentID = $scope.UniqueRefID;

	$scope.callInterfaceOverride = function(){
		
	$scope.iteratedObj = {};
    $scope.fieldDetails = {
		section:[],
		subSection:[]
	};
	$scope.fieldData = {};
		
		/*$scope.alerts1 = [{
				type : 'danger',
				msg : 'Please fill all the mandatory fields'
			}];*/

			
			$('.alert-danger').hide();

	

    $http.post(BASEURL+RESTCALL.PaymentInterface,{"PaymentID":$scope.PayId}).then(function(val){
		
		$scope.allResponse = val;
		
		$scope.globalObj = angular.copy(val.data);
		$scope.globalObj.CMetaInfo = JSON.parse(atob($scope.globalObj.metaInfo)).Data;
		$scope.globalObj.CData = JSON.parse(atob($scope.globalObj.data));
		
		$timeout(function(){

			$scope.fieldData = $scope.globalObj.CData;
		},100)
		

		$scope.backupData = angular.copy($scope.globalObj.CData)
		
	
		
		$scope.fieldDetails = [];
		$scope.iteratedObj = {}

	webformIttration($scope.globalObj.CMetaInfo)


	
},function(data){
	
		$scope.alerts1 = [{
						type : 'danger',
						msg : data.data.error.message
					}
				];
				

	})
	
	
	}
	
	
	
	
	$scope.iteratedFields = [];

	function constructObj(inFields)
	{
		
		for(var i in inFields)
		{
			if(inFields[i].type != 'Section'){
					//console.log(inFields[i])
			}
			else if(inFields[i].type == 'Section'){

				$scope.iteratedFields.push({
						'name':inFields[i].name,
						'type':inFields[i].type,
						'showsectionheader':inFields[i].fieldGroup1.webformsectiongroup.showsectionheader,
						'sectionheader':inFields[i].fieldGroup1.webformsectiongroup.sectionheader,
						'indentsubfields':inFields[i].fieldGroup1.webformsectiongroup.indentsubfields,
						'customsectionlayout':inFields[i].fieldGroup1.webformsectiongroup.customsectionlayout,
						'minoccurs':inFields[i].fieldGroup1.webformsectiongroup.minoccurs,
						'maxoccurs':inFields[i].fieldGroup1.webformsectiongroup.maxoccurs,
						'group':[]
						
				})
					$scope.TempVal = [];


						for(var j in inFields[i].fieldGroup1.webformsectiongroup.fields.field)
						{
							if(inFields[i].fieldGroup1.webformsectiongroup.fields.field[j].type == 'Section')
								{
									
									constructObj(inFields[i].fieldGroup1.webformsectiongroup.fields.field[j].fieldGroup1.webformsectiongroup.fields.field)

								}
						}
						$scope.iteratedObj = {};
					}	
		}
	}


	$scope.cleantheinputdata = function(newData){

		$.each(newData, function(key,value){
			//console.log(key,value)
			delete newData.$$hashkey;

			if($.isPlainObject(value)){	
				var isEmptyObj = $scope.cleantheinputdata(value)				
				if($.isEmptyObject(isEmptyObj)){
					delete newData[key]
				}
			}
			else if(Array.isArray(value)){
//console.log(value)
				if(value.length){
					$.each(value, function(k,v){
						if(typeof(v) != 'object'){
							//console.log(value,newData[key])
							if(Array.isArray(newData[key])){
								newData[key] = newData[key].toString()
							}
						}
						else{
							var isEmptyObj = $scope.cleantheinputdata(v)						
						}
					})					
				}
				else{
					delete newData[key]
				}
			}
			else if(value === "" || value === undefined || value === null){
				delete newData[key]
			}
		})
		
		return newData
	}


	$scope.formSubmitted = false;
	$scope.interfaceSubmit = function(val)
	{
		
		$scope.formSubmitted = true;

		$scope.rfclicked = true;

		val = $scope.cleantheinputdata(val)
		$scope.responseObj = {
							"paymentID": $scope.PayId,
							"domainInWebFormName": $scope.globalObj.metaInfoName,
							"DomainIn": btoa(JSON.stringify(val))
							}

		$http.post(BASEURL+RESTCALL.PaymentInterfaceOverride,$scope.responseObj).success(function(data)
		{
			$('.modal').modal('hide')

			$scope.alerts = [{
				type : 'success',
				msg : data.responseMessage
			}]
			$scope.rfclicked = false;
			var iCnt = 0;
			var iInterval = '';
			clearInterval(iInterval)
			iInterval = setInterval(function(){

				$scope.fetchDataAgain()
				iCnt++;
				if(iCnt == 3)
				{
					clearInterval(iInterval)
				}
			},1500)
			
		}).error(function(data)
		{
			$scope.rfclicked = false;
				$scope.alerts1 = [{
				type : 'danger',
				msg : data.error.message
			}];
		})

		
	}





	var fArr = [];
	
	
var chkVal = '';

var newArr = [];
var splitVal;
var propObj={};



var prev = '';



	$scope.diabledFields = function(val,field,allfields,ind)
	{	
		
		prev = field.name;
		newArr = [];
		if(val)
		{
			for(var i in field.property)
			{
				if(field.property[i].name.indexOf('|') != -1 && field.property[i].name.split('|')[0] == 'VALUE')
				{
					if(val == field.property[i].name.split('|')[1])
					{
						$scope.parsedVal = JSON.parse(field.property[i].value)
						
						for(var i in $scope.parsedVal)
						{
							for(var j in allfields)
							{	
								if(i == allfields[j].name)
								{
									for(x in $scope.parsedVal[i])
									{
										allfields[j][x.toLowerCase()] = $scope.parsedVal[i][x];
										$scope.fieldData[i] = '';
									}
									
								}
								
							}
						}
						break;
					}
					else
					{
						for(var i in field.property)
						{
							if(field.property[i].name.indexOf('|') != -1 && field.property[i].name.split('|')[0] == 'VALUE')
							{	
								$scope.pVal = JSON.parse(field.property[i].value)

								for(var j in $scope.pVal)
								{

									for(var x in $scope.backupObj.value)
									{
										if($scope.backupObj.value[x].name == j)
										{
											allfields[x] = angular.copy($scope.backupObj.value[x])
										}
									}
									

									
								}
							}
						}
					}

					
				}
				
				
			}
	}
	else
	{
		for(var i in field.property)
		{
			if(field.property[i].name.indexOf('|') != -1 && field.property[i].name.split('|')[0] == 'VALUE')
			{	
				$scope.pVal = JSON.parse(field.property[i].value)

				for(var j in $scope.pVal)
				{

					for(var x in $scope.backupObj.value)
					{
						if($scope.backupObj.value[x].name == j)
						{
							allfields[x] = angular.copy($scope.backupObj.value[x])
						}
					}
					

					
				}
			}
		}
		
	}
	

	}






	$scope.aBtnVal = 'override';
	$scope.chkBtnvalue = function(val)
	{
		$scope.aBtnVal = val.toLowerCase();
	}

	$scope.chkProperty=function(arg)
	{
		if($.isArray(arg))
		{
			$scope.chkProperty(arg[0])
		}
		else{
			if('property' in arg)
			{
				for(var i in arg['property'])
				{
					if(arg['property'][i].name == 'SUBMIT')
					{
						$scope.finalRestPath = arg['property'][i].value;
						break;
					}
				}
				
			}
			else{
				$scope.chkProperty(arg.subArr)
			}
		}
		
	}
	
	

	$scope.actionWebformSubmit = function(val)
	{
	_val = angular.copy(val)
		
		$scope.rfclicked = true;
		_val = removeEmptyValueKeys(_val)
		//console.log(removeEmptyValueKeys(_val))
    
		
    _val = $scope.cleantheinputdata(_val);
		$scope.actionObj = {
			//PaymentID:$scope.PayId
		}
		$scope.actionObj = _val;

		//$scope.actionObj[$scope.rfData.metaInfoName] = val
		
		$scope.finalRestPath = '';

		if($scope.fieldDetails.section.length)
		{
			for(var l in $scope.fieldDetails.section[0].property)
			{
				if($scope.fieldDetails.section[0].property[l].name == "SUBMIT")
				{
					if($scope.aBtnVal == 'paymentconfirmation')
					{
						$scope.finalRestPath = $scope.fieldDetails.section[0].property[l].value.split('{')[0]+$scope.cData.MethodOfPayment
					}
					else{
						$scope.finalRestPath = $scope.fieldDetails.section[0].property[l].value;
					}
				}
			}

		}
		else
		{
			$scope.chkProperty($scope.fieldDetails.subSection[0]);
		}

		


		if(!$scope.selectedAction.FunctionName || $scope.selectedAction.FunctionName == 'DisplayPopUpWithWebFormInput')
		{
			$scope.responseObj = $scope.actionObj;
		}
		else if($scope.selectedAction.FunctionName == 'DisplayPopUpWithWebFormInputForOverride')
		{
			$scope.responseObj = {
				"paymentID": $scope.PayId,
				"domainInWebFormName": $scope.rfData.metaInfoName,
				"DomainIn": btoa(JSON.stringify($scope.actionObj))
				}
		}
		
	$http.post(BASEURL+"/rest/v2/"+$scope.finalRestPath,$scope.responseObj).success(function(data){
			
			
			$scope.rfclicked = false;
			if($scope.rfData.metaInfoName == 'ReturnOfFunds')
			{
				$scope.chkObj['GenerateROF'] = true;
			}

			$('#overRide').modal('hide')
			$scope.alerts = [{
				type : 'success',
				msg : data.responseMessage
			}];
    
    $timeout(function(){
      $('.alert-success').hide();
    },6000)

	$scope.nActionBtns = [];

	var iCnt = 0;
	var iInterval = '';
	//clearInterval(iInterval)
	$scope.fetchDataAgain()
		iInterval = setInterval(function(){

			$scope.fetchDataAgain()
			clearInterval(iInterval)
			//iCnt++;

			// if(iCnt == 1)
			// {
			// 	clearInterval(iInterval)
			// }
		},2000)
	}).error(function(data)
		{
			$scope.rfclicked = false;
			$scope.alerts1 = [{
					type : 'danger',
					msg : data.error.message
				}]
		})
	}	


	$scope.sectionCnt=0;
	$scope.addsubSection = function(x)
	{	

			$scope.sectionCnt ++;
			if($scope.sectionCnt == x.subArr.length)
			{
				$scope.sectionCnt = x.subArr.length-1;
			}
			
			$('.'+x.name).css('display','none')
			$('#'+x.name+'_'+$scope.sectionCnt).css('display','block')

	}

	$scope.removesubSection = function(x)
	{
		$scope.sectionCnt --;
			if($scope.sectionCnt < 0 )
			{
				$scope.sectionCnt = 0;
				
			}
		$('.'+x.name).css('display','none')
		$('#'+x.name+'_'+$scope.sectionCnt).css('display','block')

	}

	$scope.actionWebform = function(arg)
	{
		$scope.submitted = false;
		$scope.selectedAction = arg;
		$scope.rfclicked = false;
		
		$scope.fieldDetails = {
			section:[],
		   subSection:[]
		   };
		   $scope.metaInfoName  = '';

	
		

			prevObj = {};
			$('.alert-danger').hide();
			$scope.fieldData = {};
			
			
			$http.post(BASEURL+'/rest'+arg.RestURL, {"PaymentID":$scope.PayId}).success(function(data){

				
				$scope.rfData = data;

				$scope.metaInfoName = $filter('camelCaseFormatter')($scope.rfData.metaInfoName)
				$scope.actionWebformData = JSON.parse(atob(data.metaInfo)).Data;
				$scope.fieldData = JSON.parse(atob(data.data))
				$('.actionWebForm').find('[type=submit]').css('display','block');
				webformIttration($scope.actionWebformData)
			}).error(function(data){

				$('.actionWebForm').find('[type=submit]').css('display','none');
				
				$scope.alerts1 = [{
					type : 'danger',
					msg : data.error.message
				}]

			})
		//}
	}


	$scope.multipleEmptySpace = function (e) {
		if($.trim($(e.currentTarget).val()).length == 0){
		$(e.currentTarget).val('');
		}
		
		if($(e.currentTarget).is('.DatePicker, .DateTimePicker, .TimePicker')){
			$(e.currentTarget).data("DateTimePicker").hide();
		}
	}

	$scope.checkType = function (eve, type) {

		var compareVal = '';
		var regex = {
			'Integer' : /^[0-9]$/,
			'BigDecimal' : /^[0-9.]$/,
			'Double': /^[0-9.]$/,
			'String' : /^[a-z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~ ]*$/i
		}
		for (var keys in regex) {
			if (type === keys) {
				compareVal = regex[type]
			}
		}

		if (compareVal.test(eve.key) || eve.keyCode == 16 || eve.keyCode == 36 || eve.keyCode == 46 || eve.keyCode == 8 || eve.keyCode == 9 || eve.keyCode == 35 || eve.keyCode == 37 || eve.keyCode == 39 || eve.keyCode == 38 || eve.keyCode == 40) {
			return true
		} else {
			eve.preventDefault();
		}
	}



	$scope.resetInterface = function()
	{
		$scope.fieldData = angular.copy($scope.backupData);
	}


	$scope.gotoPaymentDetail = function()
	{
		GlobalService.fileListId = $state.params.input.UIR;
		GlobalService.UniqueRefID = $state.params.input.PID;
		GlobalService.fromPage = $state.params.input.from;
       


        $scope.Obj = {
		'uor':$state.params.input.uor,
		'nav':{
			  'UIR':$state.params.input.UIR,
			  'PID':$state.params.input.PID
				},
		'from': $state.params.input.from
	    }

		$state.go('app.paymentdetail', {
			input: $scope.Obj
		})
	}

	
	$scope.widthOnScroll = function()
    	{	var mq = window.matchMedia( "(max-width: 991px)" );
    		var headHeight
    		if (mq.matches) {
    		 headHeight =0;
    		 $scope.alertWidth = $('.pageTitle').width();
    		} else {
    		   $scope.alertWidth = $('.pageTitle').width();
    			headHeight = 10;
			}
			$scope.alertStyle=headHeight;
    	}

			$scope.widthOnScroll();
			

	$(window).bind('scroll',function()
	{
		$scope.widthOnScroll()
	})

	$scope.activatePicker = function(e){

	
		var prev = null;
		$('.DatePicker').datetimepicker({
			format:"YYYY-MM-DD",
			useCurrent: false,
			showClear: true
		}).on('dp.change', function(ev){
			
			var split = $(ev.currentTarget).attr('aName').split('_')
			
			if(split.length == 5)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]][split[4]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 4)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 3)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 2)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]] = $(ev.currentTarget).val();	
			}
			else
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]] = $(ev.currentTarget).val();	
			}

			
			/*if ($(ev.currentTarget).attr('ng-model').split('[')[0] != 'subData') {
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
			} else {
				var pId = $(ev.currentTarget).parent().parent().parent().parent().parent().attr('id');
				$scope.subSectionfieldData[pId][$(ev.currentTarget).attr('name').split('_')[1]][$(ev.currentTarget).attr('name').split('_')[0]] = $(ev.currentTarget).val()
			}*/

		}).on('dp.show', function(ev){
			
				
	}).on('dp.hide', function(ev){
		

	});



		$('.TimePicker').datetimepicker({
			format: 'HH:mm:ss',
			useCurrent: false,
		}).on('dp.change', function(ev){
			var split = $(ev.currentTarget).attr('aName').split('_')
			
			if(split.length == 5)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]][split[4]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 4)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 3)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 2)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]] = $(ev.currentTarget).val();	
			}
			else
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]] = $(ev.currentTarget).val();	
			}
		}).on('dp.show', function(ev){
					
		}).on('dp.hide', function(ev){
			
		});


		$('.DateTimePicker').datetimepicker({
			format : "YYYY-MM-DDTHH:mm:ss",
			useCurrent : false,
			showClear : true
		}).on('dp.change', function (ev) {
			var split = $(ev.currentTarget).attr('aName').split('_')
			
			if(split.length == 5)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]][split[4]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 4)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 3)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 2)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]] = $(ev.currentTarget).val();	
			}
			else
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]] = $(ev.currentTarget).val();	
			}
		}).on('dp.show', function (ev) {
			
			
			
		}).on('dp.hide', function (ev) {
			
			
			
		});

		$('.ISODateTime').datetimepicker({
			format : "YYYY-MM-DDTHH:mm:ssZZ",
			useCurrent : false,
			showClear : true
		}).on('dp.change', function (ev) {
			
			
			var isoDate = moment(e.date).format().split('+')
			$(ev.currentTarget).val(isoDate[0]+":"+new Date().getMilliseconds()+'+'+isoDate[1])
			

			var split = $(ev.currentTarget).attr('aName').split('_')
			if(split.length == 5)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]][split[4]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 4)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]][split[3]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 3)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]][split[2]] = $(ev.currentTarget).val();	
			}
			else if(split.length == 2)
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]][split[1]] = $(ev.currentTarget).val();	
			}
			else
			{
				$scope[$(ev.currentTarget).attr('ng-model').split('[')[0]][split[0]] = $(ev.currentTarget).val();	
			}
		}).on('dp.show', function (ev) {
			var isoDate = moment(e.date).format().split('+')
			$(ev.currentTarget).val(isoDate[0]+":"+new Date().getMilliseconds()+'+'+isoDate[1])
		}).on('dp.hide', function (ev) {
			
		});

	}


	$scope.setIVal = function(arg,arg1)
	{
		//console.log("arg",arg)
		var split = arg.split('_')

		
	}

	 $scope.getTextAreaRows= function(val1){
    	return Math.ceil(val1);
    }

	 $scope.triggerPicker = function(e)
	  {
		 	if ($(e.currentTarget).prev().is('.DatePicker, .DateTimePicker, .TimePicker')) {
				$scope.activatePicker($(e.currentTarget).prev());
				$('input[name=' + $(e.currentTarget).prev().attr('name') + ']').data("DateTimePicker").show();
			}
	  }


	  $scope.printFn = function()
	  {
		  window.print();
	  }
	
	var backup_value = {};
	
	$scope.diableFields = function(_data,_input){
		for(var val in _input.property){
			if(_input.property[val].name.indexOf('|') != -1){
				//console.log('came',_data,_input.property[val].name.split('|')[1].indexOf(_data))
				if(_data.length){
					_data.forEach(function(dropVal,dropvalIndex){
						if(_input.property[val].name.split('|')[1].indexOf(dropVal) != -1){
							//console.log('if',dropvalIndex)
							var checkValue = JSON.parse(_input.property[val].value)	
							$scope.fieldDetails.section.forEach(function(currentValue, index, arr){
								if(checkValue[currentValue.name]){
									if(!backup_value[val]){
										backup_value[val] =  {
																	'index' : angular.copy(index),
																	'value' : angular.copy(currentValue),
																	'keys'  : Object.keys(checkValue[currentValue.name])
																}										
										Object.keys(checkValue[currentValue.name]).forEach(function(Value, index, arr){
											currentValue[Value.toLowerCase()] = checkValue[currentValue.name][Value];
										})									
									}
								}
							})
						}
						else{
							if(backup_value[val]){
								backup_value[val]['keys'].forEach(function(vals){
									//console.log(val,backup_value[val])
									$scope.fieldDetails.section[backup_value[val]['index']][vals.toLowerCase()] = backup_value[val]['value'][vals.toLowerCase()];
								})
								delete backup_value[val]
							}							
						}
					})					
				}
				else{
					for(var val_ in backup_value){
						backup_value[val_]['keys'].forEach(function(val){
							$scope.fieldDetails.section[backup_value[val_]['index']][val.toLowerCase()] = backup_value[val_]['value'][val.toLowerCase()];
						})
					}
					backup_value = {};										
				}
			}
		}
		//console.log('backup_value',backup_value)
	}
	
	$scope.appendSelectBox = function(){
		$('.appendSelect2').each(function(){
			var parsedVal = JSON.parse($(this).attr('detailsoffield'))
			if(parsedVal.choiceOptions[parsedVal.choiceOptions.length-1].displayvalue == "MULTISELECT"){
				$(this).attr({'multiple':true,'data-placeholder':'Select'})	
				if($scope.fieldData[$(this).attr('name')] && $scope.fieldData[$(this).attr('name')].indexOf(',') != -1){
					$(this).val($scope.fieldData[$(this).attr('name')].split(','))
				}
				else{
					$(this).val($scope.fieldData[$(this).attr('name')])					
				}
				if(!$(this).find('option:first-child').attr('value') || !$(this).find('option:first-child').text()){
					$(this).find('option:first-child').remove();
				}
				$(this).select2({
					allowClear : true
				}); 
			}
		})		 
	}
	
	
	$("#overRide").on('hidden.bs.modal', function(){
		backup_value = {}
		//console.log(backup_value)
	});
	
	$("#overRide").on('shown.bs.modal', function(){
		$scope.appendSelectBox()
	});

	$scope.taskStatusArr = '';

		$http.get(BASEURL + '/rest/v2/payments/taskstatus/'+$scope.UniqueRefID).success(function (data, status, headers, config) {
			for(i in data)
			{
				$scope.taskStatusArr = $scope.taskStatusArr + ' ' + $filter('hex2a')(data[i].payload);
			}
		// console.log($scope.taskStatusArr,"$scope.taskStatusArr")
		
		}).error(function (data, status, headers, config) {

		});

		$scope.target_table = function(target,id){
			var id = $filter('specialCharactersRemove')(target)+'_'+id;
			// if($('.overflowPaymentSection').hasClass('active')){

			// }
			$('.overflowPaymentSection').removeClass('active')
			$('#tab_'+id).addClass('active')
			// console.log($('#tab_'+id))

		}

});
