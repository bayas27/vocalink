VolpayApp.controller('viewApprovalDataCtrl', function ($scope, $state, $stateParams, $http, $timeout, CommonService, GlobalService) {
	
	//$scope.text_remaining = 300;
		/* $('#idforNotes').bind('keydown',function(){
			 var text_max = 300;
			 $scope.text_remaining = 300;
			 var text_length = $('#idforNotes').val().length;
			 
			$scope.text_remaining = text_max - text_length;
			console.log($scope.text_remaining, text_max,text_length)
			
		});*/
	
	
		//var max = 300;

		$scope.ApproveDetail = {
			approved:false,
			rejected:false
		}


		$scope.data = $stateParams.input;


		$scope.delArr = ["mpitemplate","configurations","idconfigurations","routeregistry"];

		$scope.bankDataArr = [];

		GlobalService.sidebarVal = GlobalService.sidebarVal?GlobalService.sidebarVal:JSON.parse(sessionStorage.menuList  )

		GlobalService.sidebarVal.forEach(function(val){
			if(val.Link == 'bankData')
			{
				val.subMenu.forEach(function(menu){
					if($scope.delArr.indexOf(menu.Link) == -1)
					{
						$scope.bankDataArr.push({"Name":menu.TableName,"Link":menu.Link})
					}
					
				})
			}
		})

		
		$scope.fieldDetails = [];

		function crudRequest(_method, _url, _data, _params){ 
			return $http({
				method: _method,
				url: BASEURL + "/rest/v2/" + _url,
				data: _data,
				params:_params
			}).then(function(response){
				
				$scope.restResponse={
					data:response
				}
				
				return $scope.restResponse
			},function(error){
				
			})
		}
		
		
		var max = 270
		$("#idforNotes").keydown(function(e){
		  $("#lCnt").text((max - $(this).val().length)+" Characters Left");
		});
	
	
	
	
		
		//console.log($scope.data)
		$scope.approvalNotes = '';
		if ($scope.data.TableName == 'BusinessRules') {
	
			splitData($scope.data.OldData, $scope.data.NewData)
	
		}
		$timeout(function () {
			if ($('.newData').height() > $('.oldData').height()) {
				$('.preData').height($('.newData').height())
			} else {
				$('.preData').height($('.oldData').height())
			}
		}, 100)
	
		$scope.approveData = {};
		$scope.approveData.IsApproved = false;
	
		$scope.approveRefData = function (notes,flag) {
		//console.log(notes,flag)
			var dataObj = {};
			dataObj.ApproveID = $scope.data.ApprovalID;
			dataObj.isApproved = flag;
			dataObj.Notes = notes ? notes.approvalNotes : '';


			$scope.ApproveDetail.approved = true;
				/*$scope.ApproveDetail = {
					approved:flag?true:false,
					rejected:!flag?true:false
				}*/
				
			
	
			$http.put(BASEURL + RESTCALL.ReferenceDataApproval, dataObj).success(function (data) {
	
				CommonService.refDataApproved.flag = true;
				CommonService.refDataApproved.msg = data.responseMessage;

				$scope.ApproveDetail.approved = false;
				// $scope.ApproveDetail = {
				// 	approved:false,
				// 	rejected:false
				// }

	
				$state.go('app.approvals')
	
			}).error(function (err) {
				CommonService.refDataApproved.flag = false;
				//console.log(err)
				$scope.alerts = [{
						type: 'danger',
						msg: err.error.message
					}
				];
	
				$timeout(function () {
					$scope.alertStyle = alertSize().headHeight;
					$scope.alertWidth = alertSize().alertWidth;
				}, 100)
	
			})
		}
	
		$scope.cancelApproval = function () {
			$state.go('app.approvals');
		}
	
		$scope.alertWidth = 0;
		$scope.widthOnScroll = function () {
			var mq = window.matchMedia("(max-width: 991px)");
			var headHeight
			if (mq.matches) {
				headHeight = 0;
				$timeout(function () {
					$scope.alertWidth = $('.pageTitle').width();
				}, 100)
			} else {
				$timeout(function () {
					$scope.alertWidth = $('.pageTitle').width();
				}, 100)
				headHeight = $('.main-header').outerHeight(true) + 10;
			}
	
			$scope.alertStyle = headHeight;
	
		}
		$scope.widthOnScroll();
	
		$(window).scroll(function () {
			$scope.widthOnScroll();
		});
	
	
	
		$scope.multipleEmptySpace = function (e) {
			$(e.currentTarget).val($.trim($(e.currentTarget).val()))
	
			if($.trim($(e.currentTarget).val()).length == 0)
			{
				 //console.log(max,$(e.currentTarget).val().length)
			
				$(e.currentTarget).val('');
				
			}
				$("#lCnt").text(max - $(e.currentTarget).val().length+" Characters Left");
		}
	
	
	
		var nodeText1 = [],
		nodeText2 = [];
		var difference = [];
		$scope.NewDataLabel = [];
		$scope.ApprovalNewData = [];
		$scope.ApprovalNewLabel = [];
		$scope.ApprovalOldData = [];
		$scope.ApprovalOldLabel = [];
	
		function getVal1() {
	
			nodeText1.push(comp1.trim());
		}
		function getVal2() {
			nodeText2.push(comp2.trim());
		}
	
		if ($scope.data.TableName.toUpperCase() == 'ROLERESOURCEPERMISSION') {
	
			conversion($scope.data.OldData, $scope.data.NewData)
	
		}
		function conversion(OldData, NewData) {
			$scope.xmlData = [{
					'hexdata': OldData,
					'convXml': ''
				}
			]
	
			$scope.xmlNewData = [{
					'hexdata': NewData,
					'convXml': ''
				}
			]
	
			if ($scope.xmlData[0].hexdata) {
	
				for (var k = 0; k < $scope.xmlData.length; k++) {
					for (var i = 0; i < $scope.xmlData[k].hexdata.length; i += 2) {
						var v = parseInt($scope.xmlData[k].hexdata.substr(i, 2), 16);
						if (v)
							$scope.xmlData[k].convXml += String.fromCharCode(v);
					}
					$scope.xmlData[k].convXml = $scope.xmlData[k].convXml.replace(/&lt;/g, '<');
					$scope.xmlData[k].convXml = $scope.xmlData[k].convXml.replace(/&gt;/g, '>');
				}
	
			}
	
			
			for (var k = 0; k < $scope.xmlNewData.length; k++) {
				for (var i = 0; i < $scope.xmlNewData[k].hexdata.length; i += 2) {
					var v = parseInt($scope.xmlNewData[k].hexdata.substr(i, 2), 16);
					if (v)
						$scope.xmlNewData[k].convXml += String.fromCharCode(v);
				}
				$scope.xmlNewData[k].convXml = $scope.xmlNewData[k].convXml.replace(/&lt;/g, '<');
				$scope.xmlNewData[k].convXml = $scope.xmlNewData[k].convXml.replace(/&gt;/g, '>');
			}
	
			return $scope.xmlData,
			$scope.xmlNewData;
	
		}
		function splitData(OldData, NewData) {
			
			
			$scope.xmlData = [{
					'convXml': OldData
				}
			]
	
			$scope.xmlNewData = [{
					'convXml': NewData
				}
			]
	
		}
	
		for (k in $scope.xmlNewData) {
	
			$($scope.xmlNewData[k].convXml).children().each(function () {
				if ($(this).children().length) {
					$(this).children().each(function () {
						if ($(this).children().length) {
							$(this).children().each(function () {
	
								$scope.NewDataLabel.push(this.nodeName);
								$scope.ApprovalNewLabel.push(this.nodeName)
								$scope.ApprovalNewData.push($(this).text().trim())
								comp2 = $(this).text();
								getVal2();
							})
						} else {
	
							$scope.NewDataLabel.push(this.nodeName);
							$scope.ApprovalNewLabel.push(this.nodeName)
							$scope.ApprovalNewData.push($(this).text().trim())
	
							comp2 = $(this).text();
							getVal2();
						}
					})
				} else {
					if (this.nodeName.indexOf("_PK") == -1) {
						//console.log(this.nodeName.indexOf("_PK"))
						$scope.NewDataLabel.push(this.nodeName);
						$scope.ApprovalNewLabel.push(this.nodeName)
						$scope.ApprovalNewData.push($(this).text().trim())
						comp2 = $(this).text();
						getVal2();
					}
				}
	
			})
	
		}
	
		for (k in $scope.xmlData) {
	
			$($scope.xmlData[k].convXml).children().each(function () {
	
				if ($(this).children().length) {
					$(this).children().each(function () {
						if ($(this).children().length) {
							$(this).children().each(function () {
								$scope.ApprovalOldLabel.push(this.nodeName)
								$scope.ApprovalOldData.push($(this).text().trim())
								comp1 = $(this).text();
								getVal1();
							})
						} else {
	
							$scope.ApprovalOldLabel.push(this.nodeName)
							$scope.ApprovalOldData.push($(this).text().trim())
							comp1 = $(this).text();
							getVal1();
						}
					})
				} else {
					if (this.nodeName.indexOf("_PK") == -1) {
						//console.log(this.nodeName.indexOf("_PK"))
						$scope.ApprovalOldLabel.push(this.nodeName)
						$scope.ApprovalOldData.push($(this).text().trim())
						comp1 = $(this).text();
						getVal1();
					}
				}
	
			})
	
		}
	
		/*function arr_diff(a1, a2) {
	
			var a = [],
			diff = [];
	
			for (var i = 0; i < a1.length; i++) {
				a[a1[i]] = true;
			}
	
			for (var i = 0; i < a2.length; i++) {
				if (a[a2[i]]) {
					delete a[a2[i]];
				} else {
					a[a2[i]] = true;
				}
			}
	
			for (var k in a) {
				diff.push(k);
			}
	
			return diff;
		};*/
	
		var DiffArr = arr_diff($scope.ApprovalNewLabel, $scope.ApprovalOldLabel)
	
		function approvalDataDiff(Label, NData, OData, Diffs) {
			//alert(Label)
			var aDD = [];
			if (OData.length > 0) {
	
				for (i = 0; i < Diffs.length; i++) {
	
					for (j = 0; j < Label.length; j++) {
	
						var tempObj = {};
					   //console.log(Label[j])
						if (Label[j].indexOf("_PK") == -1) {
							//console.log(Label[j].indexOf("_PK"))
							if (aDD.length <= j) {
	
								if (Diffs[i] == Label[j] && OData[j] != NData[j] || OData[j] == undefined) {
									tempObj.Label = Label[j];
									tempObj.NewData = NData[j]
										tempObj.OldData = OData[j];
									tempObj.Difference = true;
									aDD.push(tempObj);
								} else {
									tempObj.Label = Label[j];
									tempObj.NewData = NData[j]
										tempObj.OldData = OData[j];
									tempObj.Difference = false;
									aDD.push(tempObj);
								}
							} else {
								if (Diffs[i] == Label[j] && OData[j] != NData[j] || OData[j] == undefined) {
									aDD[j].Difference = true;
								}
							}
						}
	
					}
				}
			} else {
				for (j = 0; j < Label.length; j++) {
					var tempObj = {};
					tempObj.Label = Label[j]
						tempObj.NewData = NData[j]
						tempObj.OldData = OData[j]
						tempObj.Difference = false;
					aDD.push(tempObj);
				}
			}
			return aDD;
		}
	
		if (nodeText1.length != 0 && nodeText2.length != 0) {
			jQuery.grep(nodeText2, function (el) {
				if (jQuery.inArray(el, nodeText1) == -1) {
					difference.push(el);
	
				}
			});
	
		}
	
		var DiffArrForEDITED = [];
		function findLabels(difference) {
			for (i = 0; i < difference.length; i++) {
	
				DiffArrForEDITED.push($scope.ApprovalNewLabel[$scope.ApprovalNewData.indexOf(difference[i])])
			}
	
			return DiffArrForEDITED;
		}
		findLabels(difference)
	
		if (DiffArrForEDITED.length > 0) {
			$scope.approvalDataDiffData = approvalDataDiff($scope.ApprovalNewLabel, $scope.ApprovalNewData, $scope.ApprovalOldData, DiffArrForEDITED);
		} else {
			$scope.approvalDataDiffData = approvalDataDiff($scope.ApprovalNewLabel, $scope.ApprovalNewData, $scope.ApprovalOldData, DiffArr);
		}
	
		function objectFindByKey(array, key, value) {
			for (var i = 0; i < array.length; i++) {
				if (array[i][key] === value) {
					return array[i];
				}
			}
			return null;
		}
	
		function RPRestuctureObj(OldDataArray_1, NewDataArray_1) {
			var FinalArray = [];
			
			OldDataArray = Array.isArray(OldDataArray_1) ? OldDataArray_1 : [OldDataArray_1];
			NewDataArray = Array.isArray(NewDataArray_1) ? NewDataArray_1 : [NewDataArray_1];
			for (i = 0; i < NewDataArray.length; i++) {
				var ObjTemp = {};
				ObjTemp.ResourceName = NewDataArray[i].ResourceName;
				if (objectFindByKey(OldDataArray, 'ResourceName', NewDataArray[i].ResourceName) != null) {
					// ObjTemp.OldPermission = OldDataArray[i].PermissionList;
					// console.log(Array.isArray(OldDataArray[i].PermissionList))
				ObjTemp.OldPermission = Array.isArray(OldDataArray[i].PermissionList)?OldDataArray[i].PermissionList : [OldDataArray[i].PermissionList];	
				}
				// ObjTemp.NewPermission = NewDataArray[i].PermissionList;
				// console.log(Array.isArray(NewDataArray[i].PermissionList))
				ObjTemp.NewPermission = Array.isArray(NewDataArray[i].PermissionList)?NewDataArray[i].PermissionList : [NewDataArray[i].PermissionList];	
				FinalArray.push(ObjTemp)
			}
			// console.log(FinalArray)
			return FinalArray;
		}
	
		if ($scope.data.TableName.toUpperCase() == 'ROLERESOURCEPERMISSION') {
			var x2js = new X2JS();
			$scope.RPApprovalOldData = x2js.xml_str2json($scope.xmlData[0].convXml);
			$scope.RPApprovalNewData = x2js.xml_str2json($scope.xmlNewData[0].convXml);
			$scope.RPApprovalFinalData = RPRestuctureObj($scope.RPApprovalOldData.GroupResourcePermissions.ResourceGroupPermissions, $scope.RPApprovalNewData.GroupResourcePermissions.ResourceGroupPermissions)
	
		}
	
		function hex2a(hexx) {
			var hex = hexx.toString();
			var str = '';
			for (var i = 0; i < hex.length; i += 2)
				str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
			return str;
		}
	
		function convertXml2JSon(xml) {
			var x2js = new X2JS();
			return x2js.xml_str2json(xml);
		}
	
		function getMainKeysFromJSON(jsonData) {
			for (var k in jsonData) {
				return k
			}
		}
	
		function getKeysFromJSON(jsonData) {
			var keys = [];
			for (var k in jsonData) {
				for (var i in jsonData[k]) {
					if (i.indexOf("_PK") == -1) {
						keys.push(i);
					}
				}
			}
			return keys;
		}

		//$scope.MOPdata = []
		function buildFields(argu,fieldset){
			$scope.fieldDetails.push({
				'FieldName' 			: ('name' in argu ? argu.name : ''),
				'Label'			:	('label' in argu.fieldGroup1.webformfieldgroup.webformfieldgroup_2 ? argu.fieldGroup1.webformfieldgroup.webformfieldgroup_2.label : ''),
			});

			//console.log($scope.fieldDetails)

		}



		var OldData = $scope.data.OldData;
		var NewData = $scope.data.NewData;
		var Oldarr = [];

		function getFinalData(ODJSON, NDJSON, finalLabels) {
			// console.log(ODJSON, NDJSON, finalLabels,"ODJSON, NDJSON, finalLabels")
	
			var approvalDataArray = [];
			if (Object.keys(ODJSON).length > 0) {
				for (i = 0; i < finalLabels.length; i++) {
					var tempObj = {};
						
						
						
					if ((ODJSON[finalLabels[i]]) != undefined) {
						//console.log(typeof ODJSON[finalLabels[i]],typeof NDJSON[finalLabels[i]])
						if(typeof ODJSON[finalLabels[i]] == "object" || typeof NDJSON[finalLabels[i]] == "object" )
						{
							//console.log(JSON.stringify(ODJSON[finalLabels[i]]),JSON.stringify(NDJSON[finalLabels[i]]));
							NDJSON[finalLabels[i]] = JSON.stringify(NDJSON[finalLabels[i]])
							ODJSON[finalLabels[i]] = JSON.stringify(ODJSON[finalLabels[i]])
						}
	
						if (NDJSON[finalLabels[i]] != ODJSON[finalLabels[i]]) {
						//console.log(NDJSON[finalLabels[i]],ODJSON[finalLabels[i]])
							tempObj.label = finalLabels[i];
							tempObj.Origlabel = finalLabels[i];
							tempObj.updatedData = NDJSON[finalLabels[i]];
							tempObj.currentData = ODJSON[finalLabels[i]];
							tempObj.diffence = true;
						} else {
							tempObj.label = finalLabels[i];
							tempObj.Origlabel = finalLabels[i];
							tempObj.updatedData = NDJSON[finalLabels[i]];
							tempObj.currentData = ODJSON[finalLabels[i]];
							tempObj.diffence = false;
						}
					} else {
						//console.log(NDJSON[finalLabels[i]],ODJSON[finalLabels[i]])
						tempObj.label = finalLabels[i];
						tempObj.Origlabel = finalLabels[i];
						
						tempObj.updatedData = NDJSON[finalLabels[i]];
						tempObj.currentData = "";
						tempObj.diffence = true;
					}
					approvalDataArray.push(tempObj);
				}
	
			} else {
				for (j = 0; j < finalLabels.length; j++) {
					var tempObj = {};
					tempObj.label = finalLabels[j];
					tempObj.Origlabel = finalLabels[j];
					tempObj.updatedData = NDJSON[finalLabels[j]];
					tempObj.currentData = ODJSON[finalLabels[j]];
					tempObj.diffence = false;
					approvalDataArray.push(tempObj);
	
				}
			}


			 $scope.bankDataArr.forEach(function(val){
				//console.log(val.Name, val.Link, $scope.data.TableName)
			 	if((val.Name == $scope.data.TableName) && (val.Link != 'methodofpayments'))
			 	{
			 		crudRequest("GET",val.Link+'/metainfo',"").then(function(response){
						var obtainedFields = response.data.data.Data.webformuiformat.fields.field;
						for(k in obtainedFields)
						{
							if("webformfieldgroup" in obtainedFields[k].fieldGroup1){

								$scope.fieldDetails.push({
									'FieldName' : ('name' in obtainedFields[k] ? obtainedFields[k].name : ''),
									'Label' : ('label' in obtainedFields[k].fieldGroup1.webformfieldgroup.webformfieldgroup_2 ? obtainedFields[k].fieldGroup1.webformfieldgroup.webformfieldgroup_2.label : '')
								})
							}
							else{
								var subSectionData = [];
								for(j in obtainedFields[k].fieldGroup1.webformsectiongroup.fields.field){
									subSectionData.push({ 
										'FieldName' : ('name' in obtainedFields[k].fieldGroup1.webformsectiongroup.fields.field[j] ? obtainedFields[k].fieldGroup1.webformsectiongroup.fields.field[j].name : ''),
										'Label' : ('label' in obtainedFields[k].fieldGroup1.webformsectiongroup.fields.field[j].fieldGroup1.webformfieldgroup.webformfieldgroup_2 ? obtainedFields[k].fieldGroup1.webformsectiongroup.fields.field[j].fieldGroup1.webformfieldgroup.webformfieldgroup_2.label : '')
									})

								}
								$scope.fieldDetails.push({	
									'Label' : ('sectionheader' in obtainedFields[k].fieldGroup1.webformsectiongroup ? obtainedFields[k].fieldGroup1.webformsectiongroup.sectionheader : ''),
									'FieldName' : ('name' in obtainedFields[k] ? obtainedFields[k].name : ''),
								})			
							}
						}
						
							$scope.fieldDetails.forEach(function(val){
								for(var j in approvalDataArray)
								{
									if($scope.data.TableName != 'BusinessRules')
									{
										if(val.FieldName == approvalDataArray[j].label)
										{
											approvalDataArray[j].Origlabel = angular.copy(approvalDataArray[j].label);
											approvalDataArray[j].label = val.Label;
											
										}
									}
									else if($scope.data.TableName == 'BusinessRules')
									{
										if(val.FieldName.toUpperCase() == approvalDataArray[j].label)
										{
											approvalDataArray[j].Origlabel = angular.copy(approvalDataArray[j].label);
											approvalDataArray[j].label = val.Label;
										}
									}
								}
							})

						// }
						// else if($scope.data.TableName == 'BusinessRules')
						// {
						// 	console.log("else")
						// }
						

						// console.log(val.Name,$scope.data.TableName,"busi")

						//console.log(approvalDataArray)
						return approvalDataArray;
						})
					}
					else if((val.Link == 'methodofpayments') && (val.Name == $scope.data.TableName))
					{
						crudRequest("GET",val.Link+'/metainfo',"").then(function(response){
							var fieldset = false;
							var FieldGroupval = 0;
							$scope.colspanArr = [];
							var obtainedFields = response.data.data.Data.webformuiformat.fields.field;
							for(k in obtainedFields){
								if(obtainedFields[k].name == "FieldGroup" || obtainedFields[k].name == "FieldGroupEnd"){
									fieldset = 'fieldGroup1' in obtainedFields[k] ? obtainedFields[k].fieldGroup1.webformsectiongroup : false;
									if(obtainedFields[k].name == "FieldGroup"){
										FieldGroupval = k;
									}
									if(obtainedFields[k].name == "FieldGroupEnd"){
										$scope.colspanArr.push(k - FieldGroupval -1)
									}
								}
								else{
									buildFields(obtainedFields[k],fieldset)
								}				
							}
							
							$scope.fieldDetails.forEach(function(val){
								for(var j in approvalDataArray)
								{
									if(val.FieldName == approvalDataArray[j].label)
									{
										approvalDataArray[j].label = val.Label
									}
								}
							})
							return approvalDataArray;

						})
					}
			 })

			return approvalDataArray;
		}
		var ruleObj = {
					"ruleOldData": [],
					"ruleNewData": []
	
			}
			
		var finalLabels=[];
		var finalLabelsOld=[];
		//console.log(ruleObj)
		function keyvalueForBR(XML,keysval)
		{
			//alert(keysval)
			$(XML).children().each(function () {  
			
				if ($(this).children().length) {
					$(this).children().each(function () {
						if ($(this).children().length) {
							$(this).children().each(function () {
								//console.log(this.nodeName,$(this).text().trim())
								var nodeNames = this.nodeName;
								var textValus = $(this).text().trim();
								//ruleObj[keysval].push({key:nodeNames,value:textValus})
								ruleObj[keysval][nodeNames] = textValus; 
							})
						} else {
								var nodeNames = this.nodeName;
								var textValus = $(this).text().trim();
								//ruleObj[keysval].push({key:nodeNames,value:textValus})
								ruleObj[keysval][nodeNames] = textValus; 
						}
					})
				} else {
					if (this.nodeName.indexOf("_PK") == -1) {
							var nodeNames = this.nodeName;
							var textValus = $(this).text().trim();
							//ruleObj[keysval].push({key:nodeNames,value:textValus})
							ruleObj[keysval][nodeNames] = textValus; 
	
					}
				}
		
			})
			return ruleObj;
		}
		
		
		function isEmptyObject(obj) {
						var name;
						for (name in obj) {
							return false;
						}
						return true;
					};
	
					 function jsondiff(obj1, obj2) {
						var result = {};
						var change;
						for (var key in obj1) {
							if (typeof obj2[key] == 'object' && typeof obj1[key] == 'object') {
								change = jsondiff(obj1[key], obj2[key]);
								if (isEmptyObject(change) === false) {
									result[key] = change;
								}
							}
							else if (obj2[key] != obj1[key]) {
								result[key] = obj2[key];
							}
						}
						//console.log(result)
						return result;
					};
			
		
	/*	function getFinalRuleData(ODJSON, NDJSON, finalLabels,finalLabelsOld) {
			console.log(ODJSON, NDJSON, finalLabels)
			var approvalDataArray = [];
			if (Object.keys(ODJSON).length > 0) {
				for (i = 0; i < finalLabels.length; i++) {
					var tempObj = {};
					//console.log(ODJSON,NDJSON)
					if ((ODJSON[i]) != undefined) {
						//console.log(NDJSON[i].value)
						if (NDJSON[i].value != ODJSON[i].value) {
							tempObj.label = finalLabels[i];
							tempObj.updatedData = NDJSON[i].value;
							tempObj.currentData = ODJSON[i].value;
							tempObj.diffence = true;
						} else {
							tempObj.label = finalLabels[i];
							tempObj.updatedData = NDJSON[i].value;
							tempObj.currentData = ODJSON[i].value;
							tempObj.diffence = false;
						}
					} else {
						tempObj.label = finalLabels[i];
						tempObj.updatedData = NDJSON[i].value;
						tempObj.currentData = "";
						tempObj.diffence = true;
					}
					approvalDataArray.push(tempObj); 
				}
	
			} else {
				for (j = 0; j < finalLabels.length; j++) {
					var tempObj = {};
					tempObj.label = finalLabels[j];
					tempObj.updatedData = NDJSON[j].value;
					tempObj.currentData = "";
					tempObj.diffence = false;
					approvalDataArray.push(tempObj);
	
				}  
			}
			console.log(approvalDataArray)
			return approvalDataArray;
		}
		
		*/
		
		// $scope.stateArr = ["CREATED","Instruction Uploaded","File Uploaded","Request For Information Generated","Response To Request For Information Generated","Return Of Funds Generated","Response To Return Of Funds Generated","Response To Request For Payment Generated","DuplicateInstructionAccepted","HoldInstructionAccepted","InstructionRejected","AcceptPayment","RejectPayment","DuplicateInstructionRejected","HoldInstructionRejected","Proprietary Acknowledgement Generated","PaymentWaitForApproval","InstructionWaitForApproval","Cancel Payment"];

		//if ($scope.data.State != 'CREATED' && ($scope.data.State != 'Instruction Uploaded' && $scope.data.State != 'File Uploaded' && $scope.data.State != 'Request For Information Generated' && $scope.data.State != 'Response To Request For Information Generated' && $scope.data.State != 'Return Of Funds Generated' && $scope.data.State != 'Response To Return Of Funds Generated' && $scope.data.State != 'Response To Request For Payment Generated' )) 
		
		function viewNewAppData()
		{
				var NewData = $scope.data.NewData;
				
				if($scope.data.TableName == 'BusinessRules')
				{
					NDXML = NewData;
					keyvalueForBR(NDXML,'ruleNewData');
					finalLabels = Object.keys(ruleObj['ruleNewData'])
					// console.log(finalLabels)
					$scope.checkOdataLength =Object.keys(ruleObj['ruleOldData']).length;	
					$scope.FinalDataArray = getFinalData(ruleObj['ruleOldData'], ruleObj['ruleNewData'], finalLabels)
					
					
				//	$('#QComment').text(databack.replace(/\r\n/g,EOL));
				
					
				}
				else
				{
				NDXML = hex2a(NewData);
				NDJSON = convertXml2JSon(NDXML);
				var mainKey = getMainKeysFromJSON(NDJSON);
				var finalLabels = getKeysFromJSON(NDJSON);
				$scope.FinalDataArray = getFinalData(Oldarr, NDJSON[mainKey], finalLabels)
				
				}
		}
		
		if($scope.data.OldData && $scope.data.NewData)	
		{
		// if($scope.stateArr.indexOf($scope.data.State) == -1)	
		// {
	
		var OldData = $scope.data.OldData;
		var NewData = $scope.data.NewData;
		
		if($scope.data.TableName == 'BusinessRules')
		{
			ODXML = OldData;
			NDXML = NewData;
			keyvalueForBR(ODXML,'ruleOldData')
			keyvalueForBR(NDXML,'ruleNewData')
			finalLabels = Object.keys(ruleObj['ruleNewData'])
			//console.log(finalLabels)
			$scope.checkOdataLength =Object.keys(ruleObj['ruleOldData']).length;	
			 $scope.FinalDataArray = getFinalData(ruleObj['ruleOldData'], ruleObj['ruleNewData'], finalLabels)	
		}
		else
		{
			ODXML = hex2a(OldData);
			NDXML = hex2a(NewData);
			ODJSON = convertXml2JSon(ODXML);
			NDJSON = convertXml2JSon(NDXML);
			//console.log(ODJSON)
			var finalLabels = getKeysFromJSON(NDJSON);
			var mainKey = getMainKeysFromJSON(NDJSON);
			var mainKey2 = getMainKeysFromJSON(ODJSON);
			if(mainKey != mainKey2)
			{
				$scope.checkOdataLength = 0;
			}
			else
			{

				$scope.checkOdataLength = Object.keys(ODJSON).length;
			}

			
			//console.log(ODJSON)
			var keydiff = jsondiff(ODJSON,NDJSON)
			//console.log(keydiff)
			for(i in keydiff)
			{
				for(j in keydiff[i])
				{
					//console.log(keydiff[i][j],j)
					if(keydiff[i][j] == undefined)
					{
						//alert(j)
						var index = Object.keys(ODJSON[i]).indexOf(j);
						//alert(index);
						//finalLabels[index] = j;
						finalLabels.splice(index, 0, j);
						
					}
					
					
				}
			}
			// console.log(mainKey,"mainKey",ODJSON[mainKey])

			
			if(ODJSON[mainKey] != undefined)
			{
				$scope.FinalDataArray = getFinalData(ODJSON[mainKey], NDJSON[mainKey], finalLabels) 

			}
			else
			{
				viewNewAppData();
			}
			
			
		}
		
		
		} else {

				viewNewAppData();
			
		}


			//if ( $scope.data.State == 'CREATED' || $scope.data.State == 'Instruction Uploaded' || $scope.data.State == 'File Uploaded' || $scope.data.State == 'Request For Information Generated' || $scope.data.State == 'Response To Request For Information Generated' || $scope.data.State == 'Return Of Funds Generated' || $scope.data.State == 'Response To Return Of Funds Generated' || $scope.data.State == 'Response To Request For Payment Generated') {

				// if($scope.stateArr.indexOf($scope.data.State) != -1)
				// {		
					
				//}
		
		var newObj = {
			"FDCParameters": {
				"oldData": [],
				"newData": []
	
			},
			"PDCParameters": {
				"oldData": [],
				"newData": []
			},
			"AdditionalConfig":{
				"oldData": [],
				"newData": []
			}
	
		}
		
		var key='';
		function iterate(obj, label, dType, key) {

			//console.log(obj,label,dType)

			for(var property in obj)
			{
				if(typeof(obj[property]) == 'object')
				{
					//console.log(property,obj[property],label, dType)
					//iterate(obj[property], label, dType, property)
					//key = property;
					// console.log(obj[property],"obj[property]")

					newObj[label][dType].push({
						'key': property,
						'value': JSON.stringify(obj[property]),
						'isobject':true
					})

				}
				else{
					//console.log(property,obj[property],dType)
					newObj[label][dType].push({
						'key': property,
						'value': obj[property],
						'isobject':false
					})
				}
				
			}

			// console.log(newObj,"iterate")
				
			/*for (var property in obj) {
				if (obj.hasOwnProperty(property)) {
					if (typeof obj[property] == "object") {
						
						
						iterate(obj[property], label, dType, property);
	
					} else {
						
						newObj[label][dType].push({
							'key': property,
							'value': obj[property],
							'parent':(prop)?prop:false
						})
					}
	
				}
			}
			
			
			if(label == 'AdditionalConfig')
			{
				console.log("aaaa",newObj)
			}
			
	
			return newObj;*/
		}
	
		//var ab;
		//var PdcFdcheader = [];
		$scope.sendXML = function (getval, label, dType) {
	
			
	
			var data = (dType == 'oldData')?getval['currentData']:(getval['updatedData'])?getval['updatedData']:'';
			
			

			if((data.indexOf('<') != -1) || (data.indexOf('{') != -1))
			{

				
				if(data)
				{
					if(data.indexOf('<') != -1)
						{
					
							$scope.FdcPdcparam = convertXml2JSon(data)
						}
						else{
		
					
							$scope.FdcPdcparam = JSON.parse(data);
							// console.log($scope.FdcPdcparam,"$scope.FdcPdcparam")
						}
		
						iterate($scope.FdcPdcparam, label, dType)
				}

			}
			else{
				$timeout(function(){
					$('#'+getval.Origlabel+dType).html(data)
				},100)
				
			}
			
	
			
			
				
		}
		
		
		setTimeout(function () {
	
			var keyArrObj = {
	
				"FDCParameters": {
					"oldData": [],
					"newData": []
	
				},
				"PDCParameters": {
					"oldData": [],
					"newData": []
				},
				"AdditionalConfig":{
					"oldData": [],
					"newData": []
				}
	
			}
	
			//var keyArr = [];
			var paramval = {
				"FDCParameters": {
					"oldData": [],
					"newData": [],
					"header": "FileDuplicateCheckConfig"
	
				},
				"PDCParameters": {
					"oldData": [],
					"newData": [],
					"header": "PaymentDuplicateCheckConfig"
				},
				"AdditionalConfig":{
					"oldData": [],
					"newData": [],
					"header": "Additional Config"
				}
			};
			var count = {
				"FDCParameters": {
					"oldData": [],
					"newData": []
	
				},
				"PDCParameters": {
					"oldData": [],
					"newData": []
				},
				"AdditionalConfig":{
					"oldData": [],
					"newData": []
				}
			};
			//var nn = [];
			var newNN = [{
					'label': '',
					'arr': []
				}
			]
	
			for (var i in newObj) {
				// console.log(newObj[i],"new[i]",i)
				for (var j in newObj[i]) {
					for (var k in newObj[i][j]) {
						
						keyArrObj[i][j].push(newObj[i][j][k].key)
	
					}
	
				}
			}
	
			for (var i in keyArrObj) {
				for (var j in keyArrObj[i]) {
					keyArrObj[i][j].forEach(function (k) {
						count[i][j][k] = (count[i][j][k] || 0) + 1;
	
					});
				}
			}
	
			
			//console.log("newN",newObj,"Count",count)
	
			for (var i in count) {
				//nn = [];
				newNN = [{
						'label': '',
						'arr': []
					}
				]
	
				for (var j in count[i]) {
					//nn = [];
					newNN = [{
							'label': '',
							'arr': []
						}
					]

					for (var k in newObj[i][j]) {
	
						for (var l in count[i][j]) {
	
							if ((l == newObj[i][j][k].key) && (count[i][j][l] == 1)) {
								//nn = [];
								newNN = [{
										'label': '',
										'header': '',
										'arr': []
									}
								]
								paramval[i][j].push({
									'key': l,
									'value': newObj[i][j][k].value,
									'isobject':newObj[i][j][k].isobject
								})
							} else if ((l == newObj[i][j][k].key) && (count[i][j][l] != 1)) {
								//nn.push(newObj[i][j][k].value)
								//console.log(l,i,j,k,newObj[i][j][k].key,count[i][j][l])
								newNN[0].label = l;
								newNN[0].arr.push(newObj[i][j][k].value)
							}
	
						}
					}
	
					for (var x in newNN) {
	// console.log(i,"paramheader")
						paramval[i][j].push({
							'key': newNN[x].label,
							'value': newNN[x].arr,
							
							
						})
	
					}
				}
	
			}
	
			
			var tD = "";
			var list = '';
			var inTable,inTd,prevLabel = '';
			var rowSpan = 0;
			var nPrev = '';
			

		//	console.log("param",paramval)
			//var tData = "<table class='table table-bordered'><tbody>" + tD + "</tbody></table>"
				for (var i in paramval) {
					tD = "";
					list = "";
					for (var j in paramval[i]) {
						tD = "";
						list = "";

						for (var k in paramval[i][j]) {
							
							if (paramval[i][j][k].value instanceof Array && !paramval[i][j][k].isobject) {
								for (var m = 0; m < paramval[i][j][k].value.length; m++) {
									list = list + "<li>" + paramval[i][j][k].value[m] + "</li>"
								}
								if (paramval[i][j][k].key) {
									tD = tD + "<tr><td>" + paramval[i][j][k].key + "</td>" + "<td><ul>" + list + "</ul></td></tr>"
	
								}
	
							} 
							else if(paramval[i][j][k].isobject)
							{


								// console.log(paramval,i,"paramval[i][j][k]")
								inTable='';
								inTd = '';
								prevLabel = '';
								rowSpan = '';
								var nObj = [];
								inTd = '';
								list = ''
								var iList = '';

								paramval[i][j][k].value = JSON.parse(paramval[i][j][k].value)
								for (var m = 0; m < paramval[i][j][k].value.length; m++) {
									
									for(var x in paramval[i][j][k].value[m])
									{
										// console.log(paramval[i][j][k].value[m],"paramval[i][j][k]")
										nObj.push({key:x,val:paramval[i][j][k].value[m][x]})
									
									}
								}

								if(i != 'AdditionalConfig' )
								{
										var res = nObj.reduce(function(res, currentValue) {
											if ( res.indexOf(currentValue.key) === -1 ) {
											res.push(currentValue.key);
											}
											return res;
										}, []).map(function(key) {
											return {
												key: key,
												val: nObj.filter(function(_el) {
												return _el.key === key;
											}).map(function(_el) { return _el.val; })
											}
										});

										
										
										
										var listFlag = false;
										for(var t in res)
										{
											for(var u in res[t].val)
											{
												if(res[t].val.length == 1)
												{
													listFlag = false;
													list = list + "<li class='tableRow'><span>" +res[t].key+"</span>"+ "<span class='tableCell'>"+res[t].val[u] + "</span></li>"	
												}
												else{
													listFlag = true;
													iList = iList + "<li>" +res[t].val[u] + "</li>"	

													
													
												}
												
											}

											if(listFlag)
											{
												list = list + "<li><b>" +res[t].key+"</b><ul class='innerlist'>"+iList+"</ul></li>"
											}
										}

										
										tD = tD + "<tr><td>" + paramval[i][j][k].key + "</td>" + "<td><ul>" + list + "</ul></td></tr>"

								}
								else
								{
									// console.log(nObj,"nobj[s]")
									for(s in nObj)
									{
										list = list + "<li class='tableRow "+(nObj[0].key == nObj[s].key ? 'borderCls' : '')+"'><span>" +nObj[s].key+"</span><span class='tableCell'>"+nObj[s].val + "</span></li>";

										// list = list + "<li class='tableRow' " + 'ng-class' +"="+(nObj[0].key == nObj[s].key ? 'cls1' : 'cls2'  )+" ><span>" +nObj[s].key+"</span>"+ "<span class='tableCell'>"+nObj[s].val + "</span></li>"
		
									}

									// for(v=1;v<nObj.length;v++)	style=''								
									// {
									// 	if(nObj[0].key == nObj[v].key)
									// 	{
									// 		console.log(v,nObj[v],"nObj[v]",$(".tableRow"))
									// 	}
									// }

									tD = tD + "<tr><td>" + paramval[i][j][k].key + "</td>" + "<td><ul>" + list + "</ul></td></tr>";

									

								}

								

								//tD = tD + "<tr><td>" + paramval[i][j][k].key + "</td>" + "<td>" + inTd + "</td></tr>"
								
								

							}
							else {
								tD = tD + "<tr><td>" + paramval[i][j][k].key + "</td>" + "<td>" + paramval[i][j][k].value + "</td></tr>"
							}
	
						}
						$('#' + i + j).html("<table class='table table-bordered dynamicborder'><tbody><tr><th colspan='2'>" + paramval[i].header + "</th></tr>" + tD + "</tbody></table>")
	
					}
	
				}


				$('.tableRow').parent().css({'display':'table','width':'100%'});


				if($(".dynamicborder").find("ul").find("li:first").hasClass("borderCls"))
				{
					$(".dynamicborder").find("ul").find("li:first").removeClass("borderCls")
				}
				// $('.borderCls').each(function(index,item){
				// 	if(parseInt(index)%2 != 0 && index != 0)
				// 	{
						
				// 		$($('.borderCls')[index+1]).css("background-color","rgb(242, 242, 242)");
				// 	}
				// });
				
	
		}, 100)
		
	
		$scope.is_hexadecimal = function(str)
		{
		regexp = /^[0-9a-fA-F]+$/;
		  
				if (regexp.test(str))
				  {
					return true;
				  }
				else
				  {
					return false;
				  }
		}

		$scope.atobConversion = function(data)
		{
			return atob(data)
		}
		
		
		
	
	})