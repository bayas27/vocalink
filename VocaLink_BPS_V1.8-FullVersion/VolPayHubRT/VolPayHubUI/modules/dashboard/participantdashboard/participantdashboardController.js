VolpayApp.controller('ParticipantController', function ($scope, $http, $filter, $timeout, $state, $translate, $location, $window, PersonService1, AllPaymentsGlobalData, GlobalService, LogoutService, DashboardService) {
  
  /*table keys*/
  $scope.tableFields={  
  "partictionKey":[
  {
    'FieldName' : "PEN",
    'Label'    : "PEN"
  },
  {
    'FieldName' : "USD",
    'Label'    : "USD"
  }
  ]
}

$scope.initial=function(){
  $http.get( BASEURL + '/rest/v2/bpsui/schedule').success(function (data, status) {
    $scope.BulkSchedule=data;
    $scope.getBulk($scope.BulkSchedule[0].actualvalue);
    $scope.selectedSchedule=$scope.BulkSchedule[0];
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
  $http.get( BASEURL + '/rest/v2/bpsui/session').success(function (data, status) {
    $scope.ChequeSession=data;
    $scope.getCheque($scope.ChequeSession[1].actualvalue);
    $scope.selectedSession=$scope.ChequeSession[1];
  }).error(function (data, status, headers, config) {
    alert(data.error.message)
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
}
$scope.initial();  
$scope.getBulk = function(n){
  $http.post( BASEURL + '/rest/v2/bpsui/dash/bulk',{"schemeNm":n,"dashboardType":"participant"}).success(function (data) {
    $scope.bulkData=data;
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
}


$scope.getCheque = function(session){
  $http.post( BASEURL + '/rest/v2/bpsui/dash/cheque',{"schemeNm":session,"dashboardType":"participant"}).success(function (data) {
    $scope.chequeData=data;
  }).error(function (data, status, headers, config) {

    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];

  });
}
/*print function*/
$scope.dashboardPrint = function () {
  $('[data-toggle="tooltip"]').tooltip('hide');
  window.print()
}



});