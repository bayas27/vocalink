VolpayApp.controller('systemdiagnosisCtrl', function ($scope, $state, bankData, GlobalService, $timeout,$location,$http,LogoutService) {
	
	$scope.permission = {
		'C' : false,
		'D'	: false,
		'R'	: false,
		'U'	: false
	}
	$scope.getDta = '';	
	$scope.alerts = []
if(sessionStorage.ROLE_ID)
{	
	
	$http.post(BASEURL+RESTCALL.ResourcePermission,{
		"RoleId": sessionStorage.ROLE_ID,
		"ResourceName": "System Diagnosis"
	}).then(function(response) {
        for(k in response){
			for(j in Object.keys($scope.permission)){
				if(Object.keys($scope.permission)[j] == response[k].ResourcePermission){
					$scope.permission[Object.keys($scope.permission)[j]] = true;
				}
			}
		}
    }, function(error) {
		console.log(error);
		$scope.alerts = [{
			type: 'danger',
			msg: error.data.error.message
		}]
    });
}           	
	$scope.dataFound = false;
	
	//I Load the initial set of datas onload
	$scope.initData = function(){
		$http.get(BASEURL+RESTCALL.SystemDiagnosisRead).then(function(response){
			$scope.getDta = response;
		},function(error){
			console.log(error);
			$scope.alerts = [{
				type: 'danger',
				msg: error.data.error.message
			}]
		})
	}		
	$scope.initData()
	function callAtTimeout() {
		$('.alert').hide();
	}
            	
            	
	/*** To Maintain Alert Box width, Size, Position according to the screen size and on scroll effect ***/

	$scope.widthOnScroll = function(){
		var mq = window.matchMedia( "(max-width: 991px)" );
		var headHeight
		if (mq.matches) {
		 headHeight =0;
		 $scope.alertWidth = $('.pageTitle').width();
		} else {
		   $scope.alertWidth = $('.pageTitle').width();
			headHeight = $('.main-header').outerHeight(true)+10;
		}
		$scope.alertStyle=headHeight;
	}
	$scope.widthOnScroll();

	/*** On window resize ***/
	$(window).resize(function(){
		$scope.$apply(function () {
			$scope.alertWidth = $('.alertWidthonResize').width();
		});

	});
});