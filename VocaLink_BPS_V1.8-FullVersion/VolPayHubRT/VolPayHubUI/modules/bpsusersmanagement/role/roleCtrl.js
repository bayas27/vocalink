VolpayApp.controller('rolesCtrl', function ($scope, $http, $filter, $timeout, $state, $translate, $location, $window, PersonService1, AllPaymentsGlobalData, GlobalService, LogoutService, DashboardService) {
  $scope.tableFields=[{
	"FieldName":"RoleName",
	"Label":"Role Name",
}]
$scope.request={"start":0,"count":20,"sorts":[]};
$scope.searchEmpty=false;
$scope.initial= function(req){
    if(req==undefined){
      var req=$scope.request;
    }
  $http.post( BASEURL + '/rest/v2/bpsui/role/readall',req).success(function (data, status) {
    if($scope.searchEmpty==true){
       delete $scope.userData;
       $scope.searchEmpty=false;  
     }
    if($scope.userData!=undefined){
      for(i=0;i<data.Details.length;i++){
        $scope.userData.push(data.Details[i]);    
      }
    }
    else{
        $scope.userData=data.Details;
    }
    $scope.len=data.count;
    $scope.itemsPerPage = 5;
    if($scope.currentPage){
      $scope.currentPage=$scope.currentPage+1;
    }
    else{
      $scope.currentPage = 0;    
    }
    
    $scope.items=$scope.userData;

  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });  
}
$scope.initial($scope.request);
 $http.get( BASEURL + '/rest/v2/bpsui/user/list/role').success(function (data, status) {
    $scope.roles=data;
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
  $scope.searchTitle = function(val){
      $scope.sugges=true;
      if(val==''){
        
        delete $scope.search;

      }
      else{
        $scope.value = $filter('filter')($scope.roles, val);
      }
  }
$scope.selectValue = function(val){
    $scope.search=val;
    $scope.sugges=false;
}
$scope.getmore = function(){
    $scope.request.start=$scope.request.start+20;
    $scope.request.count=$scope.request.count;
    if($scope.len >= $scope.request.start){
      $scope.initial($scope.request);  
    }
}
$scope.range = function() {
    if($scope.items.length <= $scope.itemsPerPage){
      var rangeSize =1;

    }
    else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
      var rangeSize =2;
    }
    else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
      var rangeSize =3;
    }
    else{
      var rangeSize =4;
    }

    var ret = [];
    var start;

    start = $scope.currentPage;
    if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
    }

    for (var i=start; i<start+rangeSize; i++) {
      ret.push(i);
    }
    return ret;
  };
  $scope.navigate = function(n){
    $scope.currentPage=n-1;
  }
  $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
      $scope.currentPage--;
    }

  };
  $scope.prevRange = function() {

    if ($scope.currentPage >= 4) {

      $scope.currentPage=$scope.currentPage-4;
    }

  };
  $scope.nextRange = function() {
    var len =$scope.items.length/$scope.itemsPerPage;
    if (len-$scope.currentPage > 4) {
      $scope.currentPage=$scope.currentPage+4;
    }

  };
  $scope.prevPageDisabled = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
  };

  $scope.pageCount = function() {
    return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
  };

  $scope.nextPage = function() {
    if ($scope.currentPage < $scope.pageCount()) {
      $scope.currentPage++;
    }

  };

  $scope.nextPageDisabled = function() {
    if($scope.currentPage === $scope.pageCount()){
      if($scope.request.start>=$scope.len){
        $scope.hide=false;  
      }
      else if($scope.len<$scope.request.count){
        $scope.hide=false;
      }
      else{
         $scope.hide=true; 
      }

    }
    else{
      $scope.hide=false; 
    }
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
  };
 $scope.gotoRole = function(name){
      $state.go('app.viewRole',{RoleName:name})
  }
   $scope.searchRole =  function(val){
     if(val==undefined || val==""){
       $scope.alerts = [{
      type: 'danger',
      msg:'Enter the search value'}
    ];
     }
     else{
       $scope.req={
  "filters": {
    "logicalOperator": "AND",
    "groupLvl1": [{
        "logicalOperator": "AND",
        "groupLvl2": [{
            "logicalOperator": "AND",
            "groupLvl3": [{
                "logicalOperator": "AND",
                "clauses": [{
                    "columnName": "RoleName",
                    "operator": "=",
                    "value": val
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }, 
    "sorts": [],
  "start": 0,
  "count": 20
}
$http.post( BASEURL + '/rest/v2/bpsui/role/search',$scope.req).success(function (data, status) {
    $scope.searchEmpty=true;
    $scope.userData=data.Details;
    $scope.itemsPerPage = 5;
    $scope.currentPage = 0;   
    $scope.items=data.Details;
    $scope.len=data.count;
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
     }
   
}
});