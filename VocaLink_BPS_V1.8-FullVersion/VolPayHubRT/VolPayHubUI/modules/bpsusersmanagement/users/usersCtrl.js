VolpayApp.controller('usersCtrl', function ($scope, $http, $filter, $timeout, $state, $translate, $location, $window, PersonService1, AllPaymentsGlobalData, GlobalService, LogoutService, DashboardService) {
$scope.tableFields=[{
	"FieldName":"FullName",
	"Label":"Full Name",
},
{
	"FieldName":"EmailAddress",
	"Label":"Email Address",
}]
$scope.request={"start":0,"count":20,"sorts":[]};
$scope.searchEmpty=false;
$scope.initial = function(val){
  if(req==undefined){
      var req=$scope.request;
    }
   $http.post( BASEURL + '/rest/v2/bpsui/user/readall',$scope.request).success(function (data, status) {
     
     if($scope.searchEmpty==true){
       delete $scope.userData;
       $scope.searchEmpty=false;  
     }
    if($scope.userData!=undefined){
      for(i=0;i<data.Details.length;i++){
        $scope.userData.push(data.Details[i]);    
      }
    }
    else{
        $scope.userData=data.Details;
    }
    $scope.userlen=data.count;
    $scope.itemsPerPage = 5;
    if($scope.currentPage){
      $scope.currentPage=$scope.currentPage+1;
    }
    else{
      $scope.currentPage = 0;    
    }
    $scope.items=$scope.userData;    
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  }); 
}
$scope.initial($scope.request);
$scope.getmore = function(){

    $scope.request.start=$scope.request.start+20;
    $scope.request.count=$scope.request.count;
    if($scope.userlen >= $scope.request.start){
      $scope.initial($scope.request);  
    }
} 
$scope.selectValue = function(val){
    $scope.search=val;
    $scope.sugges=false;
}
$scope.range = function() {
    if($scope.items.length <= $scope.itemsPerPage){
      var rangeSize =1;

    }
    else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
      var rangeSize =2;
    }
    else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
      var rangeSize =3;
    }
    else{
      var rangeSize =4;
    }

    var ret = [];
    var start;

    start = $scope.currentPage;
    if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
    }

    for (var i=start; i<start+rangeSize; i++) {
      ret.push(i);
    }
    return ret;
  };
  $scope.navigate = function(n){
    $scope.currentPage=n-1;
  }
  $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
      $scope.currentPage--;
    }

  };
  $scope.prevRange = function() {

    if ($scope.currentPage >= 4) {

      $scope.currentPage=$scope.currentPage-4;
    }

  };
  $scope.nextRange = function() {
    var len =$scope.items.length/$scope.itemsPerPage;
    if (len-$scope.currentPage > 4) {
      $scope.currentPage=$scope.currentPage+4;
    }

  };
  $scope.prevPageDisabled = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
  };
  $scope.prevRangeDisabled = function() {
    return $scope.currentPage < 4 ? "disabled" : "";
  };
  $scope.nextRangeDisabled = function() {
    var len =$scope.items.length/$scope.itemsPerPage;


    return len-$scope.currentPage < 4 ? "disabled" : "";
  };

  $scope.pageCount = function() {
    return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
  };

  $scope.nextPage = function() {
    if ($scope.currentPage < $scope.pageCount()) {
      $scope.currentPage++;
    }

  };

  $scope.nextPageDisabled = function() {
  if($scope.currentPage === $scope.pageCount()){
      if($scope.request.start>=$scope.userlen){
        $scope.hide=false;  
      }
      else if($scope.userlen<$scope.request.count){
        $scope.hide=false;
      }
      else{
         $scope.hide=true; 
      }

    }
    else{
      $scope.hide=false; 
    }
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
  };

  $scope.gotousers = function(userid){
  		
  		$state.go('app.bpsaddrole',{UserID:userid})
  }

    $scope.searchTitle = function(val){
      $scope.sugges=true;
      if(val==''){
        
        delete $scope.search;

      }
      else{
        $scope.value = $filter('filter')($scope.titles, val);
      }
  }
  
$http.get( BASEURL + '/rest/v2/bpsui/user/title').success(function (data, status) {
   $scope.titles=data;
  });
  $scope.searchUser =  function(val){
  if(val=='' || val==undefined){
    $scope.alerts = [{
      type: 'danger',
      msg:'Enter the search value'}
    ];
  }
  else{
    $scope.req={
  "filters": {
    "logicalOperator": "AND",
    "groupLvl1": [{
        "logicalOperator": "AND",
        "groupLvl2": [{
            "logicalOperator": "AND",
            "groupLvl3": [{
                "logicalOperator": "AND",
                "clauses": [{
                    "columnName": "FullName",
                    "operator": "=",
                    "value": val
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }, 
    "sorts": [],
  "start": 0,
  "count": 20
}

$http.post( BASEURL + '/rest/v2/bpsui/user/search',$scope.req).success(function (data, status) {
    $scope.searchEmpty=true;
    $scope.userData=data.Details;
    $scope.itemsPerPage = 5;
    $scope.currentPage = 0;   
    $scope.items=data.Details;
    $scope.userlen=data.count;
    
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
  }

  	
}

  

});