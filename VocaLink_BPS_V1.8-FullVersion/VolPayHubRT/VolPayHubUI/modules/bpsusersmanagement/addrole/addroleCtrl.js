VolpayApp.controller('addroleCtrl', function ($stateParams ,$scope, $http, $filter, $timeout, $state, $translate, $location, $window, PersonService1, AllPaymentsGlobalData, GlobalService, LogoutService, DashboardService) {

$scope.formfield=[{
	"Label":"First Name",
	"FieldName":"FirstName",
	"type":"text"
},
{
	"Label":"Sur Name",
	"FieldName":"SurName",
	"type":"text"
}
,{
	"Label":"User Role",
	"FieldName":"user_role",
	"type":"select"
},
{
	"Label":"Roles Association",
	"FieldName":"BPSUserRoleAssociation",
	"type":"multi"
}
]

 $http.post( BASEURL + '/rest/v2/bpsui/user/read',{"UserID":$stateParams.UserID}).success(function (data, status) {
    $scope.userdata=data;
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });

  $http.get( BASEURL + '/rest/v2/bpsui/user/list/role').success(function (data, status) {
    $scope.roles=data;    
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });

  $scope.addRole = function(val){
	if($scope.userdata.BPSUserRoleAssociation==undefined){
		$scope.userdata.BPSUserRoleAssociation=[];
		$scope.userdata.BPSUserRoleAssociation.push(val.RoleName);
	}
	else if($scope.userdata.BPSUserRoleAssociation.indexOf(val.RoleName)==-1){
		$scope.userdata.BPSUserRoleAssociation.push(val.RoleName)
	}
	else{
		$scope.alerts=[{
			"type":"danger",
			"msg":"Role already added"
		}]
	}
	
}
$scope.updateRoles = function(userData){
	delete userData.EmailAddress;
	delete userData.Status;
	$http.post( BASEURL + '/rest/v2/bpsui/user',userData).success(function (data, status) {
    	$scope.alerts = [{
      		type: 'success',
      		msg: data.responseMessage
    	}];
  	}).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
}
$scope.deleteRole  = function(index){
	$scope.userdata.BPSUserRoleAssociation.splice(index, 1);
}

$scope.cancel = function(){

	$state.go('app.bpsusers');
}
});