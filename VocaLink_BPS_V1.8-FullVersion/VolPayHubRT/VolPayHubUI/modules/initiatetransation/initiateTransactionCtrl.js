VolpayApp.controller('initiateTransactionCtrl', function ($scope, $http, $state, $location, $timeout, GlobalService, LogoutService, $filter, getMethodService, $translate, $rootScope) {

	$timeout(function () {
		$('.select2-container').removeClass('select2-container--open')
	}, 0)

	$rootScope.$on("MyEvent", function(evt,data){ 
			$("#changesLostModal").modal("show");
		})

	$scope.PaymentDetails = {};
	$scope.OrderingCustomer = {};
	$scope.BeneficiaryBank = {};
	$scope.Beneficiary = {};
	$scope.RemittanceInformation = {};
	$scope.IntermediaryBankDetails123 = []
	$scope.TemplateDetails = {};
	$scope.PartySelectedFlag = false;
	$scope.ProductsSupported123 = "";
	$scope.ServiceNotAvailable = "";
	$scope.PSANotAvailable = "";
	$scope.SaveTemplate = false;
	$scope.Templateloading = false;
	$scope.RemitInfoMaxLength = 140;
	$scope.DescriptionMaxLength = 3000;
	
	$scope.CXCFlag=false;

	$scope.tiggerTemplate = function () {
		$rootScope.dataModified = false;
		$scope.SaveTemplate = !$scope.SaveTemplate;
		$scope.SaveTemplateCollapsed = false;
		setTimeout(function () {
			$scope.remoteDataConfig21()
		}, 100)
		
		setTimeout(function () {
			$('.DatePicker').datepicker({
				format : "yyyy-mm-dd",
				showClear : true,
				autoclose : true,
				startDate : new Date()

			})
			$('.input-group-addon').on('click focus', function (e) {
				$(this).prev().focus().click()
			});
		}, 1000)
		
	}

	$(document).ready(function () {

		$scope.select2Arr = [{
				"name" : "party",
				"key" : "PartyName",
				"url" : "/rest/v2/parties/readall",
				"method" : "POST"
			}
		]
		//remoteDataConfig('AllAccountNumber','AccountName', '/rest/v2/accounts/readall', 'POST')

		$scope.currentSelect2 = {};

		$scope.limit = 500;
		var ddddd = {
			"start" : 0,
			"count" : 500
		};
		var newObj = JSON.stringify(ddddd);

		$scope.querySearchContructor = function (key, value123, start, count) {
			//////console.log(key, value123,start, count)
			$scope.query = {
				"Queryfield" : [{
						"ColumnName" : key,
						"ColumnOperation" : "like",
						"ColumnValue" : value123
					},{
							"ColumnName" : "Status",
							"ColumnOperation" : "=",
							"ColumnValue" : "ACTIVE"
						}
				],
				"start" : start * count ? start * count : 0,
				"count" : count
			}
			if (value123 != '') {
				$scope.query = constructQuery($scope.query);
			} else {
				$scope.query = {
					"start" : 0,
					"count" : count,
					"Queryfield" : [{
							"ColumnName" : "Status",
							"ColumnOperation" : "=",
							"ColumnValue" : "ACTIVE"
						}
					]
				};
				$scope.query = constructQuery($scope.query);
			}
			
			return $scope.query;
		}

		var remoteDataConfig = function (ID, key, RESTCALL, METHOD) {

			$('.appendselect2').select2({
				ajax : {
					url : function () {
						//////console.log($(this).attr('name'))

						for (var i in $scope.select2Arr) {
							if ($scope.select2Arr[i].name == $(this).attr('name')) {
								$scope.currentSelect2 = $scope.select2Arr[i];
								return BASEURL + $scope.select2Arr[i].url
							}
						}
					},
					method : "POST",
					headers : {
						"Authorization" : "SessionToken:" + sessionStorage.SessionToken,
						"source-indicator" : configData.SourceIndicator,
						"Content-Type" : "application/json"
					},
					dataType : 'json',
					delay : 250,
					xhrFields : {
						withCredentials : true
					},
					beforeSend : function (xhr) {
						xhr.setRequestHeader('Cookie', document.cookie),
						xhr.withCrendentials = true
					},
					crossDomain : true,
					data : function (params) {

						var query = $scope.querySearchContructor('', '', params.page, $scope.limit);

						if (params.term) {
							query = $scope.querySearchContructor($scope.currentSelect2.key, params.term, params.page, $scope.limit);
						}

						//////console.log("query", query, params)
						return JSON.stringify(query);
					},
					processResults : function (data, params) {

						//////console.log(this,$scope.currentSelect2)
						params.page = params.page ? params.page : 0;
						var myarr = []
						//////console.log(data)
						for (j in data) {

							if (data[j][$scope.currentSelect2.key] == undefined) {
								myarr.push({
									'id' : JSON.stringify(data[j]),
									'text' : data[j].PartyCode
								})
							} else {
								if ($scope.currentSelect2.key == 'PartyName') {
									myarr.push({
										'id' : JSON.stringify(data[j]),
										'text' : data[j][$scope.currentSelect2.key]
										//'text' : data[j][$scope.currentSelect2.key]+ "  - "+data[j].PartyCode
									})
								} else {
									myarr.push({
										'id' : JSON.stringify(data[j]),
										'text' : data[j][$scope.currentSelect2.key]
										//'text' : data[j][$scope.currentSelect2.key]+ "  - "+data[j].PartyCode
									})
								}
							}
						}
						return {
							results : myarr,
							pagination : {
								more : data.length >= $scope.limit
							}
						};
					},
					cache : true
				},
				placeholder : '--Select--',
				minimumInputLength : 0,
				allowClear : true
			}).on('select2:select', function () {

				//////console.log(this,$(this).val())

				if ($(this).attr('name') == 'templates') {
					$scope.tempVal = JSON.parse($(this).val());
					$scope.tempVal.Template = $filter('hex2a')($scope.tempVal.Template)
						$scope.tempVal.Template = JSON.parse($scope.tempVal.Template)
						$scope.tempQuery = $scope.querySearchContructor("PartyCode", $scope.tempVal.Template.Party, 0, 500);

					$http.post(BASEURL + '/rest/v2/parties/readall', $scope.tempQuery).success(function (data) {

						$scope.partyOptions = data;
						setTimeout(function () {
							delete data[0].$$hashKey;
							$scope.party = JSON.stringify(data[0]);
							$('select[name=party]').val($scope.party)
						}, 100)
					})

				}
			})
		}
		remoteDataConfig()
		var pageLimitCount = 100;
		var remoteDataConfig2 = function () {

			$('.appendselect212').select2({
				ajax : {
					url : function(params){
						// var query = "?start=" + (params.page * pageLimitCount ? params.page * pageLimitCount : 0) +"&count=" + pageLimitCount;
						// if (params.term) {
						// 	query = "?search="+ params.term + "&start=" + (params.page * pageLimitCount ? params.page * pageLimitCount : 0) +"&count=" + pageLimitCount;
						// }
						return BASEURL + '/rest/v2/manualpaymentinitiationtemplate/parties/rolesaccessible/readall';
					},
					method : "GET",
					headers : {
						"Authorization" : "SessionToken:" + sessionStorage.SessionToken,
						"source-indicator" : configData.SourceIndicator,
						"Content-Type" : "application/json"
					},
					dataType : 'json',
					delay : 250,
					xhrFields : {
						withCredentials : true
					},
					beforeSend : function (xhr) {
						xhr.setRequestHeader('Cookie', document.cookie),
						xhr.withCrendentials = true
					},
					crossDomain : true,
					processResults : function (data, params) {
						var myarr = []
						for (j in data) {

							myarr.push({
								'id' : JSON.stringify(data[j]),
								'text' : data[j].TemplateName + '(' + data[j].PartyCode + ')'
							})
						}
						return {
							results : myarr,
						};
					},
					cache : true
				},
				placeholder : '--Select--',
				minimumInputLength : 0,
				allowClear : true
			}).on('select2:select', function () {

				/* if ($(this).attr('name') == 'templates') {
					console.log($(this).val())
					$scope.tempVal = JSON.parse($(this).val());					
					$scope.tempVal.Template = $scope.tempVal.actualvalue
					//	$scope.tempQuery = $scope.querySearchContructor("PartyCode", $scope.tempVal.Template.Party, 0, 500);

				} */

			})

		}
		remoteDataConfig2()

		$scope.ROLESOptions123 = [];
		$scope.remoteDataConfig21 = function () {

			////console.log('this',this)
			$('.appendselect21211').select2({
				ajax : {
					url : BASEURL + '/rest/v2/roles/readall',
					method : "POST",
					headers : {
						"Authorization" : "SessionToken:" + sessionStorage.SessionToken,
						"source-indicator" : configData.SourceIndicator,
						"Content-Type" : "application/json"
					},
					dataType : 'json',
					data : function (params) {

						////console.log(params)
						var query = $scope.querySearchContructor('', '', params.page, $scope.limit);

						if (params.term) {
							query = $scope.querySearchContructor("RoleName", params.term, params.page, $scope.limit);
							////console.log("query", query, params)
						}

						////console.log("query", query, params)
						return JSON.stringify(query);
					},
					delay : 250,
					xhrFields : {
						withCredentials : true
					},
					beforeSend : function (xhr) {
						xhr.setRequestHeader('Cookie', document.cookie),
						xhr.withCrendentials = true
					},
					crossDomain : true,
					processResults : function (data, params) {
						////console.log(data)

						params.page = params.page ? params.page : 0;
						var myarr = []
						for (j in data) {

							myarr.push({
								'id' : data[j].RoleID,
								'text' : data[j].RoleName
							})
						}
						//////console.log(data)

						//////console.log(myarr)
						return {
							results : myarr,
							pagination : {
								more : data.length >= $scope.limit
							}
						};
					},
					cache : true
				},
				placeholder : '--Select--',
				minimumInputLength : 0,
				allowClear : true
			}).on('select2:select', function () {})

		}
		$scope.remoteDataConfig21()

	})

	var today = new Date();
	var month = '';
	if ((today.getMonth() + 1) <= 9) {
		month = '0' + (today.getMonth() + 1);
	} else {
		month = today.getMonth() + 1;
	}
	var date = todayDate();
	$scope.date = todayDate();

	$scope.getServiceList = function (party, flag) {

		$scope.dropdownPartyValue = party;
		////console.log(party)
		////console.log(flag)
		$rootScope.dataModified = true;
		if ($scope.PartySelectedFlag == false) {
			$scope.query = {
				"Queryfield" : [{
						"ColumnName" : "PartyCode",
						"ColumnOperation" : "=",
						"ColumnValue" : JSON.parse(party).PartyCode
					},{
							"ColumnName" : "Status",
							"ColumnOperation" : "=",
							"ColumnValue" : "ACTIVE"
						}
				],
				"start" : 0,
				"count" : 1000
			}

			$scope.query = constructQuery($scope.query);

			$http({
				url : BASEURL + '/rest/v2/partyserviceassociations/readall',
				method : "POST",
				data : $scope.query,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				if (data.length >= 1) {
					$scope.query_Service = {
						"Queryfield" : [],
						"start" : 0,
						"count" : 1000
					}
					

					if (data.length > 1) {
						for (i = 0; i < data.length; i++) {
							$scope.query_Service.Queryfield.push({
								"ColumnName" : "ServiceCode",
								"ColumnOperation" : "=",
								"ColumnValue" : data[i].ServiceCode
							})
						}
						$scope.query_Service.Queryfield.push({"ColumnName":"Status","ColumnOperation":"=","ColumnValue":"ACTIVE"})
					} else {
						$scope.query_Service.Queryfield.push({
							"ColumnName" : "ServiceCode",
							"ColumnOperation" : "=",
							"ColumnValue" : data[0].ServiceCode
						})
						$scope.query_Service.Queryfield.push({"ColumnName":"Status","ColumnOperation":"=","ColumnValue":"ACTIVE"})
					}

					$scope.query_for_serive = constructQuery($scope.query_Service);
					$scope.service123 = "";
					$http({
						url : BASEURL + '/rest/v2/services/readall',
						method : "POST",
						data : $scope.query_for_serive,
						headers : {
							'Content-Type' : 'application/json'
						}
					}).success(function (data, status, headers, config) {

						////console.log(data)
						if (data.length > 1) {
							//$scope.service11 = "";
							//$scope.serviceIsNotSingle = false;
							//$scope.service11 = data[0].ServiceCode;
							//$scope.service11 = '';
						} else if (data.length == 1) {

							$scope.service11 = JSON.stringify(data[0]);
							$scope.service123 = data[0];
							$scope.serviceIsNotSingle = false;
							//////console.log(party)
							//////console.log($scope.service11)
							//$scope.serviceCodeFromTemplate = false;
							$scope.getMessageType(party, JSON.stringify(data[0]))
						}

						$scope.serviceOptions = data;
						$timeout(function () {
							$('select[name=service11]').select2()
						}, 500)

					}).error(function (data, status, headers, config) {});

				} else {
					$scope.ServiceAvailabilty = false;
					$scope.ServiceNotAvailable = JSON.parse(party).PartyName;
				}
			}).error(function (data, status, headers, config) {});

			$scope.PartySelectedFlag = true;

		} else {
			
			var txt;
			var r = confirm("Please note: currently selected values may be lost.");
			if (r == true) {
				$rootScope.dataModified = false;
				$state.reload()
			} else {}

		}
		$scope.PaymentDetails.ValueDate = date;
		// console.log($scope.PaymentDetails,"$scope.PaymentDetails",$scope.service11,$scope.party)
		
	}

	$scope.alreadyExists = false;
	$scope.NewPartyRestCall = function(templateValues){

		if($scope.dropdownPartyValue){
			$http({
				url : BASEURL + '/rest/v2/manualpaymentinitiationtemplate/party/'+templateValues+'/'+JSON.parse($scope.dropdownPartyValue).PartyCode,
				method : "GET",
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				$scope.alreadyExists = false;
			}).error(function (data, status, headers, config) {
				$scope.alreadyExists = true;
				$scope.alertMessage =  data.error.message;
			});
		}
	}








	setTimeout(function(){
		$("input[type='text']").on("keydown",function(e) {
			
			if($(e.currentTarget).val())
			{
				$rootScope.dataModified = true;
			}
		});
		$("select").change( function(){ 
			if( $(this).val())
			{
				$rootScope.dataModified = true;
				$scope.madeChanges = $rootScope.dataModified;
			}
		})

		$(".DatePicker").on("change",function(){
				var selected = $(this).val();
				if(selected)
				{
					$rootScope.dataModified = true;
					$scope.madeChanges = $rootScope.dataModified;
				}
  		  });

	},100)

		$scope.gotoClickedPage = function()
		{
			$rootScope.$emit("MyEvent2",true);
		}


	$scope.getUniqueCurrency = function (AllCurrency) {
		var Currencies123 = [];
		for (i = 0; i < AllCurrency.length; i++) {
			Currencies123.push(AllCurrency[i].actualvalue);
		}
		return uniques(Currencies123);
	}

	function uniques(arr) {
		var a = [];
		for (var i = 0, l = arr.length; i < l; i++)
			if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
				a.push(arr[i]);
		return a.sort();
	}

	$scope.getMessageType = function (party, service) {
		//console.log(party,service)
		$scope.CXCFlag=false;
		var Service = JSON.parse(service).ServiceCode;
		console.log(Service);
		if (Service == 'RPX') {
			$scope.RemitInfoMaxLength = 35 * 4;

			$scope.MessageType = [{
					"actualValue" : "Customer Credit Transfer",
					"displayValue" : "Customer Credit Transfer"
				}, {
					"actualValue" : "Request for Payment",
					"displayValue" : "Request for Payment"
				}
			];

		} else if ((Service == 'GVP') || (Service == 'GLV')) {

			$scope.MessageType = [{
					"actualValue" : "Customer Credit Transfer",
					"displayValue" : "Customer Credit Transfer"
				}

			];
			$scope.PaymentDetails.MessageType = "Customer Credit Transfer";
			$scope.RemitInfoMaxLength = 35 * 10;

			//$scope.getPaymentType11 = function (mtype, Service, party)

			$scope.getPaymentType11("Customer Credit Transfer", service, party)

		} else if ((Service == 'CXC')) {

			$scope.MessageType = [{
					"actualValue" : "Customer Credit Transfer-Standard",
					"displayValue" : "Customer Credit Transfer Standard"
				}, {
					"actualValue" : "Customer Credit Transfer-Expedited",
					"displayValue" : "Customer Credit Transfer Expedited"
				}

			];
			$scope.RemitInfoMaxLength = 200;
			$scope.CXCFlag=true;

		} else {
			$http.get(BASEURL + '/rest/v2/messagetypes/readall?start=0&count=1000').success(function (data, status, headers, config) {
				////console.log(data)
				$scope.MessageType = data;
			}).error(function (data, status, headers, config) {
				//////console.log(data)
			});

			$http.get(BASEURL + '/rest/v2/debtorcustomer/code?start=0&count=1000').success(function (data, status, headers, config) {
				//////console.log(data)
				$scope.DebtorCustomerProprietaryCode = data;
			}).error(function (data, status, headers, config) {
				//////console.log(data)
			});

		}
		$timeout(function () {
			$('select[name=MessageType]').select2()
		}, 500)

		if (JSON.parse(service).InstructionCurrencies == 'ALL') {
			//$scope.forUSRTP.PaymentCurrency=getCSVtoArray(JSON.parse(service).InstructionCurrencies);
			$scope.CurrencyAll = false;
			$http.get(BASEURL + '/rest/v2/currencies/code?start=0&count=1000').success(function (data, status, headers, config) {
				//////console.log(data)
				if (JSON.parse($scope.getUniqueCurrency(data)).length == 1) {
					$scope.PaymentDetails.PaymentCurrency = JSON.parse($scope.getUniqueCurrency(data))[0];
				}
				$scope.PaymentCurrency = $scope.getUniqueCurrency(data);
			}).error(function (data, status, headers, config) {
				//////console.log(data)

			});
		} else {
			if (getCSVtoArray(JSON.parse(service).InstructionCurrencies).length == 1) {
				//////console.log(getCSVtoArray(JSON.parse(service).InstructionCurrencies).length)
				$scope.PaymentDetails.PaymentCurrency = getCSVtoArray(JSON.parse(service).InstructionCurrencies)[0];
			}
			$scope.PaymentCurrency = getCSVtoArray(JSON.parse(service).InstructionCurrencies);

		}

	}

	$scope.getPaymentType11 = function (mtype, Service, party) {

		//console.log(mtype)
		//console.log(Service)
		//console.log(party)
		$scope.psaCode11 = '';
		$scope.selectOptions = [];
		$scope.branchList='';
		$http({
			url : BASEURL + '/rest/v2/partyserviceassociations/initiatetransaction/querypsa',
			method : "POST",
			data : {
				"PartyCode" : JSON.parse(party).PartyCode,
				"ServiceCode" : JSON.parse(Service).ServiceCode,
				"MessageInput" : mtype
			},
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			//console.log(data)
			//////console.log(data.PartyServiceAssociationCode)
			$scope.selectOptions = data;
			
			/* if(data.length == 1 ){
			$scope.getPaymentDetailsByPSA(data[0])
			} */

			if (data.length == 1) {
				$scope.psaCode11 = JSON.stringify(data[0]);
				//$scope.getPaymentDetailsByPSA($scope.psaCode11)
				//////console.log("came")
				$scope.getPaymentDetailsByPSA(data[0])
			} else {
				//$scope.psaCode11 = JSON.stringify(data[0]);
				/* $scope.psaCode11 = '';
				////console.log($scope.psaCode11)
				$scope.psaCode11 = data.PartyServiceAssociationCode;
				////console.log($scope.psaCode11) */

			}
			$scope.PSANotAvailable='';
			$('select[name=psaCode]').select2()

		}).error(function (data, status, headers, config) {
			////console.log(data)
			$scope.PSANotAvailable = data.error.message;
			$scope.PaymentBranch = '';
			//$scope.branchList = '';
		});

		////console.log("mtype", mtype)
		if (mtype == 'Customer Credit Transfer') {
			$scope.DebtorCustomerProprietaryCode = [{
					"actualValue" : "Consumer",
					"displayValue" : "Consumer"
				}, {
					"actualValue" : "FAConsumer",
					"displayValue" : "FAConsumer"
				}, {
					"actualValue" : "Business",
					"displayValue" : "Business"
				}, {
					"actualValue" : "FABusiness",
					"displayValue" : "FABusiness"
				}
			];
		}

		if (mtype == 'Request for Payment') {
			$scope.DebtorCustomerProprietaryCode = [{
					"actualValue" : "Consumer",
					"displayValue" : "Consumer"
				}, {
					"actualValue" : "Business",
					"displayValue" : "Business"
				}
			];
		}
		$('select[name=DebtorCustomerProprietaryCode]').select2()
		$scope.getPaymentType(mtype)
	}

	$scope.getPaymentType = function (MessageType) {
		$http.get(BASEURL + '/rest/v2/paymenttype/' + encodeURI(MessageType) + '?start=0&count=1000').success(function (data, status, headers, config) {
			////console.log(data)
			$scope.PaymentTypePushPull = data;
		}).error(function (data, status, headers, config) {
			//////console.log(data)
		});
	}

	function getCSVtoArray(val) {

		var gSPArray = [];
		val1 = val.split(',');
		for (i = 0; i < val1.length; i++) {
			if (val1[i].trim().length > 0) {
				gSPArray.push(val1[i]);
			}
		}
		//////console.log(gSPArray)
		/* if(gSPArray.length==1){
		$scope.ProductsSupported123=gSPArray[0];
		} */
		return gSPArray;
	}

	function getSupportedProducts(val) {

		var gSPArray = [];
		val1 = val.split(',');
		for (i = 0; i < val1.length; i++) {
			if (val1[i].trim().length > 0) {
				gSPArray.push(val1[i]);
			}
		}
		////console.log(gSPArray)
		// console.log($scope.party,JSON.parse($scope.party).Country,"Shruthiiiiiiiii")
		//$scope.OrderingCustomer = JSON.parse($scope.party);
		if (gSPArray.length == 1) {
			$scope.ProductsSupported123 = gSPArray[0];
			
			$scope.getPaymentTypeDetails($scope.ProductsSupported123, $scope.party, JSON.parse($scope.service11).ServiceCode)
		}
		$('select[name=productsupported]').select2()
		return gSPArray;
	}

	$scope.ignoreEmptyValue = function (Arr) {
		var CCValue = [];
		for (i = 0; i < Arr.length; i++) {
			if (Arr[i].SupportedChargeCodes != undefined) {
				CCValue.push(Arr[i])
			}
		}
		if (CCValue.length >= 1) {
			return CCValue;
		} else {
			CCValue = '';
			return CCValue;
		}
	}

	$scope.getPaymentDetailsByPSA = function (PSACODE) {

		$scope.PaymentBranch = '';
		$scope.branchList = '';
		$scope.ProductsSupported='';
		if($scope.Templateloading==false){			
			$scope.ProductsSupported123='';
		}
		if (PSACODE != undefined) {
			$scope.PaymentBranch = '';
			//////console.log(PSACODE)

			PSACODE = (typeof(PSACODE) == 'string') ? JSON.parse(PSACODE) : PSACODE
			////console.log(PSACODE)
			$scope.PSAvalue = PSACODE;
			// var inputObj={}
			//inputObj.PartyCode=JSON.parse(PSACODE).PartyCode;
			//////console.log(PSACODE)

			//////console.log(JSON.parse(PSACODE))

			$scope.ProductsSupported = getSupportedProducts(PSACODE.ProductsSupported);

			//////console.log($scope.ProductsSupported)

			var inputObj = '';
			$scope.branchList = '';
			if (PSACODE.DeriveBranchCode == false) {

				inputObj = {
					"filters" : {
						"logicalOperator" : "AND",
						"groupLvl1" : [{
								"logicalOperator" : "AND",
								"groupLvl2" : [{
										"logicalOperator" : "AND",
										"groupLvl3" : [{
												"logicalOperator" : "AND",
												"clauses" : [{
														"columnName" : "BranchCode",
														"operator" : "=",
														"value" : PSACODE.BranchCode
													},{"columnName":"Status","operator":"=","value":"ACTIVE"}
												]
											}
										]
									}
								]
							}
						]
					},
					"start" : 0,
					"count" : 1000
				}

				//////console.log(inputObj);
				$http({
					url : BASEURL + '/rest/v2/branches/readall',
					method : "POST",
					data : inputObj,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function (data, status, headers, config) {
					////////console.log(data)
					$scope.branchList = data;
					if (data.length == 1) {
						$scope.PaymentBranch = JSON.stringify(data[0]);
					}
					if (data.length > 0) {
						$scope.branchList = data;
					} else {}
					$('select[name=Branch]').select2()
				}).error(function (data, status, headers, config) {
					////////console.log(data)
					if (err.status == 401) {
						if (configData.Authorization == 'External') {
							window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
						} else {
							LogoutService.Logout();
						}
					} else {
						$scope.alerts = [{
								type : 'danger',
								msg : data.error.message
							}
						];
					}
				});

			} else {
				//inputObj = {};

				inputObj = {
					"ServiceCode" : PSACODE.ServiceCode
				}

				$http({
					url : BASEURL + '/rest/v2/services/read',
					method : "POST",
					data : inputObj,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function (data, status, headers, config) {
					//////console.log(data)

					$scope.multipleOfferedEntity = getCSVtoArray(data.OfferedByEntity)
						//	alert($scope.multipleOfferedEntity.length)


						$scope.query_Service_forBranch = {
						"Queryfield" : [{"ColumnName":"Status","ColumnOperation":"=","ColumnValue":"ACTIVE"}],
						"start" : 0,
						"count" : 1000
					}

					if ($scope.multipleOfferedEntity.length > 1) {
						for (i = 0; i < $scope.multipleOfferedEntity.length; i++) {
							$scope.query_Service_forBranch.Queryfield.push({
								"ColumnName" : "BranchCode",
								"ColumnOperation" : "=",
								"ColumnValue" : $scope.multipleOfferedEntity[i]
							})
						}
					} else {
						$scope.query_Service_forBranch.Queryfield.push({
							"ColumnName" : "BranchCode",
							"ColumnOperation" : "=",
							"ColumnValue" : $scope.multipleOfferedEntity[0]
						})
					}

					$scope.query_Service_forBranch = constructQuery($scope.query_Service_forBranch);

					//////console.log(inputObj);
					$http({
						url : BASEURL + '/rest/v2/branches/readall',
						method : "POST",
						data : $scope.query_Service_forBranch,
						headers : {
							'Content-Type' : 'application/json'
						}
					}).success(function (data, status, headers, config) {
						////////console.log(data)
						if (data.length == 1) {
							$scope.PaymentBranch = JSON.stringify(data[0]);
						}
						if (data.length > 0) {
							$scope.branchList = data;
						} else {}
					}).error(function (data, status, headers, config) {
						////////console.log(data)
						if (err.status == 401) {
							if (configData.Authorization == 'External') {
								window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
							} else {
								LogoutService.Logout();
							}
						} else {
							$scope.alerts = [{
									type : 'danger',
									msg : data.error.message
								}
							];
						}
					});

				}).error(function (data, status, headers, config) {
					////////console.log(data)
					if (err.status == 401) {
						if (configData.Authorization == 'External') {
							window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
						} else {
							LogoutService.Logout();
						}
					} else {
						$scope.alerts = [{
								type : 'danger',
								msg : data.error.message
							}
						];
					}
				});

			}

		}
		
		//console.log($scope.PaymentBranch)
		
	}

	$scope.getPaymentTypeDetails = function (ProductsSupported, Party, Service) {

		//console.log(ProductsSupported)
		//console.log(Party)
		//console.log(Service)
		
		$scope.Templateloading=false;

		if (Service == 'RPX') {
			$scope.RemitInfoMaxLength = 35 * 4;

		} else if ((Service == 'GVP') || (Service == 'GLV')) {

			$scope.RemitInfoMaxLength = 35 * 10;

		} else if ((Service == 'CXC')) {

			$scope.RemitInfoMaxLength = 200;

		} else {}

		//$scope.loadBenenficiaryBankDetails($scope.ProductsSupported123)

		if ($scope.ProductsSupported123 != '') {

			inputObj21 = {
				"filters" : {
					"logicalOperator" : "AND",
					"groupLvl1" : [{
							"logicalOperator" : "AND",
							"groupLvl2" : [{
									"logicalOperator" : "AND",
									"groupLvl3" : [{
											"logicalOperator" : "AND",
											"clauses" : [{
													"columnName" : "ProductCode",
													"operator" : "=",
													"value" : ProductsSupported
												},
												{"columnName":"Status","operator":"=","value":"ACTIVE"}
											]
										}
									]
								}
							]
						}
					]
				},
				"start" : 0,
				"count" : 1000
			}

			$scope.ChargeCode = '';

			$http({
				url : BASEURL + '/rest/v2/methodofpayments/readall',
				method : "POST",
				data : inputObj21,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				////console.log(data)
				$scope.BankIdentifierTypeData = data;
				$scope.BankIdentifierTypeData_1 = data;
				if (data.length == 1) {					
					$scope.BeneficiaryBank.BankIdentifierType = data[0].ClearingSchemeCode;
					$scope.ChargeCodelength = 1;
					$scope.PaymentDetails.ChargeCode = $scope.ignoreEmptyValue(data[0]);
					$scope.getBankIdentifierCode($scope.BeneficiaryBank.BankIdentifierType)
				}
				if (data.length > 1) {
					$scope.ChargeCodelength = data.length;
					$scope.ChargeCode = $scope.ignoreEmptyValue(data);
				}
				$('select[name=BankIdentifierType1]').select2()
			}).error(function (data, status, headers, config) {});

		}

		$timeout(function () {
			$('select[name=MessageType]').select2()
			$('select[name=DebtorCustomerProprietaryCode]').select2()
		}, 500)

		var ClientID1234 = '';
		$http.get(BASEURL + '/rest/v2/party/code/' + JSON.parse(Party).PartyCode).success(function (data, status, headers, config) {
			////console.log(data)

			if (data.length == 1) {
				$scope.OrderingCustomer.ClientID = data[0].actualvalue;
				//////console.log($scope.OrderingCustomer.ClientID)
			}

			
			$scope.ClientID123 = data;
			ClientID1234 = data;
			fetchClientData($scope.ClientID123, Party)

		}).error(function (data, status, headers, config) {
			//////console.log(data)
		});

		$timeout(function () {
			getMethodService.fetchData(BASEURL + '/rest/v2/party/code/' + JSON.parse(Party).PartyCode).then(function (d) {
				////console.log(d)
				ClientID1234 = d;
			});
		}, 500)

		////console.log(ClientID1234)
		$scope.party123 = '';
		function fetchClientData(ClientData, Party) {
			////console.log(ClientData)
			////console.log(Party)
			//$scope.OrderingCustomer.ClientID = 'A309213';
			$scope.party123 = JSON.parse(Party);
			////console.log($scope.party123)
			////console.log($scope.party123)

			if ($scope.party123.PartyName != undefined) {
				$scope.OrderingCustomer.ClientName = $scope.party123.PartyName;
			}
			if ($scope.party123.AddressLine1 != undefined) {

				$scope.OrderingCustomer.ClientAddressLine1 = $scope.party123.AddressLine1;
			}
			if ($scope.party123.AddressLine2 != undefined) {

				$scope.OrderingCustomer.ClientAddressLine2 = $scope.party123.AddressLine2;
			}
			if ($scope.party123.City != undefined) {

				$scope.OrderingCustomer.City = $scope.party123.City;
			}
			if ($scope.party123.State != undefined) {

				$scope.OrderingCustomer.State = $scope.party123.State;
			}
			if ($scope.party123.PostCode != undefined) {

				$scope.OrderingCustomer.PostCode = $scope.party123.PostCode;
			}
			if ($scope.party123.Country != undefined) {

				$scope.OrderingCustomer.Country = $scope.party123.Country;
			}

			////console.log($scope.OrderingCustomer);

			$scope.OrderingCustomerAccountNumber = '';
			$scope.OrderingCustomer.AccountCurrency = '';
			$scope.OrderingCustomer.AccountName = '';

			inputObj3 = {
				"filters" : {
					"logicalOperator" : "AND",
					"groupLvl1" : [{
							"logicalOperator" : "AND",
							"groupLvl2" : [{
									"logicalOperator" : "AND",
									"groupLvl3" : [{
											"logicalOperator" : "AND",
											"clauses" : [{
													"columnName" : "PartyCode",
													"operator" : "=",
													"value" : ClientData[0].actualvalue
												},{
													"columnName" : "AccountType",
													"operator" : "=",
													"value" : "DDA"
												},{"columnName":"Status","operator":"=","value":"ACTIVE"}
											]
										}
									]
								}
							]
						}
					]
				},
				"start" : 0,
				"count" : 1000
			}

			////console.log(inputObj3)

			$http({
				url : BASEURL + '/rest/v2/accounts/readall',
				method : "POST",
				data : inputObj3,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				//console.log(data)
				
				if (data.length == 1) {
					$scope.OrderingCustomerAccountNumber_length = 1;
					$scope.OrderingCustomerAccountNumber = JSON.stringify(data[0]);
					$scope.OrderingCustomer_AccountCurrency = getCSVtoArray(data[0].AccountCurrency);
					$scope.OrderingCustomer.AccountCurrency = data[0].DefaultCurrency;
					$scope.OrderingCustomer.AccountName = data[0].AccountName;
					$scope.getAccountNumberCurrency(JSON.stringify(data[0]));
					$scope.AccountNumber11=data;
				} else if (data.length == 0) {
					$scope.OrderingCustomerAccountNumber_length = 0;
				}
				$scope.AccountNumber11 = data;

			}).error(function (data, status, headers, config) {
				////////console.log(data)
				if (err.status == 401) {
					if (configData.Authorization == 'External') {
						window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
					} else {
						LogoutService.Logout();
					}
				} else {
					$scope.alerts = [{
							type : 'danger',
							msg : data.error.message
						}
					];
				}
			});

		}

		$timeout(function () {
			$('select[name=MessageType]').select2()
			$('select[name=psaCode]').select2()
			$('select[name=Branch]').select2()
			$('select[name=PaymentType]').select2()
			$('select[name=ClientID]').select2()
			$('select[name=PaymentCurrency]').select2()
			$('select[name=OrderingCustomer_AccountNumber]').select2()
			$('select[name=AccountDomiciledCountry]').select2()
			$('select[name=AccountCurrency]').select2()
		}, 500)

	}

	$scope.ClientIDTextbox=false;
	$scope.getClientID=function(cid){
		//console.log(cid.length)
		//$scope.Templateloading=false;
		if(cid.length=='0'){
			//$scope.getAllCountryList123()
			$scope.ClientIDTextbox=true;
		}
	}
	
	$scope.searchParam123={"filters":{"logicalOperator":"AND","groupLvl1":[{"logicalOperator":"AND","groupLvl2":[{"logicalOperator":"AND","groupLvl3":[{"logicalOperator":"OR","clauses":[{"columnName":"Status","operator":"=","value":"ACTIVE"},{"columnName":"Status","operator":"=","value":"ACTIVE-WAITFORAPPROVAL"}]}]}]}]},"start":0,"count":1000}
	
	$scope.getAllCountryList123=function(){
	
		$http({
					url : BASEURL + '/rest/v2/countries/readall',
					method : "POST",
					data : $scope.searchParam123,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function (data, status, headers, config) {
					$scope.AccountDomiciledCountry = data;
					//////console.log($scope.OrderingCustomer)
				}).error(function (data, status, headers, config) {
					
				});
	}
	
	
	$scope.getAccountNumberCurrency = function (AccNum) {

		if ((AccNum != '') || (AccNum != undefined)) {

			$scope.AccNum1 = JSON.parse(AccNum);
			////console.log($scope.AccNum1)
			$scope.OrderingCustomer.AccountNumber = $scope.AccNum1.AccountNo;
			$scope.OrderingCustomer_AccountCurrency = getCSVtoArray($scope.AccNum1.AccountCurrency);
			$scope.OrderingCustomer.AccountCurrency = $scope.AccNum1.DefaultCurrency;
			$scope.OrderingCustomer.AccountName = $scope.AccNum1.AccountName;
			//$scope.OrderingCustomer.AccountDomiciledCountry = 'US';
			if ($scope.AccNum1.BranchCode == undefined) {

				$http({
					url : BASEURL + '/rest/v2/countries/readall',
					method : "POST",
					data : $scope.searchParam123,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function (data, status, headers, config) {
					//////console.log(data)
					//alert()
					if (data.length > 0) {
						$scope.AccountDomiciledCountry = data;

					} else {}
					//////console.log($scope.OrderingCustomer)
				}).error(function (data, status, headers, config) {
					////////console.log(data)
					if (err.status == 401) {
						if (configData.Authorization == 'External') {
							window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
						} else {
							LogoutService.Logout();
						}
					} else {
						$scope.alerts = [{
								type : 'danger',
								msg : data.error.message
							}
						];
					}
				});

			} else {

				inputObj = {
					"filters" : {
						"logicalOperator" : "AND",
						"groupLvl1" : [{
								"logicalOperator" : "AND",
								"groupLvl2" : [{
										"logicalOperator" : "AND",
										"groupLvl3" : [{
												"logicalOperator" : "AND",
												"clauses" : [{
														"columnName" : "BranchCode",
														"operator" : "=",
														"value" : $scope.AccNum1.BranchCode
													},
													{"columnName":"Status","operator":"=","value":"ACTIVE"}
												]
											}
										]
									}
								]
							}
						]
					},
					"start" : 0,
					"count" : 1000
				}
				//////console.log(inputObj);
				$http({
					url : BASEURL + '/rest/v2/branches/readall',
					method : "POST",
					data : inputObj,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function (data, status, headers, config) {
					//////console.log(data)
					$scope.AccountDomiciledCountry123 = data;
					if (data.length > 0) {
						//$scope.branchList = data;

						$http({
							url : BASEURL + '/rest/v2/countries/readall',
							method : "POST",
							data : $scope.searchParam123,
							headers : {
								'Content-Type' : 'application/json'
							}
						}).success(function (data, status, headers, config) {
							if (data.length > 0) {
								$scope.AccountDomiciledCountry = data;
							} else {
								$scope.AccountDomiciledCountry = data;
							}
						}).error(function (data, status, headers, config) {
							////////console.log(data)
							if (err.status == 401) {
								if (configData.Authorization == 'External') {
									window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
								} else {
									LogoutService.Logout();
								}
							} else {
								$scope.alerts = [{
										type : 'danger',
										msg : data.error.message
									}
								];
							}
						});

					} else {
						$http({
							url : BASEURL + '/rest/v2/countries/readall',
							method : "POST",
							data : $scope.searchParam123,
							headers : {
								'Content-Type' : 'application/json'
							}
						}).success(function (data, status, headers, config) {
							//////console.log(data)
							//alert()
							if (data.length > 0) {
								$scope.AccountDomiciledCountry = data;

							} else {
								$scope.AccountDomiciledCountry = data;
							}
							//////console.log($scope.OrderingCustomer)
						}).error(function (data, status, headers, config) {
							////////console.log(data)
							if (err.status == 401) {
								if (configData.Authorization == 'External') {
									window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
								} else {
									LogoutService.Logout();
								}
							} else {
								$scope.alerts = [{
										type : 'danger',
										msg : data.error.message
									}
								];
							}
						});
					}
				}).error(function (data, status, headers, config) {
					//////console.log(data)
				});

			}

		} else {
			
			$scope.OrderingCustomer.AccountNumber = '';
			$scope.OrderingCustomer_AccountCurrency = '';
			$scope.OrderingCustomer.AccountCurrency = '';
			$scope.OrderingCustomer.AccountName = '';
		}
		$timeout(function () {
			$('select[name=ClientID]').select2()
			$('select[name=AccountNumber]').select2()
			$scope.OrderingCustomer.AccountCurrency = $scope.AccNum1.DefaultCurrency;
		}, 500)

	}

	$scope.getBankIdentifierCode = function (BankIdentifierType) {

		////console.log(BankIdentifierType)
		$scope.BankIdentifierCode123 = '';

		//$scope.BeneficiaryBank={}
		$scope.BankIdentifierCode123 = '';
		if ((BankIdentifierType != '') && (BankIdentifierType != undefined)) {
			//BankIdentifierType = JSON.parse(BankIdentifierType);
			inputObj = {
				"filters" : {
					"logicalOperator" : "AND",
					"groupLvl1" : [{
							"logicalOperator" : "AND",
							"groupLvl2" : [{
									"logicalOperator" : "AND",
									"groupLvl3" : [{
											"logicalOperator" : "AND",
											"clauses" : [{
													"columnName" : "SchemeCode",
													"operator" : "=",
													"value" : BankIdentifierType
												},
												{"columnName":"Status","operator":"=","value":"ACTIVE"}
											]
										}
									]
								}
							]
						}
					]
				},
				"start" : 0,
				"count" : 1000
			}

			//////console.log(inputObj);
			$http({
				//url : BASEURL + '/rest/v2/memberships/readall', /v2/initiatetransaction/memebership/{product}
				url : BASEURL + '/rest/v2/initiatetransaction/memebership/'+$scope.ProductsSupported123,
				method : "POST",
				data : inputObj,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				//////console.log(data)
				$scope.BankIdentifierCode123 = data;

				if ($scope.Templateloading != true) {
					$scope.Beneficiary = {}
				}

				if (data.length > 0) {
					//$scope.branchList = data;
				} else {}

				$('select[name=BankIdentifierType]').select2()
				$('select[name=BIC1]').select2()
			}).error(function (data, status, headers, config) {
				//////console.log(data)
			});
		} else {

			$http({
				url : BASEURL + '/rest/v2/accounts/readall',
				method : "POST",
				data : {
					"start" : 0,
					"count" : 200
				},
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				$scope.AccountNumberDrop = data;
			}).error(function (data, status, headers, config) {});
		}

	}

	$scope.BIT_reselect = false;
	$scope.getBankIdentifierCode1 = function (BankIdentifierType, index) {

		console.log(BankIdentifierType, index)
		$scope.IntermediaryBankDetails123[index].BankIdentifierCode = '';
		//$scope.BeneficiaryBank={}
		$scope.BankIdentifierCode1234 = '';
		if ((BankIdentifierType != '') && (BankIdentifierType != undefined)) {
			var BIT
			if(BankIdentifierType.indexOf('{')!=-1){
				BankIdentifierType = JSON.parse(BankIdentifierType);
				BIT=BankIdentifierType.ClearingSchemeCode;
			}else{
				BIT=BankIdentifierType;
			}
			inputObj = {
				"filters" : {
					"logicalOperator" : "AND",
					"groupLvl1" : [{
							"logicalOperator" : "AND",
							"groupLvl2" : [{
									"logicalOperator" : "AND",
									"groupLvl3" : [{
											"logicalOperator" : "AND",
											"clauses" : [{
													"columnName" : "SchemeCode",
													"operator" : "=",
													"value" : BIT
												},{"columnName":"Status","operator":"=","value":"ACTIVE"}
											]
										}
									]
								}
							]
						}
					]
				},
				"start" : 0,
				"count" : 1000
			}

			//////console.log(inputObj);
			$http({
				url : BASEURL + '/rest/v2/memberships/readall',
				method : "POST",
				data : inputObj,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				//////console.log(data)
				$scope.BIT_reselect = true;
				$scope.BankIdentifierCode1234 = data;

				if (data.length > 0) {
					//$scope.branchList = data;
				} else {}

				$('select[name=BankIdentifierType2]').select2()
				$('select[name=BIC]').select2()
			}).error(function (data, status, headers, config) {
				//////console.log(data)
			});
		} else {}
 
	}

	$scope.Reload = function () {

		$rootScope.dataModified = false;
		$state.reload();
		$scope.isPaymentDetailsCollapsed = true;
		$scope.isOrderingCustomerCollapsed = true;
		$scope.isBenenficiaryBankDetailsCollapsed = true;
		$scope.isBenenficiaryDetailsCollapsed = true;
		$scope.isPaymentInfoCollapsed = true;
		$scope.isRemittanceInformationCollapsed = true;
		$scope.SaveTemplateCollapsed = true;
		
		$scope.activatePicker();

	}

	$scope.callPicker = function () {
		
		setTimeout(function () {
			$('.DatePicker').datepicker({
				format : "yyyy-mm-dd",
				showClear : true,
				autoclose : true,
				startDate : new Date()

			})
			$('.input-group-addon').on('click focus', function (e) {
				$(this).prev().focus().click()
			});
		}, 1000)
	}

	$scope.activatePicker = function () {
		//////console.log("came")

		var prev = null;
		$('.DatePicker').datepicker({
			format : "yyyy-mm-dd",
			showClear : true,
			startDate : new Date()
		}).on('dp.change', function (ev) {
			$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
		}).on('dp.show', function (ev) {}).on('dp.hide', function (ev) {});
	}

	function d2h(d) {
		return d.toString(16);
	}

	function h2d(h) {
		return parseInt(h, 16);
	}

	function stringToHex(tmp) {

		var str = '',
		i = 0,
		tmp_len = tmp.length,
		c;

		for (; i < tmp_len; i += 1) {
			c = tmp.charCodeAt(i);
			str += d2h(c);
		}
		////////console.log(str)
		return str;
	}

	function addNewlines(str) {

		var result = '';
		while (str.length > 0) {
			result += str.substring(0, 35) + '\\n';
			str = str.substring(35);
		}
		////////console.log(JSON.stringify(result))
		return JSON.stringify(result);
	}

	$scope.templateOverride = false;
	$scope.checkTemplateName = function (checkTemplateName) {
		if (($scope.templateName === checkTemplateName) && (checkTemplateName != '')) {
			$scope.templateOverride = true;
		}
	}

	$scope.addIntermediaryBankDetails = function () {
		//////console.log($scope.IntermediaryBankDetails123);
		var ggg = {}
		ggg.BankIdentifierType = "";
		ggg.BankIdentifierCode = "";
		ggg.BankAddressLine1 = "";
		ggg.BankAddressLine2 = "";
		ggg.City = "";
		ggg.State = "";
		ggg.PostCode = "";
		ggg.Country = "";
		if ($scope.IntermediaryBankDetails123.length <= 2) {
			$scope.IntermediaryBankDetails123.push(ggg)
		} else {
			$scope.IntermediaryBankDetailsMaxError = "Max occurs 3"
		}

		setTimeout(function () {
			for (i = 0; i < $scope.IntermediaryBankDetails123.length; i++) {
				delete $scope.IntermediaryBankDetails123[i].$$hashKey;
			}
		}, 100)

		//////console.log($scope.IntermediaryBankDetails123);
	}

	$scope.removeIntermediaryBankDetails = function (remove) {
		//////console.log(remove);
		delete $scope.IntermediaryBankDetails123.splice(remove, 1);
	}
	if (($scope.loadTemplateData != true) && ($scope.ServiceCodeForConditions != 'RPX')) {
		$scope.addIntermediaryBankDetails();
	}

	/* $http.get(BASEURL + '/rest/v2/roles').success(function (data, status, headers, config) {
	$scope.roles = data;
	}).error(function (data, status, headers, config) {
	////////console.log(data)
	}); */

	function cleantheinputdata(newData) {
		
		$.each(newData, function (key, value) {
			if (key == '$$hashkey') {
				delete newData.$$hashkey;
			}

			if ($.isPlainObject(value)) {

				var isEmptyObj = cleantheinputdata(value)
					if ($.isEmptyObject(isEmptyObj)) {
						delete newData[key]
						newData[key]
					}
			} else if (Array.isArray(value) && !value.length) {
				delete newData[key]
			} else if (value === "" || value === undefined || value === null) {
				delete newData[key]
			}
		})
		return newData
	}
	
	function removeHashKey(val) {
		var json = JSON.stringify(val, function (key, value) {
				if (key === "$$hashKey") {
					return undefined;
				}
				return value;
			});
		return JSON.parse(json);
	}

	function objectFilter(val) {

		var filttered = val.filter(function (a) {
				var temp = Object.keys(a).map(function (k) {
						return a[k];
					}),
				k = temp.join('|');

				if (!this[k] && temp.join('')) {
					this[k] = true;
					return true;
				}
			}, Object.create(null));

		return filttered;

	}

	$scope.multipleEmptySpace = function (e) {
		if ($.trim($(e.currentTarget).val()).length == 0) {
			$(e.currentTarget).val('');
		}
	}

	function getThePartyObj(party,service,messageType) {
		$http({
			url : BASEURL + '/rest/v2/parties/read',
			method : "POST",
			data : {
				"PartyCode" : party
			},
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			//////console.log(data)
			$scope.partyOptions = []
			$scope.partyOptions.push(data);
			$scope.party = JSON.stringify(data);
			$scope.getServiceList($scope.party,$scope.PartySelectedFlag)
			getTheServiceObj($scope.party, service,messageType)
			//$scope.PartySelectedFlag = true;
		}).error(function (data, status, headers, config) {
			//////console.log(data)
		});
	}

	function getTheServiceObj(party, service,messageType) {
		$http({
			url : BASEURL + '/rest/v2/services/read',
			method : "POST",
			data : {
				"ServiceCode" : service
			},
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			//////console.log(data)
			$scope.service123 = data;
			//$scope.serviceOptions = []
			//$scope.serviceOptions.push(data);
			$scope.service11 = JSON.stringify(data);
			$('select[name=service11]').select2()
			$scope.getMessageType(party,$scope.service11)
			$scope.getPaymentType11(messageType,$scope.service11,party)
		}).error(function (data, status, headers, config) {
			//////console.log(data)
		});
	}

	function getThePSAObj(val) {
		$http({
			url : BASEURL + '/rest/v2/partyserviceassociations/read',
			method : "POST",
			data : {
				"PartyServiceAssociationCode" : val
			},
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			//////console.log(data)

			$scope.selectOptions = []
			$scope.selectOptions.push(data);
			$scope.psaCode11 = JSON.stringify(data);
			$('select[name=psaCode]').select2()

		}).error(function (data, status, headers, config) {
			//////console.log(data)
		});
	}

	function getTheBranchObj(val) {
		
		$http({
			url : BASEURL + '/rest/v2/branches/read',
			method : "POST",
			data : {
				"BranchCode" : val
			},
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			//console.log(data)

			$scope.branchList = []
			$scope.branchList.push(data);
			$scope.PaymentBranch = JSON.stringify(data);
			$('select[name=Branch]').select2()

		}).error(function (data, status, headers, config) {
			//////console.log(data)
		});
	}

	function cleanArray(actual) {
		var newArray = new Array();
		for (var i = 0; i < actual.length; i++) {
			if (actual[i]) {
				newArray.push(actual[i]);
			}
		}
		return newArray;
	}
	
	function getCountryCodeList(){
			$http({
					url : BASEURL + '/rest/v2/countries/readall',
					method : "POST",
					data : $scope.searchParam123,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).success(function (data, status, headers, config) {
					//////console.log(data)
					//alert()
					if (data.length > 0) {
						$scope.AccountDomiciledCountry = data;

					} else {}
					//////console.log($scope.OrderingCustomer)
				}).error(function (data, status, headers, config) {
					////////console.log(data)
					
				});
	}
	
	$scope.SubmitButtonTiggered=false;
	$scope.TemplateAndSubmitButtonTiggered=false;
	
			
	$scope.CXCFieldsClear=function(PaymentObj){
		if((PaymentObj.PaymentDetails!='')&&(PaymentObj.PaymentDetails!=undefined)){	
			delete PaymentObj.PaymentDetails.DebtorCustomerProprietaryCode;
			delete PaymentObj.PaymentDetails.ChargeCode;
		}
		if((PaymentObj.OrderingCustomer!='')&&(PaymentObj.OrderingCustomer!=undefined)){	
			delete PaymentObj.OrderingCustomer.AccountNumber;
			delete PaymentObj.OrderingCustomer.AccountCurrency;
			delete PaymentObj.OrderingCustomer.AccountName;
			delete PaymentObj.OrderingCustomer.AccountDomiciledCountry;
		}
		if((PaymentObj.BeneficiaryBank!='')&&(PaymentObj.BeneficiaryBank!=undefined)){		
			delete PaymentObj.BeneficiaryBank;
		}
		if((PaymentObj.RemittanceInformation!='')&&(PaymentObj.RemittanceInformation!=undefined)){		
			delete PaymentObj.RemittanceInformation.RemittanceID;
		}
		if((PaymentObj.IntermediaryBankDetails!='')&&(PaymentObj.IntermediaryBankDetails!=undefined)){		
			delete PaymentObj.IntermediaryBankDetails;
		}
		
		if((PaymentObj.Beneficiary!='')&&(PaymentObj.Beneficiary!=undefined)){		
			delete PaymentObj.Beneficiary.AccountNumber
			delete PaymentObj.Beneficiary.Name
		}
		return PaymentObj;
	}
	
	$scope.NonCXCFieldsClear=function(PaymentObj){
		if((PaymentObj.PaymentDetails!='')&&(PaymentObj.PaymentDetails!=undefined)){	
			delete PaymentObj.NumberOfHoldDays;
		}
		if((PaymentObj.Beneficiary!='')&&(PaymentObj.Beneficiary!=undefined)){		
			delete PaymentObj.Beneficiary.PayeeFirstName
			delete PaymentObj.Beneficiary.PayeeLastName
			delete PaymentObj.Beneficiary.PayeeIdType
			delete PaymentObj.Beneficiary.Token
		}
		return PaymentObj;
	}
	
	
	$scope.createData = function (PaymentDetails, OrderingCustomer, BeneficiaryBank, Beneficiary, RemittanceInformation, IntermediaryBankDetails123, party, service11, ProductsSupported123, psaCode11, PaymentBranch, EndToEndId, TemplateDetails, SaveTemplate) {

		//console.log('PaymentDetails',PaymentDetails)
		//console.log('OrderingCustomer',OrderingCustomer)
		//console.log('BeneficiaryBank',BeneficiaryBank)
		//console.log('Beneficiary',Beneficiary)
		//console.log('RemittanceInformation',RemittanceInformation)
		//console.log('IntermediaryBankDetails123',IntermediaryBankDetails123)
		//console.log('party',party)
		console.log('service11',service11)
		//console.log('ProductsSupported123',ProductsSupported123)
		//console.log('psaCode11',psaCode11)
		//console.log('PaymentBranch',PaymentBranch)
		//console.log('EndToEndId',EndToEndId)
		//console.log('TemplateDetails',TemplateDetails)
		//console.log('SaveTemplate',SaveTemplate)


		/* //console.log(PaymentDetails)
		//console.log(OrderingCustomer)
		//console.log(BeneficiaryBank)
		//console.log(Beneficiary)
		//console.log(RemittanceInformation)
		//console.log(party) */
		$rootScope.dataModified = false;

		$scope.finalPaymentObj = {}
		$scope.finalPaymentObj.Party = JSON.parse(party).PartyCode;
		$scope.finalPaymentObj.Service = JSON.parse(service11).ServiceCode;
		$scope.finalPaymentObj.PartyServiceAssociationCode = JSON.parse(psaCode11).PartyServiceAssociationCode;
		$scope.finalPaymentObj.BranchCode = JSON.parse(PaymentBranch).BranchCode;
		$scope.finalPaymentObj.ProductsSupported = ProductsSupported123;
		$scope.finalPaymentObj.EndToEndId = EndToEndId;
		$scope.finalPaymentObj.PaymentDetails = cleantheinputdata(PaymentDetails);
		$scope.finalPaymentObj.OrderingCustomer = cleantheinputdata(OrderingCustomer);
		$scope.finalPaymentObj.BeneficiaryBank = cleantheinputdata(BeneficiaryBank);
		$scope.finalPaymentObj=cleantheinputdata($scope.finalPaymentObj);

		if ($scope.Templateloading == true) {
			if($scope.finalPaymentObj.BeneficiaryBank){
			$scope.finalPaymentObj.BeneficiaryBank.BankIdentifierType = BeneficiaryBank.BankIdentifierType;
			$scope.finalPaymentObj.BeneficiaryBank.BankIdentifierCode = BeneficiaryBank.BankIdentifierCode;
			}

			if ((IntermediaryBankDetails123 != undefined) && ($scope.finalPaymentObj.Service != "RPX")) {
				IntermediaryBankDetails123=IntermediaryBankDetails123.filter(String);
				if (IntermediaryBankDetails123.length >= 1) {
					$scope.finalPaymentObj.IntermediaryBankDetails = []
					for (i = 0; i < IntermediaryBankDetails123.length; i++) {
						IntermediaryBankDetails123[i] = cleantheinputdata(IntermediaryBankDetails123[i]);
						delete IntermediaryBankDetails123[i].$$hashKey;
					}

				}
				$scope.finalPaymentObj.IntermediaryBankDetails = IntermediaryBankDetails123;
			}

		} else {

			if ((IntermediaryBankDetails123 != undefined) && ($scope.finalPaymentObj.Service != "RPX")) {
				if (IntermediaryBankDetails123.length >= 1) {
					
					for (i = 0; i < IntermediaryBankDetails123.length; i++) {						
						
						IntermediaryBankDetails123[i]=cleantheinputdata(IntermediaryBankDetails123[i]);
						delete IntermediaryBankDetails123[i].$$hashKey;
						if($.isEmptyObject(IntermediaryBankDetails123[i])) {
							delete IntermediaryBankDetails123[i];
						}	
					}
					
					
					IntermediaryBankDetails123=cleanArray(IntermediaryBankDetails123);
					
					if ((IntermediaryBankDetails123.length >= 1)&&(IntermediaryBankDetails123!=null)) {
						$scope.finalPaymentObj.IntermediaryBankDetails = []
						$scope.finalPaymentObj.IntermediaryBankDetails = IntermediaryBankDetails123;
					}
				}
			}
			
		}
		$scope.finalPaymentObj.Beneficiary = cleantheinputdata(Beneficiary);
		$scope.finalPaymentObj.RemittanceInformation = cleantheinputdata(RemittanceInformation);
		
		$scope.finalPaymentObj=cleantheinputdata($scope.finalPaymentObj);
		
		
		$scope.finalPaymentObj=JSON.parse(JSON.stringify($scope.finalPaymentObj).replace('"IntermediaryBankDetails":[{}],',''));
		
		
		TemplateDetails.Template = stringToHex(JSON.stringify($scope.finalPaymentObj))
		TemplateDetails.Creator = sessionStorage.UserID;
		if ((TemplateDetails.RolesAccessible == "") || (TemplateDetails.RolesAccessible == undefined)) {
			delete TemplateDetails.RolesAccessible;
		}
		
		
		if($scope.finalPaymentObj.Service=='CXC'){
			$scope.finalPaymentObj=$scope.CXCFieldsClear($scope.finalPaymentObj);
		}else{			
			$scope.finalPaymentObj=$scope.NonCXCFieldsClear($scope.finalPaymentObj);
		}
		console.log($scope.finalPaymentObj)
		$scope.SubmitButtonTiggered=true;
		 $http({
			url : BASEURL + '/rest/v2/payments/initiation/' + $scope.finalPaymentObj.PartyServiceAssociationCode,
			method : "POST",
			data : $scope.finalPaymentObj,
			headers : {
				'Content-Type' : 'application/json',
				'template-name':TemplateDetails.TemplateName
			}
		}).success(function (data, status, headers, config) {
			$rootScope.PaymentIntiated = data;
//console.log("dd",data)

		GlobalService.advancedSearchEnable = true;

		GlobalService.searchParams = {
		"InstructionData" : {
			"EntryDate" : {
				"Start" : "",
				"End" : ""
			}
		},
		'InstructionID':data.BusinessPrimaryKey[0].Value
	}
	
	sessionStorage.menuSelection = JSON.stringify({"val":"PaymentModule","subVal":"ReceivedInstructions"})


		//GlobalService.FLuir = data.BusinessPrimaryKey[0].Value;

		var iId = ["InstructionID="+data.BusinessPrimaryKey[0].Value];

		sessionStorage.advancedSearchFieldArr = JSON.stringify(iId);
		sessionStorage.currentObj = JSON.stringify({"start":0,"count":20,"Queryfield":[{"ColumnName":"InstructionID","ColumnOperation":"=","ColumnValue":data.BusinessPrimaryKey[0].Value,"advancedSearch":false}],"QueryOrder":[{"ColumnName":"EntryDate","ColumnOrder":"Desc"}]})

			if (SaveTemplate == true) {
				$scope.saveAsTemplate()
				
			} else {

				
				$location.path('app/instructions');
			}
	
			//console.log(data)
		}).error(function (data, status, headers, config) {
			////////console.log(data)
			$scope.SubmitButtonTiggered=false;
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}
			];
		}); 

		//if(SaveTemplate == true) {
		$scope.saveAsTemplate = function () {
			
			if ($scope.templateName === TemplateDetails.TemplateName) {
				$scope.method = "PUT";
			} else {
				delete TemplateDetails.MPITemplate_PK;
				$scope.method = "POST";
			}
			$scope.TemplateAndSubmitButtonTiggered=true;
			
			/* $scope.roles12345="";
			for(i=0;i<TemplateDetails.RolesAccessible;i++){
			$scope.roles12345=$scope.roles12345+JSON.parse(TemplateDetails.RolesAccessible[i].id).RoleName;
			} */
			//console.log(TemplateDetails.RolesAccessible)
			
				if ($scope.Templateloading != true) {
					if (TemplateDetails.RolesAccessible != undefined) {
						$scope.roles12345 = [];
						console.log(TemplateDetails.RolesAccessible)
						for (i = 0; i < TemplateDetails.RolesAccessible.length; i++) {
							if (TemplateDetails.RolesAccessible[i].indexOf('{') != -1) {
								$scope.roles12345.push(JSON.parse(TemplateDetails.RolesAccessible[i]).RoleID);
							} else {
								$scope.roles12345.push(TemplateDetails.RolesAccessible[i]);
							}
						}
						$scope.TemplateDetails.RolesAccessible = $scope.roles12345.toString();
					}
				} else {
					if (TemplateDetails.RolesAccessible != undefined) {
						$scope.roles12345 = [];
						console.log("typeOF", typeof(TemplateDetails.RolesAccessible))
						console.log(TemplateDetails.RolesAccessible)
						for (i = 0; i < TemplateDetails.RolesAccessible.length; i++) {
							if (TemplateDetails.RolesAccessible[i].indexOf('{') != -1) {
								$scope.roles12345.push(JSON.parse(TemplateDetails.RolesAccessible[i]).RoleID);
							} else {
								$scope.roles12345.push(TemplateDetails.RolesAccessible[i]);
							}
						}
						$scope.TemplateDetails.RolesAccessible = $scope.roles12345.toString();
					}
				}

			////console.log(TemplateDetails)


			$http({
				url : BASEURL + '/rest/v2/manualpaymentinitiationtemplate',
				method : $scope.method,
				data : TemplateDetails,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				$rootScope.PaymentIntiated1 = data;
				$location.path('app/instructions');
			}).error(function (data, status, headers, config) {
				////////console.log(data)
				$scope.TemplateAndSubmitButtonTiggered=false;
				$scope.alerts = [{
						type : 'danger',
						msg : data.error.message
					}
				];
			});

		}

	}

	$scope.loadTemplateData = function (templatedata) {
		//console.log(templatedata)
		// var templateName = JSON.parse(templatedata).actualvalue;
		if (JSON.parse(templatedata).TemplateName == undefined) {
			var templateName = JSON.parse(templatedata).actualvalue;
			$scope.templateName = JSON.parse(templatedata).actualvalue;
		} else {
			var templateName = JSON.parse(templatedata).TemplateName;
			$scope.templateName = JSON.parse(templatedata).TemplateName;
		}
		var TemplateName_PartyCode_Value = JSON.parse(templatedata).PartyCode;
		$http({
			url : BASEURL + '/rest/v2/manualpaymentinitiationtemplate/read',
			method : "POST",
			data : {
				TemplateName : templateName,
				PartyCode : TemplateName_PartyCode_Value
			},
			headers : {
				'Content-Type' : 'application/json'
			}
		}).success(function (data, status, headers, config) {
			$scope.loadTemplateDataNew(JSON.stringify(data))
		}).error(function (data, status, headers, config) {
			////////console.log(data)
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}
			];
		});
	}
	$scope.loadTemplateDataNew = function (templatedata) {
		console.log(templatedata,'templatedata')
		$scope.templateName = JSON.parse(templatedata).TemplateName;
		$scope.MPITemplate_PK = JSON.parse(templatedata).MPITemplate_PK;
		$scope.SaveTemplate = true;

		$scope.Templateloading = true;
		////console.log(templatedata)
		$scope.TemplateData = JSON.parse($filter('hex2a')(JSON.parse(templatedata).Template));
		console.log($scope.TemplateData)

		$scope.EndToEndId = $scope.TemplateData.EndToEndId;
		$scope.MessageType = [];
		$scope.MessageType.push($scope.TemplateData.PaymentDetails.MessageType);
		$scope.PaymentDetails.MessageType = $scope.TemplateData.PaymentDetails.MessageType;

		
		getThePartyObj($scope.TemplateData.Party,$scope.TemplateData.Service,$scope.PaymentDetails.MessageType);
		//getTheServiceObj($scope.TemplateData.Party,$scope.TemplateData.Service);

		getThePSAObj($scope.TemplateData.PartyServiceAssociationCode)
		
		$scope.ProductsSupported = [];
		$scope.ProductsSupported.push($scope.TemplateData.ProductsSupported);
		$scope.ProductsSupported123 = $scope.TemplateData.ProductsSupported;
		
		//$scope.DebtorCustomerProprietaryCode = [];
		//$scope.DebtorCustomerProprietaryCode.push($scope.TemplateData.PaymentDetails.DebtorCustomerProprietaryCode);
		
		$scope.PaymentDetails.DebtorCustomerProprietaryCode = $scope.TemplateData.PaymentDetails.DebtorCustomerProprietaryCode;
		$scope.PaymentDetails.NumberOfHoldDays = $scope.TemplateData.PaymentDetails.NumberOfHoldDays;
		$scope.PaymentCurrency = [];
		$scope.PaymentCurrency.push($scope.TemplateData.PaymentDetails.PaymentCurrency);
		$scope.PaymentDetails.PaymentCurrency = $scope.TemplateData.PaymentDetails.PaymentCurrency;
		$scope.OrderingCustomer = $scope.TemplateData.OrderingCustomer;
		
		$scope.ClientID123 = [];
		$scope.ClientID123.push($scope.TemplateData.OrderingCustomer.ClientID);
		$scope.OrderingCustomer.ClientID = $scope.TemplateData.OrderingCustomer.ClientID;
		//$('select[name=ClientID]').select2()
		/*  */
		$scope.AccountDomiciledCountry11 = [];
		$scope.AccountDomiciledCountry11.push($scope.TemplateData.OrderingCustomer.AccountDomiciledCountry);
		//getCountryCodeList()
		$scope.OrderingCustomer.AccountDomiciledCountry = $scope.TemplateData.OrderingCustomer.AccountDomiciledCountry;
		
		$scope.AccountNumber11 = [];
		$scope.AccountNumber11.push($scope.TemplateData.OrderingCustomer.AccountNumber);
		$scope.OrderingCustomerAccountNumber = $scope.TemplateData.OrderingCustomer.AccountNumber;
		$('select[name=OrderingCustomer_AccountNumber]').select2()
		$scope.OrderingCustomer_AccountCurrency = [];
		$scope.OrderingCustomer_AccountCurrency.push($scope.TemplateData.OrderingCustomer.AccountCurrency);
		$scope.OrderingCustomer.AccountCurrency = $scope.TemplateData.OrderingCustomer.AccountCurrency;
		$scope.BeneficiaryBank = $scope.TemplateData.BeneficiaryBank;
		$('select[name=AccountCurrency]').select2()

		$scope.BankIdentifierTypeData = [];
		console.log()
		if($scope.TemplateData.BeneficiaryBank){
		$scope.BankIdentifierTypeData.push($scope.TemplateData.BeneficiaryBank.BankIdentifierType);
		$scope.BeneficiaryBank.BankIdentifierType = $scope.TemplateData.BeneficiaryBank.BankIdentifierType;
		
		$('select[name=BankIdentifierType1]').select2()


		$scope.BankIdentifierCode123 = [];
		// $scope.BankIdentifierCode123.push($scope.TemplateData.BeneficiaryBank.BankIdentifierCode);
		$scope.BankIdentifierCode123.push({'SchemeParticipantIdentifer' : $scope.TemplateData.BeneficiaryBank.BankIdentifierCode })

		$scope.BeneficiaryBank.BankIdentifierCode = $scope.TemplateData.BeneficiaryBank.BankIdentifierCode;
		$('select[name=BIC1]').select2()
		}
		

		$scope.Beneficiary = $scope.TemplateData.Beneficiary;
		console.log($scope.Beneficiary)
		$scope.RemittanceInformation = $scope.TemplateData.RemittanceInformation;
		$scope.TemplateDetails = JSON.parse(templatedata);
		console.log($scope.TemplateDetails)

		if ($scope.TemplateData.IntermediaryBankDetails != undefined) {			
			$scope.IntermediaryBankDetails123 = $scope.TemplateData.IntermediaryBankDetails;
			$scope.BankIdentifierTypeData_1 = [];
			$scope.BankIdentifierCode1234 = [];
			
			for (i = 0; i < $scope.IntermediaryBankDetails123.length; i++) {
				console.log($scope.IntermediaryBankDetails123[i])
				if($scope.IntermediaryBankDetails123[i].BankIdentifierType.indexOf('{')!=-1){					
					$scope.BankIdentifierTypeData_1.push(JSON.parse($scope.IntermediaryBankDetails123[i].BankIdentifierType).ClearingSchemeCode);
					$scope.IntermediaryBankDetails123[i].BankIdentifierType = JSON.parse($scope.IntermediaryBankDetails123[i].BankIdentifierType).ClearingSchemeCode;
				}else{					
					$scope.BankIdentifierTypeData_1.push($scope.IntermediaryBankDetails123[i].BankIdentifierType);
				}					
				

				
				$scope.BankIdentifierCode1234.push($scope.IntermediaryBankDetails123[i].BankIdentifierCode);
				$scope.IntermediaryBankDetails123[i].BankIdentifierCode = $scope.IntermediaryBankDetails123[i].BankIdentifierCode;
			}
		}else{
			$scope.IntermediaryBankDetails123[0].BankIdentifierType='';
		} 

		function arrayTrim(val) {
			var ff = [];
			for (i = 0; i < val.length; i++) {
				ff.push(val[i].trim());
			}
			return ff;
		}

		//$scope.ROLESOptions=["Super Admin", "Approver"];
		if ($scope.TemplateDetails.RolesAccessible != undefined) {

			var RolesAccessible = $scope.TemplateDetails.RolesAccessible.split(',');
			var Queryfield = [];
			for (i = 0; i < RolesAccessible.length; i++) {
				Queryfield.push({
					"ColumnName" : "RoleID",
					"ColumnOperation" : "=",
					"ColumnValue" : RolesAccessible[i].trim()
				})
				
			}
			
			var dd = {
				"start" : 0,
				"count" : 100,
				"Queryfield" : Queryfield,
				"Operator" : "AND"
			}

			setTimeout(function () {
				$scope.remoteDataConfig21()
			}, 100)

			$http({
				url : BASEURL + '/rest/v2/roles/readall',
				method : "POST",
				data : constructQuery(dd),
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function (data, status, headers, config) {
				//console.log(data)
				$scope.ROLESOptions123 = data;
				$scope.TemplateDetails.RolesAccessible = arrayTrim($scope.TemplateDetails.RolesAccessible.split(','));
				//console.log($scope.TemplateDetails.RolesAccessible)
				$('select[name=RolesAccessible]').val($scope.TemplateDetails.RolesAccessible)
				$('select[name=RolesAccessible]').select2()
			}).error(function (data, status, headers, config) {
				////////console.log(data)
				$scope.alerts = [{
						type : 'danger',
						msg : data.error.message
					}
				];
			});
			/* $scope.ROLESOptions=$scope.TemplateDetails.RolesAccessible.split(',');
			$scope.TemplateDetails.RolesAccessible=$scope.TemplateDetails.RolesAccessible.split(',');
			////console.log($scope.ROLESOptions)
			//$('select[name=RolesAccessible]').select2('destroy')
			setTimeout(function(){
			$scope.remoteDataConfig21()
			$('select[name=RolesAccessible]').val($scope.ROLESOptions)
			$('select[name=RolesAccessible]').select2()
			},100) */
		} else {
			setTimeout(function () {
				$scope.remoteDataConfig21()
				//$('select[name=RolesAccessible]').select2()
			}, 100)
		}
		
		$timeout(function () {
			getTheBranchObj($scope.TemplateData.BranchCode)
			$scope.PaymentDetails.ValueDate = '';
			$('select[name=MessageType]').select2()
			$('select[name=psaCode]').select2()
			$('select[name=Branch]').select2()
			$('select[name=PaymentType]').select2()
			$('select[name=ClientID]').select2()
			$('select[name=PaymentCurrency]').select2()
			$('select[name=OrderingCustomer_AccountNumber]').select2()
			$('select[name=AccountDomiciledCountry]').select2()
			$('select[name=AccountCurrency]').select2()
			$('select[name=BankIdentifierType]').select2()
			$('select[name=BankIdentifierType1]').select2()
			$('select[name=BIC1]').select2()
		}, 500)

	}

	$scope.collapseAll = function () {
		$scope.isPaymentDetailsCollapsed = true;
		$scope.isOrderingCustomerCollapsed = true;
		$scope.isBenenficiaryBankDetailsCollapsed = true;
		$scope.isBenenficiaryDetailsCollapsed = true;
		$scope.isPaymentInfoCollapsed = true;
		$scope.isRemittanceInformationCollapsed = true;
		$scope.SaveTemplateCollapsed = true;
		$scope.activatePicker();
	}

	$scope.expandAll = function () {
		$scope.isPaymentDetailsCollapsed = false;
		$scope.isOrderingCustomerCollapsed = false;
		$scope.isBenenficiaryBankDetailsCollapsed = false;
		$scope.isBenenficiaryDetailsCollapsed = false;
		$scope.isPaymentInfoCollapsed = false;
		$scope.isRemittanceInformationCollapsed = false;
		$scope.SaveTemplateCollapsed = false;
		$scope.activatePicker();
	}

	
	$scope.payidtype = function(){
		$scope.Beneficiary.Token='';
	}

	
			$scope.allowOnlyNumbersAlone = function (event) {
					if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
						var keyArr = [8, 9, 35, 36, 37, 39, 46]
						console.log("CheckKey",$(event.currentTarget))
						function chkKey(key) {
							if (keyArr.indexOf(key) != -1) {
								return false;
							} else {
								return true;
							}
						}

						$(event.currentTarget).val($(event.currentTarget).val().replace(/[^0-9\.]/g, ''));
						if ((chkKey(event.keyCode)) && (event.which != 46 || $(event.currentTarget).val().indexOf('.') == -1) && (event.which < 48 || event.which > 57)) {
							event.preventDefault();
						}

					} else {
						if ((event.keyCode == 46) || (event.charCode == 46)) {
							$(event.currentTarget).val($(event.currentTarget).val().replace(/[^0-9\.]/g, ''));

						}

						if ((event.which != 46 || $(event.currentTarget).val().indexOf('.') == -1) && (event.which < 48 || event.which > 57)) {
							event.preventDefault();
						}

					}

	}
	


});

VolpayApp.factory('getMethodService', function ($http) {
	var getMethodService = {
		fetchData : function (url) {
			// $http returns a promise, which has a then function, which also returns a promise
			var promise = $http.get(url).then(function (response) {
					// The then function here is an opportunity to modify the response
					////console.log(response);
					// The return value gets picked up by the then in the controller.
					return response.data;
				});
			// Return the promise to the controller
			return promise;
		}
	};
	return getMethodService;
});

VolpayApp.directive('validNumber', function () {
	return {
		require : '?ngModel',
		link : function (scope, element, attrs, ngModelCtrl) {
			if (!ngModelCtrl) {
				return;
			}

			ngModelCtrl.$parsers.push(function (val) {
				if (angular.isUndefined(val)) {
					var val = '';
				}

				var clean = val.replace(/[^-0-9\.]/g, '');
				var negativeCheck = clean.split('-');
				var decimalCheck = clean.split('.');
				if (!angular.isUndefined(negativeCheck[1])) {
					negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
					clean = negativeCheck[0] + '-' + negativeCheck[1];
					if (negativeCheck[0].length > 0) {
						clean = negativeCheck[0];
					}

				}

				if (!angular.isUndefined(decimalCheck[1])) {
					decimalCheck[1] = decimalCheck[1].slice(0, 2);
					clean = decimalCheck[0] + '.' + decimalCheck[1];
				}

				if (val !== clean) {
					ngModelCtrl.$setViewValue(clean);
					ngModelCtrl.$render();
				}
				return clean;
			});

			element.bind('keypress', function (event) {
				if (event.keyCode === 32) {
					event.preventDefault();
				}
			});
		}
	};
});

VolpayApp.directive('validAlphaNum', function () {
	return {
		require : '?ngModel',
		link : function (scope, element, attrs, ngModelCtrl) {
			if (!ngModelCtrl) {
				return;
			}

			ngModelCtrl.$parsers.push(function (val) {
				if (angular.isUndefined(val)) {
					var val = '';
				}

				var clean = val.replace(/^(\s\s?)*$/, '');
				var negativeCheck = clean.split('-');
				var decimalCheck = clean.split('.');
				if (!angular.isUndefined(negativeCheck[1])) {
					negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
					clean = negativeCheck[0] + '-' + negativeCheck[1];
					if (negativeCheck[0].length > 0) {
						clean = negativeCheck[0];
					}

				}

				if (!angular.isUndefined(decimalCheck[1])) {
					decimalCheck[1] = decimalCheck[1].slice(0, 2);
					clean = decimalCheck[0] + '.' + decimalCheck[1];
				}

				if (val !== clean) {
					ngModelCtrl.$setViewValue(clean);
					ngModelCtrl.$render();
				}
				return clean;
			});
/* 
			element.bind('keypress', function (event) {
				if (event.keyCode === 32) {
					event.preventDefault();
				}
			}); */
		}
	};
});

VolpayApp.directive('myDirective', function() {
        function link(scope, elem, attrs, ngModel) {
            ngModel.$parsers.push(function(viewValue) {
              var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9]*$/;
              // if view values matches regexp, update model value
              if (viewValue.match(reg)) {
                return viewValue;
              }
              // keep the model value as it is
              var transformedValue = ngModel.$modelValue;
              ngModel.$setViewValue(transformedValue);
              ngModel.$render();
              return transformedValue;
            });
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: link
        };      
    });
	
/* VolpayApp.directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
				//console.log(this,e)
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]); */

	

/* VolpayApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}); */

VolpayApp.directive('myDirective3', function() {
        function link(scope, elem, attrs, ngModel) {
            ngModel.$parsers.push(function(viewValue) {
                var reg = /^[^~`]*$/;
              // if view values matches regexp, update model value
              if (viewValue.match(reg)) {
                return viewValue;
              }
              // keep the model value as it is
              var transformedValue = ngModel.$modelValue;
              ngModel.$setViewValue(transformedValue);
              ngModel.$render();
              return transformedValue;
            });
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: link
        };      
    });

VolpayApp.directive('myDirective2', function() {
        function link(scope, elem, attrs, ngModel) {
            ngModel.$parsers.push(function(viewValue) {
                var reg = /^[^&|'"<>!*~]*$/;
              // if view values matches regexp, update model value
              if (viewValue.match(reg)) {
                return viewValue;
              }
              // keep the model value as it is
              var transformedValue = ngModel.$modelValue;
              ngModel.$setViewValue(transformedValue);
              ngModel.$render();
              return transformedValue;
            });
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: link
        };      
    });
	

VolpayApp.directive('checkEmailOnBlur', function(){
    var EMAIL_REGX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return {
  		restrict: 'A',
			require: 'ngModel',
			link: function(scope, elm, attr, ctrl) {

					if (attr.type === 'radio' || attr.type === 'checkbox') return;
					//elm.unbind('input').unbind('keydown').unbind('change');
					
					elm.bind('blur', function() {
						scope.$apply(function() {
							if (EMAIL_REGX.test(elm.val())) {
								ctrl.$setValidity('emails', true);
							} else {
								ctrl.$setValidity('emails', false);
							}
						});
					});
			}
    };

});	