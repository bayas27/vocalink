VolpayApp.controller('addrolesCtrl', function ($scope, $stateParams, $rootScope, $http, $location, $state, $timeout, GlobalService, editservice) {

	$scope.roleAdded = GlobalService.roleAdded;
	// $scope.madeChanges = true;
	$scope.role = {};
	
	$scope.toEdit_FromRoles = $state.params.input.FromRoles;

	$rootScope.$on("MyEvent", function (evt, data) {
		$("#changesLostModal").modal("show");
	})
	/*if($scope.roleAdded){
	$scope.alerts = [{
	type : 'success',
	msg : "Role added successfully"
	}];
	$scope.alertStyle =  alertSize().headHeight;
	$scope.alertWidth = alertSize().alertWidth;
	$timeout(callAtTimeout, 4000);
	GlobalService.roleAdded = false;

	}*/

	function callAtTimeout() {
		$('.alert-success').hide();
	}

	// $scope.role.Status=;

	$scope.addroles = function (role) {
		$rootScope.dataModified = false;
		role.IsSuperAdmin = true;

		role = cleantheinputdata(role);

		if ($scope.HttpMethod) {
			if ($scope.HttpMethod == "Created") {
				$scope.updateEntity = false
					$scope.method = 'POST'
					$scope.addRoleandForseSaveRole($scope.method, role)
			} else {
				$scope.updateEntity = true;
				$scope.method = 'PUT';
				$("#draftOverWriteModal").modal("show");

			}
		} else {
			$scope.method = 'POST';
			$scope.addRoleandForseSaveRole($scope.method, role)
		}

	}

	$scope.backupdataDraft = function (getRole) {
		$scope.takerolebckup = getRole;
	}

	$scope.addRoleandForseSaveRole = function (method, roledata) {
		console.log("From backup", method, roledata)
		var createUserObj = {
			url: BASEURL + RESTCALL.CreateRole,
			method: method,
			data: roledata,
		}

		$http(createUserObj).success(function (data, status) {

			GlobalService.roleAdded = true;
			$rootScope.roleAddedMesg = data;
			$rootScope.dataModified = false;
			$('.alert-danger').hide();
			$location.path('app/roles')
			$("#draftOverWriteModal").modal("hide");
			//$state.reload()

		}).error(function (data, status) {

			GlobalService.roleAdded = false;
			$scope.roleAdded = false;

			$("#draftOverWriteModal").modal("hide");

			//  $scope.alerts = [{
			//                type : 'danger',
			//                msg : data.error.message
			//            }];
			var _cstmMsg = data.error.message;
			//  console.log(_cstmMsg,"_cstmMsg")
			if (_cstmMsg.match(':') && _cstmMsg.split(':')[0] === 'The validation of uniqueness for field(s)') {

				if (_cstmMsg.split(':')[1].match('has failed')) {
					var _cstmMsg1 = $.trim(_cstmMsg.split(':')[1].split('has failed')[0])
						_cstmMsg = []
						var noPrimarykey = true;

					for (var _kee in $scope.role) {

						// console.log(_cstmMsg1,_kee,"_kee")

						if (_cstmMsg1.match(_kee)) {

							_cstmMsg.push((_kee) + ' : ' + $scope.role[_cstmMsg1])
							noPrimarykey = false;
						}

					}
					if (noPrimarykey) {
						for (var _kee in $scope.role) {

							if (_cstmMsg1.match(_kee) && _kee != 'Status') {
								_cstmMsg.push((_kee) + ' : ' + $scope.role[_cstmMsg1])
								noPrimarykey = true
							}

						}
					}
					if (_cstmMsg.length > 1) {
						_cstmMsg = _cstmMsg.toString() + ' already exists. Combination needs to be unique.'
					} else if (_cstmMsg.length == 1) {
						_cstmMsg = _cstmMsg.toString() + ' already exists. Value needs to be unique.'
					} else {
						_cstmMsg = data.error.message
					}
				} else {
					_cstmMsg = data.error.message
				}

			} else {
				_cstmMsg = data.error.message
			}

			$scope.alerts = [{
					type: 'danger',
					msg: _cstmMsg
				}
			];

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;

		})

	}

	$scope.resetAllDrafts = function () {
		$("#draftOverWriteModal").modal("hide");
		setTimeout(function () {
			$scope.updateEntity = false;
		}, 100)
	}

	$scope.updateRoles = function (role) {
		console.log($stateParams.input, role, "role")
		$rootScope.dataModified = false;
		role = cleantheinputdata(role)
			role.IsSuperAdmin = true;

		var changeMethod = ($stateParams.input.Operation == 'Clone') ? 'POST' : 'PUT';

		var roleObj = {
			url: BASEURL + RESTCALL.CreateRole,
			method: changeMethod,
			data: role,
			headers: {
				'Content-Type': 'application/json',
			}
		};
		if ($stateParams.input.Operation == 'Clone') {
			roleObj.headers.clone = $stateParams.input.RoleId;
		}

		console.log(roleObj, "roleObjz")

		$http(roleObj).success(function (data, status) {

			/*$scope.alerts = [{
			type : 'success',
			msg : data.responseMessage
			}
			];*/
			GlobalService.roleAdded = true;
			$rootScope.roleAddedMesg = data;
			$rootScope.dataModified = false;
			$('.alert-danger').hide();
			$location.path('app/roles')

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;
		}).error(function (data, status) {
			// $scope.alerts = [{
			// 		type : 'danger',
			// 		msg : data.error.message
			// 	}
			// ];

			var _cstmMsg = data.error.message;
			//  console.log(_cstmMsg,"_cstmMsg")
			if (_cstmMsg.match(':') && _cstmMsg.split(':')[0] === 'The validation of uniqueness for field(s)') {

				if (_cstmMsg.split(':')[1].match('has failed')) {
					var _cstmMsg1 = $.trim(_cstmMsg.split(':')[1].split('has failed')[0])
						_cstmMsg = []
						var noPrimarykey = true;

					for (var _kee in $scope.role) {

						//  console.log(_cstmMsg1,_kee,"_kee")

						if (_cstmMsg1.match(_kee)) {

							_cstmMsg.push((_kee) + ' : ' + $scope.role[_cstmMsg1])
							noPrimarykey = false;
						}

					}
					if (noPrimarykey) {
						for (var _kee in $scope.role) {

							if (_cstmMsg1.match(_kee) && _kee != 'Status') {
								_cstmMsg.push((_kee) + ' : ' + $scope.role[_cstmMsg1])
								noPrimarykey = true
							}

						}
					}
					if (_cstmMsg.length > 1) {
						_cstmMsg = _cstmMsg.toString() + ' already exists. Combination needs to be unique.'
					} else if (_cstmMsg.length == 1) {
						_cstmMsg = _cstmMsg.toString() + ' already exists. Value needs to be unique.'
					} else {
						_cstmMsg = data.error.message
					}
				} else {
					_cstmMsg = data.error.message
				}

			} else {
				_cstmMsg = data.error.message
			}

			$scope.alerts = [{
					type: 'danger',
					msg: _cstmMsg
				}
			];

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;
		})
	}

	$scope.activatePicker = function (sDate, eDate) {

		var start = new Date();
		var endDate = new Date();
		var end = new Date(new Date().setYear(start.getFullYear() + 1));
		
		if($scope.toEdit_FromRoles == true){
			console.log("enteredddd")
			var forceParseValue = false;
		}
		else{
			console.log($scope.toEdit,"$scope.toEdit")
		}
		
		
			$('#' + sDate).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			forceParse: forceParseValue
			
			

		}).on('changeDate', function (selected) {

			//$('#' + eDate).datepicker('setStartDate', new Date($(this).val()));

			start = new Date(selected.date.valueOf());

			start.setDate(start.getDate(new Date(selected.date.valueOf())));
			$('#' + eDate).datepicker('setStartDate', start);

		});

		$('#' + eDate).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			forceParse: forceParseValue
			
			

		}).on('changeDate', function (selected) {

			//$('#' + sDate).datepicker('setEndDate', new Date($(this).val()));


			endDate = new Date(selected.date.valueOf());

			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$('#' + sDate).datepicker('setEndDate', endDate);

		});
		$('#' + sDate).on('keyup', function (ev) {
			console.log("start Keyup", ev.keyCode)
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {

				//$dates.datepicker('setDate', null);
				$('#' + eDate).datepicker('setStartDate', new Date());
			}
		})

		$('#' + eDate).on('keyup', function (ev) {
			console.log("end Keyup")
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {

				//$dates.datepicker('setDate', null);
				$('#' + sDate).datepicker('setEndDate', null);
			}
		})

		//$('#dtControl').datepicker('setDate', nextMonth);
	}

	$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')


	$scope.datePlaceholderValue = "";
	$(document).ready(function () {
		

			$(".dateTypeKey").keypress(function (event) {
				console.log(event.keyCode, String.fromCharCode(keycode))
				var regex = /^[0-9]$/;
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (!(keycode == '8')) {
						console.log("keycode",keycode,String.fromCharCode(keycode))
					if (regex.test(String.fromCharCode(keycode))) {
						if ($(this).val().length == 4) {
							$(this).val($(this).val() + "-");
						} else if ($(this).val().length == 7) {
							$(this).val($(this).val() + "-");
						} else if ($(this).val().length >= 10) {
							event.preventDefault();
						}
					} else {
						console.log("keycode", keycode)
						event.preventDefault();
					}
				}
				else{
					console.log("keycode",keycode)
				return true;
				}

			});
	
		
		$(".dateTypeKey").focus(function () {

			$scope.datePlaceholderValue = $(this).attr('placeholder');
			$(this).attr('placeholder', 'YYYY-MM-DD');
		}).blur(function () {

			$(this).attr('placeholder', $scope.datePlaceholderValue);
		})

	});

	//var date = new Date();
	/* var startDate = new Date();
	var FromEndDate = new Date();
	var ToEndDate = new Date();
	ToEndDate.setDate(ToEndDate.getDate() + 365);
	var prev = null;
	$('#' + s1).datepicker({
	weekStart: 1,
	startDate: '1900-01-01',
	minDate: 1,
	endDate: FromEndDate,
	autoclose: true,
	format: 'yyyy-mm-dd'
	}).on('changeDate', function (ev) {
	console.log("onchange")
	$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
	startDate = new Date(ev.date.valueOf());
	startDate.setDate(startDate.getDate(new Date(ev.date.valueOf())));
	console.log("date", startDate)
	$('#' + s1).datepicker('setStartDate', startDate);
	$('#' + s1).datepicker('setEndDate', FromEndDate);
	}); */
	/* .on('dp.show', function (ev) {

	$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
	}).on('dp.hide', function (ev) {
	$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
	}); */

	//setTimeout(function(){

	//},100)

	/* $scope.triggerPicker = function (e) {
	$('.input-group-addon').on('click focus', function(e){
	$(this).prev().focus().click()
	});

	if ($(e.currentTarget).prev().is('.DatePicker')) {
	$scope.activatePicker($(e.currentTarget).prev(),'EffectiveFromDate','EffectiveTillDate');
	$('input[name=' + $(e.currentTarget).prev().attr('name') + ']').data("DateTimePicker").show();
	}
	};
	 */
	$timeout(function () {
		//  console.log($('.volpayIdFormValid').serializeArray(),$scope.TaskDetailsCreate)
		$rootScope.formArrayWithVal = $('#AddEditDatas').serializeArray();
	}, 100)

	$scope.GoBackFromRole = function () {
		$location.path('app/roles')
	}
	//console.log($stateParams)

	if ($stateParams) {
		if ($stateParams.input) {
			if ($stateParams.input.ToEditPage) {
				// console.log()
				$scope.getOprtion = $stateParams.input.Operation;
				$scope.toEdit = $stateParams.input.ToEditPage;
				$http.post(BASEURL + RESTCALL.RoleSpecificRead, {
					'RoleID': $stateParams.input.RoleId
				}).success(function (data) {
					$scope.role = data;

					if ($scope.getOprtion == 'Clone') {
						$scope.role.RoleID = '';
					}

				}).error(function (err) {
					$scope.alerts = [{
							type: 'danger',
							msg: err.error.message
						}
					];

				})

			} else {

				$scope.role = $stateParams.input.decrData;
				setTimeout(function () {
					$scope.HttpMethod = $stateParams.input.typeOfDraft
				}, 100)
			}
		}

	}

	// $scope.madeChanges = false;
	// $scope.listen = function() {
	// 	var Operation  = (!$scope.getOprtion) ? 'Add' : 'Edit';
	// 	if($stateParams.input)
	// 	{
	// 		if($stateParams.input.FromDraft)
	// 		{
	// 			Operation = 'Edit'
	// 		}
	// 	}
	// 	// console.log(Operation,"Operation_")
	// 	setTimeout(function(){
	// 		// console.log(Operation,"Operation",$scope.role,"$scope.role")
	// 		editservice.listen($scope, $scope.role, Operation, 'Roles');
	// 	}, 100)
	// }
	// $scope.listen();

	function isAnyFieldChanged()
   {
		setTimeout(function () {
			// console.log($("select"),$(".DatePicker"))
			$("input[type='text']").on("keydown", function (e) {
			
				if ($(e.currentTarget).val()) {
					$rootScope.dataModified = true;
					$scope.madeChanges = $rootScope.dataModified;
				}
			});

			$("select").change(function () {
				if ($(this).val()) {
					$rootScope.dataModified = true;
					$scope.madeChanges = $rootScope.dataModified;
				}
			})

			$(".DatePicker").on("change", function () {
				var selected = $(this).val();
				if (selected) {
					$rootScope.dataModified = true;
					$scope.madeChanges = $rootScope.dataModified;
				}
			});

		}, 200)

	}

	// })

	isAnyFieldChanged()

	$scope.gotoCancelFn = function () {
		$rootScope.dataModified = $scope.madeChanges;
		$scope.fromCancelClick = true;
		if (!$scope.madeChanges) {
			$scope.GoBackFromRole();
		}

	}

	$scope.gotoClickedPage = function () {
		if ($scope.fromCancelClick || $scope.breadCrumbClicked) {
			$scope.GoBackFromRole();
			$rootScope.dataModified = false;
		} else {
			$rootScope.$emit("MyEvent2", true);

		}
	}

	$scope.gotoShowAlert = function () {
		$scope.breadCrumbClicked = true;
		if ($scope.madeChanges) {
			$("#changesLostModal").modal("show");
		} else {
			$scope.GoBackFromRole();
		}
	}

	$scope.BackupDraft = '';
	$scope.primarykey = '';
	$scope.primaryKeyALert = false;

	$scope.takeBackupData = function (data) {
		$scope.BackupDraft = data;
	}

	$http.get(BASEURL + RESTCALL.RolesPermissionPK).success(function (data, status) {
		$scope.primarykey = data.responseMessage.split(',');

	}).error(function (data, status, headers, config) {
		$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
		];

		$scope.alertStyle = alertSize().headHeight;
		$scope.alertWidth = alertSize().alertWidth;
	});

	function checkPrimaryKeyValues(getDta) {

		if ($.isEmptyObject(getDta)) {
			$scope.primaryKeyALert = true;
		} else {
			$.each(getDta, function (key, val) {
				for (i = 0; i < $scope.primarykey.length; i++) {
					console.log(getDta, getDta[$scope.primarykey[i]], $scope.primarykey[i])
					if (!getDta[$scope.primarykey[i]]) {
						$scope.primaryKeyALert = true;
					}
				}
			})

		}

		// alert($scope.primaryKeyALert)
	}

	$scope.SaveAsDraft = function (formDatas1) {
		// console.log(formDatas1,"formDatas1")
		$scope.primaryKeyALert = false;
		checkPrimaryKeyValues(formDatas1);
		if ($scope.primaryKeyALert) {
			$scope.madeChanges = false;
			$("#changesLostModal").modal('show');
		} else {
			$scope.callingDraftSave(formDatas1)
		}

	}

	$scope.SaveAsModalDraft = function () {
		$scope.callingDraftSave($scope.BackupDraft)
	}

	$scope.callingDraftSave = function (formDatas) {
		$rootScope.dataModified = false;
		var backupdata = angular.copy(formDatas);
		draftdata = cleanalltheinputdataObj(backupdata);
		// console.log($scope.input,"$scope.input",formDatas)
		$http({
			method: 'POST',
			url: BASEURL + RESTCALL.CreateRole,
			data: draftdata,
			headers: {
				draft: true
			}

		}).then(function (response) {
			console.log(response)
			$rootScope.dataModified = false;
			if (response.data.Status === "Saved as Draft") {
				$scope.input = {
					'responseMessage': response.data.responseMessage
				}
				$state.go("app.roles", {
					input: $scope.input
				})
			}
		}, function (resperr) {
			if (resperr.data.error.message == "Draft Already Exists") {
				$("#draftOverWriteModal").modal("show");
			} else {
				$scope.alerts = [{
						type: 'danger',
						msg: resperr.data.error.message
					}
				]

			}

		})

	}

	$scope.forceSaveDraft = function () {
		$http({
			method: 'POST',
			url: BASEURL + RESTCALL.CreateRole,
			data: draftdata,
			headers: {
				draft: true,
				'Force-Save': true
			}

		}).then(function (response) {

			if (response.data.Status === "Draft Updated") {
				$("#draftOverWriteModal").modal("hide");
				$scope.input = {
					'responseMessage': response.data.responseMessage
				}
				$state.go("app.roles", {
					input: $scope.input
				})
			}
		}, function (resperr) {
			$("#draftOverWriteModal").modal("hide");
			$scope.alerts = [{
					type: 'danger',
					msg: resperr.data.error.message
				}
			]

		})
	}

	$(document).ready(function () {
		$('#changesLostModal').on('shown.bs.modal', function (e) {
			$('body').css('padding-right', 0)
		})
		$('#changesLostModal').on('hidden.bs.modal', function (e) {
			$scope.fromCancelClick = false;
			$scope.breadCrumbClicked = false;
		})
		$('#draftOverWriteModal').on('shown.bs.modal', function (e) {
			$('body').css('padding-right', 0)
		})
		$('#draftOverWriteModal').on('hidden.bs.modal', function (e) {

			setTimeout(function () {
				$scope.updateEntity = false;
			}, 100)

		})

	})

	$scope.multipleEmptySpace = function (e) {
		if ($.trim($(e.currentTarget).val()).length == 0) {
			$(e.currentTarget).val('');
		}
	}

	/*** To control Load more data ***/
	$(window).scroll(function () {
		$scope.widthOnScroll();
	});

	/*** To Maintain Alert Box width, Size, Position according to the screen size and on scroll effect ***/

	$scope.widthOnScroll = function () {
		var mq = window.matchMedia("(max-width: 991px)");
		var headHeight
		if (mq.matches) {
			headHeight = 0;
			$scope.alertWidth = $('.pageTitle').width();
		} else {
			$scope.alertWidth = $('.pageTitle').width();
			headHeight = $('.main-header').outerHeight(true) + 10;
		}
		$scope.alertStyle = headHeight;
	}

	$scope.widthOnScroll();

	$(window).resize(function () {

		$scope.$apply(function () {
			$scope.alertWidth = $('.tab-content').width();

		});

	});

})