VolpayApp.controller('uploadCtrl', ['$http','$scope', 'Upload', '$timeout', function ($http, $scope, Upload, $timeout, $interval) {
    $scope.options=true;
    $scope.cancelbtn=false;
    $scope.$watch('online', function(newStatus) {});
    /*upload function*/
    $scope.upload = function(file, errFiles) {
        $scope.errFile = errFiles && errFiles[0];
         /*file limit condition*/
         if(errFiles.length){
              $scope.success=false;
              $scope.alerts = [{
                    type: 'danger',
                    msg: "Your file exceeds the maximum size of 35 MB. Please try again."
                  }
                  ];
         }
         /*internet check*/
        if(!$scope.online){
            $scope.success=false;
            $scope.alerts = [{
                    type: 'danger',
                    msg: "No internet connection detected. Please connect and try again."
                  }
                  ]; 
        } else {
           /*upload operations*/
           if (file) {
             $scope.alerts = [];
            $scope.f = file;
            $scope.tfile=file.size;
            if($scope.tfile>=1000000){
              $scope.tfileM = $scope.tfile / Math.pow(1024,2);
              $scope.tfileM = $scope.tfileM.toFixed(3);
            } else {
              $scope.tfileK = $scope.tfile / Math.pow(1024,1);
              $scope.tfileK = $scope.tfileK.toFixed(3);
            }
            $scope.success=false;
            $scope.options=false;
            file.progress = 20;
            var fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onloadend = function (e) {
              var dataUrl = e.target.result;
              var base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length);
              $scope.temp=base64Data;
              var raw = atob(base64Data);
              var HEX = '';
              for ( i = 0; i < raw.length; i++ ) {
                var _hex = raw.charCodeAt(i).toString(16)
                HEX += (_hex.length==2?_hex:'0'+_hex);
              }
              var hex2 = HEX.toUpperCase();
              var reqFormat=new Object();
              reqFormat.folioID=file.name;
              reqFormat.schemeNm="BULK";
              reqFormat.fileContent=hex2;
              /*waiting for to make call or cancel*/
              setTimeout(function(){ 
                if($scope.cancelbtn==false){
                  $scope.cancelbtn=true;
                  $scope.exe(reqFormat);
                }  else{
                  $scope.options=true;
                  $scope.cancelbtn=false;
                   $scope.alerts = [{
                      type: 'success',
                      msg: "Upload canceled."
                    }
                    ];
                }
              }, 3000);
            }
            $scope.exe = function(reqFormat){
              /*call made progress*/
              file.progress = 70;
                $http.post( BASEURL + '/rest/v2/bpsui/file/upload',reqFormat).success(function (data, status) { 
                if(status==200){
                  /*completed progress*/
                  file.progress = 100;
                  setTimeout(function(){ 
                     $scope.options=true;
                     $scope.success=true;
                     $scope.cancelbtn=false;
                  }, 500);
                }
                }).error(function (data, status, headers, config) {
                  $scope.options=true;
                  $scope.cancelbtn=false;
                  $scope.success=false;
                  if(data.responseMessage){
                    $scope.alerts = [{
                    type: 'danger',
                    msg: data.responseMessage
                  }
                  ];
                  }else{
                    $scope.alerts = [{
                    type: 'danger',
                    msg: "There was a error uploading. If the problem persists please contact your Participant Scheme Administrator on 543210789"
                  }
                  ];
                  }       
                }); 
            }
            /*upload cancel function*/
            $scope.cancel = function(){
               $scope.cancelbtn=true;
               $scope.options=true;
            }
          }  
        }
    }
}]);