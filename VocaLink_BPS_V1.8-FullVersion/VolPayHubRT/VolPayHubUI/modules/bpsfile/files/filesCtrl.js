VolpayApp.controller('filesCtrl', function ($scope, $http, $location, $timeout, $filter, GlobalService) {
  $scope.restInputData={"sorts":[],"start":0,"count":20};
  $scope.BASEURL=BASEURL+'/ReportDownload';
  $scope.tableHead= [{
    'FieldName' : "Title",
    'Label'    : "Title"
  },
  {
    'FieldName' : "Size",
    'Label'    : "Size"
  },
  {
    'FieldName' : "Uploaded",
    'Label'    : "Uploaded"
  },
  {
    'FieldName' : "Downloaded",
    'Label'    : "Downloaded"
  }]

$http.get( BASEURL + '/rest/v2/bpsui/file/enable').success(function (data) {
      $scope.enablebtn = JSON.stringify(data);  
    }).error(function (data, status, headers, config) {
      $scope.alerts = [{
        type: 'danger',
        msg: data.error.message
      }
      ];
    });   

  /*initial call*/
  $scope.initial = function(req,type){
    if(type==undefined){
      req=$scope.restInputData;
      delete $scope.search;
      delete $scope.restInputData.filters;
      $scope.restInputData.start=0;
      $scope.restInputData.count=20;
      $scope.searchEmpty=false;
    }
    if($scope.search!=undefined && type=='normal'){
      delete $scope.search;  
      $scope.searchEmpty=false;
    }
    else if($scope.search && type=='sort'){
      $scope.restInputData.start=0;
      $scope.restInputData.count=20;
    }





    $http.post( BASEURL + '/rest/v2/bpsui/file/search',req).success(function (data) {
      $scope.fileLen=data.count;
      $scope.itemsPerPage = 5;
      if($scope.currentPage&&$scope.restInputData.start!=0){
          $scope.currentPage=$scope.currentPage+1;  
        }
        else{
          $scope.currentPage = 0;  
        }
      
      $scope.searchEmpty=false;
      if($scope.restInputData.start!=0 && type=='getmore'){   
        for (var i=0; i<data.FileDetails.length; i++){
          $scope.items.push(data.FileDetails[i]);
        } 
        $scope.hide=false;
      }
      else{
        $scope.items=data.FileDetails;
      }
    }).error(function (data, status, headers, config) {
      $scope.alerts = [{
        type: 'danger',
        msg: data.error.message
      }
      ];
    });       
  }
  $scope.initial($scope.restInputData,'normal');
  /*pagination getting more data*/
  $scope.getmore = function(){
    $scope.restInputData.start=$scope.restInputData.start+20;
    $scope.restInputData.count=$scope.restInputData.count;
    if($scope.restInputData.start < $scope.fileLen){
      $scope.initial($scope.restInputData,'getmore'); 

    }
  }
  $http.get( BASEURL + '/rest/v2/bpsui/file/title').success(function (data) {
    $scope.titles=data;
  }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.error.message
    }
    ];
  });
  $scope.searchTitle = function(val){
    $scope.sugges=true;
    if(val==''){
      delete $scope.search;
    }
    else{
      $scope.value = $filter('filter')($scope.titles, val);
    }
  }	
  $scope.navigate = function(n){
    $scope.currentPage=n-1;
  }
  $scope.selectValue = function(val){
    $scope.search=val;
    $scope.sugges=false;
  }
  /*search files*/
  $scope.searchFiles = function(val){
    if(val=="" || val==undefined){
       $scope.alerts = [{
        type: 'danger',
        msg: "Please enter search criteria"
      }]
    }
    else{
      $scope.restInputData={"filters":{"logicalOperator":"AND","groupLvl1":[{"logicalOperator":"AND","groupLvl2":[{"logicalOperator":"AND","groupLvl3":[{"logicalOperator":"AND","clauses":[{"columnName":"Title","operator":"=","value":val}]}]}]}]},"sorts":[],"start":0,"count":20}
      $http.post( BASEURL + '/rest/v2/bpsui/file/search',$scope.restInputData).success(function (data) {
      $scope.itemsPerPage = 5;
      $scope.currentPage = 0;  
      $scope.items=data.FileDetails;
      $scope.searchEmpty=true;
      $scope.fileLen=data.count;
    }).error(function (data, status, headers, config) {
      $scope.alerts = [{
        type: 'danger',
        msg: data.error.message
      }
      ];
    });
    }
  }
  /*sorting*/
  $scope.sorting = function(dat){
    $scope.loadMorecalled = false;
    var orderFlag = true;
    if(dat!=""){
      if('sorts' in $scope.restInputData && $scope.restInputData.sorts.length){
        for(k in $scope.restInputData.sorts){
          if($scope.restInputData.sorts[k].columnName == dat){
            if($scope.restInputData.sorts[k].sortOrder == 'Asc'){
              $('#'+dat+'_icon').attr('class','fa fa-long-arrow-down')
              $('#'+dat+'_Icon').attr('class','fa fa-caret-down')
              $scope.restInputData.sorts[k].sortOrder = 'Desc'
              orderFlag = false;
              break;
            }
            else{
              $scope.restInputData.sorts.splice(k,1);
              orderFlag = false;
              $('#'+dat+'_icon').attr('class','fa fa-minus fa-sm')
              $('#'+dat+'_Icon').removeAttr('class')
              $timeout(function(){
                $(".alert-danger").hide();
              },1000)
              break;
            }        
          }
        }
        if(orderFlag){
          $('#'+dat+'_icon').attr('class','fa fa-long-arrow-up')
          $('#'+dat+'_Icon').attr('class','fa fa-caret-up')
          $scope.restInputData.sorts.push({
            "columnName": dat,
            "sortOrder": 'Asc'
          })
        }
      }
      else{
        $('#'+dat+'_icon').attr('class','fa fa-long-arrow-up')
        $('#'+dat+'_Icon').attr('class','fa fa-caret-up')
        $scope.restInputData.sorts.push({
          "columnName": dat,
          "sortOrder": 'Asc'
        })
      }   
    }
    $scope.initial($scope.restInputData,'sort');
  }
  /*pagination*/
  $scope.range = function() {
    if($scope.items.length <= $scope.itemsPerPage){
      var rangeSize =1;
    }
    else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
      var rangeSize =2;
    }
    else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
      var rangeSize =3;
    }
    else{
      var rangeSize =4;
    }
    var ret = [];
    var start;
    start = $scope.currentPage;
    if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
    }
    for (var i=start; i<start+rangeSize; i++) {
      ret.push(i);
    }
    return ret;
  };
  $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
      $scope.currentPage--;
    }
  };
  $scope.prevRange = function() {
    if ($scope.currentPage >= 4) {
      $scope.currentPage=$scope.currentPage-4;
    }
  };
  $scope.nextRange = function() {
    var len =$scope.items.length/$scope.itemsPerPage;
    if (len-$scope.currentPage > 4) {
      $scope.currentPage=$scope.currentPage+4;
    }
  };
  $scope.prevPageDisabled = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
  };
  $scope.prevRangeDisabled = function() {
    return $scope.currentPage < 4 ? "disabled" : "";
  };
  $scope.nextRangeDisabled = function() {
    var len =$scope.items.length/$scope.itemsPerPage;
    return len-$scope.currentPage <= 4 ? "disabled" : "";
  };
  $scope.pageCount = function() {
    return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
  };
  $scope.nextPage = function() {
    if ($scope.currentPage < $scope.pageCount()) {
      $scope.currentPage++;
    }
  };
  $scope.nextPageDisabled = function() {
    if($scope.currentPage === $scope.pageCount()){
      if($scope.restInputData.start>=$scope.fileLen){
        $scope.hide=false;  
      }
      else if($scope.fileLen<$scope.restInputData.count){
        $scope.hide=false;
      }
      else{
         $scope.hide=true; 
      }
    }
    else{
      $scope.hide=false; 
    }
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
  };
});