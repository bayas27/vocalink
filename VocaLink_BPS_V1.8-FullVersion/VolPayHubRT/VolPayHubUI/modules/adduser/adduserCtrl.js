VolpayApp.controller('adduserCtrl', function ($scope, $http, $location, $state, $rootScope, LogoutService, GlobalService, $timeout, $stateParams, editservice) {

	$rootScope.$on("MyEvent", function (evt, data) {
		$("#changesLostModal").modal("show");
	})

	$scope.madeChanges = true;
	$scope.selectOptions = [];

	$scope.setInitVal = function (_query) {
		return $http({
			method: "GET",
			url: BASEURL + RESTCALL.CreateRole,
			params: _query
		}).then(function (response) {
			// $scope.selectOptions = response.data;
			// return $scope.selectOptions;
		})
	}

	$scope.setInitMultipleVal = function (_multiplequery) {
		$scope.selectMultOptions = [];
		$scope.pushAllRoleValues = [];
		$scope.selectOptions = [];
		return $http({
			method: "GET",
			url: BASEURL + RESTCALL.CreateRole,
			params: _multiplequery
		}).then(function (response) {
			for (i in response.data) {
				$scope.selectMultOptions.push(response.data[i])
				$scope.pushAllRoleValues.push(response.data[i])
			}

			$scope.selectOptions = removeDuplicates($scope.pushAllRoleValues, 'RoleID');
			//  $scope.selectOptions =  $scope.pushAllRoleValues;


			setTimeout(function () {

				//  $("[name=RoleID]").select2('destroy');
				// $("[name=RoleID]").val($scope.ArrVal);
				// $("[name=RoleID]").select2();
				$scope.remoteDataConfig()

			}, 100)
			return $scope.selectMultOptions;
		})
	}

	$scope.selectOptions = [];
	$scope.timezoneOptions = [];
	$scope.TimezoneVals = '';

	function triggerUnselect() {
		$(document).ready(function () {

			$('.clearRoleModelValue').on("select2:unselecting", function (e) {

				$scope.createData.UserRoleAssociation[$(e.currentTarget).attr('name').charAt($(e.currentTarget).attr('name').length - 1)][$(e.currentTarget).attr('customattr')] = '';
				//	console.log($scope.createData.UserRoleAssociation)
				// $scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')]
			})

		})
	}
	triggerUnselect();
	var optionValues = [];
	/* $('#timeZone').select2().on("select2:open", function (evt) {


	for (var jk in timeZoneDropValues.TimeZone) {

	var timezonevalues = angular.copy(timeZoneDropValues.TimeZone[jk].TimeZoneId)

	$(this).append('<option value=' + timezonevalues + '>' + timezonevalues + '</option>');

	console.log("data", this.value)

	}

	}); */

	/* var selectedCountry;
	$("#timeZone").select2().on("select2:unselecting", function (e) {
	// selectedCountry = $("#timeZone option:selected").val();
	selectedCountry = ($(this).val())
	//alert(selectedCountry)
	$("#timeZone option:selected").remove();
	$("#timeZone").val("");


	}).trigger('change');


	$("#timeZone").select2().on("select2:close", function (e) {

	var selectedCountry1 = $("#timeZone option:selected").val();
	console.log(selectedCountry1)
	$("#timeZone option:selected").remove();
	$(this).append('<option value='+selectedCountry1+'>'+selectedCountry1+'</option>')
	$(this).val(selectedCountry1)

	}); */

	//$scope.showMoreFieldOnCreateUser = Boolean(sessionStorage.showMoreFieldOnCreateUser);
	if ((sessionStorage.showMoreFieldOnCreateUser == true) || (sessionStorage.showMoreFieldOnCreateUser == 'true')) {
		$scope.showMoreFieldOnCreateUser = true;
	} else {
		$scope.showMoreFieldOnCreateUser = false;
	}

	

	if(configData.Authorization=='External'){
			$scope.showMoreFieldOnCreateUser = true;
			}
	

	$scope.userCreated = GlobalService.userCreated;

	if ($scope.userCreated) {
		console.log($rootScope.userCreateMsg)
		$scope.alerts = [{
				type: 'success',
				msg: $rootScope.userCreateMsg
			}
		];
		GlobalService.userCreated = false;
	}

	$timeout(callAtTimeout, 4000);

	function callAtTimeout() {
		$('.alert').hide();
	}

	//$scope.showMoreFieldOnCreateUser = false;


	if ($scope.showMoreFieldOnCreateUser) {
		// var createObj = {}
		// createObj.UserId = sessionStorage.UserID;

		/* $http.get(BASEURL + RESTCALL.CreateRole).success(function (data, status) {

		for (var i = 0; i < data.length; i++) {
		$scope.selectOptions.push({
		'label' : data[i].RoleName,
		'value' : data[i].RoleID
		})
		}

		sessionStorage.selectOptions = JSON.stringify($scope.selectOptions);

		//  console.log($scope.selectOptions)

		}).error(function (data, status, headers, config) {
		$scope.alerts = [{
		type : 'danger',
		msg : data.error.message
		}];

		$scope.alertStyle =  alertSize().headHeight;
		$scope.alertWidth = alertSize().alertWidth;
		});*/

	}

	/*$http.get(BASEURL+RESTCALL.TimezoneOptions).success(function (data, status) {

	for (var i = 0; i < data.TimeZone.length; i++) {
	$scope.timezoneOptions.push({
	'value' : data.TimeZone[i].TimeZoneId
	})

	}
	// console.log(data.TimeZone)

	}).error(function (data, status, headers, config) {
	$scope.alerts = [{
	type : 'danger',
	msg : data.error.message
	}];

	$scope.alertStyle =  alertSize().headHeight;
	$scope.alertWidth = alertSize().alertWidth;
	});*/

	$scope.current1 = true;
	$scope.hello = 50;

	$scope.createData = {
		UserRoleAssociation: []
	};

	if (!$scope.showMoreFieldOnCreateUser) {
		$scope.createData.UserRoleAssociation = [{
				'RoleID': 'Super Admin',
				'Status': 'ACTIVE'
			}
		]
	}

	$scope.ArrVal = [];
	function setMultiSelectRole() {
		for (let i in $scope.createData.UserRoleAssociation) {
			// console.log($scope.createData.UserRoleAssociation[i],"rerreer")
			$scope.ArrVal.push($scope.createData.UserRoleAssociation[i].RoleID)

		}
		$scope.createData.RoleID1 = $scope.ArrVal;
		// console.log($scope.ArrVal, "$scope.ArrVal")
	}

     $scope.Operation  = '';

    if($stateParams.input)
    {
        if($stateParams.input.FromDraft)
        {
             $scope.Operation  = 'Edit';
            $scope.createData =  $stateParams.input.decrData;
            setTimeout(function(){
                  
                     $scope.HttpMethod = $stateParams.input.typeOfDraft;
                       $('input[value="'+$scope.createData.RoleID+'"]').prop('checked', true)  
            },100)
           

			// console.log($scope.createData.UserRoleAssociation, $scope.createData, "check")
			// if(!$scope.createData.UserRoleAssociation)
			// {
			//   $scope.createData['UserRoleAssociation'] =  $scope.createData.UserRoleAssociation ? $scope.createData.UserRoleAssociation : [{}]
			//       console.log("inside")

			// }
			// else
			// {


			if ($scope.createData.UserRoleAssociation == undefined) {
				$scope.createData['UserRoleAssociation'] = []
			} else if (Array.isArray($scope.createData.UserRoleAssociation)) {
				for (i in $scope.createData.UserRoleAssociation) {
					if (Array.isArray($scope.createData.UserRoleAssociation[i])) {
						$scope.createData.UserRoleAssociation = $scope.createData.UserRoleAssociation[i]

					}

				}
				setMultiSelectRole();
				$scope.roleIdsaved = $scope.createData.RoleID;
				//    console.log($scope.createData.RoleID,"$scope.createData.RoleID")


			}

			_query = {
				search: $scope.createData.RoleID,
				start: 0,
				count: 100
			}
			$scope.setInitVal(_query);

			for (k in $scope.createData.UserRoleAssociation) {

				_queryvals = {
					search: $scope.createData.UserRoleAssociation[k].RoleID,
					start: 0,
					count: 100
				}

				$scope.setInitMultipleVal(_queryvals);
			}

			if ($scope.createData.TimeZone) {
				$scope.timezoneOptions.push($scope.createData.TimeZone)
			}
			// $('#timeZone').select2('destroy')
			// setTimeout(function(){
			//     $('#timeZone').find('option').remove('option')
			//  $('#timeZone').append('<option value="">--Select--</option>')
			//  for(var jk in $scope.timezoneOptions){

			//    $('#timeZone').append('<option value='+$scope.timezoneOptions[jk]+'>'+$scope.timezoneOptions[jk]+'</option>')

			// }

			// console.log("ti",$scope.cData.TimeZone)
			//   $('#timeZone').val($scope.createData.TimeZone)

			//    $('#timeZone').select2()
			// },100)



        }
    }
    else
    {
        $scope.Operation  = 'Add';
    }

	$scope.madeChanges = false;
	$scope.listen = function () {

		
		setTimeout(function(){
			// console.log(Operation,"Operation",$scope.role,"$scope.role")
			editservice.listen($scope,$scope.createData, $scope.Operation,'UserManagement');
		},100)
    }
    $scope.listen();

	$scope.emailValidate = function () {

		if ($("#AddUserMail").val() != "") {
			$scope.eFlag = emailValidation("#AddUserMail")
				if (!$scope.eFlag) {

					// $(window).scrollTop(20);
					// setTimeout(function(){
					//     callAtTimeout()
					// },3000)
					$scope.alertStyle = alertSize().headHeight;
					$scope.alertWidth = alertSize().alertWidth;
					$("#AddUserMail").focus()
					setTimeout(function () {
						$("#AddUserMail").val('')
						$scope.alerts = [{
								type: 'danger',
								msg: "Please Enter Valid Email Address"
							}
						];
					}, 200)
					return false;

				} else {
					$('.alert-danger').hide()
				}

		}

	}

	$scope.continue1 = function (createData) {
		createData.UserRoleAssociation = cleantheinputdata(createData.UserRoleAssociation)
			$scope.userDataRoleID = $(".radiobtns:checked").val();
		// console.log($scope.userDataRoleID,"heloo")
		//console.log(createData,"createData",$scope.createData.UserRoleAssociation[0].RoleID)
		if ($scope.showMoreFieldOnCreateUser) {
			for (i = 0; i < $scope.selectOptions.length; i++) {

				if ($scope.selectOptions[i].value == createData.RoleID) {
					$scope.RoleID = $scope.selectOptions[i].label;
					console.log($scope.RoleID)
				}

				/* if($scope.selectOptions[i].value == createData['ROLE_ID']){

				console.log($scope.selectOptions[i])
				// $scope.userData.ROLE_ID = $scope.selectOptions[i].label

				}*/

			}
		}

		if (createData.Password == $scope.ConfirmPW) {
			$(".alert-danger").alert("close");

			$scope.userData = createData;

			//console.log($scope.userData,createData)
			$scope.page = 2;
			$scope.current1 = false;
			$scope.current2 = true;
			$scope.hello = 100;
			$('.tab-pane').removeClass("active");
			$('#tab2').addClass("active");
			$('.tab_li').removeClass("active");
			$('#li_1').addClass("done");
			$('#li_2').addClass("active");
		} else {
			$scope.alerts = [{
					type: 'danger',
					msg: "Password and confirm password does not match."
				}
			];
			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;

		}
	}

	$scope.ConfirmCreateUser = function () {

		/*var createUserObj = {};
		createUserObj.UserId = sessionStorage.createUserLoginName;
		createUserObj.Data = btoa(JSON.stringify($scope.userData))*/
		delete $scope.userData.RoleID1;
		// console.log($(".radiobtns:checked").val(),"checkedchecked")
		$scope.userData.RoleID = $(".radiobtns:checked").val()
			$scope.userData.RoleID = "Super Admin";
		//$scope.userData.CRT_USER_ID = sessionStorage.createUserLoginName;
		//$scope.userData.CRT_TS = new Date().toISOString();
		$scope.userData.IsForceReset = true;
		$scope.userData.Status = 'ACTIVE';

		console.log($scope.userData)
		$http.post(BASEURL + RESTCALL.CreateNewUser, $scope.userData).success(function (response) {

			$rootScope.dataModified = false;
			$rootScope.alerts = [{
					"type": "success",
					"msg": response.responseMessage
				}
			]

			LogoutService.Logout(true);

			// GlobalService.userCreated = true;

			//LogoutService.Logout();

			/*if(configData.Authorization=='External'){
			window.location.href='/VolPayHubUI'+configData['401ErrorUrl'];
			}
			else{
			LogoutService.Logout();
			}*/
		}).error(function (data, status, headers, config) {
			console.log("1", data, status)
			$scope.alerts = [{
					type: 'danger',
					msg: data.error.message
				}
			];
		});

		$timeout(callAtTimeout, 4000);

	}

	$scope.backupRoleId1 = '';
	$scope.ConfirmCreateUserbySuperAdmin = function (userData) {

		//$scope.userData.CRT_USER_ID = sessionStorage.createUserLoginName;
		//$scope.userData.CRT_TS = new Date().toISOString();
		// $scope.userData = removeEmptyValueKeys($scope.userData)
		/* var createUserObj = {};
		createUserObj.UserId = sessionStorage.createUserLoginName;
		createUserObj.Data = btoa(JSON.stringify($scope.userData))*/
		//console.log($scope.userData)
		$scope.backupRoleId1 = angular.copy(userData.RoleID1);
		delete $scope.userData.RoleID1;
		$scope.userData.RoleID = $(".radiobtns:checked").val()
			userData = cleantheinputdata(userData)
			$scope.userData.IsForceReset = Boolean($scope.userData.IsForceReset);
		$scope.userData.Status = $scope.userData.Status;

		// $scope.method = $scope.HttpMethod
		if ($scope.HttpMethod) {
			if ($scope.HttpMethod == "Created") {
				$scope.updateEntity = false;
				$scope.method = 'POST';
				$scope.addUserandForseSaveUser($scope.method, $scope.userData)
			} else {
				$scope.updateEntity = true;
				$scope.method = 'PUT';
				$("#draftOverWriteModal").modal("show");
			}
		} else {
			$scope.method = 'POST';
			$scope.addUserandForseSaveUser($scope.method, $scope.userData)
		}

	}

	$scope.backupdataDraft = function (getUser) {
		$scope.takeuserbckup = getUser;
		$scope.takeuserbckup.IsForceReset = typeof($scope.takeuserbckup.IsForceReset) === 'boolean' ? String($scope.takeuserbckup.IsForceReset) : $scope.takeuserbckup.IsForceReset
		// console.log($scope.takeuserbckup,'IsForceReset')
	}

	$scope.addUserandForseSaveUser = function (method, userdata) {
		
		var createUserObj = {
			url: BASEURL + RESTCALL.CreateNewUser,
			method: method,
			data: userdata,
		}

		$http(createUserObj).success(function (data, status, headers, config) {

			// GlobalService.userCreated = true;
			$rootScope.dataModified = false;
			$rootScope.userCreateMsg = data.responseMessage
				//  $state.reload();

				$scope.input = {
				'responseMessage': data.responseMessage
			};
			$state.go('app.users', {
				input: $scope.input
			})

			$("#draftOverWriteModal").modal("hide");

		}).error(function (data, status, headers, config) {
			$scope.createData.RoleID1 = $scope.backupRoleId1;

			$("#draftOverWriteModal").modal("hide");
			//   $scope.alerts = [{
			//           type : 'danger',
			//           msg : data.error.message
			//       }];

			// $scope.alertStyle =  alertSize().headHeight;
			//  $scope.alertWidth = alertSize().alertWidth;
			var _cstmMsg = data.error.message;
			console.log(_cstmMsg, "_cstmMsg")
			if (_cstmMsg.match(':') && _cstmMsg.split(':')[0] === 'The validation of uniqueness for field(s)') {

				if (_cstmMsg.split(':')[1].match('has failed')) {
					var _cstmMsg1 = $.trim(_cstmMsg.split(':')[1].split('has failed')[0])
						_cstmMsg = []
						var noPrimarykey = true;

					for (var _kee in $scope.userData) {
						//console.log('1')
						console.log(_cstmMsg1, _kee, "_kee")

						if (_cstmMsg1.match(_kee)) {
							// console.log(_kee,$scope.userData[_cstmMsg1])
							_cstmMsg.push((_kee) + ' : ' + $scope.userData[_cstmMsg1])
							noPrimarykey = false;
						}
						// console.log(_cstmMsg,"_cstmMsg")

					}
					if (noPrimarykey) {
						for (var _kee in $scope.userData) {
							//if(_cstmMsg1.match(_kee)){
							if (_cstmMsg1.match(_kee) && _kee != 'Status') {
								_cstmMsg.push((_kee) + ' : ' + $scope.userData[_cstmMsg1])
								noPrimarykey = true
							}
							//console.log(_cstmMsg1,_kee)
						}
					}
					if (_cstmMsg.length > 1) {
						_cstmMsg = _cstmMsg.toString() + ' already exists. Combination needs to be unique.'
					} else if (_cstmMsg.length == 1) {
						_cstmMsg = _cstmMsg.toString() + ' already exists. Value needs to be unique.'
					} else {
						_cstmMsg = data.error.message
					}
				} else {
					_cstmMsg = data.error.message
				}
				//_cstmMsg = 'Entered '+_cstmMsg.split(':')[1].split(' ')[1]+' already exists.'
				//console.log(_cstmMsg, error.data.error.message)
			} else {
				_cstmMsg = data.error.message
			}

			$scope.alerts = [{
					type: 'danger',
					msg: _cstmMsg
				}
			];

		});

	}

	$scope.resetAllDrafts = function () {
		$("#draftOverWriteModal").modal("hide");
		setTimeout(function () {
			$scope.updateEntity = false;
		}, 100)
	}

	$scope.back2to1 = function () {

		$('.alert-danger').alert('close');

		$scope.hello = 50;
		$scope.current1 = true;
		$scope.current2 = false;
		$('.tab-pane').removeClass("active");
		$('#tab1').addClass("active");

		$('.tab_li').removeClass("done active");
		$('#li_1').addClass("active");
		$scope.page = 1;

		$('#tab1').addClass('slideInLeft')

		$scope.createData = $scope.userData;

	}

	$scope.multipleEmptySpace = function (e) {
		if ($.trim($(e.currentTarget).val()).length == 0) {
			$(e.currentTarget).val('');
		}
	}

	$scope.validatePassWord = function (val, e) {

		if (val) {

			/*  var PWObj = {};
			PWObj.UserId = sessionStorage.UserID;
			PWObj.Data = btoa(JSON.stringify({'UserId':sessionStorage.UserID,'Password':val}))*/

			$http.post(BASEURL + RESTCALL.ValidatePW, {
				'UserId': sessionStorage.UserID,
				'Password': val
			}).success(function (data, status, headers, config) {

				$('.alert-danger').alert('close');

			}).error(function (data, status, headers, config) {

				$scope.alerts = [{
						type: 'danger',
						msg: data.error.message
					}
				];

				$(e.currentTarget).val('');
				$scope.alertStyle = alertSize().headHeight;
				$scope.alertWidth = alertSize().alertWidth;
			});

		} else {
			$('.alert-danger').alert('close');
		}

	}
	$scope.activatePicker = function (sDate, eDate) {
	//console.log("e",e.currentTarget.id,$(e.currentTarget).attr('id'))
	//var dateId = $(e.currentTarget).attr('id');
	$(document).ready(function(){
	
		var start = new Date();
		var endDate = new Date();
		var end = new Date(new Date().setYear(start.getFullYear() + 1));
	
	$('#'+sDate).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			todayHighlight : true,
			format: 'yyyy-mm-dd'
			

		}).on('changeDate', function (selected) {
			
			start = new Date(selected.date.valueOf());

			start.setDate(start.getDate(new Date(selected.date.valueOf())));
			$('#' + eDate).datepicker('setStartDate', start);
			
			

		});
			$('#' + eDate).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
	format: 'yyyy-mm-dd',
	todayHighlight : true
	
		}).on('changeDate', function (selected) {
			endDate = new Date(selected.date.valueOf());

			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$('#' + sDate).datepicker('setEndDate', endDate);

			//$('#' + sDate).datepicker('setEndDate', new Date($(this).val()));
		});
		
		
		$('#' + sDate).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + eDate).datepicker('setStartDate', new Date());
			}
		})
		$('#' + eDate).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + sDate).datepicker('setEndDate', null);
			}
		})
		
	})
	}
	$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')
	
	
	 $scope.triggerPicker = function (e) {
     console.log($(e.currentTarget).prev())
		if ($(e.currentTarget).prev().is('.DatePicker')) {
			$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate');
		//$(e.currentTarget).prev().data("DateTimePicker").show();
		$(e.currentTarget).prev().datepicker("show");
		}
	};
	
	
	$scope.datePlaceholderValue = "";
		function regexCheck() {
	$(document).ready(function () {
	
console.log("dateTypeKeydateTypeKey")
			$(".dateTypeKey").keypress(function (event) {
				//console.log(event.keyCode, String.fromCharCode(keycode))
				var regex = /^[0-9]$/;
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (!(keycode == '8')) {
					//	console.log(String.fromCharCode(keycode))
					if (regex.test(String.fromCharCode(keycode))) {
						if ($(this).val().length == 4) {
							$(this).val($(this).val() + "-");
						} else if ($(this).val().length == 7) {
							$(this).val($(this).val() + "-");
						} else if ($(this).val().length >= 10) {
							event.preventDefault();
						}
					} else {
						event.preventDefault();
					}
				}

			});
		
	
		
	

	});
		}
		function FocusBlur(){
		$(document).ready(function () {
	
	$(".dateTypeKey").focus(function () {

			$scope.datePlaceholderValue = $(this).attr('placeholder');
				
			$(this).attr('placeholder', 'YYYY-MM-DD');
		}).blur(function () {
	
			$(this).attr('placeholder', $scope.datePlaceholderValue);
		})
		})
	
		}
		
			regexCheck();
			FocusBlur();
	
		//var prev = null;
		/* $('.DatePicker').datetimepicker({
			format: "YYYY-MM-DD",
			useCurrent: false,
			showClear: true
		}).on('dp.change', function (ev) {
			// console.log($(ev.currentTarget).val(),"$(ev.currentTarget).val()")
			if ($(ev.currentTarget).attr('roledateattr') && $(ev.currentTarget).val()) {
				$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')] = $(ev.currentTarget).val()

			}

		}).on('dp.show', function (ev) {

			// console.log($(ev.currentTarget).val(),"$(ev.currentTarget).val()")
			// if($(ev.currentTarget).attr('roledateattr') && $(ev.currentTarget).val())
			// {}
			if ($(ev.currentTarget).attr('roledateattr')) {
				$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')] = $(ev.currentTarget).val();

			}

		}).on('dp.hide', function (ev) {
			// console.log($(ev.currentTarget).val(),"$(ev.currentTarget).val()")
			if ($(ev.currentTarget).attr('roledateattr') && $(ev.currentTarget).val()) {

				$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')] = $(ev.currentTarget).val();

			}
		}); */
	
	 /* $scope.triggerPicker = function (e) {

		if ($(e.currentTarget).prev().is('.DatePicker')) {
			$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate');
		
		}
	}; */ 

	/*  $scope.setInitMultipleVal = function(_multiplequery){
	return $http({
	method : "GET",
	url : BASEURL + RESTCALL.CreateRole,
	params : _multiplequery
	}).then(function (response) {
	for(i in response.data){
	$scope.selectOptions.push(response.data[i])
	}
	return $scope.selectOptions;
	})
	}*/

	setTimeout(function () {

		$('#RoleId').on('select2:select', function (e) {
			var data = e.params.data;
			$scope.submitted = false;
			//   console.log($(e.currentTarget).find("option:selected:last"))
			$(e.currentTarget).find("option:selected:last").remove();
			$timeout(function () {
				$scope.remoteDataConfig();
				//triggerSelectDrops();
				$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')

			}, 100)
			$scope.createData.UserRoleAssociation.push({
				'RoleID': data.id
			})
			for (i in $scope.createData.UserRoleAssociation) {
				_set_queryvals = {
					search: $scope.createData.UserRoleAssociation[i].RoleID,
					start: 0,
					count: 100
				}

				$scope.setInitMultipleVal(_set_queryvals);

			}
		});

	}, 100)

	setTimeout(function () {
		$('#RoleId').on('select2:unselect', function (e) {
			var data = e.params.data;
			console.log(data, "__")
			for (i in $scope.createData.UserRoleAssociation) {
				if ($scope.createData.UserRoleAssociation[i].RoleID == data.id) {

					$scope.createData.UserRoleAssociation.splice(i, 1);
				}
			}

			for (k in $scope.createData.UserRoleAssociation) {
				_set_queryvals = {
					search: $scope.createData.UserRoleAssociation[k].RoleID,
					start: 0,
					count: 100
				}

				$scope.setInitMultipleVal(_set_queryvals);
			}
			$timeout(function () {
				$scope.remoteDataConfig();
				//triggerSelectDrops();
				$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')

			}, 100)

		});

	}, 50)

	$scope.createDynamicSubsection = function (RoleArr) {
		var roleFlag = false;
		for (s in RoleArr) {
			if (RoleArr[s] == $scope.createData.RoleID) {
				roleFlag = true;
			}
		}
		// console.log(roleFlag,"roleFlag")
		setTimeout(function () {
			if (!roleFlag) {
				$($('.radiobtns')[0]).prop('checked', true);

			} else {
				$('input[value="' + $scope.createData.RoleID + '"]').prop('checked', true)
			}
		}, 100)

	}

	$scope.addNewRoleSection = function (index, e) {
		$scope.submitted = false;
		var Objkey = [];

		if ($scope.createData.UserRoleAssociation[index] && $scope.createData.UserRoleAssociation[index].$$hashKey) {
			delete $scope.createData.UserRoleAssociation[index].$$hashKey;

		}

		Object.keys($scope.createData.UserRoleAssociation[index]).forEach(function (key, value) {
			console.log($scope.createData.UserRoleAssociation[index], "hi")
			if ($scope.createData.UserRoleAssociation[index][key] != '' && $scope.createData.UserRoleAssociation[index][key] != undefined) {
				//console.log($scope.createData.UserRoleAssociation[index][key],key)
				Objkey.push(key)

				// console.log("with value",$scope.createData.UserRoleAssociation[index][key],aa)
			}
		})
		console.log(Objkey)
		if (Objkey.length >= 3) {
			// $('.setDynamicWidth').css({'height':'102px'})

			// $('.setDynamicWidth').animate({
			// 	scrollTop : ($('.setDynamicWidth').offset().top) - $('#'+index+'').offset().top
			// }, 'slow')

			$('.setDynamicWidth').animate({
				scrollTop: ($("#" + index).outerHeight() * (index + 1)) + 'px'
			});

			$scope.createData.UserRoleAssociation.push({});

			$timeout(function () {
				$scope.remoteDataConfig();
				//triggerSelectDrops();
				$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')
				triggerUnselect()
			}, 500)
		}

	}

	$scope.removeCurrentRoleSection = function (index, e) {

		$scope.createData.UserRoleAssociation.splice(index, 1);
		$('#RoleId' + index).select2('destroy')
		$('#RoleId' + index).find('option').remove('option')
		for (var jk in $scope.createData.UserRoleAssociation) {
			//  $('#RoleId'+index).append('<option value='+$scope.createData.UserRoleAssociation[jk].RoleID+'>'+$scope.createData.UserRoleAssociation[jk].RoleID+'</option>')
		}

		if ($scope.createData.UserRoleAssociation[index]) {

			if ($scope.createData.UserRoleAssociation[index].RoleID) {
				for (i in $scope.createData.UserRoleAssociation) {
					_set_queryvals = {
						search: $scope.createData.UserRoleAssociation[i].RoleID,
						start: 0,
						count: 100
					}
					//console.log($scope.createData.UserRoleAssociation,$scope.createData.UserRoleAssociation[index].RoleID,"index")
					$scope.setInitMultipleVal(_set_queryvals);
					$('#RoleId' + index).val($scope.createData.UserRoleAssociation[index].RoleID)
				}

			} else {
				setTimeout(function () {

					if ($('#RoleId' + index).find('option').val().indexOf('undefined') != -1) {
						$('#RoleId' + index).find('option').remove('option')
					}
				}, 100)
			}
		}
		$('#RoleId' + index).select2({
			ajax: {
				url: BASEURL + RESTCALL.CreateRole,
				headers: {
					"Authorization": "SessionToken:" + sessionStorage.SessionToken,
					"source-indicator": configData.SourceIndicator,
					"Content-Type": "application/json"
				},
				dataType: 'json',
				delay: 250,
				xhrFields: {
					withCredentials: true
				},
				beforeSend: function (xhr) {
					xhr.setRequestHeader('Cookie', document.cookie),
					xhr.withCredentials = true
				},
				crossDomain: true,
				data: function (params) {
					// console.log(params,"params")
					var query = {
						start: params.page * $scope.limit ? params.page * $scope.limit : 0,
						count: $scope.limit
					}

					if (params.term) {
						query = {
							search: params.term,
							start: params.page * $scope.limit ? params.page * $scope.limit : 0,
							count: $scope.limit
						};
					}
					return query;
				},
				processResults: function (data, params) {
					params.page = params.page ? params.page : 0;
					var myarr = []

					for (j in data) {
						myarr.push({
							'id': data[j].RoleID,
							'text': data[j].RoleName + '(' + data[j].RoleID + ')'
						})
					}

					return {
						results: myarr,
						pagination: {
							more: data.length >= $scope.limit
						}
					};

				},
				cache: true
			},
			placeholder: 'Select an option',
			minimumInputLength: 0,
			allowClear: true

		})
		//triggerSelectDrops();
		 $timeout(function () {
			$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')
		}, 500) 

	}

	
$(document).ready(function(){
	 $scope.activatePickerSubsec = function (sDate,eDate, index) {
	
		var start = new Date();
		var endDate = new Date();
		var end = new Date(new Date().setYear(start.getFullYear() + 1));
		console.log(start,sDate,eDate)
		
			console.log($('#'+sDate+index),$('#'+eDate+index))
			
			$('#'+sDate+index).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			todayHighlight : true,
			format: 'yyyy-mm-dd',
			useCurrent: false,
			showClear: true
			

		}).on('changeDate', function (selected) {
			
			//$('#' + eDate+index).datepicker('setStartDate', new Date($(this).val()));
			start = new Date(selected.date.valueOf());

			start.setDate(start.getDate(new Date(selected.date.valueOf())));
			$('#' + eDate).datepicker('setStartDate', start);

		});
		
		$('#'+eDate+index).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			todayHighlight : true,
			format: 'yyyy-mm-dd',
			useCurrent: false,
			showClear: true
			

		}).on('changeDate', function (selected) {
				console.log("eDate",new Date($(this).val()),$(this).val())
			//$('#' + sDate+index).datepicker('setEndDate', new Date($(this).val()));
			endDate = new Date(selected.date.valueOf());

			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$('#' + sDate).datepicker('setEndDate', endDate);

		});
		
		$('#' + sDate+index).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + eDate+index).datepicker('setStartDate', new Date());
			}
		})
		$('#' + eDate+index).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + sDate+index).datepicker('setEndDate', null);
			}
		})
	
	setTimeout(function(){
			regexCheck();
			//FocusBlur();
			
	},100)
		
				
			
		}
});
		
	 $scope.activatePick = function(e,index){
	
				console.log("document",$(e.currentTarget).attr('id'));
				
		$scope.activatePickerSubsec('Subsec_EffectiveFromDate','Subsec_EffectiveTillDate',index);
		if($(e.currentTarget).attr('id') == 'Subsec_EffectiveFromDate'+index){
					console.log("from")
					$('#Subsec_EffectiveFromDate'+index).datepicker('show');
				}
				else{
					$('#Subsec_EffectiveTillDate'+index).datepicker('show');
				}
				
			
		}
		
		
		
		$scope.triggerPickerSubSec = function (e,index) {

		console.log($(e.currentTarget).prev())
		$scope.activatePickerSubsec('Subsec_EffectiveFromDate','Subsec_EffectiveTillDate',index);
		
		$(e.currentTarget).prev().datepicker("show");
		
		
	}; 
	
	//$scope.activatePickerSubsec('Subsec_EffectiveFromDate','Subsec_EffectiveTillDate');
		/* var prev = null;
		$('.DatePicker').datetimepicker({
			format: "YYYY-MM-DD",
			useCurrent: false,
			showClear: true
		}).on('dp.change', function (ev) {
			// console.log($(ev.currentTarget).val(),"$(ev.currentTarget).val()")
			if ($(ev.currentTarget).attr('roleattr')) {
				$scope.createData.UserRoleAssociation[index][$(ev.currentTarget).attr('roleattr')] = $(ev.currentTarget).val();

			}
			// else
			// {
			//     console.log($scope.createData.UserRoleAssociation[index])
			// }

		}).on('dp.show', function (ev) {

			$(ev.currentTarget).parent().parent().parent().parent().parent().parent().css({
				"overflow-y": ""
			});

			if ($(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().length > 1 && $(ev.currentTarget).attr('roleattr')) {

				$(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().each(function () {
					if ($(this).is("#" + $(ev.currentTarget).parent().parent().parent().parent().parent().attr('id'))) {}
					else {
						$(this).css({
							"display": "none"
						});
					}
				})
			}

			//    }


		}).on('dp.hide', function (ev) {
			// console.log($(ev.currentTarget).val(),"$(ev.currentTarget).val()")
			$(ev.currentTarget).closest('.setDynamicWidth').css({
				"overflow-y": "auto"
			});
			$(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().each(function () {
				$(this).css({
					"display": ""
				});
			})

			if ($(ev.currentTarget).attr('roleattr') && $(ev.currentTarget).val()) {

				$scope.createData.UserRoleAssociation[index][$(ev.currentTarget).attr('roleattr')] = $(ev.currentTarget).val()

			}
			//  else
			// {
			//     console.log($scope.createData.UserRoleAssociation[index])
			// }

		});

	}

	$scope.triggerPickerSubSec = function (e, index) {

		if ($(e.currentTarget).prev().is('.DatePicker')) {
			$scope.activatePickerSubsec($(e.currentTarget).prev(), index);
			$($(e.currentTarget).prev()).data("DateTimePicker").show();
		}
	};

	/*** To Maintain Alert Box width, Size, Position according to the screen size and on scroll effect ***/

	$scope.widthOnScroll = function () {
		var mq = window.matchMedia("(max-width: 991px)");
		var headHeight
		if (mq.matches) {
			headHeight = 0;
			$scope.alertWidth = $('.pageTitle').width();
		} else {
			$scope.alertWidth = $('.pageTitle').width();
			headHeight = $('.main-header').outerHeight(true) + 10;
		}
		$scope.alertStyle = headHeight;
	}

	$scope.widthOnScroll();

	function triggerSelectDrops() {
		$scope.select2Arr = ["IsForceReset", "Status", "TimeZone", "Subsec_Status"]
		$(document).ready(function () {

			for (var i in $scope.select2Arr) {
				// console.log("dssgf", $scope.select2Arr[i])
				$("select[name=" + $scope.select2Arr[i] + "]").select2({
					placeholder: 'Select an option',
					minimumInputLength: 0,
					allowClear: true

				})

				if ($scope.select2Arr[i] == 'TimeZone') {

					for (var jk in timeZoneDropValues.TimeZone) {

						var timezonevalues = angular.copy(timeZoneDropValues.TimeZone[jk].TimeZoneId)

							$('#timeZone').append('<option value=' + timezonevalues + '>' + timezonevalues + '</option>');

						// console.log("data")

					}
				}
			}
		})

	}
	setTimeout(function () {
		triggerSelectDrops();
	}, 1000);

	$(window).resize(function () {

		$scope.$apply(function () {
			$scope.alertWidth = $('.tab-content').width();

		});

	});

	$scope.limit = 100;
	$(document).ready(function () {
		$scope.remoteDataConfig = function () {
			//    alert()
			$(".appendRoleId").select2({
				ajax: {
					url: BASEURL + RESTCALL.CreateRole,
					headers: {
						"Authorization": "SessionToken:" + sessionStorage.SessionToken,
						"source-indicator": configData.SourceIndicator,
						"Content-Type": "application/json"
					},
					dataType: 'json',
					delay: 250,
					xhrFields: {
						withCredentials: true
					},
					beforeSend: function (xhr) {
						xhr.setRequestHeader('Cookie', document.cookie),
						xhr.withCredentials = true
					},
					crossDomain: true,
					data: function (params) {
						// console.log(params,"params")
						var query = {
							start: params.page * $scope.limit ? params.page * $scope.limit : 0,
							count: $scope.limit
						}

						if (params.term) {
							query = {
								search: params.term,
								start: params.page * $scope.limit ? params.page * $scope.limit : 0,
								count: $scope.limit
							};
						}
						return query;
					},
					processResults: function (data, params) {
						params.page = params.page ? params.page : 0;
						var myarr = []

						for (j in data) {
							myarr.push({
								'id': data[j].RoleID,
								'text': data[j].RoleName + '(' + data[j].RoleID + ')'
							})
						}

						return {
							results: myarr,
							pagination: {
								more: data.length >= $scope.limit
							}
						};

					},
					cache: true
				},
				placeholder: 'Select an option',
				minimumInputLength: 0,
				allowClear: true

			})
		}

		$scope.remoteDataConfig()
	});

	$scope.checkMandatory = function (index, field) {

		if (index != 0) {
			for (var keyName in $scope.createData.UserRoleAssociation[index]) {
				if (keyName != field) {
					if ($scope.createData.UserRoleAssociation[index][keyName] != '') {
						//console.log("Status: "+$scope.createData.UserRoleAssociation[index][keyName])
						return true;
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}
	/*
	$(document).ready(function() {
	$select.on('select2-removed', function(event) {
	console.log("success");
	});
	}); */

	/*$(document).ready(function() {
	for(var i=0;i<$scope.createData.UserRoleAssociation.length;i++) {
	$('#select2-RoleId'+i+'-container .select2-selection__clear').click(function(){
	$scope.createData.UserRoleAssociation[i].RoleID = '';
	})
	}


	});

	$(document).ready(function() {
	for(var i=0;i<$scope.createData.UserRoleAssociation.length;i++) {
	$('#select2-Status'+i+'-container .select2-selection__clear').click(function(){
	$scope.createData.UserRoleAssociation[i].Status = '';

	})
	}
	});*/

	$scope.gotoCancelFn = function () {
		$rootScope.dataModified = $scope.madeChanges;
		$scope.fromCancelClick = true;
		if (!$scope.madeChanges) {
			$scope.GoBackFromUser();
		}

	}

	$scope.gotoClickedPage = function () {
		if ($scope.fromCancelClick) {
			$scope.GoBackFromUser();
			$rootScope.dataModified = false;
		} else {
			$rootScope.$emit("MyEvent2", true);

		}
	}

	$scope.GoBackFromUser = function () {
		$state.go('app.users')

	}

	$scope.BackupDraft = ''
		$scope.takeBackupData = function (data) {
		$scope.BackupDraft = data;
	}

	$scope.primarykey = '';
	$scope.primaryKeyALert = false;
	var draftdata = '';
	$http.get(BASEURL + RESTCALL.UserManagementPK).success(function (data, status) {
		$scope.primarykey = data.responseMessage.split(',');

	}).error(function (data, status, headers, config) {
		$scope.alerts = [{
				type: 'danger',
				msg: data.error.message
			}
		];

		$scope.alertStyle = alertSize().headHeight;
		$scope.alertWidth = alertSize().alertWidth;
	});

	function checkPrimaryKeyValues(getDta) {
		if ($.isEmptyObject(getDta)) {
			$scope.primaryKeyALert = true;
		} else {
			$.each(getDta, function (key, val) {
				for (i = 0; i < $scope.primarykey.length; i++) {
					if (!getDta[$scope.primarykey[i]]) {
						$scope.primaryKeyALert = true;
					}
				}
			})

		}

		// alert($scope.primaryKeyALert)
	}

	$scope.SaveAsDraft = function (formDatas1) {

		$scope.primaryKeyALert = false;
		checkPrimaryKeyValues(formDatas1);
		if ($scope.primaryKeyALert) {
			$scope.madeChanges = false;
			$("#changesLostModal").modal('show');
		} else {
			$scope.callingDraftSave(formDatas1)
		}

	}

	$scope.SaveAsModalDraft = function () {
		$scope.callingDraftSave($scope.BackupDraft)
	}

	$scope.callingDraftSave = function (formDatas) {
		$rootScope.dataModified = false;
		var backupdata = angular.copy(formDatas);
		draftdata = cleanalltheinputdataObj(backupdata);
		delete draftdata.RoleID1;
		draftdata.RoleID = $(".radiobtns:checked").val();
		// console.log($scope.input,"$scope.input",formDatas)
		$http({
			method: 'POST',
			url: BASEURL + RESTCALL.CreateNewUser,
			data: draftdata,
			headers: {
				draft: true
			}

		}).then(function (response) {
			console.log(response)
			$rootScope.dataModified = false;
			if (response.data.Status === "Saved as Draft") {
				$scope.input = {
					'responseMessage': response.data.responseMessage
				}
				$state.go("app.users", {
					input: $scope.input
				})
			}
		}, function (resperr) {

			if (resperr.data.error.message == 'Draft Already Exists') {
				$("#draftOverWriteModal").modal("show");
			} else {
				$scope.alerts = [{
						type: 'danger',
						msg: resperr.data.error.message
					}
				]

			}
		})

	}

	$scope.forceSaveDraft = function () {
		$http({
			method: 'POST',
			url: BASEURL + RESTCALL.CreateNewUser,
			data: draftdata,
			headers: {
				draft: true,
				'Force-Save': true
			}

		}).then(function (response) {

			if (response.data.Status === "Draft Updated") {
				$("#draftOverWriteModal").modal("hide");
				$scope.input = {
					'responseMessage': response.data.responseMessage
				}
				$state.go("app.users", {
					input: $scope.input
				})
			}
		}, function (resperr) {
			$("#draftOverWriteModal").modal("hide");
			$scope.alerts = [{
					type: 'danger',
					msg: resperr.data.error.message
				}
			]

		})
	}

	$('#changesLostModal').on('shown.bs.modal', function (e) {
		$('body').css('padding-right', 0)
	})

	$('#changesLostModal').on('hidden.bs.modal', function (e) {
		$scope.fromCancelClick = false;
	})

	$('#draftOverWriteModal').on('shown.bs.modal', function (e) {
		$('body').css('padding-right', 0)
	})
	$('#draftOverWriteModal').on('hidden.bs.modal', function (e) {

		setTimeout(function () {
			$scope.updateEntity = false;
		}, 100)

	})

});