VolpayApp.controller('userMgmtController',function($scope,$rootScope,$http,$filter, $state, userMgmtService,CommonService, $timeout, bankData, GlobalService, $location,LogoutService,$stateParams) {

	if($rootScope.alertData)
	{
		$scope.alerts = [{
			type : 'success',
			msg : $rootScope.alertData
		}];

		$rootScope.alertData = '';

		$timeout(function(){
			$('.alert-success').hide()
		},4000)
	}
		// // console.log($stateParams.input,$stateParams)
		// if($stateParams.input.responseMessage)
		// {
		// 		$scope.alerts = [{
		// 			type : 'success',
		// 			msg : $stateParams.input.responseMessage
		// 		}];
		// }

		$stateParams.input ?
		 $stateParams.input.responseMessage ? 	
		$scope.alerts = [{
					type : 'success',
					msg : $stateParams.input.responseMessage
				}]	 : ''
				
				 : ''

				 $timeout(function(){
			$('.alert-success').hide()
		},4000)

		// console.log($scope.alerts,"$scope.alerts")

	 /*$scope.customDate = 
	 {
		startDate:'',
		endDate:''
	}*/

	$scope.cUser = sessionStorage.UserID;
	$scope.isSuperAdmin = sessionStorage.ROLE_ID;
	$scope.viewMe = true;
    $scope.permission = {
            		'C' : false,
            		'D'	: false,
            		'R'	: false,
            		'U'	: false
            	}

        $http.post(BASEURL+RESTCALL.ResourcePermission,{
            "RoleId": sessionStorage.ROLE_ID,
            "ResourceName": "User Management"
        }).success(function(response){
            for(k in response){
                    for(j in Object.keys($scope.permission)){
                        if(Object.keys($scope.permission)[j] == response[k].ResourcePermission){
                            $scope.permission[Object.keys($scope.permission)[j]] = true;
                        }
                    }
            }
    })

	var len = 20;

	$http.get(BASEURL + RESTCALL.UserProfile).success(function (data, status, header, config) {
		$scope.cData = data;
	   
	  
	 $scope.backupData = angular.copy(data)
		$scope.profileName = $scope.cData.TimeZone; 
		textVal = $scope.profileName;
	   
	})

	$scope.search = {
		"EffectiveFromDate" : {
			"Start" : "",
			"End" : ""
		}
	};

	var restServer = RESTCALL.CreateNewUser+'/readall';
	var delData = {};
	$scope.backUp = {};
	$scope.indexx = "";
	$scope.dataFound = false;
	$scope.loadMorecalled = false;
	$scope.CRUD = "";
	$scope.restVal = [];

	$scope.changeViewFlag = GlobalService.viewFlag;

			function autoScrollDiv(){
			$(".listView").scrollTop(0);
			}

	$scope.$watch('changeViewFlag', function(newValue, oldValue, scope) {
				GlobalService.viewFlag = newValue;
				var checkFlagVal = newValue;	
				if(checkFlagVal){
					$(".maintable > thead").hide();
					autoScrollDiv();
				}
				else{
					$(".maintable > thead").show();
					if($(".dataGroupsScroll").scrollTop() == 0){
						$table = $("table.stickyheader")
						$table.floatThead('destroy');
						
					}
					autoScrollDiv();
				}
				
			})


    $scope.takeBackup = function(val,Id,flag){
		$scope.backUp = angular.copy(val);
		$scope.indexx = angular.copy(Id);
        $scope.viewMe = flag;
		//console.log(flag)
	}

	var delData='';
	$scope.takeDeldata = function(val,Id){
		delData = val;
		$scope.delIndex = Id;
	}


	 /*** Sorting ***/
        $scope.orderByField  = 'UserID';
        $scope.SortReverse  = false;
        $scope.SortType = 'Asc';





	$scope.prev = null;



	
	


/*** Export to Excel function ***/
	/*$scope.exportToExcel = function(eve){
		var tabledata = angular.element( document.querySelector('#exportTable') ).clone();
		$(tabledata).find('thead').find('tr').find('th:first-child').remove()
		$(tabledata).find('tbody').find('tr').find('td:first-child').remove()

		var table_html = $(tabledata).html();
		bankData.exportToExcel(table_html, 'Users')
	}*/
	$scope.printFn = function () {
		$('[data-toggle="tooltip"]').tooltip('hide');
		window.print()
	}

	var colName = ["UserID","FirstName","LastName","RoleID",
	"Status","EmailAddress","TimeZone","IsForceReset","EffectiveFromDate","EffectiveTillDate","UserRoleAssociation"];


	$scope.ExportMore = function(argu,excelLimit){
		if(argu > excelLimit){
			JSONToExport(bankData,$scope.dat, (argu > excelLimit) ?  "Users" + '_'+(''+excelLimit)[0]: "Users", true,colName);
			$scope.dat = [];
			excelLimit += 1000000
		}
		
		RESTCALL.CreateNewUser+'/readall'

		$http.post(BASEURL+RESTCALL.CreateNewUser+'/readall',{"start": argu,"count": ($scope.totalForCountBar > 1000) ? 1000 : $scope.totalForCountBar}).success(function(data){

			
			$scope.dat = $scope.dat.concat(data)
			if(data.length >= 1000){
				
				argu += 1000;
				$scope.ExportMore(argu,excelLimit)				
			}
			else{
				JSONToExport(bankData,$scope.dat,(argu > excelLimit) ?  "Users" + '_'+(''+excelLimit)[0]: "Users", true,colName);
			}
		})
	}

	 $scope.exportAsExcel = function(data){
		
		$scope.dat = [];
		if($("input[name=excelVal][value='All']").prop("checked")){	
			$scope.ExportMore(0,1000000);
		}
		else{
			$scope.dat = angular.copy($scope.restVal);
			JSONToExport(bankData,$scope.dat, "Users", true,colName);
		}
	}


$scope.userDataFn = function(val,Id,flag)
    {
		console.log("Callimg User Data fn")
		val.Status = val.Status ? String(val.Status) : '';
		val.IsForceReset = String(val.IsForceReset);
		
		$scope.userData1 = angular.copy(val);

        if($scope.prev != null)
        {
            $('#collapse'+$scope.prev).collapse('hide');
        }

        $scope.prev = Id;

        $scope.takeBackup(val,Id,flag);
        $scope.takeDeldata(val,Id);

        
	
     $(".alert").hide();
	 $("div").find("#ViewUserMail").removeAttr("style");

 }

 $scope.gotoView = function(data,flag,drObj)
 {
	$scope.input = {
		'Data' : data,
		'DraftTotObj' : drObj ? drObj : ''
	};
	data.View = flag;
	data.UserRoleAssociation = data.UserRoleAssociation ? data.UserRoleAssociation : [{}]
	$state.go('app.userdetail',{input:$scope.input, permission:$scope.permission})
 }
 $scope.gotoEdit = function(data,flag,drObj)
 {
	 console.log(data,"data")
	$scope.input = {
		'Data' : data,
		'DraftTotObj' : drObj ? drObj : ''
	};

	data.View = flag;
	data.UserRoleAssociation = data.UserRoleAssociation ? data.UserRoleAssociation : [{}]
	$state.go('app.userdetail',{input:$scope.input, permission:$scope.permission})
 }



	$scope.takeDeldata = function(val,Id){
		delData = val;
		$scope.delIndex = Id;
    }
	$scope.userData ={};
	$scope.userData1 ={};
	//I Load the initial set of datas onload
	$scope.refreshCall=function()
	{
		$scope.UserData ={};
		$scope.CRUD = "";
		
		$scope.UserData.QueryOrder = [{"ColumnName":$scope.orderByField,"ColumnOrder": $scope.SortType}]
		$scope.UserData.start=0;
		$scope.UserData.count=20;
		$scope.UserData.Operator = "AND";
		$scope.UserData = constructQuery($scope.UserData);
		
		restServer = RESTCALL.CreateNewUser+'/readall';
		$scope.initialObj = {};
		$scope.initialObj.UserId =  sessionStorage.UserID;
		bankData.crudRequest("POST", restServer, $scope.UserData).then(applyRestData,errorFunc);
	}

	/*$scope.commonObj = CommonService.userMgmt;
	$scope.commonObj.currentObj.start = 0;
	CommonService.userMgmt.currentObj.sortBy=[];
	$scope.fieldArr = $scope.commonObj.currentObj;*/
	$scope.fieldArr = {
		"sortBy" : [],
		"params" : [],
		"start" : 0,
		"count" : 20
	}
	
	$scope.uorQueryConstruct = function(arr)
	{
		//CommonService.userMgmt.currentObj = arr;
		$scope.fieldArr = arr;

		$scope.Qobj = {};
		$scope.Qobj.start = arr.start;
		$scope.Qobj.count = arr.count;
		$scope.Qobj.Queryfield = [];
		$scope.Qobj.QueryOrder = [];


			for(var i in arr)
			{
				if(i == 'params')
				{
					for(var j in arr[i])
					{
						$scope.Qobj.Queryfield.push(arr[i][j])
					}
				}
				else if(i == 'sortBy')
				{
					for(var j in arr[i])
					{
						$scope.Qobj.QueryOrder.push(arr[i][j])
					}
				}
			}

			$scope.Qobj = constructQuery($scope.Qobj);
			return $scope.Qobj;
	}

	$scope.initCall = function(_query)
	{
		bankData.crudRequest("POST", restServer, _query).then(applyRestData,errorFunc);
	}

	//console.log($scope.fieldArr)
	$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))

	

	$scope.initSetting = function()
	{
		$scope.FieldsValues = [{
				"label" : "User ID",
				"value" : "UserID",
				"type"  : "text",
				"allow" : "text",
				"visible" : true
			}, {
				"label" : "First Name",
				"value" : "FirstName",
				"type" : "text",
				"allow" : "string",
				"visible" : true
			}, 
			{
				"label" : "Last Name",
				"value" : "LastName",
				"type" : "text",
				"allow" : "string",
				"visible" : true
			}, {
				"label" : "Role ID",
				"value" : "RoleID",
				"type" : "dropdown",
				"visible" : true
			}, 
			{
				"label" : "Status",
				"value" : "Status",
				"type" : "dropdown",
				'multiple':false,
				"visible" : true
			},
			{
				"label" : "E-Mail",
				"value" : "EmailAddress",
				"type" : "text",
				'multiple':false,
				"visible" : true
			}, {
				"label" : "Time Zone",
				"value" : "TimeZone",
				"type" : "dropdown",
				'multiple':false,
				"visible" : true
			},
			{
				"label" : "Is Force Reset",
				"value" : "IsForceReset",
				"type" : "text",
				'multiple':false,
				"visible" : true
			},
			{
				"label" : "Effective From Date",
				"value" : "EffectiveFromDate",
				"type" : "dateRange",
				"allow" : "number",
				"visible" : true
			},
			{
				"label" : "Effective Till Date",
				"value" : "EffectiveTillDate",
				"type" : "dateRange",
				"allow" : "number",
				"visible" : true
			}
		]
	}
	
	//$scope.buildSearchClicked = false;
	//$rootScope.usercustomDate={};
	//$scope.search = ($rootScope.usersearch)?$rootScope.usersearch:{};
	//$scope.dSearch = $scope.search;
	//$scope.resetBtnClicked = false;
	//$scope.newSearch = false;

	//$scope.commonObj = CommonService.userMgmt;
	//CommonService.userMgmt.currentObj.sortBy=[];

	$scope.dateSet = function()
	{
		$scope.dateFilter = CommonService.userMgmt.dateFilter;

		for(i in $scope.dateFilter)
		{
			if($scope.dateFilter[i])
			{
				$('#dropTxt').text($filter('ucwords')(i))
				for(var j=0;j<$('.menuClass').length;j++)
				{
						if($($('.menuClass')[j]).text().toLowerCase().indexOf(i) != -1)
						{
							$($('.menuClass')[j]).addClass('listSelected')
						}
						else{
							$($('.menuClass')[j]).removeClass('listSelected')
						}
				}
				 
			}
		}
	}
	//$scope.dateSet()

	$scope.initSetting()

	$timeout(function(){
		customDateRangePicker("EffectiveFromDateStart","EffectiveFromDateEnd")
	},10)

	$scope.loadedData='';	
	//$scope.uorVal = $scope.uorFound  = $scope.commonObj.uorVal;

	//$scope.fieldArr = $scope.commonObj.currentObj;
	
	$scope.retExpResult = function()
	{
		if(!$scope.dateFilter.custom)
		{
			$scope.customDate = 
						{
						startDate:'',
						endDate:''
						}
		}
		
		if($scope.dateFilter.all)
		{
			$scope.dateArr = [];
		}
		else if($scope.dateFilter.today)
		{
			$scope.dateArr = [
					{
						"ColumnName": "EffectiveFromDate",
						"ColumnOperation": "=",	
						"ColumnValue": todayDate()
					}
				]
		}
		else if($scope.dateFilter.week)
		{
			$scope.dateArr = [
				{
					"ColumnName":"EffectiveFromDate",
					"ColumnOperation":">=",	
					"ColumnValue":week().lastDate
				},
				{
					"ColumnName":"EffectiveFromDate",
					"ColumnOperation":"<=",	
					"ColumnValue":week().todayDate
				}
			]
		}	
		else if($scope.dateFilter.month)
		{
			$scope.dateArr = [
					{
						"ColumnName":"EffectiveFromDate",
						"ColumnOperation":">=",	
						"ColumnValue":month().lastDate
					},
					{
						"ColumnName":"EffectiveFromDate",
						"ColumnOperation":"<=",	
						"ColumnValue":month().todayDate
					}
				]

		}
		else if($scope.dateFilter.custom)
		{	
			customDateRangePicker('CstartDate','CendDate')
			
			$scope.customDate.startDate = CommonService.userMgmt.customDate.startDate;
			$scope.customDate.endDate = CommonService.userMgmt.customDate.endDate;
			$('#customDate').modal('hide')
			$scope.dateArr = [
					{
						"ColumnName":"EffectiveFromDate",
						"ColumnOperation":">=",	
						"ColumnValue":$scope.customDate.startDate
					},
					{
						"ColumnName":"EffectiveFromDate",
						"ColumnOperation":"<=",	
						"ColumnValue":$scope.customDate.endDate
					}
				]

		}

		return $scope.dateArr;

	}
	//$scope.retExpResult()

	$scope.buildSearch = function(){

		
		$(".listView").scrollTop(0);
		$scope.uorFound = $scope.uorVal = "";
		$scope.search = cleantheinputdata($scope.search)
		$scope.showSearchWarning = $.isEmptyObject($scope.search);
		console.log($scope.showSearchWarning)
		$rootScope.usersearch = angular.copy($scope.search);
		$scope.searchArr = [];
		for(i in $scope.search) 
		{
			if(i == 'EffectiveFromDate')
			{
				if($scope.search[i].Start && $scope.search[i].End)
				{
					
					$scope.searchArr.push({"ColumnName":i,"ColumnOperation":">=","ColumnValue":$scope.search[i].Start,'advancedSearch':true});
					$scope.searchArr.push({"ColumnName":i,"ColumnOperation":"<=","ColumnValue":$scope.search[i].End,'advancedSearch':true});
				}
				
			}
			else{
					if(Array.isArray($scope.search[i]))
					{
						for(var j in $scope.search[i])	
						{
							$scope.searchArr.push({"ColumnName":i,"ColumnOperation":"=","ColumnValue":$scope.search[i][j],'advancedSearch':true})
						}
					}
					else{
						$scope.searchArr.push({"ColumnName":i,"ColumnOperation":"=","ColumnValue":$scope.search[i],'advancedSearch':true})
					}
				}

		}

		$scope.dSearch = angular.copy($scope.search);
		$scope.fieldArr.params = $scope.retExpResult() 
		$scope.fieldArr.params = $scope.searchArr.concat($scope.fieldArr.params);
		$scope.fieldArr.start = 0;
		if((!$scope.showSearchWarning || $scope.spliceSearch || $scope.buildSearchClicked))
		{	
			$scope.distSearch = true;
			if($scope.resetBtnClicked)
			{
				$scope.distSearch = false;
				setTimeout(function(){
					$scope.resetBtnClicked = false
				},1000)
			}
			$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))
			$scope.spliceSearch = false;
		}
		$scope.savedSearchSelected = false;
		
		return {
			'service':CommonService.userMgmt,
			'searchParams':$scope.search
			};
		
		
	}

	$scope.FilterByDate = function(params,eve)
	{
		
		$(".listView").scrollTop(0);
		$('.menuClass').removeClass('listSelected')
		if(params != 'custom')
		{
			$(eve.currentTarget).addClass('listSelected')
		}
		else{
			$('.menuClass:nth-child(5)').addClass('listSelected')
		}

		$('#dropTxt').html($filter('ucwords')(params))

		for(var i in $scope.dateFilter)
		{
			$scope.dateFilter[i] = false;
		}
		
		$scope.dateFilter[params] = true;

		

		if($scope.dateFilter.custom)
		{	
			$('#customDate').modal('hide')
			CommonService.userMgmt.customDate.startDate = $scope.customDate.startDate;
			CommonService.userMgmt.customDate.endDate = $scope.customDate.endDate;
		}

		
		$scope.dateArr = $scope.retExpResult()

		//console.log("arr1",$scope.fieldArr.params)


		for(var i in $scope.fieldArr.params)
		{
			if(($scope.fieldArr.params[i].ColumnName == 'EffectiveFromDate') || (!$scope.fieldArr.params[i].advancedSearch))	
			{
				$scope.fieldArr.params.splice(i)
			}
		}
	
		for(var i in $scope.dateArr)
		{
			$scope.fieldArr.params.push({"ColumnName":$scope.dateArr[i].ColumnName,"ColumnOperation":$scope.dateArr[i].ColumnOperation,"ColumnValue":$scope.dateArr[i].ColumnValue,'advancedSearch':false});
			
		}

		$scope.fieldArr.start = 0;
		

	$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))
		
	}

	/*$scope.$watch('fieldArr.params',function(nArr,oArr){
		
		if(nArr.length)
		{CommonService.userMgmt.searchFound = true;
		}else
		{CommonService.userMgmt.searchFound = false;
		}

		
	})*/
	
	$scope.uorSearch = function()
	{
		if($scope.uorVal)
		{
			
			$scope.search = {
				"EffectiveFromDate" : {
					"Start" : "",
					"End" : ""
				}
			}
			
			$scope.uorFound = $scope.uorVal;

		//	CommonService.userMgmt.uorVal = $scope.uorVal;
			len = 20;	
			$scope.uorSearchFound = false;


			for(var i in $scope.fieldArr.params)
			{
				if(!$scope.fieldArr.params[i].advancedSearch)
				{
					if($scope.fieldArr.params[i].ColumnName == 'UserID')
					{
						$scope.uorSearchFound = true;
						$scope.fieldArr.params[i].ColumnName = 'UserID';
						$scope.fieldArr.params[i].ColumnOperation = 'like';
						$scope.fieldArr.params[i].ColumnValue = $scope.uorVal;
						$scope.fieldArr.params[i].advancedSearch = false;
					}
				}
				else
				{
					$scope.fieldArr.params.splice(i,1);
				}
			}


			if(!$scope.uorSearchFound)
			{
				$scope.fieldArr.params.push({"ColumnName":"UserID","ColumnOperation":"like",	"ColumnValue":$scope.uorVal,'advancedSearch':false})
			}

			$scope.fieldArr.start = 0;
			$scope.fieldArr.count = len;

			$scope.query = $scope.uorQueryConstruct($scope.fieldArr)
			$scope.initCall($scope.query)  
			
		}				

	}

	$scope.CustomDatesReset = function()
	{
		$scope.customDate = 
						{
						startDate:'',
						endDate:''
						}
	
	}

	$scope.getExistVal = function(eve)
	{
		if($scope.uorVal)
		{
			if (eve.keyCode == 13)
			{
				$scope.uorSearch()
			}

		}
		else{

				if($scope.dateFilter.all)
				{
				$scope.uorFound = '';
				CommonService.userMgmt.uorVal = '';
				
						for(var j in $scope.fieldArr.params)
						{
							if($scope.fieldArr.params[j].ColumnName == "UserID")
							{
								$scope.fieldArr.params.splice(j,1)

								
							}
						}
			$scope.fieldArr.start = 0;
			$scope.fieldArr.count = 20;
			$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))

			}
		}
	}

	$scope.clearSearch = function()
	{
		
		//$scope.searchFound = false;
		$scope.loadedData='';	
		$(".listView").scrollTop(0);
		
		$scope.fieldArr = {"sortBy":[],"params":[],"start":0,"count":20};
		len = 20;
		$scope.uorVal = '';
		$scope.uorFound = '';
		CommonService.userMgmt.uorVal = '';
		$scope.search = {
				"EffectiveFromDate" : {
					"Start" : "",
					"End" : ""
				}
			}
		$scope.newSearch = false;
		$scope.dSearch = $scope.search;
		$rootScope.usersearch = {
			"EffectiveFromDate" : {
				"Start" : "",
				"End" : ""
			}
		}
			
		$scope.customDate = 
					{
					startDate:'',
					endDate:''
					}
			
		
		//$scope.dateArr = [];

		$scope.dArr = ["RoleID","Status","TimeZone"]
		$timeout(function () {

			for (var i in $scope.dArr) {
				
					$("select[name='" + $scope.dArr[i] + "']").select2("destroy");
					$("select[name='" + $scope.dArr[i] + "']").val("");
					$("select[name='" + $scope.dArr[i] + "']").select2({
						placeholder : 'Select an option',
						allowClear:true
					});
			}
			$scope.remoteDataConfig()

		}, 100)
			
	

		$timeout(function(){
			customDateRangePicker("EffectiveFromDateStart","EffectiveFromDateEnd")
		},10)

		CommonService.userMgmt.dateFilter = {
													all:true,
													today:false,
													week:false,
													month:false,
													custom:false
													}
		$scope.dateSet()										
		$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))
	}

	$scope.resetFilter = function(){
		$scope.buildSearchClicked = true;
		$scope.resetBtnClicked = true;
		$scope.AdsearchParams = {
				"EffectiveFromDate" : {
					"Start" : "",
					"End" : ""
				}
				
		}

		$scope.search = angular.copy($scope.AdsearchParams);
		$scope.search = {
			"EffectiveFromDate" : {
				"Start" : "",
				"End" : ""
			}
		};
		$rootScope.usersearch = {
			"EffectiveFromDate" : {
				"Start" : "",
				"End" : ""
			}
		}
		/*$timeout(function(){
			customDateRangePicker("EffectiveFromDateStart","EffectiveFromDateEnd")
			for(var i in $scope.sortMenu)
			{
				
				if($scope.sortMenu[i].type == 'dropdown')	
				{
					$('select[name='+$scope.sortMenu[i].fieldName+']').select2({data:[]})
				}	
			}
				$scope.remoteDataConfig()

				$('.input-group-addon').on('click focus', function(e){
					$(this).prev().focus().click()
				})
		},0)*/
		$('#saveSearchBtn, #AdSearchBtn').removeAttr('disabled', 'disabled');

		//$scope.setSortMenu()
		$scope.buildSearch()

	}

	$scope.SelectValue = function(index)
	{
		
		$scope.seeVisible = false;
		$scope.FieldsValues[index]['visible'] = !$scope.FieldsValues[index]['visible'];

			if($scope.FieldsValues[index].value == 'EffectiveFromDate')
			{
				setTimeout(function(){
					customDateRangePicker("EffectiveFromDateStart","EffectiveFromDateEnd")
					$('.input-group-addon').on('click focus', function(e){
						$(this).prev().focus().click()
						
					})
				},1000)
			}

			

		for (var i in $scope.FieldsValues) {
			if ($scope.FieldsValues[i].visible) {
				$scope.seeVisible = true;
			}
		}

		if ($scope.seeVisible) {
			$('#saveSearchBtn, #AdSearchBtn').removeAttr('disabled', 'disabled');
		} else {
			$scope.distSearch = true;
			$('#saveSearchBtn, #AdSearchBtn').attr('disabled', 'disabled');
		}

	}




	
	
	$scope.loadData = function()
	{
		
		
		$(".listView").scrollTop(0);
		len = 20;
		
		$scope.fieldArr = {
			"sortBy" : [],
			"params" : [],
			"start" : 0,
			"count" : 20
		}
		restServer = RESTCALL.CreateNewUser+'/readall';
		$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))
		
		$('.table-responsive').find('table').find('thead').find('th').find('span').removeAttr('class')

	}
    //I Load More datas on scroll

	$scope.loadMore = function(){

		/*$scope.loadMorecalled = true;

		$scope.UserData.sorts = [{"columnName":$scope.orderByField,"sortOrder": $scope.SortType}]
        $scope.UserData.start=len;
        $scope.UserData.count=20;
		
		bankData.crudRequest("POST", restServer,$scope.UserData).then(applyRestData,errorFunc);
		len = len + 20;*/

		$scope.fieldArr.start = len;
		$scope.fieldArr.count = 20;

		$http.post(BASEURL+RESTCALL.CreateNewUser+'/readall',$scope.uorQueryConstruct($scope.fieldArr)).success(function(data){
			$scope.restData = data;
			$scope.restVal = $scope.restVal.concat(data);
			$scope.loadedData = data;
			len = len+20;
		}).error(function(data){
			
		})

	}

	$scope.gotoSorting = function(dat){
		
				console.log(dat)
			$scope.fieldArr.start = 0;
			$scope.fieldArr.count = len;
			
			var orderFlag = true;
			if($scope.fieldArr.sortBy.length)
			{
				for(var i in $scope.fieldArr.sortBy)
				{
					if($scope.fieldArr.sortBy[i].ColumnName == dat.value)
					{	
						if($scope.fieldArr.sortBy[i].ColumnOrder =='Asc')
						{
							$('#'+dat.value+'_icon').attr('class','fa fa-long-arrow-down')
							$('#'+dat.value+'_Icon').attr('class','fa fa-caret-down')
							$scope.fieldArr.sortBy[i].ColumnOrder = 'Desc';
							orderFlag = false;
							break;
						}
						else{
							$scope.fieldArr.sortBy.splice(i,1);
							orderFlag = false;
							$('#'+dat.value+'_icon').attr('class','fa fa-minus fa-sm')
							$('#'+dat.value+'_Icon').removeAttr('class')
							break;
						}
		
					}
				}
		
				if(orderFlag){
				$('#'+dat.value+'_icon').attr('class','fa fa-long-arrow-up')
				$('#'+dat.value+'_Icon').attr('class','fa fa-caret-up')
					$scope.fieldArr.sortBy.push({
								"ColumnName": dat.value,
								"ColumnOrder": 'Asc'
							})
		
				}
		}
		else{
			
			$('#'+dat.value+'_icon').attr('class','fa fa-long-arrow-up')
			$('#'+dat.value+'_Icon').attr('class','fa fa-caret-up')
		
				$scope.fieldArr.sortBy.push({
								"ColumnName": dat.value,
								"ColumnOrder": 'Asc'
							})
			
		} 
		
		
		$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))
		}
	


	$scope.deleteData = function() {
		/*delete delData.$$hashKey

		$scope.delObj = {};
		$scope.delObj.UserId = delData.UserID;
		

		restServer = RESTCALL.CreateNewUser+'/delete';

		bankData.crudRequest("POST", restServer,$scope.delObj).then(getData,errorFunc);



			$('.modal').modal("hide");
			$('body').removeClass('modal-open')*/


			//delete $scope.data.$$hashKey;
			delete delData.$$hashKey
			$scope.delObj = {};
			$scope.delObj.UserId = delData.UserID;
			
	
			var restServer = RESTCALL.CreateNewUser+'/delete';
	
			$http.post(BASEURL+restServer,$scope.delObj).success(function(data){
				
				$scope.alerts = [{
						type : 'success',
						msg : (data.responseMessage)?data.responseMessage:'Deleted successfully'
					}];

					$scope.loadData()
					for(var i in $scope.Status){
						$scope.getCountbyStatus($scope.Status[i])
					}
					
					setTimeout(function(){
						$('.alert-success').hide()
					},4000) 

			}).error(function(data)
			{
				$scope.alerts = [{   
					type : 'danger',
					msg : data.error.message
				}];
			})
	
			$('.modal').modal("hide");
			$('body').removeClass('modal-open')

	};

	
 
	// I load the rest data from the server.
	function getData(response) {

		$scope.CRUD = (response.data.responseMessage)?response.data.responseMessage:'Deleted successfully';
		$scope.loadMorecalled = false;

		$scope.UserData.sorts = [{"columnName":$scope.orderByField,"sortOrder": $scope.SortType}]
        $scope.UserData.start=0;
        $scope.UserData.count=20;

        len = 20;
        restServer = RESTCALL.CreateNewUser+'/readall';
		bankData.crudRequest("POST", restServer,$scope.UserData).then(applyRestData,errorFunc);
		
	}

	// I apply the rest data to the local scope.

	function applyRestData(restDat) {

		//console.log("head",restDat.headers().totalcount)
		//$scope.userRoles.splice(0,0,{})
		$scope.totalForCountBar = restDat.headers().totalcount;
		var restData = restDat.data
		$scope.restData = restData;
		$scope.restData.splice(0,0,{});

		if($scope.loadMorecalled){
			$scope.restVal = $scope.restVal.concat(restData);
		}
		else{
				if(restData.length == 0){
					$scope.dataFound = true;
				}
				else
				{
					$scope.dataFound = false;
				}
			$scope.restVal = restData;

				if($scope.CRUD != "")
				{
					$scope.alerts = [{
						type : 'success',
						msg : $scope.CRUD		//Set the message to the popup window
					}];
					$timeout(callAtTimeout, 4000);
				}
		}

		$scope.CRUD = '';
			var isOnClickedMyProfilePage = ($stateParams.input ?( $stateParams.input.UserProfileDraft ? $stateParams.input.UserProfileDraft : '') : '');
			
				// console.log(isOnClickedMyProfilePage)
				if(isOnClickedMyProfilePage)
				{
						$scope.gotoEditDraft("View",'',$stateParams.input.totData)
				}
	}

	// I apply the Error Message to the Popup Window.
	function errorFunc(errorMessag){
	    var errorMessage = errorMessag.data;
		    if(errorMessag.status == 401)
			{
				if(configData.Authorization=='External'){										
					window.location.href='/VolPayHubUI'+configData['401ErrorUrl'];
				}
				else{
					LogoutService.Logout();
				}
			}
			else
			{
				$scope.alerts = [{
					type : 'danger',
					msg : errorMessag.error.message	
				}];
			}

			$timeout(callAtTimeout, 4000);

	}

	function callAtTimeout() {
		$('.alert').hide();
	}
	$scope.callStyle = function(){	
		return $('#listViewPanelHeading_1').outerHeight();
	}

	$scope.goToCreateUser = function(){
	$location.path('app/adduser');

	}




	var createObj = {};
	createObj.UserId = sessionStorage.UserID;

	$scope.selectOptions=[];

	$scope.setInitVal = function()
	{
		if(Object.keys($scope.search).indexOf('RoleID') != -1)
		{
			var _query = {
            search : $scope.search.RoleID,
            start : 0,
            count : 500
			}
			return $http({
				method : "GET",
				url : BASEURL + RESTCALL.CreateRole,
				params : _query
			}).then(function (response) {
				$scope.selectOptions = response.data;
				console.log($scope.selectOptions)
				return $scope.selectOptions;
			})
		}
		
	}
	$scope.setInitVal()

	


    $scope.Sorting = function(orderByField)
    {
        $scope.loadMorecalled = false;
        $scope.orderByField = orderByField;
        $scope.CRUD = '';

        if($scope.SortReverse == false)
        {
           $scope.SortType = 'Desc';
           $scope.SortReverse = true;
        }
        else
        {
            $scope.SortType = 'Asc';
            $scope.SortReverse = false;
        }

        var QueryOrder={};
        QueryOrder.ColumnName = orderByField;
        QueryOrder.ColumnOrder = $scope.SortType;

        var sortObj = {};
        sortObj.sorts = [{"columnName":$scope.orderByField,"sortOrder":$scope.SortType}];
        sortObj.start=0;
        sortObj.count=len;

        bankData.crudRequest("POST", restServer, sortObj).then(applyRestData,errorFunc);
    }


	/*$scope.activatePicker = function (e) {
		
				var prev = null;
				$('.DatePicker').datetimepicker({
					format : "YYYY-MM-DD",
					useCurrent : false,
					showClear : true
				}).on('dp.change', function (ev) {
					$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
				}).on('dp.show', function (ev) {
					
					$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()

					$('#lflag').removeClass('listView')
					
				}).on('dp.hide', function (ev) {
					$scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('name')] = $(ev.currentTarget).val()
					$('#lflag').addClass('listView')
					
				});
			}

		$scope.triggerPicker = function (e) {
			console.log("aaa",$scope.UserData)
		$('.input-group-addon').on('click focus', function(e){
			$(this).prev().focus().click()
			});
			
				if ($(e.currentTarget).prev().is('.DatePicker')) {
					$scope.activatePicker($(e.currentTarget).prev());
					$('input[name=' + $(e.currentTarget).prev().attr('name') + ']').data("DateTimePicker").show();
				}
		};
		*/

    $scope.printFn = function()
    {
    window.print()
    }

	//$scope.tZoneOptions = [];
	// $timeout(function(){
	// 	$scope.tZoneOptions = timeZoneDropValues.TimeZone;
	// },1000)
	
	

	 

	 $scope.auditLogDetails = "";
	 $scope.commentVal = "";	
	 $scope.costructAudit = function(argu,index){
		
		 $scope.auditLogDetails = argu
		 //$('#auditModel_'+index).find('tbody').html('')
		 $('.auditTable').find('tbody').html('')

		 
		 if(argu.oldData && argu.newData){
			$('.auditTable').find('tbody').append('<tr><th>Field</th><th style="width:200px;">Old Data</th><th style="width:200px;">New Data</th></tr>')			
		 }else{
			$('.auditTable').find('tbody').append('<tr><th>Field</th><th>Data</th></tr>')
		 }
		 var _keys = ''
		
		 if($.isPlainObject(argu.oldData) && $.isPlainObject(argu.newData)){
			 _keys = (Object.keys(argu.oldData).length >= Object.keys(argu.newData).length) ? Object.keys(argu.oldData) : Object.keys(argu.newData)
		 }else if($.isPlainObject(argu.oldData)){
			 _keys = Object.keys(argu.oldData)
		 }else if($.isPlainObject(argu.newData)){
			 _keys = Object.keys(argu.newData)
		 }
		 //console.log(_keys)
		// console.log("keys",_keys)
		 for(var j in _keys){
			 if(!_keys[j].match(/_PK/g)){
				// console.log(j,_keys[j])
				 var _tr = ""
				 if(j%2){
					 _tr = "<tr style='background-color: rgb(245, 245, 245)'>"
				 }else{
					 _tr = "<tr style='background-color: #fff'>"
				 }
				 _tr = _tr +"<td>"+$filter('camelCaseFormatter')(_keys[j])+"</td>";
				 if(argu.oldData && argu.newData){
					// console.log("both")
					 if(argu.oldData){
							 _tr = _tr + "<td>"
						 if(argu.oldData[_keys[j]]){
							 if(typeof(argu.oldData[_keys[j]]) == 'object'){
								 _tr = _tr + "<pre>" + $filter('json')(argu.oldData[_keys[j]]) + "</pre>"
							 }else{
								 _tr = _tr + argu.oldData[_keys[j]];
							 }
						}
							 _tr = _tr + "</td>"
					 }
					 if(argu.newData){
						 if(argu.newData[_keys[j]]){
							 if(argu.oldData && argu.newData[_keys[j]] != argu.oldData[_keys[j]]){
								 _tr = _tr + "<td class=\"modifiedClass\">"
							 }else{
								 _tr = _tr + "<td>"								
							 }
							 if(typeof(argu.newData[_keys[j]]) == 'object'){
								 _tr = _tr + "<pre>" + $filter('json')(argu.newData[_keys[j]]) + "</pre>"
							 }else{
								 _tr = _tr + argu.newData[_keys[j]];
							 }
							 _tr = _tr + "</td>"
						 }
					 }

					
				 }else{
					 if(argu.newData){
						//console.log("new")
							 _tr = _tr + "<td>"
						 if(argu.newData[_keys[j]]){
							 if(typeof(argu.newData[_keys[j]]) == 'object'){
								 _tr = _tr + "<pre>" + $filter('json')(argu.newData[_keys[j]]) + "</pre>"
							 }else{
								 _tr = _tr + argu.newData[_keys[j]];
							 }
						 }
							 _tr = _tr + "</td>"
					 }else if(argu.oldData){
						//console.log("old")
							 _tr = _tr + "<td>"
						 if(argu.oldData[_keys[j]]){
							 if(typeof(argu.oldData[_keys[j]]) == 'object'){
								 _tr = _tr + "<pre>" + $filter('json')(argu.oldData[_keys[j]]) + "</pre>"
							 }else{
								 _tr = _tr + argu.oldData[_keys[j]];
							 }
						 }
							 _tr = _tr + "</td>"
					 }
				 }
				 
				 $('.auditTable').find('tbody').append(_tr)				
			 }
		 }		
		 
		 if((argu.action).match(/:/g)){
			 $scope.commentVal = (argu.action).split(/:(.+)/)
		 }
		 else{
			 $scope.commentVal = ""
		 }
	 }

	 $scope.showEditLog = false;

	

	 $scope.gridViewLog = function()
	 {
		$scope.showEditLog = true;
		$scope.auditTableshow=false;
	 }

	 $scope.listviewLog = function()
	 {
		$scope.showEditLog = false;
		$scope.auditTableshow=false;
	 }

	 $scope.auditTableshow=false;
	 $scope.showaudit = function(argu,index){
		console.log(argu)
		$scope.tempData = argu;
		$scope.costructAudit(argu,index)
		$scope.auditTableshow=true;

		$('#innerModel').modal('toggle');
	}

	
	 $scope.spliceSearchArr = function (key) {
		
		delete $scope.search[key];
		$scope.buildSearchClicked = true;
		$scope.dArr = ["RoleID","Status","TimeZone"]
		$timeout(function () {

			for (var i in $scope.dArr) {
				if (key == $scope.dArr[i]) {
					$("select[name='" + $scope.dArr[i] + "']").select2("destroy");
					$("select[name='" + $scope.dArr[i] + "']").val("");
					$("select[name='" + $scope.dArr[i] + "']").select2({
						placeholder : 'Select an option',
						allowClear:true
					});

					$scope.remoteDataConfig()
				}
			}

		}, 100)

		$scope.spliceSearch = true;
		$scope.buildSearch()
	}

	$scope.retainAlert = function (eve) {
		
				$(eve.currentTarget).parent().removeClass('in')
				$scope.showSearchWarning = false;
			}

	 $scope.ClearAlert = function()
	 {
		 $scope.searchname='';
		 $scope.searchNameDuplicated = false;
		 $scope.isAnyFieldFilled = false;
	 }
 

       /*setTimeout(function(){
        $scope.timeZone()
        },100)*/

	/*** To control Load more data ***/
	var debounceHandler = _.debounce($scope.loadMore, 700, true);
	jQuery(
		function ($) {
			$('.listView').bind('scroll', function () {
				$scope.widthOnScroll();
				if (Math.round($(this).scrollTop() + $(this).innerHeight()) >= $(this)[0].scrollHeight) {
					if ($scope.restData.length >= 20) {
						//$scope.loadMore();

						debounceHandler()
						// $scope.loadCnt = 0;
					}
				}
			})
			setTimeout(function () { }, 1000)
		}  
	);



	/*****Search starts *******/

	$scope.filterBydate = [{
		'actualvalue' : todayDate(),
			'displayvalue' : 'Today'
		}, {
			'actualvalue' : week(),
			'displayvalue' : 'This Week'
		}, {
			'actualvalue' : month(),
			'displayvalue' : 'This Month'
		}, {
			'actualvalue' : year(),
			'displayvalue' : 'This Year'
		}, {
			'actualvalue' : '',
			'displayvalue' : 'Custom'
		}
	]

$scope.Status = [{
		"actualvalue" : "ACTIVE",
		"displayvalue" : "ACTIVE"
		},  {
			"actualvalue": "DELETED",
			"displayvalue": "DELETED"
		},
		{
		"actualvalue" : "INACTIVE",
		"displayvalue" : "INACTIVE"
		},
		{
		"actualvalue" : "SUSPENDED",
		"displayvalue" : "SUSPENDED"
		}
	]

	$timeout(function(){
	$scope.fields = [
		{
			'type'	: "string",
			'label'	: "User ID",
			'name'	: "UserID"
		},
		{
			'type'	: "string",
			'label'	: "First Name",
			'name'	: "FirstName"
		},
		{
			'type'	: "string",
			'label'	: "Last Name",
			'name'	: "LastName"
		},
		{
			'type'	: "select",
			'label'	: "Role ID",
			'value'	: 	[],
			'smartSearch' : true,
			'name'	: "RoleID"
		},
		{
			'type'	: "select",
			'label'	: "Status",
			'value'	: 	[],
			'smartSearch' : true,
			'name'	: "Status"
		},
		{
			'type'	: "select",
			'label'	: "TimeZone",
			'value'	: 	[],
			'smartSearch' : true,
			'name'	: "TimeZone"
		},
		{
			'type'	: "DateOnly",
			'label'	: "Effective From Date",
			'name'	: "EffectiveFromDate"
		},
		{
			'type'	: "DateOnly",
			'label'	: "Effective Till Date",
			'name'	: "EffectiveTillDate"
		}
	]
	

	for(var i in $scope.fields)
	{
		if($scope.fields[i].name == "TimeZone")
		{
			
				$scope.tZoneOptions = timeZoneDropValues.TimeZone;
				$scope.fields[i].value = $scope.tZoneOptions;
		
		}
	}
},1000)
	$scope.filterParams = {};
	$scope.selectedStatus = [];
	
	$scope.setStatusvalue = function(val,to){	
		var addme = true;
		if($scope.selectedStatus.length){			
			for(k in $scope.selectedStatus){
				if($scope.selectedStatus[k] == val){
					$('#'+val).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
					$scope.selectedStatus.splice(k,1);
					
					addme = false
					break
				}
			}
			if(addme){
				$('#'+val).css({'background-color':'#d8d5d5','box-shadow':''})
				$scope.selectedStatus.push(val);
			}
		}
		else{
			$('#'+val).css({'background-color':'#d8d5d5','box-shadow':''})
			$scope.selectedStatus.push(val);
		}
		to['Status'] = $scope.selectedStatus;
	}
	
	
	$scope.setEffectivedate = function(val,to){	
		
		to['EffectiveDate'] = val;
		if($scope.selectedDate == val.displayvalue){
			$scope.showCustom = false;
			$('.filterBydate').css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
			$scope.selectedDate = '';
		}
		else{
			$scope.showCustom = true;
			$scope.selectedDate = angular.copy(val.displayvalue);	
			$('.filterBydate').css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
			$('#'+$scope.selectedDate.replace(/\s+/g, '')).css({'box-shadow':'1.18px 3px 2px 1px rgba(0,0,0,0.40)','background-color':'#d8d5d5'})
		}
		
		if(typeof(val.actualvalue) == "object"){
			var date = []
			for(k in val.actualvalue){
				date.push(val.actualvalue[k])
			}
			$('#customPicker').find('input').each(function(i){
				if(i == 0){
					if(date[i] < date[Number(i+1)]){
						$(this).val(date[i])
						$(this).parent().children().each(function(){
							$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
						})
					}				
					else{
						$(this).val(date[Number(i+1)])
						$(this).parent().children().each(function(){
							$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
						})
					}
				}
				else{
					$(this).val(date[Number(i-1)])
					$(this).parent().children().each(function(){
						$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
					})
				}
			}) 			
		}
		else if(val.displayvalue == 'Custom'){
			$('#customPicker').find('input').each(function(i){
				$(this).parent().children().each(function(){
					$(this).css({'cursor': 'pointer'}).removeAttr('disabled').val('')
				})
			})
		}
		else{
			$('#customPicker').find('input').each(function(i){
				$(this).val(val.actualvalue)
				$(this).parent().children().each(function(){
					$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
				})
			}) 	
		}
	}
	
	$scope.showCustom = false;
	$scope.selectedDate = '';
	
	$scope.clearSort = function (id) {
		$(id).find('i').each(function () {
			$(this).removeAttr('class').attr('class', 'fa fa-minus fa-sm');
			$('#' + $(this).attr('id').split('_')[0] + '_Icon').removeAttr('class');
		});
		$scope.fieldArr.sortBy =  [];
		$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))	
	}

	$scope.buildFilter = function (argu1) {
		var argu2 = []
		for (k in $scope.fields) {
			if ($scope.fields[k].type === 'string') {
				argu2.push({
					"columnName" : $scope.fields[k].name,
					"operator" : "LIKE",
					"value" : argu1
				})
			}else if($scope.fields[k].type === 'select' && $scope.fields[k].name != 'Status'){
				argu2.push({
					"columnName" : $scope.fields[k].name,
					"operator" : "=",
					"value" : argu1
				})
			}
		}
		return argu2;
		
	}
	
	$scope.searchFilter = function (val){
		val = removeEmptyValueKeys(val)
		
		$scope.fieldArr.start = 0
		$scope.fieldArr.count = len;
		$scope.fieldArr.params = [];

		$scope.adFilter = {
			"filters":{
				"logicalOperator": "AND",
				"groupLvl1": [
				  {
					"logicalOperator": "AND",
					"groupLvl2": [
					  {
						"logicalOperator": "AND",
						"groupLvl3": []
					  }
					]
				  }
				]
			},
			"sorts":[],
			"start": $scope.fieldArr.start,
			"count": $scope.fieldArr.count
		}
		
		
		for(var i in $scope.fieldArr)
		{
			if(i == 'sortBy')
			{
				for(var j in $scope.fieldArr[i])
				{
					$scope.adFilter.sorts.push({"columnName":$scope.fieldArr[i][j].ColumnName,"sortOrder":$scope.fieldArr[i][j].ColumnOrder})
					console.log($scope.fieldArr[i][j])
				}
			}
		}


		for(var j in Object.keys(val)){
			if(val[Object.keys(val)[j]]){
				if(Object.keys(val)[j] == 'Status'){
				
					$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3 = [{
						"logicalOperator" : (val[Object.keys(val)[j]].length >= 1)?'OR':'AND',
						"clauses" : []
					}]


						for(var i in val[Object.keys(val)[j]]){
						

							$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3[0].clauses.push({
								"columnName": Object.keys(val)[j],
								"operator": "=",
								"value": val[Object.keys(val)[j]][i]
							})	

					}
				}else if(Object.keys(val)[j] == 'EffectiveDate'){
					
					$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
						"logicalOperator" : "AND",
						"clauses" : [{
							"columnName": "EffectiveFromDate",
							"operator": $('#startDate').val() == $('#endDate').val() ? '=': $('#startDate').val() > $('#endDate').val() ? '<=' : '>=',
							"value": $('#startDate').val()
						}]
					})
	
					$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
							"logicalOperator" : "AND",
							"clauses" : [{
								"columnName": "EffectiveFromDate",
								"operator": $('#startDate').val() == $('#endDate').val() ? '=': $('#startDate').val() < $('#endDate').val() ? '<=' : '>=',
								"value": $('#endDate').val()
							}]
						})
	
	
	
				}else if(Object.keys(val)[j] == 'SearchSelect'){
					
					val.SearchSelect = JSON.parse(val.SearchSelect)
					
					
					$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
						"logicalOperator" : "OR",
						"clauses" : [{
							"columnName": val.SearchSelect.name,
							"operator": (val.SearchSelect.type == 'select') ? "=" : "LIKE",
							"value": val.keywordSearch
						}]
				})
	
	
	
				}else if(Object.keys(val)[j] == 'keywordSearch' && !val['SearchSelect']){

					
						$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
							"logicalOperator" : "OR",
							"clauses" : $scope.buildFilter(val[Object.keys(val)[j]])
						})
					
				}				
			}
		}
		$scope.initCall($scope.adFilter)
		setTimeout(function(){
			$('select[name=SearchSelect]').val(null).trigger("change");
		},100)
		$scope.filterParams = {};
		$('.filterBydate').each(function(){
			$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
		})
	
		$scope.selectedStatus = [];	
		$('.filterBystatus').each(function(){
			$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
		})
		
		$scope.showCustom = false;
		$scope.selectedDate = '';
	}
	
		$scope.clearFilter = function () {
			$scope.fieldArr = {
				"start" : 0,
				"count" : 20,
				"sortBy" : []
			}
				
			setTimeout(function(){
				$('select[name=SearchSelect]').val(null).trigger("change");
			},100)
			$scope.filterParams = {};
			$('.filterBydate').each(function(){
				$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
			})
	
			$scope.selectedStatus = [];	
			$('.filterBystatus').each(function(){
				$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
			})
			
			$scope.showCustom = false;
			$scope.selectedDate = '';
			$('.customDropdown').removeClass('open');
	
			$scope.initCall($scope.uorQueryConstruct($scope.fieldArr))
		}
	/*****Search ends *******/





	$scope.triggerSelect = function(arg)
	{
		if(arg.name == 'RoleID')
		{
			$scope.remoteDataConfig() 
		}
		else if(arg.name == 'TimeZone')
		{
			setTimeout(function(){
				var parentElement = $(".parent");  
					$("select[name='keywordSearch']").select2({
						dropdownParent: parentElement
					})
			},100)
			
		}
		

	}

















































	$scope.limit = 500;
    $(document).ready(function () {
        $scope.remoteDataConfig = function () {
			setTimeout(function(){

			var parentElement = $(".parent"); 
             
            $("select[name='keywordSearch']").select2({
                ajax : {
                    url : BASEURL + RESTCALL.CreateRole,
                    headers : {
                        "Authorization" : "SessionToken:" + sessionStorage.SessionToken,
                        "source-indicator":configData.SourceIndicator,
                        "Content-Type" : "application/json"
                    },
                    dataType : 'json',
                    delay : 250,
                    xhrFields : {
                        withCredentials : true
                    },
                    beforeSend : function (xhr) {
                        xhr.setRequestHeader('Cookie', document.cookie),
                        xhr.withCredentials = true
                    },
                    crossDomain : true,
                    data : function (params) {
                        var query = {
                            start : params.page * $scope.limit ? params.page * $scope.limit : 0,
                            count : $scope.limit
                        }

                        if (params.term) {
                            query = {
                                search : params.term,
                                start : params.page * $scope.limit ? params.page * $scope.limit : 0,
                                count : $scope.limit
                            };
                        }
                        return query;
                    },
                    processResults : function (data, params) {
                        params.page = params.page ? params.page : 0;
                        var myarr = []

                        for (j in data) {
                            myarr.push({
                                'id' : data[j].RoleID,
                                'text' : data[j].RoleName
                            })
						}
						myarr.push({
							'id' : "Super Admin",
							'text' : "Super Admin"
						})

                        return {
                            results : myarr,
                            pagination : {
                                more : data.length >= $scope.limit
                            }
                        };

                    },
                    cache : true
                },
                placeholder : 'Select',
                minimumInputLength : 0,
                allowClear : true,
				dropdownParent: parentElement

			})
		},1000)
		}
		$scope.remoteDataConfig()
		
       
    });



	/*** To Maintain Alert Box width, Size, Position according to the screen size and on scroll effect ***/

            $scope.widthOnScroll = function()
            {
            	var mq = window.matchMedia( "(max-width: 991px)" );
                var headHeight
                if (mq.matches) {
                 headHeight =0;
                 $scope.alertWidth = $('.pageTitle').width();
            	} else {
                   $scope.alertWidth = $('.pageTitle').width();
                	headHeight = $('.main-header').outerHeight(true)+10;
                }
                $scope.alertStyle=headHeight;
            }

            	$scope.widthOnScroll();

            /*** On window resize ***/
    		$(window).resize(function(){
    			$scope.$apply(function () {
                    $scope.alertWidth = $('.alertWidthonResize').width();
    			});
			});
			
			$('.DatePicker').datetimepicker({
				format:"YYYY-MM-DD",
				showClear: true
			}).on('dp.change', function(ev){
				$scope['filterParams'][$(ev.currentTarget).attr('id')] = $(ev.currentTarget).val()
			}).on('dp.show', function(ev){
				$(this).change();	
			})

			$(document).ready(function () {
		$(".FixHead").scroll(function (e) {
			var $tablesToFloatHeaders = $('table.maintable');
			//console.log($tablesToFloatHeaders)
			$tablesToFloatHeaders.floatThead({
				useAbsolutePositioning: true,
				scrollContainer: true
			})
			$tablesToFloatHeaders.each(function () {
				var $table = $(this);
				//console.log($table.find("thead").length)
				$table.closest('.FixHead').scroll(function (e) {
					$table.floatThead('reflow');
				});
			});
		})
		$(".FixHeadDraft").scroll(function (e) {
			var $tablesToFloatHeaders = $('table.drafttable');
			// console.log($tablesToFloatHeaders)
			$tablesToFloatHeaders.floatThead({
				useAbsolutePositioning: true,
				scrollContainer: true
			})
			$tablesToFloatHeaders.each(function () {
				var $table = $(this);
				//console.log($table.find("thead").length)
				$table.closest('.FixHeadDraft').scroll(function (e) {
					$table.floatThead('reflow');
				});
			});
		})
		


		$(window).bind("resize",function(){
			setTimeout(function(){
             autoScrollDiv();
			},300)
			if($(".dataGroupsScroll").scrollTop() == 0){
				$(".dataGroupsScroll").scrollTop(50)
			}
			
			
		})
		$(window).trigger('resize');  
		$('#DraftListModal').on('shown.bs.modal', function (e) {
				$('body').css('padding-right',0);
				$(".draftViewCls").scrollTop(0);
			})
	})
	$scope.TotalCount = 0;    
	$scope.getCountbyStatus = function(argu)
	{
			$http.get(BASEURL+"/rest/v2/users/"+argu.actualvalue+"/count").success(function(data)
			{
				
				argu.TotalCount = data.TotalCount;
				$scope.TotalCount = $scope.TotalCount + data.TotalCount;
				return data.TotalCount
			})
	}
			

		$scope.getCurrentDrafts = function()
		{
		
			$http.post(BASEURL + "/rest/v2/draft/UserProfile/readall",
			{'start' : 0,'count' : 20 }).success(function(data){
					console.log(data,"data")
					$scope.draftdatas = data;
					$scope.dataLen = data;

			}).error(function(error){
				$scope.alerts = [{
				type : 'Error',
				msg : error.responseMessage	//Set the message to the popup window  /v2/draft/read/{tableName}
			}];
			})
			
		}

		$scope.gotodeleteDraft = function()
		{
			
			$scope.deleteObj = {
				'UserID' : delData.UserID,
				'Entity' : delData.Entity,
				'BPK' : delData.BPK
			}
			   // console.log($scope.deleteObj,"delData")
				$http.post(BASEURL + "/rest/v2/draft/delete",$scope.deleteObj).success(function(response){
					
						//if(response.Status === 'Success'){
									$('.modal').modal("hide");
									$scope.alerts = [{
									type : 'success',
									msg : "Deleted successfully"	
								}];
										//}
				}).error(function(error){
					
					/*$scope.alerts = [{
					type : 'Error',
					msg : error.responseMessage	
				}];*/
					
				})
			
			
		}

		// console.log($stateParams.input.UserProfileDraft,"UserProfileDraft")
		draftlen = 20;
		argu = {};
		var loadMoreDrafts = function(){
			// console.log($scope.dataLen,$scope.dataLen.length)
			if(($scope.dataLen.length >= 20)){
				argu.start = draftlen;
				argu.count = 20;

					$http.post(BASEURL + "/rest/v2/draft/UserProfile/readall",argu).success(function(response){
						console.log(response,"response")
					$scope.dataLen = response;
					if(response.length != 0){
						$scope.draftdatas = $scope.draftdatas.concat($scope.dataLen)
						draftlen = draftlen + 20;		
					}	
					}).error(function(error){
							$scope.alerts = [{
							type : 'Error',
							msg : error.responseMessage	
						}];
					})
			}
		//	console.log($scope.dataLen)
		}	

		var debounceHandlerDraft = _.debounce(loadMoreDrafts, 700, true);
		setTimeout(function(){

		$(document).ready(function(){

			$('.draftViewCls').on('scroll', function() { 
				                                         
				$scope.widthOnScroll();
				if( Math.round($(this).scrollTop() + $(this).innerHeight())>=$(this)[0].scrollHeight) {
					debounceHandlerDraft();
				}
			});

		})

	},200)
	
		// $rootScope.clickedOnDraftIcon = false;
	    $scope.gotoEditDraft = function(opr,index,draftblob)
		{
			
			var gostateObj = {
				'decrData' : "",
				'draftdata' : draftblob,
				'FromDraft' : true,
				'typeOfDraft' : ''
			}
			var decryptedDraft = $filter('hex2a')(draftblob.Data ? draftblob.Data : draftblob.totData.Data)
			var jsonDraft = $filter('Xml2Json')(decryptedDraft)
			var backupWholeData = angular.copy(jsonDraft)
			//console.log(jsonDraft,"jsonDraft")
			for(i in backupWholeData)
			{
				for(j in backupWholeData[i])
				{
					// console.log(backupWholeData[i],'backupWholeData[i]')
					// backupWholeData[i][j] = (backupWholeData[i][j] == 'true') ? true : (backupWholeData[i][j] == 'false') ? false : backupWholeData[i][j] ;
					if(typeof backupWholeData[i][j] == 'object')
					{
						var backupObj = backupWholeData[i][j];
						delete backupWholeData[i][j];
						backupWholeData[i][j] = [];
						backupWholeData[i][j].push(backupObj);
						//console.log(backupObj,jsonDraft[i][j])
					}
					
				}
				backupWholeData[i] = cleantheinputdata(backupWholeData[i])
				 gostateObj.decrData = backupWholeData[i];
			}

			var specificReadObject = {
					"UserID": gostateObj.draftdata.UserID,
					"Entity": gostateObj.draftdata.Entity,
					"BPK": gostateObj.draftdata.BPK
				}
			$http.post(BASEURL + RESTCALL.DraftSpecificRead,specificReadObject).then(function(response){
					
					// console.log(response,response.headers().type)
					gostateObj.typeOfDraft = response.headers().type
						
				},function(error){
					
						$scope.alerts = [{
							type : 'Error',
							msg : error.responseMessage	
						}];
						
				})

				
				$state.go('app.adduser',{input:gostateObj})
			

		}

			//  if(opr == "View" )
			//  {
			// 	$scope.listviewLog();
			// 	$scope.userDataFn(gostateObj.decrData,index,true);
			// 	$scope.gotoView(gostateObj.decrData,true,gostateObj)
			//  }
			//  else
			//  {
			// 	// console.log(gostateObj.decrData,"decrData")
			// 	$scope.gotoEdit(gostateObj.decrData,false,gostateObj)
			//  }
			

    });