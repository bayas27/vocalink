VolpayApp.controller('usermanagementdetail',function($scope,$rootScope,$stateParams,$http,$filter, $state, userMgmtService,CommonService, $timeout, bankData, GlobalService, $location,LogoutService,editservice) {
    //  $scope.madeChanges = true;

    $rootScope.$on("MyEvent", function(evt,data){ 
			$("#changesLostModal").modal("show");
		})


   


     if(!$stateParams.input)
     {
         $state.go('app.users');
     }

    $scope.userId = sessionStorage.UserID;
    $scope.data = $stateParams.input.Data;
    console.log("aa",$scope.userId,RESTCALL.CreateRole)

   

    $scope.ArrVal = [];
    for(let i in $scope.data.UserRoleAssociation)
    {
        $scope.ArrVal.push($scope.data.UserRoleAssociation[i].RoleID) 
    }   
    $scope.data.RoleID1 = $scope.ArrVal;


	$scope.permission = $stateParams.permission;
    $scope.isSuperAdmin = sessionStorage.ROLE_ID;
	$scope.cUser = sessionStorage.UserID;
    $scope.selectOptions= [];
    $scope.pushAllRoleValues = [];
    $scope.entityDraft = $stateParams.input.DraftTotObj
	//console.log($stateParams.input,"$stateParams.input")
    $scope.setInitVal = function(_query)
    {
        return $http({
			method : "GET",
			url : ($scope.data.UserID === $scope.userId) ? BASEURL + '/rest/v2/userrole/self/readall' : BASEURL + RESTCALL.CreateRole,
			params : ($scope.data.UserID === $scope.userId) ? '' : _query
		}).then(function (response) {
            // $scope.selectOptions = response.data;

            // $("[name=RoleID]").select2('destroy')

            // $("[name=RoleID]").val($scope.ArrVal);
            // $("[name=RoleID]").select2();


            //return $scope.selectOptions;
		})
    }
	$scope.objRolename = []
	if($scope.data.UserID === $scope.userId){
		$http({
			method : "GET",
			url : BASEURL + '/rest/v2/roles',
			params : ''
		}).then(function (res) {
			$scope.objRolename = res.data
		})
	}
     $scope.setInitMultipleVal = function(_multiplequery,IndexVal){
          $scope.selectMultOptions = [];
          $scope.selectOptions = [];
          $scope.pushAllRoleValues = [];
         $http({
			method : "GET",
			url : ($scope.data.UserID === $scope.userId) ? BASEURL + '/rest/v2/userrole/self/readall' : BASEURL + RESTCALL.CreateRole,
			params : ($scope.data.UserID === $scope.userId) ? '' : _multiplequery
		}).then(function (response) {
            console.log($scope.data.RoleID1,'$scope.data.RoleID1')
            if(response.data.length > 0){
                for(i in response.data){
                    for(var rolename in $scope.objRolename){
                        //console.log($scope.objRolename[rolename])
                        if($scope.objRolename[rolename].RoleID === response.data[i].RoleID){
                            response.data[i]['RoleName'] = $scope.objRolename[rolename].RoleName;
                        }
                    } 
                    $scope.selectMultOptions.push(response.data[i]);
                    $scope.pushAllRoleValues.push(response.data[i]);
                    $scope.selectOptions =  removeDuplicates($scope.pushAllRoleValues,'RoleID');

                    // console.log($scope.selectMultOptions,'cddfdfdf',$scope.selectOptions)
                    setTimeout(function(){
                        $scope.remoteDataConfig()
                    },100)
                }
            }
            else{

                $scope.data.UserRoleAssociation.splice(IndexVal,1)
                // console.log(IndexVal,'Index', $scope.data)
               
            }
		})
		//console.log($scope.selectMultOptions)
    }



   // $timeout(function(){
        $scope.tZoneOptions = [];
        $scope.tZoneOptions.push($scope.data.TimeZone)
    //},100)

     function triggerSelectDrops()
        {
                // $scope.select2Arr = ["RoleID","Subsec_Status"]
                $(document).ready(function(){
                        $("[name='Subsec_Status']").select2({
                            placeholder : 'Select an option',
                            minimumInputLength: 0,
                            allowClear : true
                        })
                    })

        }
        setTimeout(function(){
        triggerSelectDrops();

        },100)


    $('#userFormTimeZone').select2().on("select2:open", function(evt) {
        $(this).find('option').remove('option')
        $(this).append('<option value="">--Select--</option>') 
        for(var jk in timeZoneDropValues.TimeZone){
         $(this).append('<option value='+timeZoneDropValues.TimeZone[jk].TimeZoneId+'>'+timeZoneDropValues.TimeZone[jk].TimeZoneId+'</option>')   
        }
        $(this).val($scope.data.TimeZone)
        
       });
       $('#userFormTimeZone').select2().on("select2:close", function(evt) {
        $(this).find('option').remove('option')
        $(this).append('<option value="">--Select--</option><option value='+$scope.data.TimeZone+'>'+$scope.data.TimeZone+'</option>')
        $(this).val($scope.data.TimeZone)  
       });
    

       $scope.roleArrvalues = [];

    $timeout(function(){
        

        if(!$scope.data.View)
        {
            $scope.data.View = false;
            
           


              _query = {
                // search : $scope.data.RoleID,
                start : 0,
                count : 100
            } 
           
              $scope.setInitVal(_query);


            for(k in $scope.data.UserRoleAssociation)
            {

                  _queryvals = {
                    search : $scope.data.UserRoleAssociation[k].RoleID,
                    start : 0,
                    count : 100
                }

                 $scope.setInitMultipleVal(_queryvals,k);
            }

           // console.log($scope.data.UserRoleAssociation,"in edit")
    
            
            
            $scope.remoteDataConfig()
			$('.appendSelect2').select2() 


             $scope.madeChanges = false;
            $scope.listen = function() {

                var Operation  = 'Edit';
                setTimeout(function(){
                    // console.log(Operation,"Operation",$scope.role,"$scope.role")
                    editservice.listen($scope,$scope.data,Operation,'UserManagement');
                },100)
            }
            $scope.listen();
           
        }
        
    },100)
    

    // setTimeout(function(){
    //        var test = $('#myRoleId');
    //     test.on("select2:select", function(event) {
    //         console.log($(event.currentTarget))
    //     var value = $(event.currentTarget).find("option:selected:last").val();
    //     // console.log(value);
    //     });
    
    // },100)

setTimeout(function(){

    $('#myRoleId').on('select2:select', function (e) {
        var data = e.params.data;
        $scope.updatesubmitted = false;
		console.log(data)
        $(e.currentTarget).find("option:selected:last").remove();
            //console.log(data,"__",$(e.currentTarget).find("option:selected:last"))
			if($scope.data.UserID === $scope.userId){
                delete data.other.User_ID;
                if(!$scope.data.UserRoleAssociation)
                {
                    $scope.data.UserRoleAssociation = [];
                }
				$scope.data.UserRoleAssociation.push(data.other)				
			}
			else{
				$scope.data.UserRoleAssociation.push({'RoleID':data.id})
			}
             $timeout(function(){
                        $scope.remoteDataConfig();
                        triggerSelectDrops();
                        // $scope.activatePicker(e);
                        for(i in $scope.data.UserRoleAssociation)
							{
								  _set_queryvals = {
										search : $scope.data.UserRoleAssociation[i].RoleID,
										start : 0,
										count : 100
									}
								
										$scope.setInitMultipleVal(_set_queryvals,i);
										
							}
                },100)
     });



   


 },100) 
       

       setTimeout(function(){
            $('#myRoleId').on('select2:unselect', function (e) {
                var data = e.params.data;
                console.log(data,"__")
                    for(i in $scope.data.UserRoleAssociation)
                    {
                        if($scope.data.UserRoleAssociation[i].RoleID == data.id)
                        {
                            $scope.data.UserRoleAssociation.splice(i,1);
                        }
                    }
 
                    for(k in $scope.data.UserRoleAssociation)
                    {
                            _set_queryvals = {
                                                search : $scope.data.UserRoleAssociation[k].RoleID,
                                                start : 0,
                                                count : 100
                                            }
                                        
                            $scope.setInitMultipleVal(_set_queryvals,k);
                    }

                     $timeout(function(){
                        $scope.remoteDataConfig();
                        triggerSelectDrops();
                        // $scope.activatePicker(e);
                       
                },100) 
                    
            });

       },50) 

      
    $scope.createDynamicSubsection = function(RoleArr)
    {
		console.log('RoleArr',RoleArr)
        var roleFlag = false;
       for(s in RoleArr)
       {
            if(RoleArr[s] == $scope.data.RoleID)
            {
                roleFlag = true;
            } 
       }
       if(!roleFlag)
       {
           setTimeout(function(){
                $($('.radiobtns')[0]).prop('checked', true);
           },100)
       }
    }

 

    $scope.editedLog = [];

    if($scope.data.View)
    {
    var len = 0;
    $http.post(BASEURL+RESTCALL.userAudiLog+'?count='+20+'&start='+len, {'UserID':$scope.data.UserID}).success(function(data){
        // alert()
        $scope.editedLog = data;
        $scope.dataLen = data;
        for(var j in $scope.editedLog){	
            for(var keyj in $scope.editedLog[j]){
                if(keyj == 'oldData' || keyj == 'newData'){
                    $scope.editedLog[j][keyj] = $filter('hex2a')($scope.editedLog[j][keyj])	
                    //console.log($scope.editedLog[j][keyj], $.parseXML($scope.editedLog[j][keyj]))
                    if($scope.editedLog[j][keyj].match(/</g) && $scope.editedLog[j][keyj].match(/>/g)){
                        var xmlDoc = $.parseXML($scope.editedLog[j][keyj]); //is valid XML
                        var xmlData = xmlDoc.getElementsByTagName($scope.editedLog[j].tableName);
                        var constuctfromXml = {};
                        var constuctfromXmlObj = {};
                        var constuctfromXmlarr = [];					
                        $(xmlDoc).children().each(function(e){
                            $(this).children().each(function(e){
                                var parentName = $(this).prop("tagName")
                                if($(this).children().length){
                                    constuctfromXml[parentName] = constuctfromXmlarr
                                    $(this).children().each(function(e){
                                        constuctfromXmlObj[$(this).prop("tagName")] = $(this).text()
                                        constuctfromXmlarr.push(constuctfromXmlObj)
                                        constuctfromXmlObj = {}
                                    })
                                }
                                else{
                                    constuctfromXml[parentName] = $(this).text()
                                }
                            })
                        });
                        $scope.editedLog[j][keyj] = constuctfromXml
                    }else{
                        $scope.editedLog[j][keyj] = false
                    }							
                }
            }
        }
        
        }).error(function(data){

        })
    }

    //I Load More datas on scroll
	var len = 20;
	var loadMore = function(){
		// console.log($scope.parentInput.parentLink+'/audit/readall',"hello")
		if(($scope.dataLen.length >= 20)){
            console.log("Got it")
			 $http.post(BASEURL+RESTCALL.userAudiLog+'?count='+20+'&start='+len, {'UserID':$scope.data.UserID}).success(function(data){
                //  console.log(data,"data")	
				$scope.dataLen = data;			
				if(data.length != 0){
					$scope.editedLog = $scope.editedLog.concat($scope.dataLen)
                    // console.log($scope.editedLog,"hiii")
                    console.log($scope.editedLog.length)
					len = len + 20;		
				}
			})
		}
	}

    $(document).ready(function(){
        //var debounceHandler = _.debounce(loadMore, 700, true);
        $('.editBody').on('scroll', function(){ 
            if( Math.round($(this).scrollTop() + $(this).innerHeight())>=$(this)[0].scrollHeight) {
                //debounceHandler();
                // console.log('came')
            }
        });
    })

    $scope.auditLogDetails = "";
    $scope.commentVal = "";	
    
	$scope.costructAudit = function(argu){
		
		$scope.auditLogDetails = argu
		$('#auditModel').find('tbody').html('')

		if(argu.oldData && argu.newData){
			$('#auditModel').find('tbody').append('<tr><th>Field</th><th>Old Data</th><th>New Data</th></tr>')			
		}else{
			$('#auditModel').find('tbody').append('<tr><th>Field</th><th>Data</th></tr>')
		}
		var _keys = ''
		
		if($.isPlainObject(argu.oldData) && $.isPlainObject(argu.newData)){
			_keys = (Object.keys(argu.oldData).length >= Object.keys(argu.newData).length) ? Object.keys(argu.oldData) : Object.keys(argu.newData)
		}else if($.isPlainObject(argu.oldData)){
			_keys = Object.keys(argu.oldData)
		}else if($.isPlainObject(argu.newData)){
			_keys = Object.keys(argu.newData)
		}
	
		
		for(var j in _keys){
			if(!_keys[j].match(/_PK/g)){
				var _tr = ""
				if(j%2){
					_tr = "<tr style='background-color: rgb(245, 245, 245)'>"
				}else{
					_tr = "<tr style='background-color: #fff'>"
				}
				_tr = _tr +"<td>"+$filter('camelCaseFormatter')(_keys[j])+"</td>";
				if(argu.oldData && argu.newData){
					if(argu.oldData){
							_tr = _tr + "<td>"
						if(argu.oldData[_keys[j]]){
							if(typeof(argu.oldData[_keys[j]]) == 'object'){
								_tr = _tr + "<pre>" + $filter('json')(argu.oldData[_keys[j]]) + "</pre>"
							}else{

                               // console.log("yes",is_hexadecimal(argu.oldData[_keys[j]]))
                                console.log("hex",argu.oldData[_keys[j]], is_hexadecimal(argu.oldData[_keys[j]]))

                                if(is_hexadecimal(argu.oldData[_keys[j]]) && argu.oldData[_keys[j]].length != 1)
                                {
                                    _tr = _tr + "<pre>"+ $filter('beautify')($filter('hex2a')(argu.oldData[_keys[j]]))+"</pre>";
                                }
                                else{
                                    _tr = _tr + argu.oldData[_keys[j]];
                                }

								
							}
						}
							_tr = _tr + "</td>"
					}
					if(argu.newData){
						if(argu.newData[_keys[j]]){
							if(argu.oldData && argu.newData[_keys[j]] != argu.oldData[_keys[j]]){
								_tr = _tr + "<td class=\"modifiedClass\">"
							}else{
								_tr = _tr + "<td>"								
							}
							if(typeof(argu.newData[_keys[j]]) == 'object'){
								_tr = _tr + "<pre>" + $filter('json')(argu.newData[_keys[j]]) + "</pre>"
							}else{

                                if(is_hexadecimal(argu.newData[_keys[j]]) && argu.newData[_keys[j]].length != 1)
                                {
                                    _tr = _tr + "<pre>"+ $filter('beautify')($filter('hex2a')(argu.newData[_keys[j]]))+"</pre>";
                                }
                                else{
                                    _tr = _tr + argu.newData[_keys[j]];
                                }


							//	_tr = _tr + argu.newData[_keys[j]];
							}
							_tr = _tr + "</td>"
						}
					}
				}else{
					if(argu.newData){
							_tr = _tr + "<td>"
						if(argu.newData[_keys[j]]){
							if(typeof(argu.newData[_keys[j]]) == 'object'){
								_tr = _tr + "<pre>" + $filter('json')(argu.newData[_keys[j]]) + "</pre>"
							}else{

                                if(is_hexadecimal(argu.newData[_keys[j]]) && argu.newData[_keys[j]])
                                {
                                    _tr = _tr + "<pre>"+ $filter('beautify')($filter('hex2a')(argu.newData[_keys[j]]))+"</pre>";
                                }
                                else{
                                    _tr = _tr + argu.newData[_keys[j]];
                                }


								//_tr = _tr + argu.newData[_keys[j]];
							}
						}
							_tr = _tr + "</td>"
					}else if(argu.oldData){
							_tr = _tr + "<td>"
						if(argu.oldData[_keys[j]]){
							if(typeof(argu.oldData[_keys[j]]) == 'object'){
								_tr = _tr + "<pre>" + $filter('json')(argu.oldData[_keys[j]]) + "</pre>"
							}else{

                                if(is_hexadecimal(argu.oldData[_keys[j]]) && argu.oldData[_keys[j]])
                                {
                                    _tr = _tr + "<pre>"+ $filter('beautify')($filter('hex2a')(argu.oldData[_keys[j]]))+"</pre>";
                                }
                                else{
                                    _tr = _tr + argu.oldData[_keys[j]];
                                }

								//_tr = _tr + argu.oldData[_keys[j]];
							}
						}
							_tr = _tr + "</td>"
					}
				}
				$('#auditModel').find('tbody').append(_tr)				
			}
		}		
		
		if((argu.action).match(/:/g)){
			$scope.commentVal = (argu.action).split(/:(.+)/)
		}
		else{
			$scope.commentVal = ""
		}
	}

    $scope.showaudit = function(argu){
		
		$scope.costructAudit(argu)
		$('#auditModel').modal('toggle');
    }
    var _quer;

    console.log($scope.data.View)

   



    $scope.gotoEdit = function(){
        $scope.data.View = false;
       
        _query = {
            search : $scope.data.RoleID,
            start : 0,
            count : 100
        }
        $scope.setInitVal(_query);

         for(k in $scope.data.UserRoleAssociation)
            {

                  _queryvals = {
                    search : $scope.data.UserRoleAssociation[k].RoleID,
                    start : 0,
                    count : 100
                }

                 $scope.setInitMultipleVal(_queryvals,k);
            }
      
        
         
        $timeout(function(){
           
            $scope.remoteDataConfig()
			$('.appendSelect2').select2() 
        },100)
		
        $scope.madeChanges = false;
            $scope.listen = function() {

                var Operation  = 'Edit';
                setTimeout(function(){
                    // console.log(Operation,"Operation",$scope.role,"$scope.role")
                    editservice.listen($scope,$scope.data,Operation,'UserManagement');
                },100)
            }
            $scope.listen();
        

    }

   

    


    $scope.gotoParent = function(){

        $state.go('app.users')
    }


 
$scope.gotoCancelFn = function()
        {
            $rootScope.dataModified =  $scope.madeChanges;
			$scope.fromCancelClick = true;
            console.log($scope.madeChanges,"$scope.madeChanges",$rootScope.dataModified)
			if (!$scope.madeChanges) {
				 $scope.gotoParent();
			}

        }

		$scope.gotoClickedPage = function()
		{
			if($scope.fromCancelClick || $scope.breadCrumbClicked)
			{
				$rootScope.dataModified = false;
				 $scope.gotoParent();
			}
			else
			{
				$rootScope.$emit("MyEvent2",true);
				
			}
		}

        $scope.gotoShowAlert = function()
		{
            console.log($scope.madeChanges,"$scope.madeChanges")
			$scope.breadCrumbClicked = true;
			if($scope.madeChanges)
			{
				$("#changesLostModal").modal("show");
			}
			else
			{
				$scope.gotoParent();
			}
		}

    var delData='';
	$scope.takeDeldata = function(val,Id){
		delData = $scope.data;
		//$scope.delIndex = Id;
    }

    $scope.deleteData = function() {
		delete $scope.data.$$hashKey;

		$scope.delObj = {};
		$scope.delObj.UserId = $scope.data.UserID;
		

		var restServer = RESTCALL.CreateNewUser+'/delete';

        $http.post(BASEURL+restServer,$scope.delObj).success(function(data){
            
                $rootScope.alertData = (data)?data.responseMessage:'Deleted Successfully'
                $state.go('app.users')
            
        }).error(function(data)
        {
            $scope.alerts = [{
                type : 'danger',
                msg : data.error.message
            }];
        })

        $('.modal').modal("hide");
        $('body').removeClass('modal-open')

		
		
	};
    
	
	$scope.activatePicker = function (sDate, eDate) {
	//console.log("e",e.currentTarget.id,$(e.currentTarget).attr('id'))
	//var dateId = $(e.currentTarget).attr('id');
	$(document).ready(function(){
		var start = new Date();
		var endDate = new Date();
		var end = new Date(new Date().setYear(start.getFullYear() + 1));
	console.log(sDate,eDate, start)
	
	$('#'+sDate).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			todayHighlight : true,
			format: 'yyyy-mm-dd',
			forceParse: false
			

		}).on('changeDate', function (selected) {
				
			//$('#' + eDate).datepicker('setStartDate', new Date($(this).val()));
			start = new Date(selected.date.valueOf());

			start.setDate(start.getDate(new Date(selected.date.valueOf())));
			$('#' + eDate).datepicker('setStartDate', start);

		});
			$('#' + eDate).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			format: 'yyyy-mm-dd',
			todayHighlight : true,
			forceParse: false
	
		}).on('changeDate', function (selected) {

			//$('#' + sDate).datepicker('setEndDate', new Date($(this).val()));
			endDate = new Date(selected.date.valueOf());

			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$('#' + sDate).datepicker('setEndDate', endDate);
		});
		
		
		$('#' + sDate).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + eDate).datepicker('setStartDate', new Date());
			}
		})
		$('#' + eDate).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + sDate).datepicker('setEndDate', null);
			}
		})
	})
	}
	$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate')
	
	
	 $scope.triggerPicker = function (e) {
     console.log($(e.currentTarget).prev())
		if ($(e.currentTarget).prev().is('.DatePicker')) {
			$scope.activatePicker('EffectiveFromDate', 'EffectiveTillDate');
		//$(e.currentTarget).prev().data("DateTimePicker").show();
		$(e.currentTarget).prev().datepicker("show");
		}
	};
	
	
	
	
	
	
    /*  $scope.activatePicker = function (e) {
		
				var prev = null;
				$('.DatePicker').datetimepicker({
					format : "YYYY-MM-DD",
					useCurrent : false,
					showClear : true
				}).on('dp.change', function (ev) {
                    if($(ev.currentTarget).attr('roledateattr'))
                    {
                    $scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')] = $(ev.currentTarget).val()

                    }
				}).on('dp.show', function (ev) {
                if($(ev.currentTarget).attr('roledateattr'))
                {
                    $scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')] = $(ev.currentTarget).val()

                }
                }).on('dp.hide', function (ev) {
                        if($(ev.currentTarget).attr('roledateattr') && $(ev.currentTarget).val())
                        {
                                            $scope[$(ev.currentTarget).attr('ng-model').split('.')[0]][$(ev.currentTarget).attr('roledateattr')] = $(ev.currentTarget).val()

                        }
				});
			}

		$scope.triggerPicker = function (e) {
			$('.input-group-addon').on('click focus', function(e){
			    $(this).prev().focus().click()
			});
			
            if ($(e.currentTarget).prev().is('.DatePicker')) {
                $scope.activatePicker($(e.currentTarget).prev());

                if($(e.currentTarget).attr('roledateattr') && $(e.currentTarget).val())
                {
                $('input[roledateattr=' + $(e.currentTarget).prev().attr('roledateattr') + ']').data("DateTimePicker").show();

                }
            }
		}; */





    $scope.addNewRoleSection = function(index,e)
    {

         var Objkey = [];

        if($scope.data.UserRoleAssociation[index] && $scope.data.UserRoleAssociation[index].$$hashKey)
        {
            delete $scope.data.UserRoleAssociation[index].$$hashKey;

        }

        Object.keys($scope.data.UserRoleAssociation[index]).forEach(function(key,value)
        {
           
            if($scope.data.UserRoleAssociation[index][key] != '')
            {
                Objkey.push(key)
           
            }
        })
        if(Objkey.length >= 3)
        {
            // $('.setDynamicWidth').css({'height':'102px'})


             $('.setDynamicWidth').animate({
                          scrollTop: ($("#"+index).outerHeight() * (index + 1 )) + 'px'});

                $scope.data.UserRoleAssociation.push({});

                $timeout(function(){
                        $scope.remoteDataConfig();
                        triggerSelectDrops();
                        $scope.activatePicker(e);
                },500)
        }
   

     }

    $scope.removeCurrentRoleSection = function(index)
    {
        $scope.data.UserRoleAssociation.splice(index,1);
         $('#RoleId'+index).select2('destroy')
               $('#RoleId'+index).find('option').remove('option')
                 for(var jk in $scope.data.UserRoleAssociation){
                   //  $('#RoleId'+index).append('<option value='+$scope.data.UserRoleAssociation[jk].RoleID+'>'+$scope.data.UserRoleAssociation[jk].RoleID+'</option>')   
                    }


                   if($scope.data.UserRoleAssociation[index])
                    {

                        if($scope.data.UserRoleAssociation[index].RoleID)
                        {

							for(i in $scope.data.UserRoleAssociation)
							{
								  _set_queryvals = {
										search : $scope.data.UserRoleAssociation[i].RoleID,
										start : 0,
										count : 100
									}
									// console.log($scope.data.UserRoleAssociation,$scope.data.UserRoleAssociation[index].RoleID,"index")
										$scope.setInitMultipleVal(_set_queryvals,i);
										$('#RoleId'+index).val($scope.data.UserRoleAssociation[index].RoleID)
							}
                      
                           
                        }
                        else
                        {
                            setTimeout(function(){

                                if($('#RoleId'+index).find('option').val().indexOf('undefined') != -1)
                                {
                                    $('#RoleId'+index).find('option').remove('option')
                                }
                            },100)
                        }
                    }
            $('#RoleId'+index).select2({
                ajax : {
                    url : ($scope.data.UserID === $scope.userId) ? BASEURL + '/rest/v2/userrole/self/readall' : BASEURL + RESTCALL.CreateRole,
                    headers : {
                        "Authorization" : "SessionToken:" + sessionStorage.SessionToken,
                        "source-indicator":configData.SourceIndicator,
                        "Content-Type" : "application/json"
                    },
                    dataType : 'json',
                    delay : 250,
                    xhrFields : {
                        withCredentials : true
                    },
                    beforeSend : function (xhr) {
                        xhr.setRequestHeader('Cookie', document.cookie),
                        xhr.withCredentials = true
                    },
                    crossDomain : true,
                    data : function (params) {
                        // console.log(params,"params")
                        var query = {
                            start : params.page * $scope.limit ? params.page * $scope.limit : 0,
                            count : $scope.limit
                        }

                        if (params.term) {
                            query = {
                                search : params.term,
                                start : params.page * $scope.limit ? params.page * $scope.limit : 0,
                                count : $scope.limit
                            };
                        }
						if($scope.data.UserID === $scope.userId){
							query = ''
						}
                        return query;
                    },
                    processResults : function (data, params) {
                        params.page = params.page ? params.page : 0;
                        var myarr = []
						if($scope.data.UserID === $scope.userId){
							for (j in data) {
								myarr.push({
									'id' : data[j].RoleID,
									'text' : data[j].RoleID,
									'other' : data[j]
								})
							}							
						}
						else{
							for (j in data) {
								myarr.push({
									'id' : data[j].RoleID,
									'text' : data[j].RoleName+'('+data[j].RoleID+')'
								})
							}
						}

                        return {
                            results : myarr,
                            pagination : {
                                more : data.length >= $scope.limit
                            }
                        };

                    },
                    cache : true
                },
                placeholder : 'Select an option',
                minimumInputLength : 0,
                allowClear : true

            })
            setTimeout(function(){
         triggerSelectDrops();

            },100)
    }

	
	$scope.activatePickerSub = function (sDate,eDate, index) {
	
		var start = new Date();
		var endDate = new Date();
		var end = new Date(new Date().setYear(start.getFullYear() + 1));
		console.log(start,sDate,eDate)
		$(document).ready(function(){
			console.log($('#'+sDate+index),$('#'+eDate+index))
			
			$('#'+sDate+index).datepicker({
			startDate: start,
			endDate: end,
			autoclose: true,
			todayHighlight : true,
			format: 'yyyy-mm-dd',
			useCurrent: false,
			showClear: true,
			forceParse: false
			

		}).on('changeDate', function (selected) {
				
			//$('#' + eDate+index).datepicker('setStartDate', new Date($(this).val()));
				start = new Date(selected.date.valueOf());

			start.setDate(start.getDate(new Date(selected.date.valueOf())));
			$('#' + eDate).datepicker('setStartDate', start);

		});
		
		$('#'+eDate+index).datepicker({
			startDate: start,
			//endDate: end,
			autoclose: true,
			todayHighlight : true,
			format: 'yyyy-mm-dd',
			useCurrent: false,
			showClear: true,
			forceParse: false
			

		}).on('changeDate', function (selected) {
				
			//$('#' + sDate+index).datepicker('setEndDate', new Date($(this).val()));
			endDate = new Date(selected.date.valueOf());

			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$('#' + sDate).datepicker('setEndDate', endDate);

		});
		
		$('#' + sDate+index).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + eDate+index).datepicker('setStartDate', new Date());
			}
		})
		$('#' + eDate+index).keyup(function (ev) {
			
			if (ev.keyCode == 8 || ev.keyCode == 46 && !$(this).val()) {
				
				//$dates.datepicker('setDate', null);
				$('#' + sDate+index).datepicker('setEndDate', null);
			}
		})
		
		setTimeout(function(){
			regexCheck();
			//FocusBlur();
			
	},100)
	})
	
	
		}
		
		$scope.activatePickerSubsec = function(e, index){
		index = $(e.currentTarget).attr("id")[($(e.currentTarget).attr("id").length)-1]
		$scope.activatePickerSub('Subsec_EffectiveFromDate','Subsec_EffectiveTillDate',index);
		setTimeout(function(){
			if($(e.currentTarget).attr('id') == 'Subsec_EffectiveFromDate'+index){
					console.log("from")
					$('#Subsec_EffectiveFromDate'+index).datepicker('show');
				}
				else{
					$('#Subsec_EffectiveTillDate'+index).datepicker('show');
				}
			
		},100)
		
		}
		
		
		
		$scope.triggerPickerSubSec = function (e,index) {

		console.log($(e.currentTarget).prev())
		$scope.activatePickerSubsec('Subsec_EffectiveFromDate','Subsec_EffectiveTillDate',index);
		
		//$(e.currentTarget).prev().datepicker("show");
		
		
	};
	
	$scope.datePlaceholderValue = "";
		function regexCheck() {
	$(document).ready(function () {
	
console.log("dateTypeKeydateTypeKey")
			$(".dateTypeKey").keypress(function (event) {
				//console.log(event.keyCode, String.fromCharCode(keycode))
				var regex = /^[0-9]$/;
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (!(keycode == '8')) {
					//	console.log(String.fromCharCode(keycode))
					if (regex.test(String.fromCharCode(keycode))) {
						if ($(this).val().length == 4) {
							$(this).val($(this).val() + "-");
						} else if ($(this).val().length == 7) {
							$(this).val($(this).val() + "-");
						} else if ($(this).val().length >= 10) {
							event.preventDefault();
						}
					} else {
						event.preventDefault();
					}
				}

			});
		
	
		
	

	});
		}
		function FocusBlur(){
		$(document).ready(function () {
	
	$(".dateTypeKey").focus(function () {

			$scope.datePlaceholderValue = $(this).attr('placeholder');
				
			$(this).attr('placeholder', 'YYYY-MM-DD');
		}).blur(function () {
	
			$(this).attr('placeholder', $scope.datePlaceholderValue);
		})
		})
	
		}
		
			regexCheck();
			FocusBlur();
	

       /*  $scope.activatePickerSubsec = function (e,index) {
        console.log(e,index,"e,index")
		var prev = null;
		$('.DatePicker').datetimepicker({
			format : "YYYY-MM-DD",
			useCurrent : false,
			showClear : true			
		}).on('dp.change', function (ev) {
            if($(ev.currentTarget).attr('roleattr'))
            {
            $scope.data.UserRoleAssociation[index][$(ev.currentTarget).attr('roleattr')] = $(ev.currentTarget).val();

            }
          
		}).on('dp.show', function (ev) {
        //    console.log( $(ev.currentTarget).attr('roleattr'))
            //  if($(ev.currentTarget).attr('roleattr'))
            // {
			$(ev.currentTarget).parent().parent().parent().parent().parent().parent().css({
				"overflow-y" : ""
			});
            // console.log($(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().length)
			if ($(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().length > 1 && $(ev.currentTarget).attr('roleattr')) {
				$(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().each(function () {
                     //console.log('ss',this,$(ev.currentTarget))
					if ($(this).is("#" + $(ev.currentTarget).parent().parent().parent().parent().parent().attr('id'))) {}
					else {
						$(this).css({
							"display" : "none"
						});
					}
				})
			}


            // }
		
		}).on('dp.hide', function (ev) {
			$(ev.currentTarget).closest('.setDynamicWidth').css({
				"overflow-y" : "auto"
			});
            $(ev.currentTarget).parent().parent().parent().parent().parent().parent().children().each(function () {
					$(this).css({
						"display" : ""
					});
				})
            if($(ev.currentTarget).attr('roleattr') && $(ev.currentTarget).val())
            {

            $scope.data.UserRoleAssociation[index][$(ev.currentTarget).attr('roleattr')] = $(ev.currentTarget).val()

            }

		});


	}


     $scope.triggerPickerSubSec = function (e,index) {

                if ($(e.currentTarget).prev().is('.DatePicker')) {
                    $scope.activatePickerSubsec($(e.currentTarget).prev(),index);
                     if($(e.currentTarget).attr('roleattr') && $(e.currentTarget).val())
                     {
                            $($(e.currentTarget).prev()).data("DateTimePicker").show();

                     }
                }
            };

 */






    $scope.updateData = function(data){
        

        if(data.RoleID1 != undefined)
        {

            $scope.backupRoleId1 = angular.copy(data.RoleID1);
            delete data.View;
            delete data.$$hashKey;
            delete data.RoleID1;
        

            // console.log($(".radiobtns:checked").val(),"checkedchecked")
            data.RoleID = $(".radiobtns:checked").val();
            $scope.roleIDbackup = angular.copy(data.RoleID);
            data = cleantheinputdata(data);
            data.UserRoleAssociation = cleantheinputdata(data.UserRoleAssociation)

            var changeMethod = ($stateParams.input.DraftTotObj)?'POST':'PUT';

                var userObj = {
                        url : BASEURL+RESTCALL.CreateNewUser,
                        method : changeMethod,
                        data : data,
                        headers : {
                            'Content-Type' : 'application/json',
                        }
                    };


            $http(userObj).success(function(data){
                
                    $rootScope.alertData = data.responseMessage;
                    $rootScope.dataModified = false;
                    CommonService.alertLoadCnt = 0;
                    $rootScope.NotifLoaded = false;
                    
                    $rootScope.$emit("CallParentMethod", {});
                    
                    $state.go('app.users')
                
            }).error(function(data)
            {
            
                $scope.data.RoleID1 = $scope.backupRoleId1;
                $scope.data.RoleID = $scope.roleIDbackup;
                $scope.alerts = [{
                    type : 'danger',
                    msg : data.error.message
                }];
                
            })
        }
    }

   

    $scope.limit = 100;
    $(document).ready(function () {
        $scope.remoteDataConfig = function () {
               
            $(".appendRoleId").select2({
                ajax : {
                    url : ($scope.data.UserID === $scope.userId) ? BASEURL + '/rest/v2/userrole/self/readall' : BASEURL + RESTCALL.CreateRole,
                    headers : {
                        "Authorization" : "SessionToken:" + sessionStorage.SessionToken,
                        "source-indicator":configData.SourceIndicator,
                        "Content-Type" : "application/json"
                    },
                    dataType : 'json',
                    delay : 250,
                    xhrFields : {
                        withCredentials : true
                    },
                    beforeSend : function (xhr) {
                        xhr.setRequestHeader('Cookie', document.cookie),
                        xhr.withCredentials = true
                    },
                    crossDomain : true,
                    data : function (params) {
                        // console.log(params,"params")
                        var query = {
                            start : params.page * $scope.limit ? params.page * $scope.limit : 0,
                            count : $scope.limit
                        }

                        if (params.term) {
                            query = {
                                search : params.term,
                                start : params.page * $scope.limit ? params.page * $scope.limit : 0,
                                count : $scope.limit
                            };
                        }
						if($scope.data.UserID === $scope.userId){
							query = ''
						}
                        return query;
                    },
                    processResults : function (data, params) {
                        params.page = params.page ? params.page : 0;
                        var myarr = []

                        if($scope.data.UserID === $scope.userId){
							for (j in data) {
								for(var rolename in $scope.objRolename){
									//console.log($scope.objRolename[rolename])
									if($scope.objRolename[rolename].RoleID === data[j].RoleID){
										myarr.push({
											'id' : data[j].RoleID,
											'text' : $scope.objRolename[rolename].RoleName+'('+data[j].RoleID+')',
											'other' : data[j]
										})
									}
								}
							}							
						}
						else{
							for (j in data) {
								myarr.push({
									'id' : data[j].RoleID,
									'text' : data[j].RoleName+'('+data[j].RoleID+')'
								})
							}
						}

                        return {
                            results : myarr,
                            pagination : {
                                more : data.length >= $scope.limit
                            }
                        };

                    },
                    cache : true
                },
                placeholder : 'Select an option',
                minimumInputLength : 0,
                allowClear : true
            })/* .on('select2:unselect', function (e) {
				console.log($(e.currentTarget).attr('n'))
				if($(e.currentTarget).attr('multiple')){
					for(i in $scope.data.UserRoleAssociation){
						if($scope.data.UserRoleAssociation[i].RoleID == e.params.data.id){
							$scope.$apply(function () {
								$scope.data.UserRoleAssociation.splice(i,1);
							});
						}
					}					
				}
            }).on('select2:select', function (e) {
					console.log(e)
					var data = e.params.data;
					$(e.currentTarget).find("option:selected:last").remove();
					console.log(data,"__",$(e.currentTarget).find("option:selected:last"))
					 if($scope.data.UserID === $scope.userId){
						 $scope.$apply(function () {
							$scope.data.UserRoleAssociation.push(data.other);
						});						 
					 }
					 else{
						 $scope.data.UserRoleAssociation.push(data.id)
					 }
		}); */
        }

       
    });

    $scope.widthOnScroll = function()
    {
        var mq = window.matchMedia( "(max-width: 991px)" );
        var headHeight
        if (mq.matches) {
         headHeight =0;
         $scope.alertWidth = $('.pageTitle').width();
        } else {
           $scope.alertWidth = $('.pageTitle').width();
            headHeight = $('.main-header').outerHeight(true)+10;
        }
        $scope.alertStyle=headHeight;
    }

    $(window).scroll(function(){
           // console.log("yes")
        $scope.widthOnScroll()
    })
        $scope.widthOnScroll()

        var backupdata;

         $http.get(BASEURL + RESTCALL.UserManagementPK).success(function (data, status) {
                 $scope.primarykey = data.responseMessage.split(',');
          
            }).error(function (data, status, headers, config) {
                    $scope.alerts = [{
                            type : 'danger',
                            msg : data.error.message
                        }];

                  $scope.alertStyle =  alertSize().headHeight;
                  $scope.alertWidth = alertSize().alertWidth;
                   });

        
    function checkPrimaryKeyValues(getDta)
    {
       if ($.isEmptyObject(getDta)) {
			$scope.primaryKeyALert = true;
		}
        else
        {
                    $.each(getDta, function (key, val) {
                    for (i = 0; i < $scope.primarykey.length; i++) {
                        if (!getDta[$scope.primarykey[i]]) {
                            $scope.primaryKeyALert = true;
                        }
                    }
                	})
        }
    }


    

        $scope.SaveAsDraft = function(formDatas1)
    {
		// console.log(formDatas1,"formDatas1")
        $scope.primaryKeyALert = false;
        checkPrimaryKeyValues(formDatas1);
        if($scope.primaryKeyALert)
		{
			$scope.madeChanges = false;
			$("#changesLostModal").modal('show');
		}
        else
        {
            $scope.callingDraftSave(formDatas1)
        }

      
    }

    $scope.SaveAsModalDraft = function()
    {
        $scope.callingDraftSave($scope.BackupDraft)
    }

    $scope.takeBackupData = function(data)
    {
        $scope.BackupDraft = data;
    }


      $scope.callingDraftSave = function(formDatas)
        {
            backupdata   = angular.copy(formDatas);
               delete backupdata.View;
                delete backupdata.$$hashKey;
        delete backupdata.RoleID1;
        backupdata.RoleID = $(".radiobtns:checked").val();       
        backupdata = cleantheinputdata(backupdata);
        backupdata.UserRoleAssociation = cleantheinputdata(backupdata.UserRoleAssociation);
        $rootScope.dataModified = false;
        	$http({
				method: 'POST',
				url: BASEURL + RESTCALL.CreateNewUser,
				data: backupdata,
				headers: {
					draft: true
				}
                }).then(function (response) {
                    console.log(response);
                    $rootScope.dataModified = false;
                    if (response.data.Status === "Saved as Draft") {
                        $scope.input = {
                                'responseMessage' : response.data.Status
                            }
                        $state.go("app.users", {input : $scope.input} )
                    }
                }, function (resperr) {

                    if(resperr.data.error.message == "Draft Already Exists")
					{
						$("#draftOverWriteModal").modal("show");
					}
					else
					{
						$scope.alerts = [{
								type: 'danger',
								msg: resperr.data.error.message
							}
						]

					}
                 
                    $timeout(callAtTimeout, 4000);

                })



           
        }

		 $scope.forceSaveDraft = function()
        {
             $http({
                        method: 'POST',
                        url: BASEURL + RESTCALL.CreateNewUser,
                        data: backupdata,
                        headers: {
                             draft: true,
                           'Force-Save': true
                        }

                    }).then(function (response) {
                        
                        if (response.data.Status === "Draft Updated") {
                            $("#draftOverWriteModal").modal("hide");
                           $scope.input = {
                                'responseMessage' : response.data.Status
                            }
                            $state.go("app.users", {input : $scope.input} )
                        }
                    }, function (resperr) {
						 $("#draftOverWriteModal").modal("hide");
                        $scope.alerts = [{
                                type: 'danger',
                                msg: resperr.data.error.message
                            }
                        ]
                        

                    })
        }

		$(document).ready(function(){
		$('#changesLostModal').on('shown.bs.modal', function (e) {
				$('body').css('padding-right',0)
			})
             $('#changesLostModal').on('hidden.bs.modal', function (e) {
				$scope.fromCancelClick = false;
                $scope.breadCrumbClicked = false;
			})

			 $('#draftOverWriteModal').on('shown.bs.modal', function (e) {
				$('body').css('padding-right',0)
			})
            $('#draftOverWriteModal').on('hidden.bs.modal', function (e) {
				
				setTimeout(function(){
					$scope.updateEntity = false;
				},100)
			
			})

		})


        /* BackUp Draft

         $scope.SaveAsDraft = function(formDatas)
        {
            // console.log(formDatas,"formDatas")
            var backupdata = angular.copy(formDatas);
                delete backupdata.View;
                    delete backupdata.$$hashKey;

            backupdata = cleantheinputdata(backupdata);
            backupdata.UserRoleAssociation = cleantheinputdata(backupdata.UserRoleAssociation)
                $http({
                    method: 'POST',
                    url: BASEURL + RESTCALL.CreateNewUser,
                    data: backupdata,
                    headers: {
                        draft: true
                    }

                }).then(function (response) {
                    console.log(response)
                    if (response.data.Status === "Saved as Draft") {
                        $scope.input = {
                                'responseMessage' : response.data.Status
                            }
                        $state.go("app.users", {input : formDatas} )
                    }
                }, function (resperr) {

                    $scope.alerts = [{
                            type: 'danger',
                            msg: resperr.data.error.message
                        }
                    ]
                    $timeout(callAtTimeout, 4000);

                })
            }

    */
    var delData='';
	$scope.takeDeldata = function(val,Id){
		delData = val;
		$scope.delIndex = Id;
	}

    function callAtTimeout() {
		$('.alert').hide();
	}

    $scope.deletedData = false;
    $scope.gotodeleteDraft = function()
		{
			
			$scope.deleteObj = {
				'UserID' : delData.UserID,
				'Entity' : delData.Entity,
				'BPK' : delData.BPK
			}
			   // console.log($scope.deleteObj,"delData")
				$http.post(BASEURL + "/rest/v2/draft/delete",$scope.deleteObj).success(function(response){
					
						//if(response.Status === 'Success'){
                            $scope.deletedData = true;
									$('.modal').modal("hide");
									$scope.alerts = [{
									type : 'success',
									msg : "Deleted successfully"	
								}];

                                $timeout(callAtTimeout, 4000);
										//}
				}).error(function(error){
					
					/*$scope.alerts = [{
					type : 'Error',
					msg : error.responseMessage	
				}];*/
					
				})
			
			
		}
			

});