VolpayApp.controller('forgotpasswordCtrl', function ($scope, $http, $state, $rootScope, $timeout, $location, GlobalService) {

    $('html').css({"background":"transparent"})

    $scope.fgpassword,$scope.resp = "";
	$scope.$on('$viewContentLoaded', function(){

		$timeout(function(){
            $('.main-sidebar').css({'display':'none'});
	        $('.notiIcons').css('display','none');
    		$('.content-wrapper').css({'margin-left':'0px'});
    	}, 200);
	});
	//alert('3');
	$scope.clicked = false;
	$scope.forgotPassword = function(val){
		$scope.clicked = true;
		// $('[data-toggle="popover"]').popover();
		//console.log(val)
		$scope.backupData = angular.copy(val);
		if($scope.resp == ""){
			$scope.URL = BASEURL + RESTCALL.ForgotPassword;
		}
		else if($scope.resp == 1){
			$scope.URL = BASEURL + RESTCALL.ForgotPwdOTP;
			delete $scope.backupData.EmailId;
		}
		else if($scope.resp == 2){

			if($scope.backupData.ConfirmPassword == $scope.backupData.Password){
				$scope.URL = BASEURL + RESTCALL.ForgotPwdReset;
				delete $scope.backupData.EmailId;
				delete $scope.backupData.ConfirmPassword;
			}
			else{
				$scope.showMsg('danger', "New password and confirm password does not match.");
				$('input[name="Password"]').focus();
				return false;
			}
		}
		$scope.RestCallFn();
	}
	
	$scope.RestCallFn = function(){
			
		/*$scope.objFgtPwd = {
			"UserId" : $scope.backupData.UserId,
			"Data": btoa(JSON.stringify($scope.backupData))
		}*/

		$scope.objFgtPwd = JSON.stringify($scope.backupData);
		//console.log($scope.objFgtPwd,$scope.backupData,$scope.resp)

		$http.post($scope.URL, $scope.objFgtPwd).success(function (data, status, header, config) {
			$scope.clicked = false;
			$scope.resp = Number($scope.resp) + 1;
			$timeout(function(){
				$('[data-toggle="popover"]').popover();
			},200)
			$scope.showMsg('success', data.responseMessage);
			if($scope.resp > 2){
				$rootScope.alerts = [{
				"type": "success",
				"msg":data.responseMessage
				}]
				/* GlobalService.passwordChanged = true;
				GlobalService.responseMessage = data.responseMessage;*/
				window.location.href="#/login";
			}
		}).error(function (data, status, header, config) {
			$scope.clicked = false;
			$scope.showMsg('danger', data.error.message);
		}); 
					
	}
	
	$scope.showMsg = function(x, y){
		$scope.alerts = [{
					type : x,
					msg : y
				}];
			/*$timeout(function(){
				$('.alert').hide();
			}, 4000);*/
		return $scope.alerts
	}	

	$scope.pwCancel = function(){
		$('.top-menu').css('display','block')
		GlobalService.logoutMessage = false;
		$rootScope.logOutMsg='';
		$location.path("login");
	}


		$scope.multipleEmptySpace = function (e) {


                if($.trim($(e.currentTarget).val()).length == 0)
        	    {
        	    $(e.currentTarget).val('')
        	    }
            }

        $scope.validatePW = function(pw,userid,e)
        {
            if(pw)
            {
                 $http.post(BASEURL + RESTCALL.ValidatePW, {'UserId':userid,'Password':pw}).success(function (data) {
					$scope.clicked = false;
                        //console.log(data)
                 }).error(function(data)
                 {
					$scope.clicked = false;	
                    $scope.showMsg('danger', data.error.message);
                     $(e.currentTarget).val('');

                 })
            }
        }

        $scope.popupClick = function()
        {
             $('[data-toggle="popover"]').popover();
             // console.log("clicked")
        }

		$scope.hidePassword = function () {

			$("#password").attr("type", "text")
			if ($("#password").attr("type") == "text") {

				setTimeout(function () {
					$("#password").attr("type", "password")
				}, 100)
			}
		}

		$scope.PasswordFocus = function () {

			$("#password").attr("type", "text")
			if ($("#password").attr("type") == "text") {

				setTimeout(function () {

					$("#password").attr("type", "password")
				}, 100)
			}

		}

		$scope.hideConfirmPassword = function () {

			$("#ConfirmPassword").attr("type", "text")
			if ($("#ConfirmPassword").attr("type") == "text") {

				setTimeout(function () {
					$("#ConfirmPassword").attr("type", "password")
				}, 100)
			}
		}

		$scope.ConfirmPasswordFocus = function () {

			$("#ConfirmPassword").attr("type", "text")
			if ($("#ConfirmPassword").attr("type") == "text") {

				setTimeout(function () {

					$("#ConfirmPassword").attr("type", "password")
				}, 100)
			}

		}

	
		
});