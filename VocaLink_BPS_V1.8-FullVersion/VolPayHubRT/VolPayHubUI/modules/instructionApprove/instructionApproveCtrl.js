VolpayApp.controller("instructionApproveCtrl", function($scope,$http){
 $scope.tableDisp = false;
   //date
   $http.get( BASEURL + '/rest/v2/payments/date').success(function (data, status) {
     $scope.date=data;   
   }).error(function (data, status, headers, config) {
    $scope.alerts = [{
      type: 'danger',
      msg: data.responseMessage
    }
    ];
  });

  //session
  $scope.getSession = function(sessionType){

    if(sessionType == 'cheque'){
      $http.get( BASEURL + '/rest/v2/session/readall').success(function (data, status) {
        $scope.sessions=data;
        $scope.value.SettlementSession =data[0];
      }).error(function (data, status, headers, config) {
        $scope.alerts = [{
          type: 'danger',
          msg: data.responseMessage
        }
        ];
      });
    }
    else {
      if(sessionType == 'bulk'){
        $scope.value.SettlementSession='';
        $http.get( BASEURL + '/rest/v2/bpsui/schedule').success(function (data, status) {
          $scope.sessions=data; 

        }).error(function (data, status, headers, config) {
          $scope.alerts = [{
            type: 'danger',
            msg: data.responseMessage
          }
          ];
        });
      }
    }
  }
//partycode
$scope.getParticipant = function(partyType){
  $http.get( BASEURL + '/rest/v2/partycode/readall?schemeNm='+partyType).success(function (data, status) {
   $scope.participantData=data;   
     triggerSelectDrops();
 }).error(function (data, status, headers, config) {
  $scope.alerts = [{
    type: 'danger',
    msg:data.responseMessage
  }
  ];
});
}



$scope.schemeChange = function(schemename){
  if($scope.value!=undefined){
    delete $scope.value.Participant;
  }
  $scope.getSession(schemename);
  $scope.getParticipant(schemename);

}



$scope.schemeChange('cheque');


function triggerSelectDrops() {
  $scope.select2Arr = ["IsForceReset", "Participant" , "Participant1"]
  $(document).ready(function () {

    for (var i in $scope.select2Arr) {
      $("select[name=" + $scope.select2Arr[i] + "]").select2({
        placeholder: 'Select an option',
        minimumInputLength: 0,
        allowClear: true

      })

      if ($scope.select2Arr[i] == 'Participant') {


        for (var jk in $scope.participantData) {
          $('#Participant').append('<option value=' + $scope.participantData[jk].actualvalue + '>' + $scope.participantData[jk].displayvalue + '</option>');
        }

      }
      if ($scope.select2Arr[i] == 'Participant1') {


        for (var jk in $scope.participantData) {
          $('#Participant1').append('<option value=' + $scope.participantData[jk].actualvalue + '>' + $scope.participantData[jk].displayvalue + '</option>');
        }

      }

    }
  })

}


$(window).resize(function () {

  $scope.$apply(function () {
    $scope.alertWidth = $('.tab-content').width();

  });

});

$scope.intial=function(){

  $scope.tableData=[]
  $scope.tableDisp=false;
  $scope.value.Scheme='cheque';
  $scope.value.Currency='SOL';
  $scope.value.SettlementDate='';
  delete $scope.value.Participant;  
  delete $scope.items;
  $scope.schemeChange('cheque'); 
}

$scope.instructionsearchRest = function(request){
    $http.post( BASEURL + '/rest/v2/payments/search',request).success(function (data, status) {
   if(!$scope.tableDisp &&  data.Details!=undefined){
    $scope.tableDisp=true;
  }
  $scope.tableData=data.Details;
  $scope.tableDataLen=data.count;
  $scope.items=$scope.tableData;
}).error(function (data, status, headers, config) {
  $scope.alerts = [{
    type: 'danger',
    msg: data.responseMessage
  }
  ];
});
}

$scope.instructionsearch =function(){

  Object.keys($scope.value).forEach(k =>(!$scope.value[k]&& $scope.value[k]!==undefined)&&delete $scope.value[k]);
  $scope.val=angular.copy($scope.value);   
  
  if($scope.value.SettlementDate!=undefined){
    $scope.val.SettlementDate = $scope.value.SettlementDate.actualvalue;
  }

  if($scope.value.SettlementSession!=undefined){
    $scope.val.SettlementSession = $scope.value.SettlementSession.actualvalue;  
  }
  


  $scope._value = [];
  $scope._qArray = []
  $scope._requestArray=[];
  $scope._key=[];
  let check=false;
  for(let key in $scope.val){
    $scope._key.push({"fieldName":key});
    if($scope.val[key] != null && $scope.val[key] !=""){
      $scope._value.push({"fieldName":key,"value":$scope.val[key]});
    }
  }

  
  for(let value in $scope.val){
    $scope._qArray.push({
      "columnName": value,
      "operator": '=',
      "value": $scope.val[value]
    });
    check=true;
  }
  if(check==true){
    for(i=0;i<$scope._qArray.length;i++){
      $scope._requestArray.push({
        "logicalOperator": "AND",
        "clauses":[$scope._qArray[i]]
      })    
    }
  }
  $scope.dataReq={
    "filters": {
      "logicalOperator": "AND",
      "groupLvl1": [{
        "logicalOperator": "AND",
        "groupLvl2": [{
          "logicalOperator": "AND",
          "groupLvl3": $scope._requestArray
        }
        ]
      }
      ]
    },
    "sorts": [
    ],
    "start": 0,
    "count": 20
  };

$scope.instructionsearchRest($scope.dataReq);

}


$scope.approveSave = function(){
  var validate='';
  $scope.tableData=angular.copy($scope.tableData);
  for(var i=0;i<$scope.tableData.length;i++){
    if($scope.tableData[i].Action==undefined){
      validate=true;
    }

  }
  var req= new Object();
  req.Details=$scope.tableData;
  if(validate){
    $scope.alerts = [{
      type: 'danger',
      msg: "Please select Approval action"
    }
    ];
  }else{
    $http.post( BASEURL + '/rest/v2/payments/unwound',req).success(function (data, status) {
     validate=false;
     $scope.alerts = [{
      type: 'success',
      msg: data.responseMessage
    }
    ];
  }).error(function (data, status, headers, config) {
     if(data.responseMessage){
      $scope.alerts = [{
      type: 'danger',
      msg: data.responseMessage
    }
    ];
    }else{
      $scope.alerts = [{
      type: 'danger',
      msg: data.error.errors[0].message
    }
    ];
    } 
  });
}




}


$scope.resetApprove = function(){
$scope.intial();
}


$scope.fields=
[{
  "field_name" : "SettlementDate",
  "label" : "Date",
  "type" : "select"
},{
  "field_name" : "SettlementSession",
  "label" : "Session",
  "type" : "select",
},{
  "field_name" : "Currency",
  "label" : "Currency",
  "type" : "select",
  "options" : ['SOL' , 'USD']
},{
  "field_name" : "Participant",
  "label" : "Participant",
  "type" : "select",
}]; 


$scope.tableFields=[
{
 "FieldName":"MakerId",
 "Label":"Maker ID",
},{
 "FieldName":"Participant",
 "Label":"Participant",
},{
 "FieldName":"Scheme",
 "Label":"Scheme",
},{
 "FieldName":"SettlementDate",
 "Label":"Date",
},{
 "FieldName":"SettlementSession",
 "Label":"Session",
},{
 "FieldName":"Currency",
 "Label":"Currency",
},
{
 "FieldName":"Action",
 "Label":"Action"
}
]

$scope.tableData=[];



$scope.itemsPerPage = 5;
$scope.currentPage = 0;  
$scope.range = function() {
  if($scope.items.length <= $scope.itemsPerPage){
    var rangeSize =1;

  }
  else if($scope.items.length > $scope.itemsPerPage && $scope.items.length < 2*$scope.itemsPerPage ){
    var rangeSize =2;
  }
  else if($scope.items.length > 2*$scope.itemsPerPage && $scope.items.length < 3*$scope.itemsPerPage ){
    var rangeSize =3;
  }
  else{
    var rangeSize =4;
  }

  var ret = [];
  var start;

  start = $scope.currentPage;
  if ( start > $scope.pageCount()-rangeSize ) {
    start = $scope.pageCount()-rangeSize+1;
  }

  for (var i=start; i<start+rangeSize; i++) {
    ret.push(i);
  }
  return ret;
};
$scope.navigate = function(n){
  $scope.currentPage=n-1;
}
$scope.prevPage = function() {
  if ($scope.currentPage > 0) {
    $scope.currentPage--;
  }

};
$scope.prevRange = function() {

  if ($scope.currentPage >= 4) {

    $scope.currentPage=$scope.currentPage-4;
  }

};
$scope.nextRange = function() {
  var len =$scope.items.length/$scope.itemsPerPage;
  if (len-$scope.currentPage > 4) {
    $scope.currentPage=$scope.currentPage+4;
  }

};
$scope.prevPageDisabled = function() {
  return $scope.currentPage === 0 ? "disabled" : "";
};


$scope.pageCount = function() {
  return Math.ceil($scope.items.length/$scope.itemsPerPage)-1;
};

$scope.nextPage = function() {
  if ($scope.currentPage < $scope.pageCount()) {
    $scope.currentPage++;
  }

};
$scope.getmore = function(){
  $scope.dataReq.start=$scope.dataReq.start+20;
  $scope.dataReq.count=$scope.dataReq.count;
  if($scope.dataReq.start < $scope.tableDataLen){
    $scope.instructionsearchRest($scope.dataReq);
  }
}

$scope.nextPageDisabled = function() {
  if($scope.currentPage === $scope.pageCount()){
    if($scope.dataReq.start>=$scope.tableDataLen){
      $scope.hide=false;  
    }
    else if($scope.tableDataLen<$scope.dataReq.count){
      $scope.hide=false;
    }
    else{
     $scope.hide=true; 
   }

 }
 else{
  $scope.hide=false; 
}
return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
};





});