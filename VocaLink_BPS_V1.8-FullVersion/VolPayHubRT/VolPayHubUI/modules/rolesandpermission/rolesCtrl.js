VolpayApp.controller('rolesCtrl', function ($scope, $rootScope, $http, bankData,$state,$stateParams, $location,$filter, userMgmtService, $timeout, GlobalService, LogoutService) {

	$scope.permission = {
		'C' : false,
		'D' : false,
		'R' : false,
		'U' : false
	}

	$scope.Obj = {};

	$http.post(BASEURL + RESTCALL.ResourcePermission, {
		"RoleId" : sessionStorage.ROLE_ID,
		"ResourceName" : "Roles & Permissions"
	}).success(function (response) {
		for (k in response) {
			for (j in Object.keys($scope.permission)) {
				if (Object.keys($scope.permission)[j] == response[k].ResourcePermission) {
					$scope.permission[Object.keys($scope.permission)[j]] = true;
				}
			}
		}
	})

	function autoScrollDiv(){
			$(".listView").scrollTop(0);
			}

		$scope.changeViewFlag = GlobalService.viewFlag;
		$scope.$watch('changeViewFlag', function(newValue, oldValue, scope) {
					GlobalService.viewFlag = newValue;
					var checkFlagVal = newValue;	
					if(checkFlagVal){
						$(".maintable > thead").hide();
						autoScrollDiv();
					}
					else{
						$(".maintable > thead").show();
						if($(".dataGroupsScroll").scrollTop() == 0){
							$table = $("table.stickyheader")
							$table.floatThead('destroy');
							
						}
						autoScrollDiv();
					}
					
					
				})

	
	

	if (GlobalService.roleAdded) {
		$scope.alerts = [{

				type : 'success',
				msg : $rootScope.roleAddedMesg.responseMessage
			}
		];
		$scope.alertStyle = alertSize().headHeight;
		$scope.alertWidth = alertSize().alertWidth;

		setTimeout(function () {
			$scope.callAtTimeout()

		}, 4000)

		GlobalService.roleAdded = false;

	}

	$stateParams.input ?
		 $stateParams.input.responseMessage ? 	
		$scope.alerts = [{
					type : 'success',
					msg : $stateParams.input.responseMessage
				}]	 : ''
				
				 : ''

				 setTimeout(function(){
					 $scope.callAtTimeout();
				 },4000)



	// $scope.showAlert = true;
	$scope.userRoles = [];
	

	var len = 20;
	$scope.fieldArr =  {
		"sortBy" : [],
		"params" : [],
		"start" : 0,
		"count" : 20
	}

	$scope.queryForm = function(arr)
	{
		$scope.fieldArr = arr;

		$scope.Qobj = {};
		$scope.Qobj.start = arr.start;
		$scope.Qobj.count = arr.count;
		$scope.Qobj.Queryfield = [];
		$scope.Qobj.QueryOrder = [];


			for(var i in arr)
			{
				if(i == 'params')
				{
					for(var j in arr[i])
					{
						$scope.Qobj.Queryfield.push(arr[i][j])
					}
				}
				else if(i == 'sortBy')
				{
					for(var j in arr[i])
					{
						$scope.Qobj.QueryOrder.push(arr[i][j])
					}
				}
			}

			$scope.Qobj = constructQuery($scope.Qobj);
			return $scope.Qobj;
	}


	$scope.sortMenu = [
		{
			 "label":"Role ID",
			 "FieldName":"RoleID",
			 "visible":true,
			 "Type":"String"
		 },
	   {
			 "label":"Role Name",
			 "FieldName":"RoleName",
			 "visible":true,
			 "Type":"String"
		 },{
			 "label":"Role Type",
			 "FieldName":"RoleType",
			 "visible":true,
			 "Type":"String"
		 },{
			 "label":"Status",
			 "FieldName":"Status",
			 "visible":true,
			 "Type":"Number"
		 },{
			 "label":"Effective From Date",
			 "FieldName":"EffectiveFromDate",
			 "visible":true,
			 "Type":"DateOnly"
		 },
		 {
			"label":"Effective Till Date",
			"FieldName":"EffectiveTillDate",
			"visible":true,
			"Type":"DateOnly"
		}]

		$scope.filterBydate = [{
			'actualvalue' : todayDate(),
			'displayvalue' : 'Today'
		}, {
			'actualvalue' : week(),
			'displayvalue' : 'This Week'
		}, {
			'actualvalue' : month(),
			'displayvalue' : 'This Month'
		}, {
			'actualvalue' : year(),
			'displayvalue' : 'This Year'
		}, {
			'actualvalue' : '',
			'displayvalue' : 'Custom'
		}
	]


	$scope.Status = [{
			"actualvalue" : "ACTIVE",
			"displayvalue" : "ACTIVE"
		}, 
		{
			"actualvalue" : "DELETED",
			"displayvalue" : "DELETED"
		},
		{
			"actualvalue" : "INACTIVE",
			"displayvalue" : "INACTIVE"
		},
		{
			"actualvalue" : "SUSPENDED",
			"displayvalue" : "SUSPENDED"
		}
	]

	


	$scope.initCall = function(_query)
	{
		$http.post(BASEURL+RESTCALL.GetRoles,_query).success(function(data,status,headers,config){

			$scope.totalForCountBar = headers().totalcount
			
			$scope.loadedData = angular.copy(data);

				/*for(var i in data){

					if(data[i].RoleID == 'Super Admin')
					{
						data.splice(i,1);
					}
					
				}*/
				$scope.userRoles = data;
				$scope.userRoles.splice(0,0,{})

				$stateParams.input ? $stateParams.input.UserProfileDraft ? $scope.gotoEditDraft('','',$stateParams.input.totData) : '' : '';



	}).error(function(data,status){
		$scope.loadedData = [];

			if (status == 401) {
				if (configData.Authorization == 'External') {
					window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
				} else {
					LogoutService.Logout();
				}
			} else {
				$scope.alerts = [{
						type : 'danger',
						msg : data.error.message //Set the message to the popup window
					}
				];
			}

		})
	}

	$scope.initCall($scope.queryForm($scope.fieldArr))

	$scope.loadData = function()
	{
		len = 20;
		$scope.fieldArr =  {
			"sortBy" : [],
			"params" : [],
			"start" : 0,
			"count" : 20
		}
		$(".listView").scrollTop(0);
		$scope.initCall($scope.queryForm($scope.fieldArr))
	}

	$scope.loadMore = function()
	{
		$scope.fieldArr.start = len;
		$scope.fieldArr.count = 20;

		$http.post(BASEURL+RESTCALL.GetRoles,$scope.queryForm($scope.fieldArr)).success(function(data){
			$scope.restData = data;
			$scope.userRoles = $scope.userRoles.concat(data);
			$scope.loadedData = data;
			len = len+20;
		}).error(function(data){
			$scope.loadedData = [];
		})
	}


	 $http.get(BASEURL + "/rest/v2/roles/resourcegroup").success(function (data) {
		//console.log(data)		
		// $scope.resourceGroup = removeAdminPanel(data);
		$scope.resourceGroup = data;
	});


	$scope.gotoSorting = function(dat){

		console.log(dat)
	$scope.fieldArr.start = 0;
	$scope.fieldArr.count = len;
	
	var orderFlag = true;
	if($scope.fieldArr.sortBy.length)
	{
		for(var i in $scope.fieldArr.sortBy)
		{
			if($scope.fieldArr.sortBy[i].ColumnName == dat.FieldName)
			{	
				if($scope.fieldArr.sortBy[i].ColumnOrder =='Asc')
				{
					$('#'+dat.FieldName+'_icon').attr('class','fa fa-long-arrow-down')
					$('#'+dat.FieldName+'_Icon').attr('class','fa fa-caret-down')
					$scope.fieldArr.sortBy[i].ColumnOrder = 'Desc';
					orderFlag = false;
					break;
				}
				else{
					$scope.fieldArr.sortBy.splice(i,1);
					orderFlag = false;
					$('#'+dat.FieldName+'_icon').attr('class','fa fa-minus fa-sm')
					$('#'+dat.FieldName+'_Icon').removeAttr('class')
					break;
				}

			}
		}

		if(orderFlag){
		$('#'+dat.FieldName+'_icon').attr('class','fa fa-long-arrow-up')
		$('#'+dat.FieldName+'_Icon').attr('class','fa fa-caret-up')
			$scope.fieldArr.sortBy.push({
						"ColumnName": dat.FieldName,
						"ColumnOrder": 'Asc'
					})

		}
}
else{
	
	$('#'+dat.FieldName+'_icon').attr('class','fa fa-long-arrow-up')
	$('#'+dat.FieldName+'_Icon').attr('class','fa fa-caret-up')

		$scope.fieldArr.sortBy.push({
						"ColumnName": dat.FieldName,
						"ColumnOrder": 'Asc'
					})
	
} 


$scope.initCall($scope.queryForm($scope.fieldArr))
}

$scope.filterParams = {};
$scope.selectedStatus = [];
$scope.setStatusvalue = function(val,to){	
	var addme = true;
	if($scope.selectedStatus.length){			
		for(k in $scope.selectedStatus){
			if($scope.selectedStatus[k] == val){
				$('#'+val).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
				$scope.selectedStatus.splice(k,1);
				
				addme = false
				break
			}
		}
		if(addme){
			$('#'+val).css({'background-color':'#d8d5d5','box-shadow':''})
			$scope.selectedStatus.push(val);
		}
	}
	else{
		$('#'+val).css({'background-color':'#d8d5d5','box-shadow':''})
		$scope.selectedStatus.push(val);
	}
	to['Status'] = $scope.selectedStatus;
}


$scope.setEffectivedate = function(val,to){	
	
	to['EffectiveDate'] = val;
	if($scope.selectedDate == val.displayvalue){
		$scope.showCustom = false;
		$('.filterBydate').css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
		$scope.selectedDate = '';
	}
	else{
		$scope.showCustom = true;
		$scope.selectedDate = angular.copy(val.displayvalue);	
		$('.filterBydate').css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
		$('#'+$scope.selectedDate.replace(/\s+/g, '')).css({'box-shadow':'1.18px 3px 2px 1px rgba(0,0,0,0.40)','background-color':'#d8d5d5'})
	}
	
	if(typeof(val.actualvalue) == "object"){
		var date = []
		for(k in val.actualvalue){
			date.push(val.actualvalue[k])
		}
		$('#customPicker').find('input').each(function(i){
			if(i == 0){
				if(date[i] < date[Number(i+1)]){
					$(this).val(date[i])
					$(this).parent().children().each(function(){
						$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
					})
				}				
				else{
					$(this).val(date[Number(i+1)])
					$(this).parent().children().each(function(){
						$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
					})
				}
			}
			else{
				$(this).val(date[Number(i-1)])
				$(this).parent().children().each(function(){
					$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
				})
			}
		}) 			
	}
	else if(val.displayvalue == 'Custom'){
		$('#customPicker').find('input').each(function(i){
			$(this).parent().children().each(function(){
				$(this).css({'cursor': 'pointer'}).removeAttr('disabled').val('')
			})
		})
	}
	else{
		$('#customPicker').find('input').each(function(i){
			$(this).val(val.actualvalue)
			$(this).parent().children().each(function(){
				$(this).css({'cursor': 'not-allowed'}).attr('disabled','disabled')
			})
		}) 	
	}
}

$scope.showCustom = false;
$scope.selectedDate = '';

$scope.clearSort = function (id) {
	$(id).find('i').each(function () {
		$(this).removeAttr('class').attr('class', 'fa fa-minus fa-sm');
		$('#' + $(this).attr('id').split('_')[0] + '_Icon').removeAttr('class');
	});
	$scope.fieldArr.sortBy =  [];
	$scope.initCall($scope.queryForm($scope.fieldArr))	
}

$scope.fields = [
	{
		'type'	: "string",
		'label'	: "Role ID",
		'name'	: "RoleID"
	},
	{
		'type'	: "string",
		'label'	: "Role Name",
		'name'	: "RoleName"
	},
	{
		'type'	: "string",
		'label'	: "Role Type",
		'name'	: "RoleType"
	},
	{
		'type'	: "select",
		'label'	: "Status",
		'value'	: 	[],
		'smartSearch' : true,
		'name'	: "Status"
	},
	{
		'type'	: "DateOnly",
		'label'	: "Effective From Date",
		'name'	: "EffectiveFromDate"
	},
	{
		'type'	: "DateOnly",
		'label'	: "Effective Till Date",
		'name'	: "EffectiveTillDate"
	}
]


$scope.buildFilter = function (argu1) {
	var argu2 = []
	for (k in $scope.fields) {
		if ($scope.fields[k].type === 'string') {
			argu2.push({
				"columnName" : $scope.fields[k].name,
				"operator" : "LIKE",
				"value" : argu1
			})
		}else if($scope.fields[k].type === 'select' && $scope.fields[k].name != 'Status'){
			argu2.push({
				"columnName" : $scope.fields[k].name,
				"operator" : "=",
				"value" : argu1
			})
		}
	}
	return argu2;
	
}
		$scope.callAtTimeout = function () {
				$('.alert').hide();
			}


		$scope.gotoDeleteFn = function(resource,roleid)
		{
			$scope.deleteDisRole = roleid;
		}

	$scope.takeDeldata = function(roleid) {
		
		$scope.delObj = {};
		$scope.delObj.RoleID = roleid;
		$http.post(BASEURL + RESTCALL.CreateRole +'/delete', $scope.delObj).success(function (data) {

		if(data)
		{
				$scope.alerts = [{
					type : 'success',
					msg : data.responseMessage
				}
			];
		}
		else
		{
			$scope.alerts = [{
					type : 'success',
					msg : "Deleted successfully"
				}
			];
		}
			setTimeout(function () {
				$scope.callAtTimeout()
			}, 3000)


			$('.collapse').collapse('hide')

			$scope.initCall($scope.queryForm($scope.fieldArr))
			for(var i in $scope.Status){
				$scope.getCountbyStatus($scope.Status[i])
			}
			
			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;
		}).error(function (data, status) {
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}
			];

		
			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;

			setTimeout(function () {
				$scope.callAtTimeout()
			}, 3000)

		})
		
		$('.modal').modal("hide");
		$('body').removeClass('modal-open')
	}

	$scope.searchFilter = function (val) {
	
/*	if($('#Filter').hasClass('open')){
		console.log("open")
		$('#Filter').removeClass('open')
	}*/
	
		val = removeEmptyValueKeys(val)

			$scope.fieldArr.start = 0
			$scope.fieldArr.count = len;
			$scope.fieldArr.params = [];

	$scope.adFilter = {
		"filters":{
			"logicalOperator": "AND",
			"groupLvl1": [
			  {
				"logicalOperator": "AND",
				"groupLvl2": [
				  {
					"logicalOperator": "AND",
					"groupLvl3": []
				  }
				]
			  }
			]
		},
		"sorts":[],
		"start": $scope.fieldArr.start,
		"count": $scope.fieldArr.count
	}
	
		
		for(var i in $scope.fieldArr)
		{
			if(i == 'sortBy')
			{
				for(var j in $scope.fieldArr[i])
				{
					$scope.adFilter.sorts.push({"columnName":$scope.fieldArr[i][j].ColumnName,"sortOrder":$scope.fieldArr[i][j].ColumnOrder})
					
				}
			}
		}
	
	for(var j in Object.keys(val)){
		if(val[Object.keys(val)[j]]){
			if(Object.keys(val)[j] == 'Status'){

				console.log(val, Object.keys(val)[j],val[Object.keys(val)[j]],val[Object.keys(val)[j]][i])

				$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3 = [{
					"logicalOperator" : (val[Object.keys(val)[j]].length >= 1)?'OR':'AND',
					"clauses" : []
				}]


				for(var i in val[Object.keys(val)[j]]){
						

					$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3[0].clauses.push({
						"columnName": Object.keys(val)[j],
						"operator": "=",
						"value": val[Object.keys(val)[j]][i]
					})	

				
			}


			}else if(Object.keys(val)[j] == 'EffectiveDate'){

				$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
					"logicalOperator" : "AND",
					"clauses" : [{
						"columnName": "EffectiveFromDate",
						"operator": $('#startDate').val() == $('#endDate').val() ? '=': $('#startDate').val() > $('#endDate').val() ? '<=' : '>=',
						"value": $('#startDate').val()
					}]
				})

				$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
						"logicalOperator" : "AND",
						"clauses" : [{
							"columnName": "EffectiveFromDate",
							"operator": $('#startDate').val() == $('#endDate').val() ? '=': $('#startDate').val() < $('#endDate').val() ? '<=' : '>=',
							"value": $('#endDate').val()
						}]
					})



			}else if(Object.keys(val)[j] == 'SearchSelect'){
				
				val.SearchSelect = JSON.parse(val.SearchSelect)
				
				/*$scope.fieldArr.params.push({
					"ColumnName": val.SearchSelect.name,
					"ColumnOperation":(val.SearchSelect.type == 'select') ? "=" : "LIKE",
					"ColumnValue":val.keywordSearch
				})*/
				$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
					"logicalOperator" : "OR",
					"clauses" : [{
						"columnName": val.SearchSelect.name,
						"operator": (val.SearchSelect.type == 'select') ? "=" : "LIKE",
						"value": val.keywordSearch
					}]
			})



		}else if(Object.keys(val)[j] == 'keywordSearch' && !val['SearchSelect']){
				/*$scope.tempVal = $scope.buildFilter(val[Object.keys(val)[j]]);
					for(var i in $scope.tempVal)
					{$scope.fieldArr.params.push($scope.tempVal[i])
					}*/
					$scope.adFilter.filters.groupLvl1[0].groupLvl2[0].groupLvl3.push({
						"logicalOperator" : "OR",
						"clauses" : $scope.buildFilter(val[Object.keys(val)[j]])
					})
				
			}				
		}
	}
	//$scope.initCall($scope.queryForm($scope.fieldArr))
	$scope.initCall($scope.adFilter)
	setTimeout(function(){
		$('select[name=SearchSelect]').val(null).trigger("change");
	},100)
	$scope.filterParams = {};
	$('.filterBydate').each(function(){
		$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
	})

	$scope.selectedStatus = [];	
	$('.filterBystatus').each(function(){
		$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
	})
	
	$scope.showCustom = false;
	$scope.selectedDate = '';
}

	$scope.clearFilter = function () {
		$scope.fieldArr = {
			"start" : 0,
			"count" : 20,
			"sortBy" : []
		}
			
		setTimeout(function(){
			$('select[name=SearchSelect]').val(null).trigger("change");
		},100)
		$scope.filterParams = {};
		$('.filterBydate').each(function(){
			$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
		})

		$scope.selectedStatus = [];	
		$('.filterBystatus').each(function(){
			$(this).css({'background-color':'#fff','box-shadow': '1.18px 2px 1px 1px rgba(0,0,0,0.40)'})
		})
		
		$scope.showCustom = false;
		$scope.selectedDate = '';
		$('.customDropdown').removeClass('open');

		$scope.initCall($scope.queryForm($scope.fieldArr))
	}

	$scope.multiSortObj = [];
	$scope.printFn = function () {
		$('[data-toggle="tooltip"]').tooltip('hide');
		window.print()
	}

	$scope.ExportMore = function(argu,excelLimit){
		if(argu > excelLimit){
			JSONToCSVConvertor(bankData, $scope.dat, (argu > excelLimit) ?  $scope.Title + '_'+(''+excelLimit)[0]: $scope.Title, true);
			$scope.dat = [];
			excelLimit += 1000000
		}
		
		$http.post(BASEURL+RESTCALL.GetRoles,{"start": argu,"count": ($scope.totalForCountBar > 1000) ? 1000 : $scope.totalForCountBar}).success(function(data){

			
			$scope.dat = $scope.dat.concat(data)
			if(data.length >= 1000){
				
				argu += 1000;
				$scope.ExportMore(argu,excelLimit)				
			}
			else{
				JSONToCSVConvertor(bankData, $scope.dat,(argu > excelLimit) ?  "Roles" + '_'+(''+excelLimit)[0]: "Roles", true);
			}
		})
	}

	 $scope.exportAsExcel = function(data){
		
		$scope.dat = [];
		if($("input[name=excelVal][value='All']").prop("checked")){	
			$scope.ExportMore(0,1000000);
		}
		else{
			$scope.dat = angular.copy($scope.userRoles);
			$scope.dat.shift();
			
			JSONToCSVConvertor(bankData,$scope.dat, "Roles", true);
		}
	}


	var debounceHandler = _.debounce($scope.loadMore, 700, true);
	jQuery(
		function ($) {
			$('.listView').bind('scroll', function () {
				if (Math.round($(this).scrollTop() + $(this).innerHeight()) >= $(this)[0].scrollHeight) {
					if ($scope.loadedData.length >= 20) {
						debounceHandler()
					}
				}
			})
			setTimeout(function () { }, 1000)
		}  
	);





















$scope.changevalue= false;
	$scope.checkBox = function (val, flag) {
		
console.log(flag)
		$scope.active = !$scope.active;
console.log($scope.active)
		if (!flag) {
			// $('#checkId').addClass('checked')
			$scope.changevalue= true;
			$scope.checkBoxChecked = true;
			
			$('#rolesTable td').css('padding', '6px 12px');
			console.log(flag)
		} else {
			//$('#checkId').removeClass('checked')
			$scope.changevalue= false;
			$scope.checkBoxChecked = false;
			$('#rolesTable td').css('padding', '12px 12px');
		}
	}

	$scope.checkOpt = function (val) {
		var visible = $(val.currentTarget).parent().parent().parent().parent().find('.visible');

		var visibleDropdown = $(val.currentTarget).parent().parent().find('button:first-child').find('span:first-child');

		//  console.log($(visibleDropdown).attr("class"))

		var selEle = $(val.currentTarget).find("span");
		var selClass = selEle.attr("class");

		console.log(selClass)
		console.log($(visible).attr("class"))

		$(visibleDropdown).removeAttr('class').addClass('opt checkedDropdown').addClass(selClass)
		$(visible).removeAttr("class").addClass('visible').addClass(selClass);
	}
  	$scope.getAccordion = function (v1, v2, index,roletype,status,effectivedate,effectivetilldate) {
		$scope.newObj = {};
		$scope.newObj.v1 = v1;
		$scope.newObj.v2 = v2;
		$scope.newObj.index = index;
		$scope.newObj.zero = 0;
		$scope.newObj.Roletype = roletype;
		$scope.newObj.Status = status;
		$scope.newObj.EffectiveDate = effectivedate;
		$scope.newObj.EffectiveTillDate = effectivetilldate;
		$state.go("app.viewresourcepermission",{input:$scope.newObj})
	}
	

	$scope.AddNewRole = function () {
	$scope.editObj = {};
	$scope.editObj.FromRoles = false;
		
		$state.go("app.addroles",{input:$scope.editObj})
	}

	$scope.gotoEdit = function (v1, v2, Opt) {

		$scope.editObj = {};
		$scope.editObj.Resourcename = v1;
		$scope.editObj.RoleId = v2;
		$scope.editObj.ToEditPage = true;
		$scope.editObj.Operation = Opt;
		$scope.editObj.FromRoles = true;


		$state.go("app.addroles",{input:$scope.editObj})
		
	}
	

	/*** On window resize ***/
	$(window).resize(function () {
		$scope.$apply(function () {

			$scope.alertWidth = $('.alertWidthonResize').width();
		});

	});

	$('.DatePicker').datetimepicker({
		format:"YYYY-MM-DD",
		showClear: true
	}).on('dp.change', function(ev){
		$scope['filterParams'][$(ev.currentTarget).attr('id')] = $(ev.currentTarget).val()
	}).on('dp.show', function(ev){
		$(this).change();	
	})

	$scope.TotalCount = 0;    
	$scope.getCountbyStatus = function(argu)
	{
		$http.get(BASEURL+"/rest/v2/roles/"+argu.actualvalue+"/count").success(function(data)
			{
				argu.TotalCount = data.TotalCount;
				$scope.TotalCount = $scope.TotalCount + data.TotalCount;
				return data.TotalCount
			})
	}

	/*** To Maintain Alert Box width, Size, Position according to the screen size and on scroll effect ***/
	$scope.widthOnScroll = function(){
		var mq = window.matchMedia( "(max-width: 991px)" );
		var headHeight
		if (mq.matches) {
			headHeight =0;
			$scope.alertWidth = $('.pageTitle').width();
		} else {
			$scope.alertWidth = $('.pageTitle').width();
			headHeight = $('.main-header').outerHeight(true)+10;
		}
		$scope.alertStyle=headHeight;
	}
	$scope.widthOnScroll();



		$scope.getCurrentDrafts = function()
		{
		
			$http.post(BASEURL + "/rest/v2/draft/Role/readall",
			{'start' : 0,'count' : 20 }).success(function(data){
					console.log(data,"data")
					$scope.draftdatas = data;
					$scope.dataLen = data;

			}).error(function(error){
				$scope.alerts = [{
				type : 'Error',
				msg : error.responseMessage	//Set the message to the popup window  /v2/draft/read/{tableName}
			}];
			})
			
		}

			$scope.takeDelDraftdata = function(val,Id){
		delData = val;
		$scope.delIndex = Id;
	}


		$scope.gotodeleteDraft = function()
		{
			
			$scope.deleteObj = {
				'UserID' : delData.UserID,
				'Entity' : delData.Entity,
				'BPK' : delData.BPK
			}
			   // console.log($scope.deleteObj,"delData")
				$http.post(BASEURL + "/rest/v2/draft/delete",$scope.deleteObj).success(function(response){
					
						//if(response.Status === 'Success'){
									$('.modal').modal("hide");
									$scope.alerts = [{
									type : 'success',
									msg : "Deleted successfully"	
								}];

								$timeout(function(){
									$('.alert-success').hide();
								},4000)
										//}
				}).error(function(error){
					
					/*$scope.alerts = [{
					type : 'Error',
					msg : error.responseMessage	
				}];*/
					
				})
			
			
		}

		// console.log($stateParams.input.UserProfileDraft,"UserProfileDraft")
		draftlen = 20;
		argu = {};
		var loadMoreDrafts = function(){
			// console.log($scope.dataLen,$scope.dataLen.length)
			if(($scope.dataLen.length >= 20)){
				argu.start = draftlen;
				argu.count = 20;

					$http.post(BASEURL + "/rest/v2/draft/Role/readall",argu).success(function(response){
						console.log(response,"response")
					$scope.dataLen = response;
					if(response.length != 0){
						$scope.draftdatas = $scope.draftdatas.concat($scope.dataLen)
						draftlen = draftlen + 20;		
					}	
					}).error(function(error){
							$scope.alerts = [{
							type : 'Error',
							msg : error.responseMessage	
						}];
					})
			}
		//	console.log($scope.dataLen)
		}	

	



		var debounceHandlerDraft = _.debounce(loadMoreDrafts, 700, true);
		setTimeout(function(){

		$(document).ready(function(){

				$(".FixHead").scroll(function (e) {
			var $tablesToFloatHeaders = $('table.maintable');
			//console.log($tablesToFloatHeaders)
			$tablesToFloatHeaders.floatThead({
				useAbsolutePositioning: true,
				scrollContainer: true
			})
			$tablesToFloatHeaders.each(function () {
				var $table = $(this);
				//console.log($table.find("thead").length)
				$table.closest('.FixHead').scroll(function (e) {
					$table.floatThead('reflow');
				});
			});
		})
		$(".FixHeadDraft").scroll(function (e) {
			var $tablesToFloatHeaders = $('table.drafttable');
			// console.log($tablesToFloatHeaders)
			$tablesToFloatHeaders.floatThead({
				useAbsolutePositioning: true,
				scrollContainer: true
			})
			$tablesToFloatHeaders.each(function () {
				var $table = $(this);
				//console.log($table.find("thead").length)
				$table.closest('.FixHeadDraft').scroll(function (e) {
					$table.floatThead('reflow');
				});
			});
		})

			$('.draftViewCls').on('scroll', function() { 
				                                         
				$scope.widthOnScroll();
				if( Math.round($(this).scrollTop() + $(this).innerHeight())>=$(this)[0].scrollHeight) {
					debounceHandlerDraft();
				}
			});
			$('#DraftListModal').on('shown.bs.modal', function (e) {
				$('body').css('padding-right',0);
				$(".draftViewCls").scrollTop(0);
			})

		})

	},200)


		$scope.gotoEditDraft = function(opr,index,draftblob)
		{
			
			var gostateObj = {
				'typeOfDraft' : '',
				'decrData' : "",
				'draftdata' : draftblob,
				'FromDraft' : true
			}
			var decryptedDraft = $filter('hex2a')(draftblob.Data ? draftblob.Data : draftblob.totData.Data)
			var jsonDraft = $filter('Xml2Json')(decryptedDraft)
			var backupWholeData = angular.copy(jsonDraft)
			//console.log(jsonDraft,"jsonDraft")
			for(i in backupWholeData)
			{
				for(j in backupWholeData[i])
				{
					// console.log(backupWholeData[i],'backupWholeData[i]')
					// backupWholeData[i][j] = (backupWholeData[i][j] == 'true') ? true : (backupWholeData[i][j] == 'false') ? false : backupWholeData[i][j] ;
					if(typeof backupWholeData[i][j] == 'object')
					{
						var backupObj = backupWholeData[i][j];
						delete backupWholeData[i][j];
						backupWholeData[i][j] = [];
						backupWholeData[i][j].push(backupObj);
						//console.log(backupObj,jsonDraft[i][j])
					}
					
				}
				backupWholeData[i] = cleantheinputdata(backupWholeData[i])
				 gostateObj.decrData = backupWholeData[i];
			}

			var specificReadObject = {
					"UserID": gostateObj.draftdata.UserID,
					"Entity": gostateObj.draftdata.Entity,
					"BPK": gostateObj.draftdata.BPK
				}

				$http.post(BASEURL + RESTCALL.DraftSpecificRead,specificReadObject).then(function(response){
					
					// console.log(response,response.headers().type)
					gostateObj.typeOfDraft = response.headers().type
						
				},function(error){
					
						$scope.alerts = [{
							type : 'Error',
							msg : error.responseMessage	
						}];
						
				})


			$state.go("app.addroles",{input: gostateObj})

		}


	

});