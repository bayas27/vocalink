VolpayApp.controller('viewresourcepermissionCtrl', function ($scope, $rootScope, $stateParams, $http, $state, $location, $filter, userMgmtService, $timeout, GlobalService, LogoutService) {

	$scope.sKey = '';


	$scope.permission = {
		'C' : false,
		'D' : false,
		'R' : false,
		'U' : false
	}
	$scope.detailExpanded = false;
	$scope.Obj = {};
	//$scope.rG="AddOns";
	$http.post(BASEURL + RESTCALL.ResourcePermission, {
		"RoleId" : sessionStorage.ROLE_ID,
		"ResourceName" : "Roles & Permissions"
	}).success(function (response) {
		for (k in response) {
			for (j in Object.keys($scope.permission)) {
				if (Object.keys($scope.permission)[j] == response[k].ResourcePermission) {
					$scope.permission[Object.keys($scope.permission)[j]] = true;
				}
			}
		}
	})

	function CRUDReConstruct(Array123) {
		for (i = 0; i < Array123.length; i++) {
			//console.log(Array123[i])
			if (Array123[i] == 'C') {
				Array123[i] = 'Create';
			}
			if (Array123[i] == 'R') {
				Array123[i] = 'Read';
			}
			if (Array123[i] == 'U') {
				Array123[i] = 'Update';
			}
			if (Array123[i] == 'D') {
				Array123[i] = 'Delete';
			}
		}
		return Array123;
	}

	function CRUDReConstruct123(Array123) {
		//console.log(Array123.Operations)
		//console.log(Array123.Operations.length)
		for (i = 0; i < Array123.Operations.length; i++) {
			//console.log(Array123.Operations[i].Operation)
			if (Array123.Operations[i].Operation == 'C') {
				Array123.Operations[i].Operation = 'Create';
			}
			if (Array123.Operations[i].Operation == 'R') {
				Array123.Operations[i].Operation = 'Read';
			}
			if (Array123.Operations[i].Operation == 'U') {
				Array123.Operations[i].Operation = 'Update';
			}
			if (Array123.Operations[i].Operation == 'D') {
				Array123.Operations[i].Operation = 'Delete';
			}
		}
		//console.log(Array123)
		return Array123;
	}

	$scope.resourceAttributes = '';
	$scope.trial = {};

	$scope.getResourceAttributes = function (index, val, roleID, event) {
		console.log(index)
		$scope.action123 = false;

		for (var i in $scope.Obj) {
			$scope.Obj[i] = false;
		}
 
		var currentEvent = $(event.currentTarget).parent().parent().next(); //expandrow
		var $expandrow_id = $(event.currentTarget).parent().parent().next().attr('id'); //expandrow
		var $display = $(event.currentTarget).parent().parent().next().attr('style');

		var none = $('.expandRow').css('display', 'none');
			$('.attrIcon').removeClass('fa fa-compress').addClass('fa fa-expand');
		
		if ($display.indexOf('none') != -1) {
		console.log("dgsrfsssssssss" ,$display.indexOf('none'),$display.indexOf('table-row'))
			currentEvent.css('display', 'table-row');
			$(event.currentTarget).addClass('fa fa-compress');

		} 
		else if($display.indexOf('table-row') != -1)
		{
			console.log("dgsrf")
			currentEvent.css('display', 'none');
			$(event.currentTarget).removeClass('fa fa-compress').addClass('fa fa-expand');
		} 
	 
		$http.post(BASEURL + '/rest/v2/roles/attributes/read', {
			"RoleId" : roleID,
			"ResourceName" : val
		}).success(function (response) {
			$scope.trial = response;
			$scope.test111 = [];
			for (var i in response.Attributes) {
				//console.log('#select_' + val + '_' + i)
				//$('#select_'+val+'_'+i).select2();

				//$scope.test111.push({'key':val,'cnt':i, 'value':response.Attributes[i].Permissions[j].Operation})
				$scope.test111.push({
					'key' : val,
					'cnt' : i,
					'value' : []
				})
				for (var j in response.Attributes[i].Permissions) {

					if (response.Attributes[i].Permissions[j].Permission) {

						$scope.test111[i].value.push(response.Attributes[i].Permissions[j].Operation);

					}
				}

			}

			setTimeout(function () {

				for (var i in $scope.test111) {
					//console.log("value", $scope.test111[i].value)
					//console.log("value", CRUDReConstruct($scope.test111[i].value))

					
					//console.log($filter('removeSpace')('select[name="select_' + val + '_' + i + '"]'));
					var selectName = $filter('removeSpace')('select[name="select_' + val + '_' + i + '"]');
					$(selectName).val($scope.test111[i].value)
					$(selectName).select2();

				}

			}, 10)

		})

		$http.post(BASEURL + '/rest/v2/roles/attributes/attributenames', {
			"ResourceName" : val
		}).success(function (response) {
			
			$scope.attributes = response;
		})

		$http.post(BASEURL + '/rest/v2/roles/operationlist', {
			"ResourceName" : val
		}).success(function (response) {
			
			setTimeout(function () {
				$('.js-select2-multiple').select2();
			}, 1000)
			$scope.operationlist = response;
		})

	}

	if (GlobalService.roleAdded) {
		$scope.alerts = [{

				type : 'success',
				msg : $rootScope.roleAddedMesg.responseMessage
			}
		];
		$scope.alertStyle = alertSize().headHeight;
		$scope.alertWidth = alertSize().alertWidth;

		setTimeout(function () {
			$scope.callAtTimeout()

		}, 4000)

		GlobalService.roleAdded = false;

	}

	$scope.checkBoxChecked = false;
	$scope.active = false;

	$scope.tabClickCnt = 0;

	function removeCheckTrue(Arr12345) {
		
		for (i = 0; i < Arr12345.length; i++) {

			for (j = 0; j < Arr12345[i].PermissionList.length; j++) {
				delete Arr12345[i].PermissionList[j].check;
			}
		}
		return Arr12345;
	}

	$scope.currentActiveTab = '';

	$scope.saveAll = function (toUpdateData, RoleID, index) {

		//console.log(toUpdateData,RoleID,index)
		$scope.changevalue = false;
		$scope.updateData = {};
		$scope.updateData.RoleID = RoleID;

		//console.log($('.tabbable-custom').find('ul:first-child').find('.active').text().trim())

		$scope.updateData.ResourceGroupName = $('.tabbable-custom').find('ul:first-child').find('.active').text().trim()
			//$scope.updateData.ResourceGroupName = $('#collapse' + index).find('.listGroup').find('.active').text().trim();
			$scope.currentActiveTab = $('.tabbable-custom').find('ul:first-child').find('.active').text().trim()
			$scope.updateData.ResourceGroupPermissions = removeCheckTrue(toUpdateData);

		//console.log($('#collapse' + index).find('.listGroup').find('.active'))
		$scope.active = false;
		$scope.checkBoxChecked = false;

		$http({
			method : "PUT",
			url : BASEURL + '/rest/v2/roles/groupresourcepermission',
			data : $scope.updateData
		}).success(function (data, status, headers, config) {

			$scope.alerts = [{
					type : 'success',
					msg : data.responseMessage
				}
			];

			$('.collapse').collapse('hide');

			setTimeout(function () {
				$('.alert-success').hide();
			}, 3000)

			//$state.reload();
		}).error(function (err) {
			$scope.alerts = [{
					type : 'danger',
					msg : err.error.message
				}
			];

		});
	}

	$scope.userRoles = [];
	$http.get(BASEURL + RESTCALL.CreateRole).success(function (data) {

		//$scope.userRoles = data;
		for (var i = 0; i < data.length; i++) {
			if (data[i].RoleID != 'Super Admin') {
				$scope.userRoles.push(data[i]);
			}
		}
		//console.log($scope.userRoles)
		if ($scope.userRoles.length > 0) {
			$scope.showAlert = false;
		} else {
			$scope.showAlert = true;
		}

	}).error(function (data, status) {
		//console.log(data.error.message,status)

		if (status == 401) {
			if (configData.Authorization == 'External') {
				window.location.href = '/VolPayHubUI' + configData['401ErrorUrl'];
			} else {
				LogoutService.Logout();
			}
		} else {
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message //Set the message to the popup window
				}
			];
		}

	});

	function removeAdminPanel(Arr12344) {
		var rGroupFinal = [];
		for (i = 0; i < Arr12344.length; i++) {
			if (Arr12344[i].ResourceGroupName != "Admin Panel") {
				rGroupFinal.push({
					"ResourceGroupName" : Arr12344[i].ResourceGroupName
				});
			}
		}
		//console.log(rGroupFinal)
		return rGroupFinal;
	}

	$http.get(BASEURL + "/rest/v2/roles/resourcegroup").success(function (data) {
		//console.log(data)
		// $scope.resourceGroup = removeAdminPanel(data);
		$scope.resourceGroup = data;
		$scope.rG=data[0].ResourceGroupName;

	});

	$scope.roleFlag = false;

	$scope.showRoles1 = false;
	$scope.showRoles2 = true;

	$scope.currentIndex = ''

		$scope.gotoEdit = function (v1, v2,Opt) {

		$scope.editObj = {};
		$scope.editObj.Resourcename = v1;
		$scope.editObj.RoleId = v2;
		$scope.editObj.ToEditPage = true;
		$scope.editObj.Operation = Opt;

		$state.go("app.addroles", {
			input : $scope.editObj
		})

	}

	$scope.getGroupName = function ($event) {

		$scope.grpName = $($event.currentTarget).parent().parent().parent().parent().parent().parent().find('ul').find('.active').text().trim();
	}

	$scope.setDefault = function ($event) {

		//console.log($scope.grpName)


		$('#resetRoleBox').modal('hide')

		$http.post(BASEURL + RESTCALL.DefaultPermision, {
			'RoleId' : $scope.RoleID
		}).success(function (data) {
			//console.log(data)
			$scope.alerts = [{
					type : 'success',
					msg : data.responseMessage
				}
			];

			setTimeout(function () {
				$('.alert-success').hide()
			}, 3000)

			$http.post(BASEURL + RESTCALL.ResourcePermission + "/readall", {
				'RoleID' : $scope.RoleID,
				'ResourceGroupName' : $scope.grpName
			}).success(function (data) {
				getActionHeaders(data)
				$scope.resourcePermission = data;

			});

			//$scope.updateData.ResourceGroupName


		}).error(function (err) {

			$scope.alerts = [{
					type : 'danger',
					msg : err.error.message
				}
			];
		})
	}

	$scope.callAtTimeout = function () {
		$('.alert').hide();
	}

	$scope.prevIndex = '';

	$scope.getAccordion = function (v1, v2, index) {

		$location.path("app/viewresourcepermission")

		

		// $scope.currentIndex = index;
		// if ($('#collapse' + $scope.prevIndex).hasClass('in')) {

		// 	if ($scope.active) {
		// 		$('#roleEdit1').modal('show')
		// 		$scope.newObj = {};
		// 		$scope.newObj.v1 = v1;
		// 		$scope.newObj.v2 = v2;
		// 		$scope.newObj.index = index;
		// 		$scope.newObj.zero = 0;

		// 	} else {

		// 		$scope.active = false;
		// 		$('.checkClass').removeClass('checked')
		// 		$scope.checkBoxChecked = false;
		// 		$('.collapse').collapse('hide')
		// 		$('#collapse' + index).collapse('show')
		// 		$scope.prevIndex = index;
		// 		$scope.getGroupPermission(v1, v2, index, 0);
		// 	}
		// } else {
		// 	$scope.active = false;
		// 	$('.checkClass').removeClass('checked')
		// 	$scope.checkBoxChecked = false;
		// 	$('#collapse' + index).collapse('show')
		// 	$scope.prevIndex = index;
		// 	$scope.getGroupPermission(v1, v2, index, 0);
		// }

		// $scope.showRoles1 = false
		// 	$scope.showRoles2 = true;

		// setTimeout(function () {
		// 	$('#tab_' + index).css('display', 'block');
		// }, 100)

		// console.log("flag",$scope.checkBoxChecked)
		// if(!$scope.checkBoxChecked)
		// {
		// 	$('.listGroupList').removeClass('active')
		// 	$("#listGroupListID_" + v2 + "_0").addClass('active')
		// }


	}

	$scope.updateExistingRoles = function (role) {

		role.IsSuperAdmin = true;
		$http.put(BASEURL + RESTCALL.CreateRole, role).success(function (data) {

			$scope.alerts = [{
					type : 'success',
					msg : data.responseMessage
				}
			];

			$('.collapse').collapse('hide')

			setTimeout(function () {
				$scope.callAtTimeout()
			}, 4000)

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;
		}).error(function (data, status) {
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}
			];

			setTimeout(function () {
				$scope.callAtTimeout()
			}, 4000)

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;
		})
	}

	$scope.looseChanges1 = function () {
		$scope.changevalue = false;

		//console.log($scope.newObj)

		$('#roleEdit1').modal('hide')
		$scope.active = false
			$('.checkClass').removeClass('checked')
			$scope.checkBoxChecked = false;

		$('.collapse').collapse('hide')
		$('#collapse' + $scope.currentIndex).collapse('show')
		$scope.prevIndex = $scope.currentIndex;
		$('#rolesTable td').css('padding', '12px');

		//console.log($scope.newObj)
		//	$scope.getGroupPermission($scope.newObj.v1, $scope.newObj.v2, $scope.newObj.index, $scope.newObj.zero);

		$scope.getAccordion($scope.newObj.v1, $scope.newObj.v2, $scope.newObj.index)

	}

	$scope.looseChanges = function () {
		$scope.changevalue = false;

		$('#roleEdit').modal('hide')

		if ($('#collapse' + $scope.currentIndex).hasClass('in')) {

			$scope.showRoles1 = true
				$scope.showRoles2 = false;
			$scope.active = false
				$scope.checkBoxChecked = false;
		}
		/* else{
		$('#collapse'+index).collapse('show')
		$scope.showRoles1 = true
		$scope.showRoles2 = false;
		}*/
	}

	$scope.keepOriginal = function () {
		$scope.changevalue = false;
		$scope.checkBoxChecked = false;
		$scope.active = false;
		$('#rolesTable td').css('padding', '12px');
		$('.alert-danger').hide()
	
		
		$state.go('app.roles')
	}

	$scope.GoBackFromRole = function (index) {
		$scope.active = false;
		$('#collapse' + index).collapse('hide')
	}

	$scope.enableStatus = function (cTab) {
		//$scope.active = true;
		$scope.changevalue = false;
		$scope.CurrentTab = cTab;
		$scope.checkBoxChecked = false;
		$scope.rG = cTab;
		$scope.active = false;

		setTimeout(function () {

			$('#checkId').removeClass('checked')
		}, 100)

	}

	$scope.callAtTimeout = function () {
		$('.alert').hide();
	}
	
	$scope.fromMod = '';
	$scope.gotoDeleteFn = function (resource, roleid) {
		$scope.deleteDisRole = roleid;
		$scope.fromMod= true; 
	}

	$scope.takeDeldata = function (roleid) {
//	console.log("roleid", roleid)

		$scope.delObj = {};
		$scope.delObj.RoleID = $scope.deleteDisRole;
		$http.post(BASEURL + RESTCALL.CreateRole + '/delete', $scope.delObj).success(function (data) {

			
			GlobalService.roleAdded = true;
			if (data) {

				$rootScope.roleAddedMesg = data;
			} else {
				$rootScope.roleAddedMesg = {
					"responseMessage" : "Deleted Successfully"
				};
			}
			

			$('.alert-danger').hide();
			$location.path('app/roles')

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;
		}).error(function (data, status) {
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}
			];

			$scope.alertStyle = alertSize().headHeight;
			$scope.alertWidth = alertSize().alertWidth;

			setTimeout(function () {
				$scope.callAtTimeout()
			}, 3000)

		})

		$('.modal').modal("hide");
		$('body').removeClass('modal-open')
	}

	function getObjects(obj, key, val) {
		var objects = [];
		for (var i in obj) {
			if (!obj.hasOwnProperty(i))
				continue;
			if (typeof obj[i] == 'object') {
				objects = objects.concat(getObjects(obj[i], key, val));
			} else
				//if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
				if (i == key && obj[i] == val || i == key && val == '') { //
					objects.push(obj);
				} else if (obj[i] == val && key == '') {
					//only add if the object is not already in the array
					if (objects.lastIndexOf(obj) == -1) {
						objects.push(obj);
					}
				}
		}
		return objects;
	}

	function getActionHeaders(rpArray) {
		//console.log(rpArray)
		$scope.aheader = [];
		for (i = 0; i < rpArray.length; i++) {
			//console.log(rpArray[i].PermissionList)
			for (j = 0; j < rpArray[i].PermissionList.length; j++) {
				//console.log(rpArray[i].PermissionList[j].Operation)
				$scope.aheader.push(rpArray[i].PermissionList[j].Operation)
			}
		}
		//console.log($scope.aheader)
		return _.uniq($scope.aheader);
	}
	$scope.attrHide=false;
	$scope.getGroupPermission = function (gName, roleID, parentId, curId) {
		

		$scope.dataContain = $stateParams.input;
		
console.log("$scope.dataContain", $scope.dataContain)
		$scope.grpName = gName;
		$scope.RoleID = $stateParams.input.v2;
		$scope.RoleType = $stateParams.input.Roletype;
		$scope.StatusFromRole = $stateParams.input.Status;
		$scope.EffectivedateFromRole = $stateParams.input.EffectiveDate;
	$scope.EffectivedateTillRole = $stateParams.input.EffectiveTillDate;
	
		$scope.toPostData = {}
		$scope.toPostData.RoleID = $stateParams.input.v2
			$scope.toPostData.ResourceGroupName = gName

			$http.post(BASEURL + RESTCALL.ResourcePermission + "/readall", $scope.toPostData).success(function (data) {
				$scope.actionHeader123 = getActionHeaders(data)
					$scope.resourcePermission = data;

			});
	}

	/*   $http.get('roles.json').success(function (data) {

	$scope.rolesData = data;
	});
	 */
	if(!$stateParams.input)
	{
		$location.path('app/roles');
	}

	$scope.getGroupPermission($stateParams.input.v1, $stateParams.input.v2, $stateParams.input.index, $stateParams.input.zero)

	$scope.users = [{
			name : 'All Clients',
			value : 'All Clients'
		}, {
			name : 'System',
			value : 'System'
		}, {
			name : 'Client Id',
			value : 'Client Id'
		}
	]
	$scope.changevalue = false;
	$scope.checkBox = function (val, flag) {

		
		$scope.active = !$scope.active;
		
		if (!flag) {
			// $('#checkId').addClass('checked')
			$scope.changevalue = true;
			$scope.checkBoxChecked = true;

			$('#rolesTable td').css('padding', '6px 12px');
		
		} else {
			//$('#checkId').removeClass('checked')
			$scope.changevalue = false;
			$scope.checkBoxChecked = false;
			$('#rolesTable td').css('padding', '12px 12px');
		}
	}

	$scope.checkOpt = function (val) {
		var visible = $(val.currentTarget).parent().parent().parent().parent().find('.visible');

		var visibleDropdown = $(val.currentTarget).parent().parent().find('button:first-child').find('span:first-child');

		//  console.log($(visibleDropdown).attr("class"))

		var selEle = $(val.currentTarget).find("span");
		var selClass = selEle.attr("class");

		

		$(visibleDropdown).removeAttr('class').addClass('opt checkedDropdown').addClass(selClass)
		$(visible).removeAttr("class").addClass('visible').addClass(selClass);
	}

	/* $scope.getRoleDetail = function(index){
	if($('#collapse'+index).hasClass('in')){

	if($scope.showRoles){
	$('#collapse'+index).collapse('hide')
	$scope.showRoles = false;
	}
	else{
	$('#collapse'+index).collapse('show')
	$scope.showRoles = true;
	}

	}
	else{


	if($scope.showRoles){
	$('#collapse'+index).collapse('hide')
	$scope.showRoles = false;
	}
	else{
	$('#collapse'+index).collapse('show')
	$scope.showRoles = true;
	}
	}

	}*/

	$scope.AddNewRole = function () {
		$location.path('app/addroles');
	}

	$scope.AddNewPermissions = function () {
		$location.path('app/addpermissions');
	}

	/*** On window resize ***/
	$(window).resize(function () {
		$scope.$apply(function () {

			$scope.alertWidth = $('.alertWidthonResize').width();
		});

	});

	$scope.action123 = false;
	$scope.buttonStatus = 0;
	$scope.editAllAttribute = function (val, flag) {

		console.log(val,flag)

		if ((val == '') || (val == false)) {
			$scope.action123 = true;
		 } 
		//else {
		// 	$scope.action123 = false;
		// }
		
		
	}

	$scope.operators = ["=", "!=", "<", ">", "<=", ">=", "IN"]

	$scope.addAttribute = function (as, asd, ResourceName, uRoles, key) {
		console.log("Adadad")
		setTimeout(function () {
			$('.js-select2-multiple').select2();
	}, 0)
		
		if (key == undefined) {
			key = 0;
		}
		$scope.Obj[key] = true;
		if ($scope.trial.Attributes == undefined) {
			$scope.trial.Attributes = [];
		}

		
		$scope.attributeObj = {};
		$scope.attributeObj.AttributeName = "";
		$scope.attributeObj.Operator = "";
		$scope.attributeObj.AttributeValue = "";
		$scope.attributeObj.Permissions = [];
		$scope.attributeObj.Permissions123 = [];
		/* $scope.attributeObj.Permissions = [{
		"Operation" : "C",
		"Permission" : false
		}, {
		"Operation" : "R",
		"Permission" : false
		}, {
		"Operation" : "U",
		"Permission" : false
		}, {
		"Operation" : "D",
		"Permission" : false
		}
		]; */
		//console.log($scope.resourceAttributes)
		$scope.trial.Attributes.push($scope.attributeObj);
		

	}

	function AttributeObjRestructure(Arr) {

		//console.log(Arr)

		for (i = 0; i < Arr.length; i++) {
			var InnerArr = Arr[i].Permissions;

			//console.log(InnerArr)
			for (j = 0; j < InnerArr.length; j++) {
				//console.log(InnerArr[j]);

				if ((j == 0) && (InnerArr[j].Permission == true) && (InnerArr[j].Permission != "")) {
					InnerArr[j].Permission = "C";
				} else {
					delete InnerArr[j];
				}

				if ((j == 1) && (InnerArr[j].Permission == true) && (InnerArr[j].Permission != "")) {
					InnerArr[j].Permission = "R";
				} else {
					delete InnerArr[j];
				}

				if ((j == 2) && (InnerArr[j].Permission == true) && (InnerArr[j].Permission != "")) {
					InnerArr[j].Permission = "U";
				} else {
					delete InnerArr[j];
				}

				if ((j == 3) && (InnerArr[j].Permission == true) && (InnerArr[j].Permission != "")) {
					InnerArr[j].Permission = "D";
				} else {
					delete InnerArr[j];
				}

				/*
				if(InnerArr[j].Permission==false){
				delete InnerArr[j];
				} */
			}
			delete Arr[i].Permissions123;
		}
		
		return Arr;
	}

	function AttributeObjRestructure2(Arr) {
		for (i = 0; i < Arr.length; i++) {
			var InnerArr = Arr[i].Permissions;
			
			for (j = 0; j < InnerArr.length; j++) {
				if ((j == 0) && (InnerArr[j].Permission == "C")) {
					InnerArr[j].Permission = true;
				} else {
					delete InnerArr[j].Permission;
				}
				if ((j == 1) && (InnerArr[j].Permission == "R")) {
					InnerArr[j].Permission = true;
				} else {
					delete InnerArr[j].Permission;
				}
				if ((j == 2) && (InnerArr[j].Permission == "U")) {
					InnerArr[j].Permission = true;
				} else {
					delete InnerArr[j].Permission;
				}
				if ((j == 3) && (InnerArr[j].Permission == "D")) {
					InnerArr[j].Permission = true;
				} else {
					delete InnerArr[j];
				}
			}
		}
		//console.log(Arr)
		return Arr;
	}

	function AttributeObjRestructure3(Arr) {
		
		var myVar = [];

		for (i = 0; i < Arr.length; i++) {
			var InnerArr = Arr[i].Permissions123;
			
			//console.log($scope.RoleType,Arr,InnerArr,$.isArray(InnerArr[0]),JSON.stringify(InnerArr[0]).indexOf('{') != -1)
			
			if(!$.isArray(InnerArr[0]) && JSON.stringify(InnerArr[0]).indexOf('{') != -1)
			{
				for(let i in InnerArr)
				{
					//console.log("aa",i,InnerArr[i])
					if(InnerArr[i].Permission)
					{
						myVar.push(InnerArr[i].Operation)
					}
				}
			//	console.log("myVar",myVar)
			InnerArr = myVar;
			}
			

			
			delete Arr[i].Permissions;
			Arr[i].Permissions = [];
			//console.log("Length "+InnerArr+ angular.isUndefined(InnerArr));
		/*	if(angular.isUndefined(InnerArr)){
			  $scope.alerts = [{
					type : 'danger',
					msg : 'Permissions not set'
				}
			  ];
			}*/
			
			for (j = 0; j < InnerArr.length; j++) {
			
				var OP123 = {}
				
				if($scope.RoleType != 'Approver')
				{
					/*if (InnerArr[j] == 'Create') {
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					}
					
					if (InnerArr[j] == 'Read') {
						OP123.Operation = InnerArr[j];
						OP123.Permission = true;
					}
					if (InnerArr[j] == 'Update') {
						OP123.Operation = InnerArr[j];
						OP123.Permission = true;
					}
					if (InnerArr[j] == 'Delete') {
						OP123.Operation = InnerArr[j];
						OP123.Permission = true;
					}*/
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					
				}
				else 
				{
					if (InnerArr[j] == 'approve1') {
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					}
					if (InnerArr[j] == 'approve2') {
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					}
					if (InnerArr[j] == 'approve3') {
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					}
					if (InnerArr[j] == 'approve4') {
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					}
					if (InnerArr[j] == 'approve5') {
					OP123.Operation = InnerArr[j];
					OP123.Permission = true;
					}
					
				}
				

				Arr[i].Permissions.push(OP123);

			}
			delete Arr[i].Permissions123;
		}
		
		console.log("Arr",Arr)
		
		return Arr;
	}

	$scope.submitAllAttribute = function (attributeArray, ResourceName, RoleID) {

		 console.log(attributeArray,ResourceName, RoleID)
		 	$('.expandRow ').css("display", "none");

		$scope.Obj = {};
		var FinalObj = {};
		FinalObj.ResourceName = ResourceName
		FinalObj.RoleID = RoleID

		FinalObj.Attributes = AttributeObjRestructure3(attributeArray.Attributes);
		FinalObj.Attributes = attributeArray.Attributes;

		//	console.log("FinalObj "+JSON.stringify(FinalObj));

		//var Obj123 = attributeArray;
		//Obj123.Attributes=AttributeObjRestructure(g.Attributes)


		$http.post(BASEURL + '/rest/v2/roles/attributes', FinalObj).success(function (data,status) {

				// console.log(data,status,"attr")
			//console.log(ResourceName)
			//console.log(RoleID)


			//	console.log("flag",$scope.detailExpanded )

			$scope.alerts = [{
					type : 'success',
					msg : data.responseMessage
				}
			];

			//$('.collapse').collapse('hide');

			setTimeout(function () {
				$('.alert-success').hide();
			}, 4000)

			$scope.action123 = false;
		}).error(function (err) {   
			$scope.alerts = [{
					type : 'danger',
					msg : err.error.message
				}
			];

		});
	}

	$scope.toggleAll = function (permissionArr, value111) {
		
		angular.forEach(permissionArr, function (itm) {
			itm.Permission = value111;
		});
	}

	$scope.attributeCollapse = function () {
		
		$scope.Obj = {}
		$scope.action123 = true;

	}

	$scope.deleteRow = function (rowNum) {
		
		var Attributes123 = $scope.trial.Attributes;
		Attributes123.splice(rowNum, 1);
		
	}

	//$scope.jjj_0=false;
	$scope.editRow = function (rowNum) {
		$scope.Obj[rowNum] = true;
		//$scope.jjj_0=true;
		//editRow
	}

	$scope.getActionValue123 = function (v1, Arr123) {
		//console.log(v1)
		//console.log(Arr123)
		for (i = 0; i < Arr123.length; i++) {
			//console.log(Arr123[i])
			if (Arr123[i].Operation == v1) {
				Arr123[i].check = true;
				return Arr123[i];
			}
		}
	}

	$scope.changeValue = function (val, Arr) {

		var array = Arr;
		
		for (i in array) {
			//console.log(array[i].PermissionList)
			var array1 = array[i].PermissionList;
			//	console.log(array1)
			for (j in array1) {
				array1[j].Permission = val;
			}
		}
		
		return Arr;
		//	$scope.checkBoxChecked = true;
	}

	$http.post(BASEURL + RESTCALL.RoleAuditLog+'?count='+20+'&start='+0, {
		'RoleID' : $scope.RoleID
	}).success(function (data) {

		$scope.editedLog = data;
		$scope.dataLen = data;

		for (var j in $scope.editedLog) {
			for (var keyj in $scope.editedLog[j]) {
				if (keyj == 'oldData' || keyj == 'newData') {
					$scope.editedLog[j][keyj] = $filter('hex2a')($scope.editedLog[j][keyj])
						//console.log($scope.editedLog[j][keyj], $.parseXML($scope.editedLog[j][keyj]))
						if ($scope.editedLog[j][keyj].match(/</g) && $scope.editedLog[j][keyj].match(/>/g)) {
							var xmlDoc = $.parseXML($scope.editedLog[j][keyj]); //is valid XML
							var xmlData = xmlDoc.getElementsByTagName($scope.editedLog[j].tableName);
							var constuctfromXml = {};
							var constuctfromXmlObj = {};
							var constuctfromXmlarr = [];
							$(xmlDoc).children().each(function (e) {
								$(this).children().each(function (e) {
									var parentName = $(this).prop("tagName")
										if ($(this).children().length) {
											constuctfromXml[parentName] = constuctfromXmlarr
												$(this).children().each(function (e) {
													constuctfromXmlObj[$(this).prop("tagName")] = $(this).text()
														constuctfromXmlarr.push(constuctfromXmlObj)
														constuctfromXmlObj = {}
												})
										} else {
											constuctfromXml[parentName] = $(this).text()
										}
								})
							});
							$scope.editedLog[j][keyj] = constuctfromXml
						} else {
							$scope.editedLog[j][keyj] = false
						}
				}
			}
		}

	}).error(function (data) {})

	var len = 20;
	var loadMore = function(){
		// console.log($scope.parentInput.parentLink+'/audit/readall',"hello")
		if(($scope.dataLen.length >= 20)){
		// console.log($scope.dataLen,"loading")
          $http.post(BASEURL + RESTCALL.RoleAuditLog+'?count='+20+'&start='+len, {
			'RoleID' : $scope.RoleID
		  }).success(function (data) {	
					$scope.dataLen = data;			
				if(data.length != 0){
					$scope.editedLog = $scope.editedLog.concat($scope.dataLen)
                    // console.log($scope.editedLog,"hiii")
                    // console.log($scope.editedLog.length)
					len = len + 20;		
				}
			})
		}
	}

    $(document).ready(function(){
       // var debounceHandler = _.debounce(loadMore, 700, true);
        $('.editBody').on('scroll', function(){ 
            if( Math.round($(this).scrollTop() + $(this).innerHeight())>=$(this)[0].scrollHeight) {
               // debounceHandler();
                // console.log('came')
            }
        });
    })




	$scope.auditLogDetails = "";
	$scope.commentVal = "";

	$scope.costructAudit = function (argu) {

		$scope.auditLogDetails = argu
			$('#auditModel').find('tbody').html('')

			
			if (argu.oldData && argu.newData) {
				$('#auditModel').find('tbody').append('<tr><th>Field</th><th>Old Data</th><th>New Data</th></tr>')
			} else {
				$('#auditModel').find('tbody').append('<tr><th>Field</th><th>Data</th></tr>')
			}
			var _keys = ''

			if ($.isPlainObject(argu.oldData) && $.isPlainObject(argu.newData)) {
				_keys = (Object.keys(argu.oldData).length >= Object.keys(argu.newData).length) ? Object.keys(argu.oldData) : Object.keys(argu.newData)
			} else if ($.isPlainObject(argu.oldData)) {
				_keys = Object.keys(argu.oldData)
			} else if ($.isPlainObject(argu.newData)) {
				_keys = Object.keys(argu.newData)
			}

			for (var j in _keys) {
				if (!_keys[j].match(/_PK/g)) {
					var _tr = ""
						if (j % 2) {
							_tr = "<tr style='background-color: rgb(245, 245, 245)'>"
						} else {
							_tr = "<tr style='background-color: #fff'>"
						}
						_tr = _tr + "<td>" + $filter('camelCaseFormatter')(_keys[j]) + "</td>";
					if (argu.oldData && argu.newData) {
						if (argu.oldData) {
							_tr = _tr + "<td>"
								if (argu.oldData[_keys[j]]) {
									if (typeof(argu.oldData[_keys[j]]) == 'object') {
										_tr = _tr + "<pre>" + $filter('json')(argu.oldData[_keys[j]]) + "</pre>"
									} else {
										_tr = _tr + argu.oldData[_keys[j]];
									}
								}
								_tr = _tr + "</td>"
						}
						if (argu.newData) {
							if (argu.newData[_keys[j]]) {
								if (argu.oldData && argu.newData[_keys[j]] != argu.oldData[_keys[j]]) {
									_tr = _tr + "<td class=\"modifiedClass\">"
								} else {
									_tr = _tr + "<td>"
								}
								if (typeof(argu.newData[_keys[j]]) == 'object') {
									_tr = _tr + "<pre>" + $filter('json')(argu.newData[_keys[j]]) + "</pre>"
								} else {
									_tr = _tr + argu.newData[_keys[j]];
								}
								_tr = _tr + "</td>"
							}
						}
					} else {
						if (argu.newData) {
							_tr = _tr + "<td>"
								if (argu.newData[_keys[j]]) {
									if (typeof(argu.newData[_keys[j]]) == 'object') {
										_tr = _tr + "<pre>" + $filter('json')(argu.newData[_keys[j]]) + "</pre>"
									} else {
										_tr = _tr + argu.newData[_keys[j]];
									}
								}
								_tr = _tr + "</td>"
						} else if (argu.oldData) {
							_tr = _tr + "<td>"
								if (argu.oldData[_keys[j]]) {
									if (typeof(argu.oldData[_keys[j]]) == 'object') {
										_tr = _tr + "<pre>" + $filter('json')(argu.oldData[_keys[j]]) + "</pre>"
									} else {
										_tr = _tr + argu.oldData[_keys[j]];
									}
								}
								_tr = _tr + "</td>"
						}
					}
					$('#auditModel').find('tbody').append(_tr)
				}
			}

			if ((argu.action).match(/:/g)) {
				$scope.commentVal = (argu.action).split(/:(.+)/)
			} else {
				$scope.commentVal = ""
			}
	}

	$scope.sTrial = '';
	$scope.sResource = '';
	$scope.sRoleid = '';
	$scope.setKey = function(key,trial,resource,roleid)
	{
		$scope.sKey = key;
		$scope.sTrial = trial;
		$scope.sResource = resource;
		$scope.sRoleid = roleid;
		$scope.fromMod= false; 

	}

	$scope.submitDeldata = function()
	{
		var Attributes123 = $scope.sTrial.Attributes;
		Attributes123.splice($scope.sKey, 1);

		$('#delPopup').modal('hide')

		$scope.submitAllAttribute($scope.sTrial,$scope.sResource,$scope.sRoleid)
		//$scope.detailExpanded = false;
		
	}

	$scope.cancelClear = function(){
	console.log("val")
	$('.expandRow ').css("display", "none");
	}
	$scope.showaudit = function (argu) {

		$scope.costructAudit(argu)
		$('#auditModel').modal('toggle');
	}

});