VolpayApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

	var n = new Date().getTime()
	if (configData.Authorization == 'Internal') {
		$urlRouterProvider.otherwise("/login");
	}

	if (configData.Authorization == 'External') {
		$urlRouterProvider.otherwise("/app/dashboard");
	}

	$stateProvider
	.state('login', {
		url : "/login",
		templateUrl : "templates/login/VPlogin.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "loginCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'templates/login/VPloginCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app', {
		url : "/app",
		templateUrl : "modules/app.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		params : {
			details : null
		},
		controller : "appCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'templates/sidebar/VPsidebar.css',
					'templates/sidebar/VPsidebarCtrl.js',

					'templates/header/VPheaderCtrl.js',
					'templates/header/VPheader.css',

					'modules/appCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.paymentsummary', {
		url : "/dashboard",
		templateUrl : "modules/paymentdashboard/dashboard.html?a=" + n,
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "dashboardController",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/paymentdashboard/dashboardController.js'
					]
				});
			}
			]
		}
	})
	.state('app.routeregistry', {
		url : "/routeregistry",
		templateUrl : "modules/routeregistry/routeregistry.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "routeregistryCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/routeregistry/routeregistry.js'
					]
				});
			}
			]
		}
	})
	.state('app.serviceregistry', {
		url : "/serviceregistry",
		templateUrl : "modules/serviceregistry/serviceregistry.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "serviceregistryCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/serviceregistry/serviceregistryCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.systemdiagnosis', {
		url : "/systemdiagnosis",
		templateUrl : "modules/systemdiagnosis/systemdiagnosis.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "systemdiagnosisCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/systemdiagnosis/systemdiagnosisCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.scheduledroutelogaudit', {
		url : "/scheduledroutelogaudit",
		templateUrl : "modules/scheduledroutelogaudit/scheduledroutelogaudit.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "scheduledroutelogauditCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/scheduledroutelogaudit/scheduledroutelogauditCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.payments', {
		url : "/allpayments",
		templateUrl : "modules/allpayments/allpayments.html?a=" + n,
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		params : {
			input : ''
		},
		controller : "allPaymentsCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/allpayments/allPaymentsController.js'
					]
				});
			}
			]
		}
	})

	.state('app.paymentdetail', {
		url : "/paymentdetail",
		templateUrl : "modules/paymentdetail/paymentdetail.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		params : {
			input : ''
		},
		controller : "paymentDetailCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [
					'modules/paymentdetail/paymentDetailCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.bulkoverride', {
		url : "/bulkoverride",
		templateUrl : "modules/bulkoverride/bulkoverride.html",
		data : {
			pageTitle : 'Admin Dashboard Template'
		},
		params : {
			input : ''
		},
		controller : "bulkoverride",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/bulkoverride/bulkoverride.js'
					]
				});
			}
			]
		}
	})
	.state('app.payment-repair', {
		url : "/payment-repair",
		templateUrl : "modules/paymentrepair/payment-repair.html",
		data : {
			pageTitle : 'Admin Dashboard Template'
		},
		params : {
			input : ''
		},
		controller : "paymentRepairCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/paymentrepair/paymentRepairCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.schemedashboard', {
		url : "/schemedashboard",
		templateUrl : "modules/dashboard/schemedashboard/schemedashboard.html",
		data : {
			pageTitle : 'Scheme Dashboard'
		},
		controller : "SchemeController",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
								insertBefore : '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
								files : [
								'modules/dashboard/schemedashboard/schemedashboardController.js'
								]
							});
			}
			]
		}
	})
	.state('app.participantdashboard', {
		url : "/participantdashboard",
		templateUrl : "modules/dashboard/participantdashboard/participantdashboard.html",
		data : {
			pageTitle : 'Participant Dashboard'
		},
		controller : "ParticipantController",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
								insertBefore : '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
								files : [
								'modules/dashboard/participantdashboard/participantdashboardController.js'
								]
							});
			}
			]
		}
	})
	.state('app.transactionSearch', {
		url : "/transactionSearch",
		templateUrl : "modules/search/search.html",
		data : {
			pageTitle : 'Transaction Search'
		},
		controller : "searchCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [


					'modules/search/searchCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.bpsreport', {
		url : "/bpsreport",
		templateUrl : "modules/reports/reportdownload/reportdownload.html",
		data : {
			pageTitle : 'Report Download'
		},
		controller : "reportDownloadCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [
					'modules/reports/reportdownload/reportDownloadCtrl.js',
					'lib/xmltojson/xml2json.js'
					]
				});
			}
			]
		}
	})
		.state('app.bpsfiles', {
		url : "/BPSfiles",
		templateUrl : "modules/bpsfile/files/files.html",
		data : {
			pageTitle : 'Files'
		},
		controller : "filesCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [
					'modules/bpsfile/files/filesCtrl.js',
					'lib/xmltojson/xml2json.js'
					]
				});
			}
			]
		}
	})
		.state('app.bpsupload', {
		url : "/BPSupload",
		templateUrl : "modules/bpsfile/upload/upload.html",
		data : {
			pageTitle : 'Upload'
		},
		controller : "uploadCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [
					'modules/bpsfile/upload/uploadCtrl.js',
					'lib/xmltojson/xml2json.js'
					]
				});
			}
			]
		}
	})
	.state('app.bpsusers', {
		url : "/BPSusers",
		templateUrl : "modules/bpsusersmanagement/users/users.html",
		data : {
			pageTitle : 'List Of Users'
		},
		controller : "usersCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [
					'modules/bpsusersmanagement/users/usersCtrl.js',
					'lib/xmltojson/xml2json.js'
					]
				});
			}
			]
		}
	})
	.state('app.bpsaddrole', {
		url : "/BPSaddrole",
		templateUrl : "modules/bpsusersmanagement/addrole/addrole.html",
		data : {
			pageTitle : 'User details and Roles'
		},
		controller : "addroleCtrl",
		params : {
					UserID : null
				},
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/bpsusersmanagement/addrole/addroleCtrl.js',
					'lib/xmltojson/xml2json.js'
					]
				});
			}
			]
		}
	})
	.state('app.bpsroles', {
		url : "/BPSroles",
		templateUrl : "modules/bpsusersmanagement/role/role.html",
		data : {
			pageTitle : 'List of Roles'
		},
		controller : "rolesCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					insertBefore : '#ng_load_plugins_before',
					files : [
					'modules/bpsusersmanagement/role/roleCtrl.js',
					'lib/xmltojson/xml2json.js'
					]
				});
			}
			]
		}
	})
	.state('app.createRole', {
				url : "/createRole",
				templateUrl : "modules/createrole/createRole.html",
				data : {
					pageTitle : 'Create a Role'
				},
				controller : "createRoleCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load({
								name : 'VolpayApp',
								files : [
									'modules/createrole/createRoleCtrl.js'
								]
							});
						}
					]
				}
			})
	.state('app.viewRole', {
        url : "/viewRole",
        templateUrl : "modules/createrole/viewRole.html",
        data : {
          pageTitle : 'View a Role'
        },
        controller : "viewRoleCtrl",
        params : {
			RoleName : null
		},
        resolve : {
          deps : ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load({
                name : 'VolpayApp',
                files : [
                  'modules/createrole/viewRoleCtrl.js'
                ]
              });
            }
          ]
        }
      }).state('app.reversal', {
		url : "/reversal",
		templateUrl : "modules/reversal/reversal.html",
		data : {
			pageTitle : 'Reversal'
		},
		controller : "reversalCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load({
								name : 'VolpayApp',
								files : [
									'modules/reversal/reversalCtrl.js'
								]
							});
						}
					]
				}
			})
      .state('app.instructionApprove', {
		url : "/instructionApproveCtrl",
		templateUrl : "modules/instructionApprove/instructionApprove.html",
		data : {
			pageTitle : 'Instruction Approve'
		},
		controller : "instructionApproveCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load({
								name : 'VolpayApp',
								files : [
									'modules/instructionApprove/instructionApproveCtrl.js'
								]
							});
						}
					]
				}
			})
	.state('app.filesummary', {
		url : "/filedashboard",
		templateUrl : "modules/filedashboard/fileDashboard.html?a=" + n,
		data : {
			pageTitle : 'Admin Dashboard Template'
		},
		controller : "fileDashboardController",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/filedashboard/fileDashboardController.js'
					]
				});
			}
			]
		}
	})
	.state('app.instructions', {
		url : "/instructions",
		templateUrl : "modules/allfiles/filelist.html?a=" + n,
		data : {
			pageTitle : ''
		},
		controller : "fileListController",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/allfiles/fileListController.js'
					]
				});
			}
			]
		}
	})
	.state('app.confirmation', {
		url : "/confirmation",
		templateUrl : "modules/allconfirmation/allconfirmation.html",
		data : {
			pageTitle : ''
		},
		controller : "allconfirmationCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/allconfirmation/allconfirmationCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.distributedinstructions', {
		url : "/distributedinstructions",
		templateUrl : "modules/distributedinstructions/distributedinstructions.html",
		data : {
			pageTitle : 'Distributed Instructions'
		},
		params : {
			input : ''
		},
		controller : "distributedinstructionsctrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/distributedinstructions/distributedinstructionsctrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.outputpaymentsummary', {
		url : "/outputpaymentsummary",
		templateUrl : "modules/paymentsummary/outputpaymentsummary.html",
		data : {
			pageTitle : 'Output Payment Summary'
		},
		params : {
			input : ''
		},
		controller : "outputpaymentsummaryctrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/paymentsummary/outputpaymentsummaryctrl.js'
					]
				});
			}
			]
		}
	})

	.state('app.filedetail', {
		url : "/filedetail",
		templateUrl : "modules/filedetail/filedetail.html",
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		params : {
			input : null
		},
		controller : "filedetailCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/filedetail/filedetailCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.initiatetransaction', {
		url : "/initiatetransaction",
		templateUrl : "modules/initiatetransation/initiateTransaction.html?a=" + n,
		data : {
			pageTitle : 'AngularJS File Upload'
		},
		controller : "initiateTransactionCtrl",

		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/initiatetransation/initiateTransactionCtrl.js'
					]
				});
			}
			]
		}
	})
	.state('app.fileupload', {
		url : "/fileupload",
		templateUrl : "modules/fileupload/fileupload.html?a=" + n,
		data : {
			pageTitle : 'Admin Dashboard Template'
		},
		controller : "fileuploadCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [
					'modules/fileupload/fileuploadCtrl.js'
					]
				});
			}
			]
		}
	})

	.state('app.myprofile', {
		url : "/myprofile",
		templateUrl : "modules/myprofile/myprofile.html",
		data : {
			pageTitle : 'Admin Dashboard Template'
		},
		controller : "myprofileCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [

					'lib/bootstrap-toggle-master/css/bootstrap-toggle.min.css',
					'lib/bootstrap-toggle-master/js/bootstrap-toggle.min.js',

					'modules/myprofile/myprofileCtrl.js'

					]
				});
			}
			]
		}
	})
	.state('app.notifications', {
		url : "/AlertsandNotification",
		templateUrl : "modules/alertsnotification/AlertandNotific.html",
		data : {
			pageTitle : 'Admin Dashboard Template'
		},
		controller : "AlertandNotifiCtrl",
		resolve : {
			deps : ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name : 'VolpayApp',
					files : [

					'modules/alertsnotification/alertnotification.css',
					'modules/alertsnotification/AlertandNotifiCtrl.js'
					]
				});
			}
			]
		}
	})
			// User Management
			.state('app.users', {
				url : "/usermanagement",
				templateUrl : "modules/usermanagement/usermanagement.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null
				},
				controller : "userMgmtController",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',

							files : [
							'modules/usermanagement/userManagementCtrl.js'
							]
						});
					}
					]
				}
			})
			.state('app.userdetail', {
				url : "/userdetail",
				templateUrl : "modules/usermanagement/userdetail/usermanagementdetail.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null,
					permission : null
				},
				controller : "usermanagementdetail",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',

							files : [
							'modules/usermanagement/userdetail/usermanagementdetail.js'
							]
						});
					}
					]
				}
			})
			//Add user
			.state('app.adduser', {
				url : "/adduser",
				templateUrl : "modules/adduser/adduser.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null
				},
				controller : "adduserCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [
							'modules/adduser/adduserCtrl.js'
							]
						});
					}
					]
				}
			})
			//Roles and Responsibilities
			.state('app.roles', {
				url : "/roles",
				templateUrl : "modules/rolesandpermission/roles.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null
				},
				controller : "rolesCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',

							files : [
							'modules/rolesandpermission/rolesCtrl.js'
							]

						});
					}
					]
				}
			})
			.state('app.viewresourcepermission', {
				url : "/viewresourcepermission",
				templateUrl : "modules/rolesandpermission/viewresourcepermission/viewresourcepermission.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null
				},
				controller : "viewresourcepermissionCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [
							'modules/rolesandpermission/viewresourcepermission/viewresourcepermissionCtrl.js'
							]
						});
					}
					]
				}
			})
			
			.state('app.mpitemplate', {
				url : "/mpitemplate",
				templateUrl : "modules/mpitemplate/mpitemplate.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : null
				},
				controller : "mpitemplateCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [
							'modules/mpitemplate/mpitemplateCtrl.js'
							]
						});
					}
					]
				}
			})
			.state('app.mpidetail', {
				url : "/mpidetail",
				templateUrl : "modules/mpitemplate/mpidetail/mpidetail.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : null
				},
				controller : "mpidetailsCtrl",

				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [
							'modules/mpitemplate/mpidetail/mpidetailsCtrl.js'
							]
						});
					}
					]
				}
			})
			.state('app.ruleadd', {
				url : "/addrule",
				templateUrl : "modules/brules/ruleadd.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : null
				},

				resolve : {
					"check" : function ($location, $stateParams) {
						console.log("params",$stateParams)
						if (!$stateParams.input) {
							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [
							'modules/brules/ruleaddCtrl.js'
							]
						});
					}
					]
				}
			})
			.state('app.rulegenerate', {
				url : "/rulegenerate",
				templateUrl : "modules/brules/rulegenerate.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : null
				},
				resolve : {
					"check" : function ($location, $stateParams) {
						if (!$stateParams.input) {
							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [
							'modules/brules/rulegenerate.js'
							]
						});
					}
					]
				}
			})

			//Application configuration
			.state('app.configurations', {
				url : "/AppConfig",
				templateUrl : "modules/appconfig/AppConfig.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				controller : "AppConfigCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
								insertBefore : '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
								files : [

								'modules/appconfig/AppConfigCtrl.js'
								]
							});
					}
					]
				}
			})

			//Volpayidconfig
			.state('app.idconfigurations', {
				url : "/volpayidconfig",
				templateUrl : "modules/volpayidconfig/volpayIdConfig.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				controller : "volpayidConfigCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [

							'modules/volpayidconfig/volpayidConfigCtrl.js'
							]
						});
					}
					]
				}
			})

			//Volpayidconfig Details
			.state('app.volpayidconfigdetail', {
				url : "/volpayidconfigdetail",
				templateUrl : "modules/volpayidconfig/viewvolpayidconfig/viewVolpayIdConfig.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : null
				},
				controller : "volpayidConfigDetailCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [

							'modules/volpayidconfig/viewvolpayidconfig/volpayidConfigDetailCtrl.js'
							]
						});
					}
					]
				}
			})
			//Bank Data approvals
			.state('app.approvals', {
				url : "/approval",
				templateUrl : "modules/approvals/approval.html?a=" + n,
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				controller : "approvalCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',

							files : [


							'modules/approvals/approvalCtrl.js'
							]
						});
					}
					]
				}
			})

			.state('app.viewApprovalData', {
				url : "/approval",
				templateUrl : "modules/approvals/viewapproval/viewApprovalData.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null
				},
				controller : "viewApprovalDataCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [
							'modules/approvals/viewapproval/viewApprovalDataCtrl.js'
							]
						});
					}
					]
				}
			})

			//Add Roles
			.state('app.addroles', {
				url : "/addroles",
				templateUrl : "modules/addrole/addroles.html",
				data : {
					pageTitle : 'Admin Dashboard Template'
				},
				params : {
					input : null
				},
				controller : "addrolesCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [
							'modules/addrole/addrolesCtrl.js'
							]
						});
					}
					]
				}
			})

			//Forgot Password
			.state('forgotpassword', {
				url : "/forgotpassword",
				templateUrl : "modules/forgotpassword/forgotpassword.html",
				data : {
					pageTitle : 'forgotpasswordCtrl'
				},
				controller : "forgotpasswordCtrl",
				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [
							'modules/forgotpassword/forgotpasswordCtrl.js'
							]
						});
					}
					]
				}
			})
			

			
			.state('app.reportsdetail', {
				url : "/reportsdetail",
				templateUrl : "modules/reports/reportsdetail/reportsDetail.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				controller : "reportsDetailCtrl",

				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
								insertBefore : '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
								files : [
								'modules/reports/reportsdetail/reportsDetailCtrl.js'
								]
							});
					}
					]
				}
			})
			
			.state('app.reportgenerate', {
				url : "/reportgenerate",
				templateUrl : "modules/reports/reportgenerate/reportgenerate.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				controller : "reportGenerateCtrl",

				resolve : {
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							insertBefore : '#ng_load_plugins_before',
							files : [


							'modules/reports/reportgenerate/reportGenerateCtrl.js'
							]
						});
					}
					]
				}
			})
			//Bank Data
			.state('app.bankData', {
				url : "/:urlId",
				onEnter: function ($state, $stateParams, $cookies,$filter) {
					$stateParams.urlId = $filter('removeSpace')($stateParams.input.gotoPage.Name).toLowerCase();

				},
				templateUrl : "modules/bankdata/bankData.html",
				data : {
					pageTitle : ''
				},
				params : {
					input : {
						dynamic:true
					}
				},
				controller : "bankDataCtrl",
				resolve : {
					"check" : function ($location, $stateParams) {

						console.log($stateParams)
						if ($stateParams.input.dynamic) {
							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [

							'modules/bankdata/bankDataCtrl.js'
							]
						});
					}
					]
				}
			})


			.state('app.webformPlugin', {
				url : "/addons/:urlIdForAddon",
				onEnter: function ($state, $stateParams, $cookies,$filter) {
					$stateParams.urlIdForAddon = $filter('removeSpace')($stateParams.input.gotoPage.Name).toLowerCase();

				},
				templateUrl : "modules/webformaddons/webformPlugin.html",
				data : {
					pageTitle : ''
				},
				params : {
					input : {
						dynamic:true
					}
				},
				controller : "bankDataAddonCtrl",
				resolve : {
					"check" : function ($location, $stateParams) {

						if (!$stateParams.input) {

							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',

							files : [

							'modules/webformaddons/bankDataAddonCtrl.js'
							]
						});
					}
					]
				}
			})

		// AngularJS plugins
		.state('app.view', {
			url : "/:urlId/view",
			onEnter: function ($state, $stateParams, $cookies,$filter) {
				$stateParams.urlId = $filter('removeSpace')($stateParams.input.gotoPage.Name).toLowerCase();

			},
			templateUrl : "modules/bankdata/bankdatafunctions/viewBankData.html",
			data : {
				pageTitle : 'AngularJS File Upload'
			},
			params : {
				input : {
					dynamic:true
				}
			},
			controller : "viewBankData",
			resolve : {
				"check" : function ($location, $stateParams) {

					if (!$stateParams.input) {
						$location.path('/app')
					}
				},
				deps : ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load({
						name : 'VolpayApp',
						files : [

						'modules/bankdata/bankdatafunctions/viewBankData.js'
						]
					});
				}
				]
			}
		})

			//Addon view
			.state('app.addonview', {
				url : "/addons/:urlIdForAddon/view",
				onEnter: function ($state, $stateParams, $cookies,$filter) {
					$stateParams.urlIdForAddon = $filter('removeSpace')($stateParams.input.gotoPage.Name).toLowerCase();

				},
				templateUrl : "modules/webformaddons/bankdatafunctions/viewBankData.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : {
						dynamic:true
					}
				},
				controller : "webviewBankData",
				resolve : {
					"check" : function ($location, $stateParams) {

						if (!$stateParams.input) {

							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [
							'modules/webformaddons/bankdatafunctions/viewBankData.js'
							]
						});
					}
					]
				}
			})

			

			//Addon Operation
			.state('app.addonoperation', {
				url : "/addons/:urlIdForAddon/:urlOperationForAddon",
				onEnter: function ($state, $stateParams, $cookies,$filter) {
					$stateParams.urlIdForAddon = $filter('removeSpace')($stateParams.input.gotoPage.Name).toLowerCase();
					$stateParams.urlOperationForAddon = $stateParams.input.Operation.toLowerCase();
				},

				templateUrl : "modules/webformaddons/bankdatafunctions/bankDataFunctions.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : {
						dynamic:true
					}
					
				},
				controller : "webbankDataFunctions",

				resolve : {
					"check" : function ($location, $stateParams) {

						if (!$stateParams.input) {
							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [

							'modules/webformaddons/bankdatafunctions/bankDataFunctions.js'
							]
						});
					}
					]
				}
			})
			.state('app.newmodules', {
				url : '/plugins/:newUrl',
				templateUrl : function (aa) {
					if (aa.input.tempUrl != null) {
						return aa.input.tempUrl + '/' + aa.input.url + '.html'
					}
				},
				onEnter: function ($state, $stateParams, $cookies,$filter) {
					$stateParams.newUrl = $stateParams.input.url;
				},
				data : {
					pageTitle : 'Dynamic Modules'
				},
				controllerProvider : function ($stateParams) {
					return $stateParams.input.contrl
				},
				params : {
					input:{
						dunamic:true
					}
				},
				resolve : {
					"check" : function ($location, $stateParams) {
						if ($stateParams.input.tempUrl == null) {

							$location.path('app/dashboard')
						}
					},
					otherFeature : function ($ocLazyLoad, $stateParams) {
						console.log("params",$stateParams)
						if ($stateParams.input.tempUrl != null) {
							return $ocLazyLoad.load({
								name : "VolpayApp",
								files : [$stateParams.input.tempUrl + '/' + $stateParams.input.contrl + '.js']
							})
						}

					}
				}
			})
			.state('app.operation', {
				url : "/:urlId/:urlOperation",
				onEnter: function ($state, $stateParams, $cookies,$filter) {
					$stateParams.urlId = $filter('removeSpace')($stateParams.input.gotoPage.Name).toLowerCase();
					$stateParams.urlOperation = $stateParams.input.Operation.toLowerCase();
				},
				templateUrl : "modules/bankdata/bankdatafunctions/bankDataFunctions.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				params : {
					input : {
						dynamic:true
					}
				},
				controller : "bankDataFunctions",
				resolve : {
					"check" : function ($location, $stateParams) {
						if ($stateParams.input.dynamic) {
							$location.path('/app')
						}
					},
					deps : ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							name : 'VolpayApp',
							files : [
							'lib/spectrum/spectrum.css',
							'lib/spectrum/spectrum.js',
							'modules/bankdata/bankdatafunctions/bankDataFunctions.js'
							]
						});
					}
					]
				}
			})
			.state('app.externalLink', {
				url : "/external",
				templateUrl : "modules/external/external.html",
				data : {
					pageTitle : 'AngularJS File Upload'
				},
				controller : "externalCtrl",
				params : {
					url : null,
				},
				resolve : {
					"check" : function ($location, $stateParams) {

						console.log($stateParams)
						if ($stateParams.url == null) {
							$location.path('app/dashboard')
						}
					},
					otherFeature : function ($ocLazyLoad, $stateParams) {
						return $ocLazyLoad.load({
							name : "VolpayApp",
							files : ["modules/external/externalCtrl.js"]
						})

					}
				}
			})

			

		}
		]);