﻿VolpayApp.controller('sidebarController', function ($scope, $rootScope, $http, $location, $state, $timeout, $filter, GlobalService, $translate, $stateParams) {

	sessionStorage.ROLE_ID = (sessionStorage.ROLE_ID)?sessionStorage.ROLE_ID: localStorage.ROLE_ID;
	sessionStorage.UserID = (sessionStorage.UserID)?sessionStorage.UserID:localStorage.UserID;

	$scope.translateFn = function (translateVal) {
		$scope.tVal = translateVal;
		$scope.firstFilteredVal = $filter('removeSpace')($scope.tVal);
		$scope.secondFilteredVal = $filter('specialCharactersRemove')($scope.firstFilteredVal);
		$scope.translatedVal = $filter('translate')('Sidebar.' + $scope.secondFilteredVal);
		if ($scope.translatedVal.indexOf('Sidebar.') != -1) {
			$scope.translatedVal = translateVal
		}
		return $scope.translatedVal;
	}

	function removeAdminPanel(sideBarVal) {

		for (i = 0; i < sideBarVal.length; i++) {
			if (sideBarVal[i].ParentName == "Admin Panel") {
				delete sideBarVal[i];
			}
		}
		return orderArray(sideBarVal);
	}

	function orderArray(Arr) {
		for (i = 0; i < Arr.length; i++) {
			if (Arr[i] != undefined) {
				Arr[i] = Arr[i];
			}
		}
		return Arr;
	}

	var getAddon = function (path) {
		return $.ajax({
			url : path,
			async : false,
			cache : false,
			type : 'GET',
			dataType : 'json'
		}).responseJSON;
	}

	function appendAddon(data1) {

		var addon1 = [];
		var addon = getAddon('plug-ins/addon.json');
		if (addon.length > 0) {
			var addon1 = data1.concat(addon);

		} else {
			addon1 = data1;
		}
		return addon1;
	}

	function appendNewModule(data1) {

		$scope.dupIndex = '';
		var addon1 = [];
		var addon = getAddon('plug-ins/modules.json');

		if (addon.length > 0) {
			for (k in data1) {
				for (i in addon) {

					if ((addon[i].ParentName == data1[k].ParentName) && (!addon[i].ExternalMenu)) {
						$scope.dupIndex = i;

						for (j in addon[i].subMenu) {

							data1[k].subMenu.push(addon[i].subMenu[j])
						}
						if ($scope.dupIndex != '') {
							addon.splice($scope.dupIndex, 1)
						}

					}
					addon1 = data1.concat(addon);
				}
			}

		} else {
			addon1 = data1;
		}

		return addon1;
	}

	function externalLink(sVal) {
		var extLink = getAddon('plug-ins/externallinks.json');
		sVal.push(extLink)
		return sVal;
	}
	
	$scope.sidebarVal = (sessionStorage.menuList)?JSON.parse(sessionStorage.menuList):[];
	
	$rootScope.$on("CallParentMethod", function () {
		//$scope.parentmethod();
		$scope.sidebarVal = JSON.parse(sessionStorage.menuList);
	});

	$scope.pName = '';
	$scope.cName = '';
	if ((sessionStorage.pwRest == false) || (sessionStorage.pwRest == 'false')) {

		$scope.sidebarVal = JSON.parse(sessionStorage.menuList);

		//console.log("aa",sessionStorage.selectedMenu)
		$scope.landArr = JSON.parse(sessionStorage.menuList)

			for (var i in $scope.landArr) {
				for (var j in $scope.landArr[i].subMenu) {
					//console.log($scope.landArr[i].subMenu[j].Link,sessionStorage.selectedMenu)
					if ($scope.landArr[i].subMenu[j].Link == sessionStorage.selectedMenu) {
						$scope.landArr[i].subMenu[j].Name

						$scope.landArr[i].subMenu[j].Name = $filter('removeSpace')($scope.landArr[i].subMenu[j].Name);
						$scope.landArr[i].subMenu[j].Name = $filter('specialCharactersRemove')($scope.landArr[i].subMenu[j].Name);

						$scope.landArr[i].ParentName = $filter('removeSpace')($scope.landArr[i].ParentName);
						$scope.landArr[i].ParentName = $filter('specialCharactersRemove')($scope.landArr[i].ParentName);

						$scope.pName = $scope.landArr[i].ParentName;
						$scope.cName = $scope.landArr[i].subMenu[j].Name;

						//console.log($scope.landArr[i].ParentName,$scope.landArr[i].subMenu[j].Name)

						/*setInterval(function()
					{
						sidebarMenuControl($scope.landArr[i].ParentName,$scope.landArr[i].subMenu[j].Name);
						},1000)*/

					}
				}
			}

			if ($scope.cName) {
				var newInt = ''
					clearInterval(newInt)
					newInt = setInterval(function () {

						if ($('#' + $scope.cName).length) {
							sidebarMenuControl($scope.pName, $scope.cName);
							clearInterval(newInt)
						}
					}, 100)
			}

	}

if ((configData.Authorization == "External") && (document.cookie)) {
		 $rootScope.$on("CallParentMethod1", function(evt,data){          
					$scope.sidebarVal = data;
        });
	
	}

	setTimeout(function () {
		if (userData.customDashboardWidgets.showDashboard) {
			$('#MyDashboard').css('display', 'block')
		} else {
			$('#MyDashboard').css('display', 'none')
		}
	}, 1000)

	$scope.prev = -1;
	$scope.events = {};

	$scope.gotoPage = function (eve, val, subVal, fl) {

		//$rootScope.dataModified
		//  console.log(eve, val, subVal, fl,"ff",	$rootScope.dataModified )

		$scope.events = {
			'eve' : eve,
			'val' : val,
			'subVal' : subVal,
			'fl' : fl
		}

		// if($scope.events.subVal.Link == "initiatetransaction")
		// {
		// 	$("#changesLostModal").modal("hide");
		// }
		// $rootScope.dataModified = false;
		if (sessionStorage.OkAlertClicked) {
			$rootScope.dataModified = false;
			delete sessionStorage.OkAlertClicked;
		}
		if ($rootScope.dataModified) {
			if ($scope.events.subVal.Link != "initiatetransaction" && $scope.events.subVal.Link != "routeregistry" && $scope.events.subVal.Link != "configurations") {
				$rootScope.$emit("MyEvent", true);
			}
			$scope.fromCancelClick = false;
		} else {

			if (!$("body").hasClass('sidebar-collapse')) {
				// subVal.IconName = val.IconName
				
			}
			if (subVal) {

				sessionStorage.selectedMenu = subVal.Link;
				sessionStorage.menuSelection = JSON.stringify({
						'val' : $filter('specialCharactersRemove')($filter('removeSpace')(val.ParentName)),
						'subVal' : $filter('specialCharactersRemove')($filter('removeSpace')(subVal.Name))
					})

					$('.sidebar-menu').each(function () {
						$(this).find('.sidebarSubMenu').find('li').removeClass('sideMenuSelected')
					})
					$(eve.currentTarget).parent().addClass('sideMenuSelected')

					subVal.ParentName = val.ParentName;
					subVal.ParentIcon = val.IconName;

				if ((subVal.Link == 'mpitemplate') || (subVal.Link == 'configurations') || (subVal.Link == 'idconfigurations') || (subVal.Link == 'routeregistry') || (subVal.Link == 'serviceregistry') || (subVal.Link == 'systemdiagnosis') || (subVal.Link == 'scheduledroutelogaudit')) {
					$scope.stateApp = subVal.Link;

				} else {
					$scope.stateApp = val.Link != 'app' ? val.Link : subVal.Link
				}

				$scope.addActive = $filter('removeSpace')(subVal.Name);
				$scope.addActives = $filter('removeSpace')(val.ParentName);

				if (fl) {
					setTimeout(function () {
						sidebarMenuControl($filter('specialCharactersRemove')($filter('removeSpace')(val.ParentName)), $filter('specialCharactersRemove')($filter('removeSpace')(subVal.Name)));
					}, 500)
				} else {
					sidebarMenuControl($filter('specialCharactersRemove')($filter('removeSpace')(val.ParentName)), $filter('specialCharactersRemove')($filter('removeSpace')(subVal.Name)));
				}

			} else {
				$scope.stateApp = "paymentsummary";
				$scope.callDefault()
			}
			$scope.input = {
				'gotoPage' : subVal,
				'responseMessage' : ''
			}
			if (!val.allowThirdParty) {
				if ((subVal.PlugPlay) || (val.ExternalMenu)) {
					$scope.urlVal = $filter('nospace')(subVal.Name);

					$scope.urlVal = $filter('specialCharactersRemove')($scope.urlVal)
						$scope.tUrl = $filter('nospace')(subVal.Name);
					$scope.tUrl = $filter('specialCharactersRemove')($scope.tUrl)
						$scope.dataObj = {
						"url" : $filter('lowercase')($scope.urlVal),
						"tempUrl" : 'plug-ins/modules/' + $filter('lowercase')($scope.tUrl),
						"contrl" : subVal.controller
					}

					$state.go('app.newmodules', {
						input : {
							url : $filter('lowercase')($scope.urlVal),
							tempUrl : 'plug-ins/modules/' + $filter('lowercase')($scope.tUrl),
							contrl : subVal.controller
						}
					});

				} else {
					if (fl) {
						$scope.input.triggerIs = fl;
					}

					$state.go('app.' + $scope.stateApp, {
						input : $scope.input

					});
				}

			} else {

				$state.go('app.externalLink', {
					"url" : subVal.Link
				})

			}

			$scope.mediaQuery = window.matchMedia("(max-width: 767px)");
			if ($scope.mediaQuery.matches) {
				$("body").removeClass('sidebar-open');
				$scope.sidebarToggleTooltip = "Show Menu"
			}

		}

	}

	$rootScope.$on("MyEvent2", function (evt, data) {
		$rootScope.dataModified = false;
		// console.log($scope.events)
		$scope.gotoPage($scope.events.eve, $scope.events.val, $scope.events.subVal, $scope.events.fl)
		$scope.events = {};
	})

	$scope.parentMenu = function (evt) {

		if ($('#' + $(evt.currentTarget).parent().attr('id')).hasClass('open')) {
			$('#' + $(evt.currentTarget).parent().attr('id')).removeClass('open').find('.sidebarSubMenu').slideUp();
			$('#' + $(evt.currentTarget).parent().attr('id')).find("a span").next().removeAttr('class').attr('class', 'fa fa-plus');
		} else {
			$('#' + $(evt.currentTarget).parent().attr('id')).parent().find('.menuli').removeClass('open').find('.sidebarSubMenu').slideUp();
			$('#' + $(evt.currentTarget).parent().attr('id')).parent().find('.menuli .ParentMenu span').next().removeAttr('class').attr('class', 'fa fa-plus');

			$('#' + $(evt.currentTarget).parent().attr('id')).addClass('open').find('.sidebarSubMenu').slideDown();
			$('#' + $(evt.currentTarget).parent().attr('id')).find("a span").next().removeAttr('class').attr('class', 'fa fa-minus');
		}
	}

	$scope.showSmallLogo = false;

	$('#sidebarMenu').find('.appLogo').css('display', 'block');
	$scope.sidebarToggleTooltip = "Hide Menu"
		$("body").removeClass('sidebar-collapse');
	$('.ParentMenu').find("span").next().removeAttr('class').attr('class', 'fa fa-plus').css({
		'float' : 'right'
	})
	$('.sidebarSubMenu li a').css('padding-left', '24px');
	$('.sidebarSubMenu li a span').css('margin-left', '0px');
	$('.fixedfooter').css('margin-left', '230px');
	$('.footertext1').css('left', '-210px');

	$scope.sideBar = function (value) {
		/*$('.sidebar-menu').find('.sidebarSubMenu').removeClass('open').css({
		'display' : 'none'
		})*/
		$scope.prev = -1;

		if (value == 1) {
			if ($("body").hasClass('sidebar-collapse')) {

				$scope.showSmallLogo = false;
				$('#sidebarMenu').find('.appLogo').css('display', 'block');
				$scope.sidebarToggleTooltip = "Hide Menu"
					$("body").removeClass('sidebar-collapse');
				$('.ParentMenu').find("span").next().removeAttr('class').attr('class', 'fa fa-plus').css({
					'float' : 'right'
				})
				$('.sidebarSubMenu li a').css('padding-left', '');
				$('.sidebarSubMenu li a span').css('margin-left', '0px');
				$('.fixedfooter').css('margin-left', '230px');
				$('.footertext1').css('left', '-210px');
			} else {

				$scope.showSmallLogo = true;
				$('#sidebarMenu').find('.appLogo').css('display', 'none');

				$("body").addClass('sidebar-collapse');
				$scope.sidebarToggleTooltip = "Show Menu"
					$('.ParentMenu').find("span").next().css({
						'float' : ''
					})

					$('.sidebarSubMenu li a').css('padding-left', '13px');
				$('.sidebarSubMenu li a span').css('margin-left', '10px');
				$('.fixedfooter').css('margin-left', '50px');
				$('.footertext1').css('left', '-30px');
			}
		}
	};
	//$scope.sideBar(1)

	if ($("body").hasClass('sidebar-open')) {
		$scope.sidebarToggleTooltip = "Show Menu"
	} else {
		$scope.sidebarToggleTooltip = "Hide Menu"
	}

	if ($stateParams.details) {
		//console.log($stateParams.details)
		for (j in GlobalService.sidebarVal) {
			for (k in GlobalService.sidebarVal[j].subMenu) {
				if (GlobalService.sidebarVal[j].subMenu[k].Link == $stateParams.details.toPage) {
					$scope.gotoPage('', GlobalService.sidebarVal[j], GlobalService.sidebarVal[j].subMenu[k], $stateParams.details)
				}
			}
		}
	}

	
	

	if (!$scope.sidebarVal.length) {

		

		$http.get(BASEURL + RESTCALL.UserProfile).success(function (data) {
		console.log("user",data)
		$scope.sidebarArr = [];
		var sidebarObj = {};
		sidebarObj.RoleId = data.RoleID;
		sessionStorage.ROLE_ID = data.RoleID;
		sessionStorage.UserID = data.UserID;
		localStorage.ROLE_ID = data.RoleID;
		localStorage.UserID = data.UserID;

		$translate.use("en_US");

		$http.get('config/sidebarVal.json').success(function (data) {
	  
		 sidebarObj.menu = appendAddon(data);
	  
		 $http.post(BASEURL + RESTCALL.sideBarValues, sidebarObj).success(function (data) {
	  
			
		  $scope.landData = angular.copy(data)
		   $scope.sidebarVal = appendNewModule(data)
		   sessionStorage.menuList = JSON.stringify(data);
		  $scope.sidebarVal = externalLink($scope.sidebarVal)
		   GlobalService.sidebarVal = $scope.sidebarVal;
		  $rootScope.$emit("CallParentMethod", $scope.sidebarVal);
		 }).error(function (data) {})
		}).error(function () {})
	   }).error(function (data) {})
	}  


})