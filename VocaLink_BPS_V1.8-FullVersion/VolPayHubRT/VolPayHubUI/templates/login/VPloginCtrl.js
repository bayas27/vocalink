﻿VolpayApp.controller('loginCtrl', function ($rootScope, $scope, $http, $location, $state, $timeout, $filter, $translate, AdminService, GlobalService, AllPaymentsGlobalData, CommonService) {

	delete sessionStorage.menuSelection;
	$rootScope.dataModified = false;

	var getAddon = function (path) {
		return $.ajax({
			url : path,
			async : false,
			cache : false,
			type : 'GET',
			dataType : 'json'
		}).responseJSON;
	}

	function appendAddon(data1) {

		var addon1 = [];
		var addon = getAddon('plug-ins/addon.json');
		if (addon.length > 0) {
			var addon1 = data1.concat(addon);

		} else {
			addon1 = data1;
		}
		return addon1;
	}

	function appendNewModule(data1) {

		$scope.dupIndex = '';
		var addon1 = [];
		var addon = getAddon('plug-ins/modules.json');

		
		if (addon.length > 0) {
			for (k in data1) {
				for (i in addon) {

					if ((addon[i].ParentName == data1[k].ParentName) && (!addon[i].ExternalMenu)) {
						$scope.dupIndex = i;

						for (j in addon[i].subMenu) {

							data1[k].subMenu.push(addon[i].subMenu[j])
						}
						if ($scope.dupIndex != '') {
							addon.splice($scope.dupIndex, 1)
						}

					}
					addon1 = data1.concat(addon);
				}
			}

		} else {
			addon1 = data1;
		}

		return addon1;
	}

	function externalLink(sVal) {
		var extLink = getAddon('plug-ins/externallinks.json');
		sVal.push(extLink)
		return sVal;
	}




	$('.nvtooltip').css('opacity',0);
	$('.select2-container').removeClass('select2-container--open')

			

	$('body').css('background-color','transparent')
	$('html').css({
		"background" : "url(themes/" + configData.ThemeName + "/bg.jpg) no-repeat center center fixed",
		"background-size" : "cover"
	})


	$('.nvtooltip').css({
		'display' : 'none'
	});

	setTimeout(function () {
		$('.fixedfooter,.main-footer').css({
			'background-color' : '#364150',
			'margin-left': '0'
		})
       $('.footertext1').css('left', '20px');
	}, 200)
	
	var userData = uProfileData;
	
	$scope.login = function (loginData) {

		var loginObj = {};
		loginObj = (JSON.stringify(loginData));

		$http.post(BASEURL + RESTCALL.LoginREST, loginObj).success(function (data, status) {
			console.log("login", data)

			sessionStorage.createUserLoginName = loginData.UserId;
			sessionStorage.UserID = loginData.UserId;
			sessionStorage.SessionToken = data.SessionToken;
			sessionStorage.IsProfileSetup = data.IsProfileSetup;
			
			

			uiConfiguration();
			sessionStorage.forSLSK = "SECRET" + rand(11111, 55555);
			sessionStorage.forSL = JSON.stringify(encrypt(JSON.stringify(loginObj), sessionStorage.forSLSK));

			
			$scope.aa = {
				"Queryfield" : [{
						"ColumnName" : "UserID",
						"ColumnOperation" : "=",
						"ColumnValue" : sessionStorage.UserID
					}
				],
				"Operator" : "AND"
			}

			$scope.aa = constructQuery($scope.aa);

		if('ForceResetFlag' in data.UserInfo && ((data.UserInfo.ForceResetFlag != true) || (data.UserInfo.ForceResetFlag != 'true')))
			{
				

				function callUserProfile()
				{

				
				
			$http.post(BASEURL + RESTCALL.userProfileData + '/readall', $scope.aa).success(function (data1) {
					
				if (!data1.length) {

					userData = uProfileData;
					var lObj = {};
					lObj.UserID = sessionStorage.UserID;
					lObj.ProfileData = $filter('stringToHex')(JSON.stringify(userData));

					$http.post(BASEURL + RESTCALL.userProfileData, lObj).success(function (data) {
						callUserProfile()
					}).error(function (error) {

						console.log(error)
					})
					//$translate.use('en_US');
					//checkLogin(userData)
					
				} else {

					$scope.uData = JSON.parse($filter('hex2a')(data1[0].ProfileData))
					userData = $scope.uData;
					sessionStorage.UserProfileDataPK = data1[0].UserProfileData_PK;
					
							if($scope.uData.genSetting.languageSelected)
							{
								$translate.use($scope.uData.genSetting.languageSelected);
							}
							else{
								$translate.use("en_US");
							}
					
					checkLogin($scope.uData)
				}

				

			}).error(function (error) {

				userData = uProfileData;
				checkLogin(userData)
				$translate.use("en_US");
			})

		}

		callUserProfile()



		}
		else{
			userData = uProfileData;
			checkLogin(userData)
			$translate.use("en_US");
		}





			function checkLogin(inData)
			{
			$('#themeColor').attr("href", "themes/styles/" + inData.genSetting.themeSelected + ".css");
			
			if ((sessionStorage.IsProfileSetup == true) || (sessionStorage.IsProfileSetup == 'true')) {
				sessionStorage.showMoreFieldOnCreateUser = false;
				GlobalService.pwRest = true;
				sessionStorage.pwRest = GlobalService.pwRest;
				
				sessionStorage.Name = "Default";
				$location.path('app/adduser');
			} else {
				sessionStorage.UserID = data.UserInfo.UserID;
				GlobalService.pwRest = data.UserInfo.ForceResetFlag;
				sessionStorage.pwRest = GlobalService.pwRest;
				
				if (GlobalService.pwRest) {
					sessionStorage.showMoreFieldOnCreateUser = false;
					$location.path('app/myprofile');
				} else {
					$rootScope.NotifLoaded = false;
					CommonService.alertLoadCnt = 0;

					sessionStorage.ROLE_ID = data.UserInfo.RoleID;
					sessionStorage.showMoreFieldOnCreateUser = true;
					
					/** Menulist REST Call **/

					$scope.sidebarArr = [];
					$http.get('config/sidebarVal.json').success(function (data) {
						var sidebarObj = {};
						sidebarObj.RoleId = sessionStorage.ROLE_ID;
						sidebarObj.menu = appendAddon(data);

						$http.post(BASEURL + RESTCALL.sideBarValues, sidebarObj).success(function (data) {
								
							$scope.landData = angular.copy(data)
							$scope.sidebarVal = appendNewModule(data)
							sessionStorage.menuList = JSON.stringify(data);
							$scope.sidebarVal = externalLink($scope.sidebarVal)
							GlobalService.sidebarVal = $scope.sidebarVal; 

							$scope.refArr = ["webformPlugin"];
							$scope.landingMod = [];
							$scope.lArr = [];

							
							for(var i in $scope.landData)
							{
								if($scope.refArr.indexOf($scope.landData[i].Link) == -1)
								{
									for(var j in $scope.landData[i].subMenu)
									{
										if($scope.landData[i].Link == 'bankData')
										{
											if(($scope.landData[i].subMenu[j].Link == 'configurations' || $scope.landData[i].subMenu[j].Link == 'idconfigurations'))
											{

											$scope.landingMod.push({name:$scope.landData[i].subMenu[j].Name,state:$scope.landData[i].subMenu[j].Link,static:true,stateParams:{}})

											$scope.lArr.push($scope.landData[i].subMenu[j].Link)
											}
										}
										else
										{
											$scope.landingMod.push({name:$scope.landData[i].subMenu[j].Name,state:$scope.landData[i].subMenu[j].Link,static:true,stateParams:{}})

											$scope.lArr.push($scope.landData[i].subMenu[j].Link)
										}
									}
								}
								
							}

							//console.log("land",$scope.landingMod),
							
							/** If MyProfileSetting keys is not present in the bank's user profile data **/
							if((inData.myProfileSetting != undefined))
							{	
								userData.myProfileSetting.landingPagesArr = [];
								userData.myProfileSetting.landingPagesArr = $scope.landingMod;

								if((Object.keys(inData.myProfileSetting).indexOf('nlanding') == -1))
								{
									userData.myProfileSetting.nlanding = {};
									userData.myProfileSetting.nlanding.name = '';
									userData.myProfileSetting.nlanding.stateParams = {};

									var interval = "";
									clearInterval(interval)
									interval = setInterval(function () {
										if (sessionStorage.UserProfileDataPK != undefined) {
											updateUserProfile(($filter('stringToHex')(JSON.stringify(userData))),$http).then(function(response){

												sessionStorage.selectedMenu = $scope.landingMod[0].state;
												$state.go('app.'+sessionStorage.selectedMenu, {});
											}) 
											clearInterval(interval)
										} 
										
									}, 100)

								}
								else{
									//console.log("22",inData,inData.myProfileSetting.nlanding)
									//var interval = "";
									updateUserProfile(($filter('stringToHex')(JSON.stringify(userData))),$http).then(function(response){
													
												$scope.sObj = {
													'name':$scope.landingMod[0].state,
													"stateParams":{}
												}

													if($scope.lArr.indexOf(inData.myProfileSetting.nlanding.name) != -1)
													{
														
														sessionStorage.selectedMenu = inData.myProfileSetting.nlanding.name;
														updateUserProfile(($filter('stringToHex')(JSON.stringify(userData))),$http).then(function(response){
														
														})
														findLandingModule(inData.myProfileSetting.nlanding,$state)
													}
													else{

														userData.myProfileSetting.nlanding = {};
														userData.myProfileSetting.nlanding.name = $scope.sObj.name
														userData.myProfileSetting.nlanding.stateParams = $scope.sObj.stateParams;

														sessionStorage.selectedMenu = $scope.sObj.name;

														updateUserProfile(($filter('stringToHex')(JSON.stringify(userData))),$http).then(function(response){
															console.log("asdf")
															findLandingModule($scope.sObj,$state)
														})

														
													}


												

											}) 
										
								}
							}
							else{

								userData.myProfileSetting = {};
								userData.myProfileSetting.nlanding={}
								userData.myProfileSetting.nlanding.name = $scope.landingMod[2].state?$scope.landingMod[2].state:$scope.landingMod[0].state;
								userData.myProfileSetting.nlanding.stateParams = {};

								userData.myProfileSetting.landingPagesArr = [];
								userData.myProfileSetting.landingPagesArr = $scope.landingMod;

								var interval = "";
								clearInterval(interval)
								interval = setInterval(function () {
										if (sessionStorage.UserProfileDataPK != undefined) {
											updateUserProfile(($filter('stringToHex')(JSON.stringify(userData))),$http).then(function(response){
												$state.go('app.'+$scope.landingMod[2].state?$scope.landingMod[2].state:$scope.landingMod[0].state, {});
											}) 
											clearInterval(interval)
										} 
									
								}, 100)	
											
							}

						}).error(function(data){

							$scope.alerts = [{
								type : 'danger',
								msg : data.error.message
							}]

							userData = uProfileData;
							checkLogin(userData)
							$translate.use("en_US");
					

					})


					}).error(function(data){

						$scope.alerts = [{
							type : 'danger',
							msg : data.error.message
						}];

					})

					}
			}
		}

	

		}).error(function (data, status, headers, config) {
			$scope.alerts = [{
					type : 'danger',
					msg : data.error.message
				}
			];

			
		});
		setTimeout(function(){
			console.log("autpcomplete")
			$('.logindata').attr('autocomplete', 'off');
		},0)

	}


	/*if($rootScope.profileUpdated)
	{
		$scope.alerts = [{
				type : 'success',
				msg : $rootScope.profileUpdated
			}]
	}

	if (GlobalService.passwordChanged) {
		
		$scope.alerts = [{
				type : 'success',
				msg : GlobalService.responseMessage
			}
		];
		GlobalService.passwordChanged = false;

	} else {
		GlobalService.responseMessage = "";

	}*/

	/*if (GlobalService.userCreated) {
		$scope.alerts = [{
				type : 'success',
				msg : 'User created successfully.'
			}
		];
		GlobalService.userCreated = false;
	}*/

	if($rootScope.logOutMsg)
	{
		GlobalAllPaymentReset(GlobalService, AllPaymentsGlobalData)
	}
	
	/*if (GlobalService.logoutMessage) {

		$scope.alerts = [{
				type : 'success',
				msg : (localStorage.logOutMsg)?localStorage.logOutMsg:'User session terminated successfully.'
			}
		];

		GlobalService.logoutMessage = false;
	}*/


	$scope.multipleEmptySpace = function (e) {

		if ($.trim($(e.currentTarget).val()).length == 0) {
			$(e.currentTarget).val('')
		}
	}

	

	if($rootScope.alerts && $rootScope.alerts.length)
	{
		$scope.alerts = angular.copy($rootScope.alerts);

			//$timeout(function(){
				$rootScope.alerts = [];
			//},4000)
	}

	$rootScope.alerts = [];
	

	setTimeout(function(){
		$scope.PassClick = function(ev) {
		
		console.log("CLICK")
		$("#password").attr("type", "text");
		if ($("#password").attr("type") == "text") {
			var passType = $("#password").attr("type", "password");
			
		}
		
	}
	},0)


	$(".passField").focus(function(){
		console.log("focussss")
		
			$("#password").attr("type", "text");
	if ($("#password").attr("type") == "text") {
			var passType = $("#password").attr("type", "password");
			console.log(passType, "text")
		}
		
	})  
	
	
	/* $(function() {
    $('#password').each(function() {
		console.log("password")
        
        $(this).attr('type', 'text');

        $(this).focus(function() {
			console.log("passwordFocus")
            $(this).attr('type', 'password');
            
        });

        $(this).blur(function() {
           
            $(this).attr('type', 'text');
        }); 
    });
});
	 */
	
	
	
	
	
	
	


});