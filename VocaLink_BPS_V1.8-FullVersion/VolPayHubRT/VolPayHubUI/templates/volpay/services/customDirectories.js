VolpayApp.directive('preLoaderCircle', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, ele) {
                    ele.addClass('hide');

                $rootScope.$on('$stateChangeStart', function() {
                    ele.removeClass('hide');
                });

                $rootScope.$on('$stateChangeSuccess', function() {
                    ele.addClass('hide');
                });
            }
        };
    }
])
VolpayApp.filter('offset', function() {
  return function(input, start) {
    start = parseInt(start, 10);
    return input.slice(start);
  };
});
/*hexa to ascii*/
VolpayApp.filter('hex_to_ascii', function() {
  return function(value) {
    var hex  = value.toString();
    var str = '';
    for (var n = 0; n < hex.length; n += 2) {
      str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
  };
});
/*highlight*/
 VolpayApp.filter('highlight', function($sce) {
    return function(text, phrase) {
      if (phrase) text = text.replace(new RegExp('('+phrase+')', 'i'),
        '<span class="highlighted">$1</span>')

      return $sce.trustAsHtml(text)
    }
  })
 /*Check Internet status*/
VolpayApp.run(function($window, $rootScope) {
      $rootScope.online = navigator.onLine;
      $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
          $rootScope.online = false;
        });
      }, false);
      $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
          $rootScope.online = true;
        });
      }, false);
});
VolpayApp.directive("ssnInput",function(){
    return {
        require:'ngModel',
        link: function(scop, elem, attr, ngModel){
            $(elem).mask("9999 9999 9999 9999");
            var temp;
            var regxa = /^(\d{4} ?\d{4} ?\d{4} ?\d{4})$/;
            $(elem).focusin(function(){
                $(elem).val(temp);
            });
            $(elem).on('blur',function(){
                temp = $(elem).val();
                if(regxa.test($(elem).val())){
                    $(elem).val("XXXX XXXX XXXX" + temp.slice(14));
                }
            });
        }
    }
});
VolpayApp.directive("httploader", function ($rootScope) {
    return function ($scope, element, attrs) {
        $scope.$on("showHttploader", function (event, args) {			
            return element.show();
        });
        return $scope.$on("hideHttploader", function (event, args) {
            return element.hide();
        });
    };
})
VolpayApp.factory('loadmeOnscroll', function ($q, $rootScope, $log) {

    var numofRestCalls = 0;
    return {
        request: function (config) {
            numofRestCalls++;
			onrequestTime = new Date().getTime();
            $rootScope.$broadcast("showHttploader");
            return config || $q.when(config)
        },
        requestError: function (rejection) {
            if (!(--numofRestCalls)) {
                $rootScope.$broadcast("hideHttploader");
            }
            return $q.reject(rejection);
		},
        response: function (response) {
			onresponseTime = new Date().getTime();
			timeTaken = (onresponseTime - onrequestTime)*50;
            if ((--numofRestCalls) === 0) {
				setTimeout(function(){					
                	$rootScope.$broadcast("hideHttploader");
				},timeTaken)
            }
			/*if(response.data.responseMessage){
				console.log(response)
				$rootScope.$broadcast("successMsg",{ msg: response.data.responseMessage });
			}*/
            return response || $q.when(response);
        },
        responseError: function (response) {
            if (!(--numofRestCalls)) {
                $rootScope.$broadcast("hideHttploader");
            }
			//$rootScope.$broadcast("errorMsg",{ msg: rejection.data.error });
			//console.log(response)
            return $q.reject(response);
        }
    };
})