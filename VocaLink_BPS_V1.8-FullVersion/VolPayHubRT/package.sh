#folderdatetime=$(date +%Y_%m_%d_%H_%M_%S)
folderdatetime="target"
mkdir -p  ./"$folderdatetime"

# Packaging BMS UI into war
cd VolPayHubBankSimulator
jar -cvf ../"$folderdatetime"/VolPayHubBankSimulator.war .
cd ..

# Packaging VolPay Channel into war
cd VolPayChannel
jar -cvf ../"$folderdatetime"/VolPayChannel.war .
cd ..

# Packaging VolPay Core into war
cd VolPayHub
jar -cvf ../"$folderdatetime"/VolPayHub.war .
cd ..


# Packaging NotiGate into war
cd NotiGate
jar -cvf ../"$folderdatetime"/NotiGate.war .
cd ..

# Packaging VolPayRest into war
cd VolPayRest
jar -cvf ../"$folderdatetime"/VolPayRest.war .
cd ..

# Packaging SiteminderSimulator into war
cd SiteminderSimulator
jar -cvf ../"$folderdatetime"/SiteminderSimulator.war .
cd ..

# Packaging VolPay UI into war
cd VolPayHubUI
jar -cvf ../"$folderdatetime"/VolPayHubUI.war .
cd ..

