CREATE OR REPLACE VIEW PAYMENT_VIEW ("PAYMENT_INSTRUCTION_ID", "SETTLEMENT_WINDOW_ID", "SENDER_PARTICIPANT_ID", "SENDER_TRNS_CNTR_ID", "RECEIVER_PARTICIPANT_ID", "RECEIVER_TRNS_CNTR_ID", "PAYMENT_INSTRUCTION_TYPE", "SEQUENCE_NUMBER", "CURRENCY", "AMOUNT", "COMMISSION_AMOUNT") AS 
  (
	SELECT 
		PCD.PAYMENTID AS PAYMENT_INSTRUCTION_ID,
		PCD.SETTLEMENTCYCLEINDICATOR AS SETTLEMENT_WINDOW_ID,
		SUBSTR(PCD.DA_CLEARINGSCHEMEID, 1, 4) AS SENDER_PARTICIPANT_ID,
		SUBSTR(PCD.DA_CLEARINGSCHEMEID, 5, 4) AS SENDER_TRNS_CNTR_ID,
		SUBSTR(PCD.CA_CLEARINGSCHEMEID, 1, 4) AS RECEIVER_PARTICIPANT_ID,
		SUBSTR(PCD.CA_CLEARINGSCHEMEID, 5, 4) AS RECEIVER_TRNS_CNTR_ID,
		(SELECT CASE WHEN LPCD.PARTYSERVICEASSOCIATIONCODE IN ('00010001_BULKCREDITTRANSFER_NACHAMReturnsInput_Tr') THEN 'RETURN'
			WHEN LPCD.PARTYSERVICEASSOCIATIONCODE IN ('00010001_BULKCREDITTRANSFER_NACHAMCHANNELInput') THEN 'PAYMENT'
			ELSE ''
			END CASE
			FROM PAYMENTCONTROLDATA LPCD
			WHERE LPCD.PAYMENTID = PCD.PAYMENTID) PAYMENT_INSTRUCTION_TYPE,
		PCD.PAYMENTINDEX AS SEQUENCE_NUMBER,
		PCD.CURRENCY AS CURRENCY,
		PCD.AMOUNT AS AMOUNT,
		(TO_NUMBER(ADDITIONALINFO.ENTRYCOMMISSIONAMOUNT)/100) AS COMMISSION_AMOUNT

	FROM
		PAYMENTCONTROLDATA PCD,
		PAYMENTADDITIONALINFORMATION ADDITIONALINFO
	
	WHERE
		PCD.PAYMENTID = ADDITIONALINFO.PAYMENTID
	
	UNION ALL
	
	SELECT 
		PCD.PAYMENTID AS PAYMENT_INSTRUCTION_ID,
		PCD.SETTLEMENTCYCLEINDICATOR AS SETTLEMENT_WINDOW_ID,
		SUBSTR(PCD.DA_CLEARINGSCHEMEID, 1, 4) AS SENDER_PARTICIPANT_ID,
		SUBSTR(PCD.DA_CLEARINGSCHEMEID, 5, 4) AS SENDER_TRNS_CNTR_ID,
		SUBSTR(PCD.CA_CLEARINGSCHEMEID, 1, 4) AS RECEIVER_PARTICIPANT_ID,
		SUBSTR(PCD.CA_CLEARINGSCHEMEID, 5, 4) AS RECEIVER_TRNS_CNTR_ID,
		'PRESENTED' AS PAYMENT_INSTRUCTION_TYPE,
		PCD.PAYMENTINDEX AS SEQUENCE_NUMBER,
		PCD.CURRENCY AS CURRENCY,
		PCD.AMOUNT AS AMOUNT,
		TO_NUMBER(CHEADDITIONALINFO.ENTRYCOMMISSIONAMOUNT/100) AS COMMISSION_AMOUNT

	FROM
		PAYMENTCONTROLDATA PCD,
		CHEQUESADDITIONALINFORMATION CHEADDITIONALINFO
	
	WHERE
		PCD.PAYMENTID = CHEADDITIONALINFO.PAYMENTID
	
	UNION ALL
	
	SELECT 
		PCD.PAYMENTID AS PAYMENT_INSTRUCTION_ID,
		PCD.SETTLEMENTCYCLEINDICATOR AS SETTLEMENT_WINDOW_ID,
		SUBSTR(PCD.DA_CLEARINGSCHEMEID, 1, 4) AS SENDER_PARTICIPANT_ID,
		SUBSTR(PCD.DA_CLEARINGSCHEMEID, 5, 4) AS SENDER_TRNS_CNTR_ID,
		SUBSTR(PCD.CA_CLEARINGSCHEMEID, 1, 4) AS RECEIVER_PARTICIPANT_ID,
		SUBSTR(PCD.CA_CLEARINGSCHEMEID, 5, 4) AS RECEIVER_TRNS_CNTR_ID,
		'REJECTED' AS PAYMENT_INSTRUCTION_TYPE,
		PCD.PAYMENTINDEX AS SEQUENCE_NUMBER,
		PCD.CURRENCY AS CURRENCY,
		PCD.AMOUNT AS AMOUNT,
		TO_NUMBER(AMENDSADDITIONALINFO.ENTRYCOMMISSIONAMOUNT/100) AS COMMISSION_AMOUNT

	FROM
		PAYMENTCONTROLDATA PCD,
		AMENDSADDITIONALINFORMATION AMENDSADDITIONALINFO
	
	WHERE
		PCD.PAYMENTID = AMENDSADDITIONALINFO.PAYMENTID
);

COMMIT;