INSERT INTO STATUSACTION
(STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, "ACTION", PROCESSNAME)
VALUES('ae2e4720ae2e47200953e8dd0953e8kl', 'PAYMENT', 'COMPLETED', 'SUSPENDED', TIMESTAMP '2017-01-01 00:00:00.000000', NULL, 'GANotifyOutput', 'VOCALINK-BPS');

INSERT INTO ACTIONDEFINITION
(ACTIONNAME, DESCRIPTION, ACTIONDEFINITION_PK, WORKFLOWCODE, ALIASNAME, CLIENTALIASNAME, DUALCONTROLREQUIRED, MANDATENOTE, GROUPACTIONALLOWED, RESTURL, RESTMETHOD, SUCCESSURL, FAILUREURL, FLOWNAME, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, FUNCTIONNAME, ADDITIONALCONFIG)
VALUES('GANotifyOutput', 'This is used to notify GoAnywhere of the Output file', '0AE535KB90AB4AD1B6BE7AFDLLC06293', 'PAYMENT', 'GANotifyOutput', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GoAnywhereOutputHandler', 'ACTIVE', TIMESTAMP '2017-01-01 00:00:00.000000', NULL, NULL, NULL);


COMMIT;