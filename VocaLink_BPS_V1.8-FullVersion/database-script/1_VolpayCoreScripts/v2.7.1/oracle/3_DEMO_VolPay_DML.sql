DELETE FROM ACTIONDEFINITION WHERE ACTIONNAME = 'InstructionAck' AND WORKFLOWCODE = 'INSTRUCTION';
INSERT INTO ACTIONDEFINITION(ACTIONNAME, DESCRIPTION, ACTIONDEFINITION_PK, WORKFLOWCODE, ALIASNAME, CLIENTALIASNAME, DUALCONTROLREQUIRED, MANDATENOTE, GROUPACTIONALLOWED, RESTURL, RESTMETHOD, SUCCESSURL, FAILUREURL, FLOWNAME, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, FUNCTIONNAME) VALUES('InstructionAck', 'This Action is used to generate an Acknowledgement for instruction', SYS_GUID(), 'INSTRUCTION', 'Instruction ACK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'DEMO_InstructionAckFlow', 'ACTIVE', TO_DATE('2017-01-01', 'YYYY-MM-DD'), NULL, NULL);

DELETE FROM STATUSACTION WHERE "ACTION" = 'InstructionAck' AND PROCESSSTATUS = 'DEBULKED' AND WORKFLOWCODE = 'INSTRUCTION' AND PROCESSNAME = 'VOLPAY-DEFAULT';
INSERT INTO STATUSACTION("ACTION", STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, PROCESSNAME) VALUES('InstructionAck', SYS_GUID(), 'INSTRUCTION', 'DEBULKED', 'ACTIVE', TO_DATE('2017-01-01', 'YYYY-MM-DD'), NULL, 'VOLPAY-DEFAULT');
DELETE FROM TASKSMASTER WHERE TASKCODE = 'INSTRDEMOIF';
INSERT INTO TASKSMASTER (TASKSMASTER_PK, TASKCODE, TASKNAME, TECHNICALFLOWNAME, TASKDESCRIPTION, WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER), 'INSTRDEMOIF', 'INSTRDEMOIF', 'InstrDemoFlow', NULL, 'INSTRUCTION');

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'VOLPAY-DEFAULT' AND FUNCTIONCODE = 'INSTRDEMOIF' AND WORKFLOWCODE = 'INSTRUCTION'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW), 'VOLPAY-DEFAULT', 'INSTRDEMOIF', 15, 'INSTRUCTION', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);



DELETE FROM BUSINESSFUNCTIONMASTER WHERE FUNCTIONCODE='INSTRDEMOIF';
INSERT INTO BUSINESSFUNCTIONMASTER(BUSINESSFUNCTIONMASTER_PK, FUNCTIONCODE, FUNCTIONDESCRIPTION, WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER), 'INSTRDEMOIF', NULL, 'INSTRUCTION');

DELETE FROM BUSINESSFUNCTIONWORKFLOW WHERE FUNCTIONCODE='INSTRDEMOIF' AND TASKCODE = 'INSTRDEMOIF' AND ACTIVITYINDEX = 5;
INSERT INTO BUSINESSFUNCTIONWORKFLOW(BUSINESSFUNCTIONWORKFLOW_PK, FUNCTIONCODE, TASKCODE, ACTIVITYINDEX, TASKCONFIG, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW), 'INSTRDEMOIF', 'INSTRDEMOIF', 5, NULL, 'ACTIVE', To_Date('2017-10-12','YYYY-MM-DD'), NULL);

DELETE FROM INTERFACE WHERE INTERFACEID='DEMO_INSTRINTERFACE1';
INSERT INTO INTERFACE(INTERFACEID,INVOCATIONPOINTIDENTIFIER,DOMAINOUT,DOMAININ,ISBULK,ISSYNCHRONOUS,DOMAINOUTTRANSFORMFLOW,DOMAININTRANSFORMFLOW,TIMEOUT,ISIDEMPOTENT,RETRYCOUNT,STATE,MSGREFERENCECODE,GRPREFERENCECODE,DEFAULTDOMAININFLOW,POSTCORRCALLBACKFLOW,OVERRIDEFLOW) VALUES('DEMO_INSTRINTERFACE1','DEMOINSTRINTERFACE','DemoRequest','DemoResponse',0,0,'DemoDomainOut','DemoDomainIn',20000,1,3,'ACTIVE',NULL,NULL,NULL,NULL,NULL);

UPDATE BUSINESSPROCESSWORKFLOW SET PROCESSCODE='INCOMINGROF-DEFAULT' WHERE PROCESSCODE='ROF-DEFAULT';

COMMIT;
