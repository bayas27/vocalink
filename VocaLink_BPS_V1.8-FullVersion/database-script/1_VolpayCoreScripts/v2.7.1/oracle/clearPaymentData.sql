TRUNCATE TABLE INSTRUCTIONAUDIT;
TRUNCATE TABLE PAYMENTAUDIT;
TRUNCATE TABLE SYSTEMAUDIT;
TRUNCATE TABLE PAYMENTCONTROLDATA;
TRUNCATE TABLE FLOWSCHEDULERDYNAMICDATA;
TRUNCATE TABLE BANKINTERACTIONDATA;
TRUNCATE TABLE PAYMENTFIELDDATA;
TRUNCATE TABLE CURRENCYWISESUM;
DELETE FROM INSTRUCTIONRAWDATA;
TRUNCATE TABLE PAYMENTOUTPUTDATA;
TRUNCATE TABLE PAYMENTINPUTOUTPUTCORRELATION;
TRUNCATE TABLE PAYMENTREPAIRDETAILS;
TRUNCATE TABLE ACKDETAILS;
TRUNCATE TABLE USERSESSIONAUDIT;
TRUNCATE TABLE USERPROFILELOG;
TRUNCATE TABLE REQFROMCORE;
TRUNCATE TABLE RESPFROMINTERFACE;
TRUNCATE TABLE REQTOINTERFACE;
TRUNCATE TABLE REQTRACKER;
TRUNCATE TABLE RespToCore;
TRUNCATE TABLE INSTRUCTIONDATA;
TRUNCATE TABLE APPROVALDETAILS;
TRUNCATE TABLE INTERFACEBULKING;
TRUNCATE TABLE NOTIFEVENT;
TRUNCATE TABLE CONFIRMATIONCONTROLDATA;
TRUNCATE TABLE ACCOUNTPOSTINGS;
TRUNCATE TABLE INTERFACETIMEOUTMANAGER;
TRUNCATE TABLE TRANSPORTLOGAUDIT;
--TRUNCATE TABLE ERRORINFORMATION;
TRUNCATE TABLE INSTRERRORINFO;
TRUNCATE TABLE TRANSERRORINFO;
TRUNCATE TABLE ATTACHEDMESSAGE;
TRUNCATE TABLE HASHREGISTRY;
TRUNCATE TABLE LINKEDMESSAGES;
TRUNCATE TABLE INSTRHASHREGISTRY;
TRUNCATE TABLE INSTRFLDHASHREGISTRY;
TRUNCATE TABLE INSTRBANKINTERACTIONDATA;
TRUNCATE TABLE INSTRREQFROMCORE;
TRUNCATE TABLE  INSTRREQTOINTERFACE;
TRUNCATE TABLE INSTRREQTRACKER;
TRUNCATE TABLE INSTRRESTOCORE;
TRUNCATE TABLE INSTRRESFROMINTERFACE;
TRUNCATE TABLE DISTRIBUTIONLOG;
COMMIT;
