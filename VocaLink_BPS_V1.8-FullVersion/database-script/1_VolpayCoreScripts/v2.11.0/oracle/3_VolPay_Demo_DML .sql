DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'VOLPAY-DEFAULT' AND FUNCTIONCODE = 'CreditTransferMatchWithOutCT' AND WORKFLOWCODE = 'PAYMENT'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'VOLPAY-DEFAULT', 'CreditTransferMatchWithOutCT', 6, 'PAYMENT', 'SUSPENDED', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE='VOLPAY-DEFAULT' AND FUNCTIONCODE='FraudCheck' AND WORKFLOWCODE='PAYMENT';
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW)VALUES(SYS_GUID(), 'VOLPAY-DEFAULT', 'FraudCheck', 42, 'PAYMENT', 'ACTIVE', TO_DATE('2018-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE='VOLPAY-DEFAULT' AND FUNCTIONCODE='PrepareAccountingEntries_ForFraud_TASK' AND WORKFLOWCODE='PAYMENT';
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW)VALUES(SYS_GUID(), 'VOLPAY-DEFAULT', 'PrepareAccountingEntries_ForFraud_TASK', 44, 'PAYMENT', 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE='VOLPAY-DEFAULT' AND FUNCTIONCODE='SpecialLiquidityControl' AND WORKFLOWCODE='PAYMENT';
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW)VALUES(SYS_GUID(), 'VOLPAY-DEFAULT', 'SpecialLiquidityControl', 46, 'PAYMENT', 'ACTIVE', TO_DATE('2018-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE='CAMT-DEFAULT' AND FUNCTIONCODE='HandleACWPPayment' AND WORKFLOWCODE='ATTCHMSG_TXN';
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW)VALUES(SYS_GUID(), 'CAMT-DEFAULT', 'HandleACWPPayment', 4, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'OUTGOINGRFI-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-ORFI' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'OUTGOINGRFI-DEFAULT', 'ValidateMessageFunctionEligibility-ORFI', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'OUTGOINGRRFI-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-ORRFI' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'OUTGOINGRRFI-DEFAULT', 'ValidateMessageFunctionEligibility-ORRFI', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'INCOMINGRRFP-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-ORRFP' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'INCOMINGRRFP-DEFAULT', 'ValidateMessageFunctionEligibility-ORRFP', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'OUTGOINGRROF-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-ORROF' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'OUTGOINGRROF-DEFAULT', 'ValidateMessageFunctionEligibility-ORROF', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'REMT-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-REMT' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'REMT-DEFAULT', 'ValidateMessageFunctionEligibility-REMT', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'CAMT-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-CAMT' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'CAMT-DEFAULT', 'ValidateMessageFunctionEligibility-CAMT', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, NULL);

DELETE FROM BUSINESSPROCESSWORKFLOW WHERE PROCESSCODE = 'OUTGOINGROF-DEFAULT' AND FUNCTIONCODE = 'ValidateMessageFunctionEligibility-OROF' AND WORKFLOWCODE = 'ATTCHMSG_TXN'; 
INSERT INTO BUSINESSPROCESSWORKFLOW(BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES(SYS_GUID(), 'OUTGOINGROF-DEFAULT', 'ValidateMessageFunctionEligibility-OROF', 1, 'ATTCHMSG_TXN', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL,NULL);

COMMIT; 
