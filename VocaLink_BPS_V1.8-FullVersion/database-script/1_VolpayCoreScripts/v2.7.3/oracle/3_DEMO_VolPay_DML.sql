UPDATE VOLPAYCONFIGURATION SET VALUE = 'ISOPacs8,MT103,MT101,ISOPACS008,013,MT104' WHERE NAME = 'PSAINPUTFORMAT';

UPDATE PARTYSERVICEASSOCIATION SET ADDITIONALCONFIG = NULL WHERE PARTYSERVICEASSOCIATIONCODE = '9000001235_DEMO_ISOPain013_RFP';
UPDATE PARTYSERVICEASSOCIATION SET ADDITIONALCONFIG = '{"AcceptAll":true,"RejectAll":false}' WHERE PARTYSERVICEASSOCIATIONCODE = '7765569996_DEMOISOPain013_RFP';

UPDATE BUSINESSPROCESSWORKFLOW SET ACTIVITYINDEX = 45 WHERE PROCESSCODE = 'INCOMINGRRFP-DEFAULT' AND FUNCTIONCODE = 'HandleResponseforRFP' AND WORKFLOWCODE = 'ATTCHMSG_TXN';

COMMIT;