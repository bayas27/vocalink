ALTER TABLE  MENURESOURCEASSOCIATION ADD TABLENAME VARCHAR2(100);

ALTER TABLE INSTRUCTIONRAWDATA ADD UNSTRUCTUREDINFO VARCHAR2(4000);

CREATE INDEX BID_IND1 ON BANKINTERACTIONDATA (PaymentID);

CREATE INDEX ERI_INDX1 ON ERRORINFORMATION (PaymentID);

ALTER TABLE MPITEMPLATE ADD Description VARCHAR2(3000) NULL;

ALTER TABLE BANKINTERACTIONDATA ADD ORIGINALSTATUS VARCHAR2(100) NULL;

CREATE INDEX BID_INDX2 ON BANKINTERACTIONDATA (CORRELATIONID);

COMMIT;
