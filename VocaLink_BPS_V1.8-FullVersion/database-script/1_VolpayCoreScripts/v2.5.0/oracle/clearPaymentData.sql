TRUNCATE TABLE REFERENCEDATAUSERACTIONS;
TRUNCATE TABLE AUDITTABLE;
TRUNCATE TABLE PAYMENTCONTROLDATA;
TRUNCATE TABLE FLOWSCHEDULERDYNAMICDATA;
TRUNCATE TABLE BANKINTERACTIONDATA;
TRUNCATE TABLE PAYMENTFIELDDATA;
TRUNCATE TABLE CURRENCYWISESUM;
DELETE FROM INSTRUCTIONRAWDATA;
TRUNCATE TABLE PAYMENTOUTPUTDATA;
TRUNCATE TABLE PAYMENTINPUTOUTPUTCORRELATION;
TRUNCATE TABLE PAYMENTREPAIRDETAILS;
TRUNCATE TABLE ACKDETAILS;
TRUNCATE TABLE USERSESSIONAUDIT;
TRUNCATE TABLE USERPROFILELOG;
TRUNCATE TABLE REQFROMCORE;
TRUNCATE TABLE RESPFROMINTERFACE;
TRUNCATE TABLE REQTOINTERFACE;
TRUNCATE TABLE REQTRACKER;
TRUNCATE TABLE RespToCore;
TRUNCATE TABLE INSTRUCTIONDATA;
TRUNCATE TABLE APPROVALDETAILS;
TRUNCATE TABLE INTERFACEBULKING;
TRUNCATE TABLE NOTIFEVENT;
TRUNCATE TABLE CONFIRMATIONCONTROLDATA;
TRUNCATE TABLE ACCOUNTPOSTINGS;
TRUNCATE TABLE INTERFACETIMEOUTMANAGER;
TRUNCATE TABLE TRANSPORTLOGAUDIT;
TRUNCATE TABLE ERRORINFORMATION;

COMMIT;