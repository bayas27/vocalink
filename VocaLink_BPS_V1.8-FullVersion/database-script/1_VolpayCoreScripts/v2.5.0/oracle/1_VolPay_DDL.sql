-- interface
ALTER TABLE INTERFACEBULKING DROP PRIMARY KEY;
ALTER TABLE INTERFACEBULKING ADD PRIMARY KEY(INSTRUCTIONID,PAYMENTID,INVOCATIONPOINTIDENTIFIER);
DROP TABLE INTERFACETOCORERESPONSEMANAGER;
drop table RespFromInterfaceComponents;
UPDATE CURRENCY SET COUNTRYCODE = 'ALL' WHERE COUNTRYCODE is NULL;
--ALTER TABLE CURRENCY MODIFY countrycode DEFAULT 'ALL';
ALTER TABLE CURRENCY modify countrycode  NOT NULL;
ALTER TABLE RESPFROMINTERFACE ADD STATUS VARCHAR2(30) not null;

ALTER TABLE Interface ADD (MsgReferenceCode VARCHAR2(36),GrpReferenceCode VARCHAR2(36));
ALTER TABLE Interface DROP COLUMN DefaultDomainIn;
ALTER TABLE Interface ADD DefaultDomainInFlow VARCHAR2(100);

alter table ReqToInterface rename column Success to Status;
Alter table ReqToInterface modify Status VARCHAR2(100);
ALTER TABLE ReqToInterface DROP column DESCRIPTION;


CREATE TABLE INTERFACETIMEOUTMANAGER (REQUESTID VARCHAR2(100) NOT NULL, EXTERNALCALLOUTTIME TIMESTAMP NOT NULL, EXPIRATIONTIME INTEGER NOT NULL, STATUS VARCHAR2(100) NOT NULL, PRIMARY KEY (REQUESTID));



drop table BankInteractionData;
create table BankInteractionData (BankInteractionData_PK VARCHAR2(36) not null, PaymentID VARCHAR2(36) not null, GrpReferenceId VARCHAR2(36) not null, CorrelationId VARCHAR2(36) not null, Relationship VARCHAR2(36) not null, InvocationPoint VARCHAR2(100) not null, IsSynchronous NUMBER(1,0), BIDTimeStamp TIMESTAMP not null, AttemptNumber INTEGER , Status VARCHAR2(100) not null, Object BLOB, primary key (BankInteractionData_PK));


drop table ReqTracker;
create table ReqTracker (ReqFromCorePK VARCHAR2(36) not null, ReqToInterfaceMsgId VARCHAR2(36) not null, Status VARCHAR2(100) not null, BulkedMsgId VARCHAR2(36), PaymentID VARCHAR2(36) not null, primary key (ReqFromCorePK, ReqToInterfaceMsgId));


ALTER TABLE ReqFromCore DROP column DESCRIPTION;
ALTER TABLE ReqFromCore DROP COLUMN PRIORITY;



DROP TABLE  RESPTOCORE;
create table RespToCore (RespFromInterfacePK VARCHAR2(36) not null, ReqFromCorePK VARCHAR2(36) not null, PaymentID VARCHAR2(36) not null,  InvocationPoint VARCHAR2(100) not null, RespToCoreObj BLOB not null, Status VARCHAR2(100) not null, primary key (RespFromInterfacePK, ReqFromCorePK));




ALTER TABLE COUNTRYCORRESPONDENT ADD UNIQUE(COUNTRYCODE);
ALTER TABLE USERPROFILEDATA ADD UNIQUE(USERID);

create table EntityConfiguration (EntityName VARCHAR2(100) not null, IsCacheReload VARCHAR2(100) not null, IsCamelReload VARCHAR2(100) not null, IsReferenceData VARCHAR2(100) not null, primary key (EntityName));
ALTER TABLE OFFICE ADD DEFAULTDEPARTMENT VARCHAR2(36);
--ALTER TABLE OFFICE ADD FOREIGN KEY (DEFAULTDEPARTMENT) REFERENCES DEPARTMENT (DEPARTMENTCODE);
ALTER TABLE CONFIRMATIONCONTROLDATA MODIFY PAYMENTID VARCHAR2(36);
ALTER TABLE CONFIRMATIONCONTROLDATA MODIFY CCDID VARCHAR2(36);

ALTER TABLE ISSUEDEFINITION RENAME TO INCIDENCEDEFINITION;
ALTER TABLE ISSUECONSTRAINTS RENAME TO INCIDENCECONSTRAINTS;


TRUNCATE TABLE INCIDENCEDEFINITION;
TRUNCATE TABLE STATUSACTION;
TRUNCATE TABLE STATUSDEFINITION;
TRUNCATE TABLE INCIDENCECONSTRAINTS;


ALTER TABLE STATUSACTION ADD STATUSACTION_PK VARCHAR2(36) NOT NULL;
ALTER TABLE STATUSACTION ADD STATUSTYPE VARCHAR2(100) NOT NULL;
ALTER TABLE STATUSACTION DROP PRIMARY KEY;
ALTER TABLE STATUSACTION DROP COLUMN STATUS;
ALTER TABLE STATUSACTION ADD PROCESSSTATUS VARCHAR2(80) NOT NULL;
ALTER TABLE STATUSACTION ADD DUALCONTROLREQUIRED NUMBER(1,0);
ALTER TABLE STATUSACTION ADD MANDATENOTE NUMBER(1,0);
ALTER TABLE STATUSACTION ADD GROUPACTIONALLOWED NUMBER(1,0);
ALTER TABLE STATUSACTION ADD RESTURLORFLOWNAME VARCHAR2(100) NOT NULL;
ALTER TABLE STATUSACTION ADD RESTMETHOD VARCHAR2(100);
ALTER TABLE STATUSACTION ADD SUCCESSURL VARCHAR2(100);
ALTER TABLE STATUSACTION ADD FAILUREURL VARCHAR2(100);
ALTER TABLE STATUSACTION ADD REMARKS VARCHAR2(100);
ALTER TABLE STATUSACTION ADD PRIMARY KEY (STATUSACTION_PK);
ALTER TABLE STATUSACTION ADD STATUS VARCHAR2(100) NOT NULL;
ALTER TABLE STATUSACTION ADD EFFECTIVEFROMDATE DATE DEFAULT sysdate NOT NULL;
ALTER TABLE STATUSACTION ADD EFFECTIVETILLDATE DATE;


ALTER TABLE STATUSDEFINITION DROP COLUMN STATUS;
ALTER TABLE STATUSDEFINITION ADD PROCESSSTATUS VARCHAR2(80) NOT NULL;
ALTER TABLE STATUSDEFINITION ADD STATUSTYPE VARCHAR2(100) NOT NULL;
ALTER TABLE STATUSDEFINITION ADD PRIMARY KEY (PROCESSSTATUS, STATUSTYPE);

ALTER TABLE INCIDENCECONSTRAINTS RENAME COLUMN ISSUECODE TO INCIDENCECODE;
ALTER TABLE INCIDENCECONSTRAINTS ADD STATUSTYPE VARCHAR2(100) NOT NULL;
ALTER TABLE INCIDENCECONSTRAINTS DROP COLUMN STATUSES;
ALTER TABLE INCIDENCECONSTRAINTS ADD PROCESSSTATUS VARCHAR2(300) NOT NULL;
ALTER TABLE INCIDENCECONSTRAINTS DROP PRIMARY KEY;
ALTER TABLE INCIDENCECONSTRAINTS ADD PRIMARY KEY (INCIDENCECODE);

ALTER TABLE INCIDENCEDEFINITION RENAME COLUMN ISSUECODE TO INCIDENCECODE;
ALTER TABLE INCIDENCEDEFINITION ADD INCIDENCETYPE VARCHAR2(100) NOT NULL;
ALTER TABLE INCIDENCEDEFINITION RENAME COLUMN ISSUERESOLUTION TO INCIDENCERESOLUTION;
ALTER TABLE INCIDENCEDEFINITION RENAME COLUMN ISSUEORIGIN  TO INCIDENCEORIGIN;
ALTER TABLE INCIDENCEDEFINITION ADD STATUSTYPE VARCHAR2(100);
ALTER TABLE INCIDENCEDEFINITION RENAME COLUMN STATUS  TO PROCESSSTATUS;
ALTER TABLE INCIDENCEDEFINITION DROP PRIMARY KEY;
ALTER TABLE INCIDENCEDEFINITION ADD PRIMARY KEY (PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, INCIDENCECODE);


ALTER TABLE RESTRESOURCES ADD APPROVECOUNT INTEGER;

ALTER TABLE BUSINESSPROCESSWORKFLOW ADD BUSINESSRULEHANDLERFLOW VARCHAR2(100);

ALTER TABLE APPROVALLOGAUDIT ADD Notes VARCHAR2(300);

ALTER TABLE ROLEATTRIBUTEASSOCIATION ADD PERMISSION VARCHAR2(20) NOT NULL;

create table TransportLogAudit (TransportLogAudit_PK VARCHAR(40) not null, Processor VARCHAR(100) not null, Event VARCHAR(100) not null, TimeStamp TIMESTAMP not null, RouteID VARCHAR(100), ExchangeID VARCHAR(150), MessageHeaders BLOB, MessageBody BLOB, StackTrace BLOB, Status VARCHAR(100) not null, primary key (TransportLogAudit_PK));


ALTER TABLE CONFIRMATIONCONTROLDATA ADD CONFPROCESSINGSTATUS VARCHAR(30);
ALTER TABLE CONFIRMATIONCONTROLDATA ADD RESPINSTRID VARCHAR(100);
ALTER TABLE CONFIRMATIONCONTROLDATA RENAME COLUMN CONFIRMATIONSTATUSCODE  TO CONFSTATUSCODE;
ALTER TABLE CONFIRMATIONCONTROLDATA RENAME COLUMN CONFIRMATIONMESSAGEMATCHSTATUS  TO CONFMESSAGEMATCHSTATUS;
ALTER TABLE CONFIRMATIONCONTROLDATA RENAME COLUMN TIMESTAMP TO CCDTimeStamp;

CREATE TABLE CONFIMATIONFIELDDATA (RESPINSTRUCTIONID VARCHAR2(36) NOT NULL, FILEDISTRIBUTIONID VARCHAR2(36) NOT NULL, NUMBEROFTXN INTEGER, GROUPSTATUS VARCHAR2(30), GROUPORIGINALSTATUS VARCHAR2(30), XML_FIELD BLOB, PRIMARY KEY (RESPINSTRUCTIONID));

ALTER TABLE ACTIONDEFINITION DROP PRIMARY KEY;
TRUNCATE TABLE ACTIONDEFINITION;
ALTER TABLE ACTIONDEFINITION ADD ACTIONDEFINITION_PK VARCHAR2(36) NOT NULL;
ALTER TABLE ACTIONDEFINITION RENAME COLUMN ACTION TO ACTIONNAME;
ALTER TABLE ACTIONDEFINITION ADD PROCESSTYPE VARCHAR2(100) NOT NULL;
ALTER TABLE ACTIONDEFINITION ADD ALIASNAME VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD CLIENTALIASNAME VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD DUALCONTROLREQUIRED NUMBER(1,0);
ALTER TABLE ACTIONDEFINITION ADD MANDATENOTE NUMBER(1,0);
ALTER TABLE ACTIONDEFINITION ADD GROUPACTIONALLOWED NUMBER(1,0);
ALTER TABLE ACTIONDEFINITION ADD RESTURL VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD RESTMETHOD VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD SUCCESSURL VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD FAILUREURL VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD FLOWNAME VARCHAR2(100);
ALTER TABLE ACTIONDEFINITION ADD STATUS VARCHAR2(100) NOT NULL;
ALTER TABLE ACTIONDEFINITION ADD EFFECTIVEFROMDATE DATE DEFAULT sysdate NOT NULL;
ALTER TABLE ACTIONDEFINITION ADD EFFECTIVETILLDATE DATE;
ALTER TABLE ACTIONDEFINITION ADD PRIMARY KEY (ACTIONDEFINITION_PK);

TRUNCATE TABLE INCIDENCECONSTRAINTS;
ALTER TABLE INCIDENCECONSTRAINTS ADD STATUS VARCHAR2(100) NOT NULL;
ALTER TABLE INCIDENCECONSTRAINTS ADD EFFECTIVEFROMDATE DATE DEFAULT sysdate NOT NULL;
ALTER TABLE INCIDENCECONSTRAINTS ADD EFFECTIVETILLDATE DATE;

ALTER TABLE INCIDENCEDEFINITION DROP PRIMARY KEY;
TRUNCATE TABLE INCIDENCEDEFINITION;
ALTER TABLE INCIDENCEDEFINITION ADD INCIDENCEDEFINITION_PK VARCHAR2(36) NOT NULL;
ALTER TABLE INCIDENCEDEFINITION DROP COLUMN PROCESSNAME;
ALTER TABLE INCIDENCEDEFINITION DROP COLUMN PARTYSERVICEASSOCIATIONCODE;
ALTER TABLE INCIDENCEDEFINITION ADD PROCESSNAME VARCHAR2(36);
ALTER TABLE INCIDENCEDEFINITION ADD PARTYSERVICEASSOCIATIONCODE VARCHAR2(36);
ALTER TABLE INCIDENCEDEFINITION ADD STATUS VARCHAR2(100) NOT NULL;
ALTER TABLE INCIDENCEDEFINITION ADD EFFECTIVEFROMDATE DATE DEFAULT sysdate NOT NULL;
ALTER TABLE INCIDENCEDEFINITION ADD EFFECTIVETILLDATE DATE;
ALTER TABLE INCIDENCEDEFINITION ADD PRIMARY KEY (INCIDENCEDEFINITION_PK);
ALTER TABLE INCIDENCEDEFINITION ADD CONSTRAINT PSACODE_FK1 FOREIGN KEY (PARTYSERVICEASSOCIATIONCODE) REFERENCES PARTYSERVICEASSOCIATION(PARTYSERVICEASSOCIATIONCODE);

TRUNCATE TABLE STATUSACTION;
ALTER TABLE STATUSACTION DROP COLUMN ACTIONS;
ALTER TABLE STATUSACTION ADD ACTION VARCHAR(100) NOT NULL;
ALTER TABLE STATUSACTION RENAME COLUMN STATUSTYPE TO PROCESSTYPE;
ALTER TABLE STATUSACTION DROP COLUMN PROCESSNAME;
ALTER TABLE STATUSACTION ADD PROCESSNAME VARCHAR2(100);
ALTER TABLE STATUSACTION DROP COLUMN DUALCONTROLREQUIRED;
ALTER TABLE STATUSACTION DROP COLUMN MANDATENOTE;
ALTER TABLE STATUSACTION DROP COLUMN GROUPACTIONALLOWED;
ALTER TABLE STATUSACTION DROP COLUMN RESTURLORFLOWNAME;
ALTER TABLE STATUSACTION DROP COLUMN RESTMETHOD;
ALTER TABLE STATUSACTION DROP COLUMN SUCCESSURL;
ALTER TABLE STATUSACTION DROP COLUMN FAILUREURL;
ALTER TABLE STATUSACTION DROP COLUMN REMARKS;

TRUNCATE TABLE STATUSDEFINITION;
ALTER TABLE STATUSDEFINITION ADD STATUS VARCHAR2(100) NOT NULL;
ALTER TABLE STATUSDEFINITION ADD EFFECTIVEFROMDATE DATE DEFAULT sysdate NOT NULL;
ALTER TABLE STATUSDEFINITION ADD EFFECTIVETILLDATE DATE;



ALTER TABLE ACTIONDEFINITION RENAME COLUMN PROCESSTYPE TO WORKFLOWCODE;
ALTER TABLE STATUSACTION RENAME COLUMN PROCESSTYPE TO WORKFLOWCODE;
ALTER TABLE INCIDENCEDEFINITION RENAME COLUMN STATUSTYPE TO WORKFLOWCODE;
ALTER TABLE INCIDENCECONSTRAINTS RENAME COLUMN STATUSTYPE TO WORKFLOWCODE;
ALTER TABLE STATUSDEFINITION RENAME COLUMN STATUSTYPE TO WORKFLOWCODE;

ALTER TABLE ACTIONDEFINITION ADD UNIQUE(ACTIONNAME,WORKFLOWCODE);

ALTER TABLE ReportParams DROP PRIMARY KEY;
ALTER TABLE ReportParams MODIFY ReportID VARCHAR(100);
ALTER TABLE ReportParams ADD PRIMARY KEY (ReportID,ParamKey);

ALTER TABLE ReportTemplates DROP PRIMARY KEY;
ALTER TABLE ReportTemplates MODIFY TemplateID VARCHAR(100);
ALTER TABLE ReportTemplates ADD PRIMARY KEY (TemplateID);

DROP TABLE ReportLog; 
CREATE TABLE ReportLog(FolioID VARCHAR2(50),ReportID VARCHAR2(50),ReportName VARCHAR2(50),UserName VARCHAR2(50),GeneratedDate TIMESTAMP,Status VARCHAR2(100), Detail VARCHAR2(3000), 
ReturnStack1 CLOB,                        
PRIMARY KEY(FolioID)); 

ALTER TABLE BUSINESSFUNCTIONWORKFLOW MODIFY TASKCONFIG VARCHAR2(4000);
CREATE TABLE LogConfig (LogConfig_PK VARCHAR2(50),ApplicationName VARCHAR2(50),Category VARCHAR2(100),Loglevel VARCHAR2(10),Appenders VARCHAR2(100),PRIMARY KEY (LogConfig_PK));

CREATE INDEX RESPTOCORE_REQFROMCOREPK_IDX ON RESPTOCORE (REQFROMCOREPK);
CREATE INDEX PAYMENTCONTROLDATA_HASH_IDX ON PAYMENTCONTROLDATA (HASH,RECEIVEDDATE);


CREATE TABLE ERRORINFORMATION (ERRORINFORMATION_PK VARCHAR2(36) NOT NULL, 
	INSTRUCTIONID VARCHAR2(50) NOT NULL, 
	PAYMENTID VARCHAR2(60), 
	TASKNAME VARCHAR2(100), 
	INCIDENCECODE VARCHAR2(100), 
	ISSUETIMESTAMP TIMESTAMP NOT NULL, 
	FIELDNAMEVALUES CLOB, 
	DESCRIPTION CLOB, 
	ORIGINALDESCRIPTION CLOB, 
	PROCESSSTATUS VARCHAR2(100) NOT NULL, 
	SEVERITY VARCHAR2(36), 
	PRIMARY KEY (ERRORINFORMATION_PK)
);

ALTER TABLE MEMBERSHIP DROP UNIQUE(SCHEMECODE);
ALTER TABLE MEMBERSHIP ADD UNIQUE(SCHEMECODE,SCHEMEPARTICIPANTIDENTIFER);

COMMIT;
