INSERT INTO RESOURCEMASTER(RESOURCEID, RESOURCENAME, RESOURCEGROUPNAME) VALUES('068', 'Level Of Approval', 'Utility');
INSERT INTO RESOURCEMASTER(RESOURCEID, RESOURCENAME, RESOURCEGROUPNAME) VALUES('069', 'Bulk Profile', 'Distribution Data');
INSERT INTO RESOURCEMASTER(RESOURCEID, RESOURCENAME, RESOURCEGROUPNAME) VALUES('070', 'Bulk Profile Override', 'Distribution Data');

INSERT INTO MENURESOURCEASSOCIATION(RESOURCEID, MENUID, MENUNAME) VALUES('069', '071', 'Bulk Profile');
INSERT INTO MENURESOURCEASSOCIATION(RESOURCEID, MENUID, MENUNAME) VALUES('070', '072', 'Bulk Profile Overrride');

INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '068', 'C');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '068', 'R');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '068', 'U');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '068', 'D');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '068', 'A1');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '069', 'A1');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '069', 'C');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '069', 'D');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '069', 'R');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '069', 'U');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '070', 'A1');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '070', 'C');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '070', 'D');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '070', 'R');
INSERT INTO ROLERESOURCEPERMISSION(ROLEID, RESOURCEID, PERMISSION) VALUES('Super Admin', '070', 'U');

--Deleting Permissions 'Report Params' and 'Report Templates'
DELETE FROM ROLERESOURCEPERMISSION WHERE RESOURCEID IN ('049','050');

--Deleting Requires Approval Permissions
DELETE FROM ROLERESOURCEPERMISSION WHERE PERMISSION = 'N';

--Deleting Menu Names 'Report Params' and 'Report Templates'
DELETE FROM MENURESOURCEASSOCIATION WHERE RESOURCEID IN ('049','050');

--Updating the Resource Names 'Reports Generate & Log', 'File List' to 'Report' & 'All Instructions'
UPDATE RESOURCEMASTER SET RESOURCENAME = 'Report' WHERE RESOURCEID = '047';
UPDATE RESOURCEMASTER SET RESOURCENAME = 'All Instructions' WHERE RESOURCEID = '002';

--Deleting Resources 'Report Params' and 'Report Templates'
DELETE FROM RESOURCEMASTER WHERE RESOURCEID IN ('049','050');

--Entries for new REST URLs
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/rfppermissionsconfig/metainfo','007','R','API for getting RFP Permissions Config Meta Information');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/payments/accountposting/readall','003','R','API for Getting AccountPosting Details  using PaymentId');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/payments/errorinformation/readall','003','R','API For Getting ErrorInformation Details using PymentId');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/businessrules/read','017','R','API for getting BusinessRule');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/levelofapprovals','068','C','API to Persist Level Of Approval');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'PUT','VolPayHub','/v2/levelofapprovals','068','U','API to Update Level Of Approval');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/levelofapprovals/readall','068','R','API to Read All Level Of Approval');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/levelofapprovals/read','068','R','API to Read Specific Level Of Approval');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/levelofapprovals/delete','068','D','API to Delete Level Of Approval');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/payments/linkedmessages/readall','003','R','API For Getting Linked Messages using PaymentId');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/rfppermissionsconfig/metainfo','007','R','API for getting Region Meta Information');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,PATH,RESOURCEID,OPERATION,DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/roles/operationlist','042','R','API to get list of Operations');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofiles/readall','069','R','API for getting  BulkProfile');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofiles','069','C','API for creating BulkProfile');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'PUT','VolPayHub','/v2/bulkprofiles','069','U','API for updating BulkProfile');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofiles/delete','069','D','API for deleting BulkProfile');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofiles/read','069','R','API for getting BulkProfile');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofiles/count','069','R','API for GettingBulkProfile entity Count');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofiles/audit/readall','069','R','API for getting BulkProfile log audit');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofiles/{status}/count','069','R','API for Getting BulkProfile entity CountAPI for Getting BulkProfile entity Count');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofiles/code','069','R','API for getting  BulkProfile code');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofileoverrides/readall','070','R','API for getting BulkProfileOverride');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofileoverrides','070','C','API for creating BulkProfileOverride');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'PUT','VolPayHub','/v2/bulkprofileoverrides','070','U','API for updating BulkProfileOverride');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofileoverrides/delete','070','D','API for deleting BulkProfileOverride');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofileoverrides/read','070','R','API for getting BulkProfileOverride');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofileoverrides/count','070','R','API for Getting BulkProfileOverride entity Count');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'POST','VolPayHub','/v2/bulkprofileoverrides/audit/readall','070','R','API for getting BulkProfileOverride log audit');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofileoverrides/{status}/count','070','R','API for Getting BulkProfileOverride entity Count');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofiles/metainfo','007','R','API for getting Bulk Profile Meta Information');
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK,METHOD,APPLICATION,"PATH",RESOURCEID,"OPERATION",DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES),'GET','VolPayHub','/v2/bulkprofileoverrides/metainfo','007','R','API for getting Bulk Profile Override Meta Information');

TRUNCATE TABLE REPORTTEMPLATE;
INSERT INTO REPORTTEMPLATE
(TEMPLATEID, TEMPLATENAME, DESCRIPTION, FLOWNAME, REPORTQUERY, REPORTTEMPLATE)
VALUES('Payment Completion Report', 'Payment Completion Report', 'Fetches all Payment records for today with payment status as completed', 'GenericReport', 'SELECT PAYMENTID,ORIGINALPAYMENTREFERENCE,INSTRUCTIONID,PARTYSERVICEASSOCIATIONCODE,METHODOFPAYMENT,CURRENCY,AMOUNT,RECEIVEDDATE,VALUEDATE FROM PAYMENTCONTROLDATA', '<?xml version="1.0" encoding="UTF-8"?>
<generateReport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<parameters>
		<param id="Font">Arial</param>
		<param id="OddRowColor">#C0C0C0</param>
		<param id="RowFontSize">7</param>
		<param id="ReportTitle"></param>
		<param id="ReportName">Payments Completed Today</param>
		<param id="PageOrientation">LANDSCAPE</param>
		<param id="PageType">LETTER</param>
		<param id="TitleColor">#FFFFFF</param>
		<param id="TitlebackColor">#4882BC</param>
		<param id="TitleFontSize">9</param>
		<param id="LogoImage"></param>
		<param id="FotterImage"></param>
		<param id="EnableOddRow">true</param>
		<param id="BottomPadding">4</param>
		<param id="TopPadding">4</param>
		<param id="ImageWidth">80</param>
		<param id="UserName">Admin</param>
		<param id="TextAlign">LEFT</param>
		<param id="LabelFooter">Paymnets completion report</param>
		<param id="ImageHeight">40</param>
	</parameters>
	<metaData>
		<column id="PAYMENTID" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="150" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payment ID</column>
		<column id="ORIGINALPAYMENTREFERENCE" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="90" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Orig Pay Ref</column>
		<column id="INSTRUCTIONID" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Instruction ID</column>
		<column id="PARTYSERVICEASSOCIATIONCODE" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="140" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">PSA</column>
		<column id="METHODOFPAYMENT" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">MOP</column>
		<column id="CURRENCY" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="40" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Currency</column>
		<column id="AMOUNT" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="decimal" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Amount</column>
		<column id="RECEIVEDDATE" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Receive Date</column>
		<column id="VALUEDATE" textAlign="LEFT" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Value Date</column>
	</metaData>
</generateReport>');
INSERT INTO REPORTTEMPLATE
(TEMPLATEID, TEMPLATENAME, DESCRIPTION, FLOWNAME, REPORTQUERY, REPORTTEMPLATE)
VALUES('Todays All Instruction Report', 'Todays All Instruction Report ', 'Fetches all instruction records for today', 'GenericReport', 'SELECT INSTRUCTIONID,ENTRYDATE, FILESTATUS,PAYMENTSCOMPLETED,PAYMENTSPENDING,PAYMENTSREJECTED FROM INSTRUCTIONRAWDATA', '<?xml version="1.0" encoding="UTF-8"?>
<generateReport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<parameters>
		<param id="Font">Arial</param>
		<param id="OddRowColor">#C0C0C0</param>
		<param id="RowFontSize">10</param>
		<param id="ReportTitle"></param>
		<param id="ReportName">Instruction Report for Today</param>
		<param id="PageOrientation">PORTRAIT</param>
		<param id="PageType">LETTER</param>
		<param id="TitleColor">#FFFFFF</param>
		<param id="TitlebackColor">#4882BC</param>
		<param id="TitleFontSize">9</param>
		<param id="LogoImage"></param>
		<param id="FotterImage"></param>
		<param id="EnableOddRow">true</param>
		<param id="BottomPadding">4</param>
		<param id="TopPadding">4</param>
		<param id="ImageWidth">80</param>
		<param id="UserName">Admin</param>
		<param id="TextAlign">LEFT</param>
		<param id="LabelFooter">Instruction Report for Today</param>
		<param id="ImageHeight">40</param>
	</parameters>
	<metaData>
		<column id="INSTRUCTIONID" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Instruction ID</column>
		<column id="ENTRYDATE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="150" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Entry Date Time</column>
		<column id="FILESTATUS" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">File Status</column>
		<column id="PAYMENTSCOMPLETED" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payments Completed</column>
		<column id="PAYMENTSPENDING" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payments Pending</column>
		<column id="PAYMENTSREJECTED" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payments Rejected</column>
	</metaData>
	
</generateReport>');
INSERT INTO REPORTTEMPLATE
(TEMPLATEID, TEMPLATENAME, DESCRIPTION, FLOWNAME, REPORTQUERY, REPORTTEMPLATE)
VALUES('Todays All Payment Report', 'Todays All Payment Report', 'Fetches all payments for today irrespective of payment status', 'GenericReport', 'SELECT PAYMENTID,ORIGINALPAYMENTREFERENCE,INSTRUCTIONID,PARTYSERVICEASSOCIATIONCODE,METHODOFPAYMENT,CURRENCY,AMOUNT,RECEIVEDDATE,VALUEDATE,STATUS FROM PAYMENTCONTROLDATA', '<?xml version="1.0" encoding="UTF-8"?>
<generateReport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<parameters>
		<param id="Font">Arial</param>
		<param id="OddRowColor">#C0C0C0</param>
		<param id="RowFontSize">7</param>
		<param id="ReportTitle"></param>
		<param id="ReportName">All Payments Today</param>
		<param id="PageOrientation">LANDSCAPE</param>
		<param id="PageType">LETTER</param>
		<param id="TitleColor">#FFFFFF</param>
		<param id="TitlebackColor">#4882BC</param>
		<param id="TitleFontSize">9</param>
		<param id="LogoImage"></param>
		<param id="FotterImage"></param>
		<param id="EnableOddRow">true</param>
		<param id="BottomPadding">4</param>
		<param id="TopPadding">4</param>
		<param id="ImageWidth">80</param>
		<param id="UserName">Admin</param>
		<param id="TextAlign">LEFT</param>
		<param id="LabelFooter">Todays all Paymnets Report</param>
		<param id="ImageHeight">40</param>
	</parameters>
	<metaData>
		<column id="PAYMENTID" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payment ID</column>
		<column id="ORIGINALPAYMENTREFERENCE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="90" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Orig Pay Ref</column>
		<column id="INSTRUCTIONID" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Instruction ID</column>
		<column id="PARTYSERVICEASSOCIATIONCODE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="90" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">PSA</column>
		<column id="METHODOFPAYMENT" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">MOP</column>
		<column id="CURRENCY" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="40" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Currency</column>
		<column id="AMOUNT" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="decimal" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Amount</column>
		<column id="RECEIVEDDATE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Receive Date</column>
		<column id="VALUEDATE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Value Date</column>
		<column id="STATUS" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Status</column>
	</metaData>
	
</generateReport>');
INSERT INTO REPORTTEMPLATE
(TEMPLATEID, TEMPLATENAME, DESCRIPTION, FLOWNAME, REPORTQUERY, REPORTTEMPLATE)
VALUES('Week Instruction Report', 'Week Instruction Report', 'Fetches all instructions records for a week (from a the day this report is generated)', 'GenericReport', 'SELECT INSTRUCTIONID,ENTRYDATE, FILESTATUS,PAYMENTSCOMPLETED,PAYMENTSPENDING,PAYMENTSREJECTED FROM INSTRUCTIONRAWDATA', '<?xml version="1.0" encoding="UTF-8"?>
<generateReport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<parameters>
		<param id="Font">Arial</param>
		<param id="OddRowColor">#C0C0C0</param>
		<param id="RowFontSize">10</param>
		<param id="ReportTitle"></param>
		<param id="ReportName">Weekly Instruction Report</param>
		<param id="PageOrientation">PORTRAIT</param>
		<param id="PageType">LETTER</param>
		<param id="TitleColor">#FFFFFF</param>
		<param id="TitlebackColor">#4882BC</param>
		<param id="TitleFontSize">9</param>
		<param id="LogoImage"></param>
		<param id="FotterImage"></param>
		<param id="EnableOddRow">true</param>
		<param id="BottomPadding">4</param>
		<param id="TopPadding">4</param>
		<param id="ImageWidth">80</param>
		<param id="UserName">Admin</param>
		<param id="TextAlign">LEFT</param>
		<param id="LabelFooter">Weekly Instruction Report</param>
		<param id="ImageHeight">40</param>
	</parameters>
	<metaData>
		<column id="INSTRUCTIONID" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Instruction ID</column>
		<column id="ENTRYDATE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="150" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Entry Date Time</column>
		<column id="FILESTATUS" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">File Status</column>
		<column id="PAYMENTSCOMPLETED" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payments Completed</column>
		<column id="PAYMENTSPENDING" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payments Pending</column>
		<column id="PAYMENTSREJECTED" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="75" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payments Rejected</column>
	</metaData>
	
</generateReport>');
INSERT INTO REPORTTEMPLATE
(TEMPLATEID, TEMPLATENAME, DESCRIPTION, FLOWNAME, REPORTQUERY, REPORTTEMPLATE)
VALUES('Week Payment Report', 'Week Payment Report', 'Fetches all paymnets records for a week irrespective of the status ((from a the day this report is generated)', 'GenericReport', 'SELECT PAYMENTID,ORIGINALPAYMENTREFERENCE,INSTRUCTIONID,PARTYSERVICEASSOCIATIONCODE,METHODOFPAYMENT,CURRENCY,AMOUNT,RECEIVEDDATE,VALUEDATE,STATUS FROM PAYMENTCONTROLDATA', '<?xml version="1.0" encoding="UTF-8"?>
<generateReport xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<parameters>
		<param id="Font">Arial</param>
		<param id="OddRowColor">#C0C0C0</param>
		<param id="RowFontSize">7</param>
		<param id="ReportTitle"></param>
		<param id="ReportName">Weekly Payments Report</param>
		<param id="PageOrientation">LANDSCAPE</param>
		<param id="PageType">LETTER</param>
		<param id="TitleColor">#FFFFFF</param>
		<param id="TitlebackColor">#4882BC</param>
		<param id="TitleFontSize">9</param>
		<param id="LogoImage"></param>
		<param id="FotterImage"></param>
		<param id="EnableOddRow">true</param>
		<param id="BottomPadding">4</param>
		<param id="TopPadding">4</param>
		<param id="ImageWidth">80</param>
		<param id="UserName">Admin</param>
		<param id="TextAlign">LEFT</param>
		<param id="LabelFooter">Weekly Paymnets Report</param>
		<param id="ImageHeight">40</param>
	</parameters>
	<metaData>
		<column id="PAYMENTID" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Payment ID</column>
		<column id="ORIGINALPAYMENTREFERENCE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="90" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Orig Pay Ref</column>
		<column id="INSTRUCTIONID" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Instruction ID</column>
		<column id="PARTYSERVICEASSOCIATIONCODE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="90" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">PSA</column>
		<column id="METHODOFPAYMENT" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">MOP</column>
		<column id="CURRENCY" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="40" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Currency</column>
		<column id="AMOUNT" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="decimal" width="50" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Amount</column>
		<column id="RECEIVEDDATE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Receive Date</column>
		<column id="VALUEDATE" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Value Date</column>
		<column id="STATUS" textAlign="CENTER" verticalAlign="MIDDLE" pattern="" type="string" width="100" fontSize="8" bold="false" italic="false" underline="false" color="#00000" backcolor="#FFFFFF" summary="">Status</column>
	</metaData>
</generateReport>');

INSERT INTO SERVICECONFIG (MSGNAME,MSGFORMAT,MSGDESCRIPTION,ISMAPPINGAFLOW,REJECTIONLEVEL,BATCHSECNAME,PAYMENTSECNAME,ISCUSTOMERRHANDLERREQD,ISINMEMORYPARSING,PHASEDPARSEFLOWNAME,VALIDATIONLIST,PREPARSINGCALLBACKFLOWNAME,POSTPARSINGCALLBACKFLOWNAME,VALIDATIONOVERRIDEFLOWNAME,MSGTYPE,MSGGROUP) VALUES('Pain013_RFP','Pain013','Pain013input',1,'TRANSACTION','Batch_record','Payment_record',0,1,NULL,NULL,NULL,NULL,NULL,'INPUT','RFP');
UPDATE SERVICECONFIG SET PREPARSINGCALLBACKFLOWNAME = 'MsgTypeIdentificationForRFP:FormatIdentifier' WHERE MSGFORMAT = 'DemoCHANNELInput';

INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE)VALUES ('RFPRJCT001','${msg}','Error','High','User',NULL,NULL,NULL,NULL,'REFUSED','ISSUE','PAYMENT',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25','YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('DPV001','Request for payment is rejected because: ${msg}','Error','High','User',NULL,NULL,NULL,NULL,'REJECTED','ISSUE','PAYMENT',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('DPV002','Request for payment is rejected because: Either Invalid parent account number or parent account number is not present in Account table','Error','High','User',NULL,NULL,NULL,NULL,'REJECTED','ISSUE','PAYMENT',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('DPV003','Request for payment is rejected because: No PSA found for customer transaction in PSA table','Error','High','User',NULL,NULL,NULL,NULL,'REJECTED','ISSUE','PAYMENT',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('DPV004','Request for payment is rejected because: Parent Account type of Underlined customer account is not Customer','Error','High','User',NULL,NULL,NULL,NULL,'REJECTED','ISSUE','PAYMENT',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH001','Status changed to delivered','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'DELIVERED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH002','Status changed to accepted','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'ACCEPTED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH003','Status changed to rejected','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'REJECTED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH004','Status changed to processing','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'PROCESSING','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH005','Status changed to processed','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'PROCESSED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH006','Status changed to pending','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'PENDING','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH007','Status changed to partially accepted','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'PARTIALLYACCEPTED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH008','Status changed to partially completed','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'PARTIALLYCOMPLETED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH009','Status changed to completed with amendments','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'COMPLETED WITH AMENDMENTS','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE,DESCRIPTION,INCIDENCERESOLUTION,SEVERITY,INCIDENCEORIGIN,CANCLIENTSETPREFERENCE,OPSSEVERITYPREFERENCE,CLIENTSEVERITYPREFERENCE,REPAIRANDMANUALINPUT,PROCESSSTATUS,INCIDENCETYPE,WORKFLOWCODE,INCIDENCEDEFINITION_PK,PROCESSNAME,PARTYSERVICEASSOCIATIONCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES('CONFH010','Status changed to returned','Received Confirmation','High','User',NULL,NULL,NULL,NULL,'RETURNED','ISSUE','CONFIRMATION_TXN',(SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION),'null','null','ACTIVE',TO_DATE('2017-04-25', 'YYYY-MM-DD'),NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, INCIDENCECODE, INCIDENCETYPE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE)  VALUES((SELECT max(CAST(INCIDENCEDEFINITION_PK AS INTEGER)) + 1 AS INCIDENCEDEFINITION_PK FROM INCIDENCEDEFINITION), 'null', 'null', 'DPDCPDBR07', 'ISSUE', 'Business rule has made the ${custm} party role as ${partyRole} but for party ${party} account number is not found with currency ${curr}', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, 'PAYMENT', 'REPAIR', 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL);

INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWEDPARAMETER,ALLOWSTP) VALUES('RFPRJCT001','PAYMENT','REFUSED','msg',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)  VALUES('DPV001','PAYMENT','REJECTED','msg',1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)  VALUES('DPV002','PAYMENT','REJECTED','msg',1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)  VALUES('DPV003','PAYMENT','REJECTED','msg',1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)  VALUES('DPDCPDBR07', 'PAYMENT', 'REPAIR', 'custm,partyRole,party,curr', 1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)  VALUES('DPV004','PAYMENT','REJECTED','msg',1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH001','CONFIRMATION_TXN','DELIVERED',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH002','CONFIRMATION_TXN','ACCEPTED',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH003','CONFIRMATION_TXN','REJECTED',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH004','CONFIRMATION_TXN','PROCESSING',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH005','CONFIRMATION_TXN','PROCESSED',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH006','CONFIRMATION_TXN','PENDING',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH007','CONFIRMATION_TXN','PARTIALLYACCEPTED',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH008','CONFIRMATION_TXN','PARTIALLYCOMPLETED',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH009','CONFIRMATION_TXN','COMPLETED WITH AMENDMENTS',0);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE,WORKFLOWCODE,ALLOWEDPROCESSSTATUS,ALLOWSTP)  VALUES('CONFH010','CONFIRMATION_TXN','RETURNED',0);

INSERT INTO TASKSMASTER (TASKSMASTER_PK,TASKCODE,TASKNAME,TECHNICALFLOWNAME,TASKDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER),'ValidateAndDebulkProcessor-RFP','ValidateAndDebulkProcessor','ValidateAndDebulkProcessor',NULL,'INSTRUCTION');
INSERT INTO TASKSMASTER (TASKSMASTER_PK,TASKCODE,TASKNAME,TECHNICALFLOWNAME,TASKDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER),'CheckThresholds-RFP','InstructionDuplicateCheck','CheckThresholds',NULL,'INSTRUCTION');
INSERT INTO TASKSMASTER (TASKSMASTER_PK,TASKCODE,TASKNAME,TECHNICALFLOWNAME,TASKDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER),'DebtorAcctIdentifyAndValidate-RFP','DebtorAcctIdentifyAndValidate','DebtorAcctIdentifyAndValidate',NULL,'RFP_TXN');
INSERT INTO TASKSMASTER (TASKSMASTER_PK,TASKCODE,TASKNAME,TECHNICALFLOWNAME,TASKDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER),'CheckMandate-RFP','CheckMandate','CheckMandate',NULL,'RFP_TXN');
INSERT INTO TASKSMASTER (TASKSMASTER_PK,TASKCODE,TASKNAME,TECHNICALFLOWNAME,TASKDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER),'PreparePaymentForRFPReq-RFP','PreparePaymentForRFPReq','PreparePaymentForRFPReq',NULL,'RFP_TXN');
INSERT INTO TASKSMASTER (TASKSMASTER_PK,TASKCODE,TASKNAME,TECHNICALFLOWNAME,TASKDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER), 'ExecutePaymentDuplicateCheck-RFP', 'ExecutePaymentDuplicateCheck-RFP', 'PaymentDuplicateCheck', NULL, 'RFP_TXN');

INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK,PROCESSCODE,FUNCTIONCODE,ACTIVITYINDEX,WORKFLOWCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE,BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW),'RFPPain013Req','ValidateAndDebulkProcessor-RFP',10,'INSTRUCTION','ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL,NULL);
INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK,PROCESSCODE,FUNCTIONCODE,ACTIVITYINDEX,WORKFLOWCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE,BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW),'RFPPain013Req','CheckThresholds-RFP',20,'INSTRUCTION','ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL,NULL);
INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK,PROCESSCODE,FUNCTIONCODE,ACTIVITYINDEX,WORKFLOWCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE,BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW),'RFPPain013Req','DebtorAcctIdentifyAndValidate-RFP',20,'RFP_TXN','ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL,NULL);
INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK,PROCESSCODE,FUNCTIONCODE,ACTIVITYINDEX,WORKFLOWCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE,BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW),'RFPPain013Req','CheckMandate-RFP',30,'RFP_TXN','ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL,NULL);
INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK,PROCESSCODE,FUNCTIONCODE,ACTIVITYINDEX,WORKFLOWCODE,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE,BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW),'RFPPain013Req','PreparePaymentForRFPReq-RFP',40,'RFP_TXN','ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL,NULL);
INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW) VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW), 'RFPPain013Req', 'ExecutePaymentDuplicateCheck-RFP', 10, 'RFP_TXN', 'ACTIVE', TO_DATE('2016-12-30','YYYY-MM-DD'), NULL, NULL);

INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK,FUNCTIONCODE,FUNCTIONDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER),'ValidateAndDebulkProcessor-RFP',NULL,'INSTRUCTION');
INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK,FUNCTIONCODE,FUNCTIONDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER),'CheckThresholds-RFP',NULL,'INSTRUCTION');
INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK,FUNCTIONCODE,FUNCTIONDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER),'DebtorAcctIdentifyAndValidate-RFP',NULL,'RFP_TXN');
INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK,FUNCTIONCODE,FUNCTIONDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER),'CheckMandate-RFP',NULL,'RFP_TXN');
INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK,FUNCTIONCODE,FUNCTIONDESCRIPTION,WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER),'PreparePaymentForRFPReq-RFP',NULL,'RFP_TXN');
INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK, FUNCTIONCODE, FUNCTIONDESCRIPTION, WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER), 'ExecutePaymentDuplicateCheck-RFP', NULL, 'RFP_TXN');

INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK,FUNCTIONCODE,TASKCODE,ACTIVITYINDEX,TASKCONFIG,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW),'ValidateAndDebulkProcessor-RFP','ValidateAndDebulkProcessor-RFP',10,NULL,'ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL);
INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK,FUNCTIONCODE,TASKCODE,ACTIVITYINDEX,TASKCONFIG,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW),'CheckThresholds-RFP','CheckThresholds-RFP',20,NULL,'ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL);
INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK,FUNCTIONCODE,TASKCODE,ACTIVITYINDEX,TASKCONFIG,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW),'DebtorAcctIdentifyAndValidate-RFP','DebtorAcctIdentifyAndValidate-RFP',20,NULL,'ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL);
INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK,FUNCTIONCODE,TASKCODE,ACTIVITYINDEX,TASKCONFIG,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW),'CheckMandate-RFP','CheckMandate-RFP',30,NULL,'ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL);
INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK,FUNCTIONCODE,TASKCODE,ACTIVITYINDEX,TASKCONFIG,STATUS,EFFECTIVEFROMDATE,EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW),'PreparePaymentForRFPReq-RFP','PreparePaymentForRFPReq-RFP',40,NULL,'ACTIVE',TO_DATE('2016-12-30','YYYY-MM-DD'),NULL);
INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK, FUNCTIONCODE, TASKCODE, ACTIVITYINDEX, TASKCONFIG, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW), 'ExecutePaymentDuplicateCheck-RFP', 'ExecutePaymentDuplicateCheck-RFP', 10, NULL, 'ACTIVE', TO_DATE('2016-12-30','YYYY-MM-DD'), NULL);

INSERT INTO BUSINESSPROCESSMASTER (BUSINESSPROCESSMASTER_PK,PROCESSCODE,PROCESSNAME,PROCESSDESCRIPTION) VALUES((SELECT max(CAST(BUSINESSPROCESSMASTER_PK AS INTEGER)) + 1 AS BUSINESSPROCESSMASTER_PK FROM BUSINESSPROCESSMASTER),'RFPPain013Req','Requestforpayment',NULL);

INSERT INTO WORKFLOWMASTER (WORKFLOWMASTER_PK,WORKFLOWCODE,WORKFLOWNAME,WORKFLOWDESCRIPTION) VALUES((SELECT max(CAST(WORKFLOWMASTER_PK AS INTEGER)) + 1 AS WORKFLOWMASTER_PK FROM WORKFLOWMASTER),'RFP_INSTR','RFP INSTRUCTION PROCESSING',NULL);
INSERT INTO WORKFLOWMASTER (WORKFLOWMASTER_PK,WORKFLOWCODE,WORKFLOWNAME,WORKFLOWDESCRIPTION) VALUES((SELECT max(CAST(WORKFLOWMASTER_PK AS INTEGER)) + 1 AS WORKFLOWMASTER_PK FROM WORKFLOWMASTER),'RFP_TXN','RFP Transaction',NULL);

INSERT INTO INTERFACE (INTERFACEID,INVOCATIONPOINTIDENTIFIER,DOMAINOUT,DOMAININ,ISBULK,ISSYNCHRONOUS,DOMAINOUTTRANSFORMFLOW,DOMAININTRANSFORMFLOW,TIMEOUT,ISIDEMPOTENT,RETRYCOUNT,STATE,MSGREFERENCECODE,GRPREFERENCECODE,DEFAULTDOMAININFLOW)VALUES('RFP_INTERFACE','RFP','RFPRequest','RFPResponse',0,0,'RFPRequestTransformFlow','RFPResponseTransformFlow',20000,1,3,'ACTIVE',NULL,NULL,NULL);

--Updating Incidence Codes to look more function specific
UPDATE INCIDENCEMASTER SET INCIDENCECODE = 'PMNTDUP101' WHERE INCIDENCECODE = 'DUP101';
UPDATE INCIDENCEMASTER SET INCIDENCECODE = 'INSTRDUP101' WHERE INCIDENCECODE = 'INSTRUCTIONDUP101';
UPDATE INCIDENCEMASTER SET INCIDENCECODE = 'INSTRDUP102' WHERE INCIDENCECODE = 'INSTRUCTIONDUP102';

UPDATE INCIDENCEDEFINITION SET INCIDENCECODE = 'PMNTDUP101' WHERE INCIDENCECODE = 'DUP101';
UPDATE INCIDENCEDEFINITION SET INCIDENCECODE = 'INSTRDUP101' WHERE INCIDENCECODE = 'INSTRUCTIONDUP101';
UPDATE INCIDENCEDEFINITION SET INCIDENCECODE = 'INSTRDUP102' WHERE INCIDENCECODE = 'INSTRUCTIONDUP102';

DELETE FROM TASKSMASTER WHERE TASKCODE = 'ResponseHandling';
DELETE FROM BUSINESSPROCESSWORKFLOW WHERE FUNCTIONCODE = 'ResponseHandling';
DELETE FROM BUSINESSFUNCTIONMASTER WHERE FUNCTIONCODE = 'ResponseHandling';
DELETE FROM BUSINESSFUNCTIONWORKFLOW WHERE FUNCTIONCODE = 'ResponseHandling';

--Deleting Approval Count for deprecated Resources
DELETE FROM RESOURCEAPPROVALPERMISSION WHERE RESOURCEID IN ('041','049','050','052');

--Approval Count Entries for new Resources
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '067', 'C', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '067', 'U', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '067', 'D', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '069', 'C', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '069', 'U', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '069', 'D', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '070', 'C', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '070', 'U', 1, NULL);
INSERT INTO RESOURCEAPPROVALPERMISSION(RESOURCEAPPROVALPERMISSION_PK, RESOURCEID, OPERATION, APPROVALCOUNT, CONDITIONID) VALUES((SELECT max(CAST(RESOURCEAPPROVALPERMISSION_PK AS INTEGER)) + 1 AS RESOURCEAPPROVALPERMISSION_PK FROM RESOURCEAPPROVALPERMISSION), '070', 'D', 1, NULL);

--Updating Role Type for Role Super Admin
UPDATE "ROLE" SET ROLETYPE = 'System Admin' WHERE ROLEID = 'Super Admin';

INSERT INTO SEQUENCEGENERATOR VALUES (1);

--RFP

UPDATE SERVICECONFIG SET PREPARSINGCALLBACKFLOWNAME='MsgTypeIdentificationForRFP:FormatIdentifier' WHERE MSGNAME='SWIFT';

INSERT INTO STATUSACTION ("ACTION", STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, PROCESSNAME) VALUES('SendNAKRTPPAIN014', (SELECT max(CAST(STATUSACTION_PK AS INTEGER)) + 1 AS STATUSACTION_PK FROM STATUSACTION), 'PAYMENT', 'REFUSED', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL, 'RFPPain013Req');
INSERT INTO STATUSACTION ("ACTION", STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, PROCESSNAME) VALUES('SendRTPPACS002', (SELECT max(CAST(STATUSACTION_PK AS INTEGER)) + 1 AS STATUSACTION_PK FROM STATUSACTION), 'PAYMENT', 'COMPLETED', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL, 'RFPPain013Req');
INSERT INTO STATUSACTION ("ACTION", STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, PROCESSNAME) VALUES('SendNAK', (SELECT max(CAST(STATUSACTION_PK AS INTEGER)) + 1 AS STATUSACTION_PK FROM STATUSACTION), 'PAYMENT', 'REPAIR', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL, 'RFPPain013Req');
INSERT INTO STATUSACTION ("ACTION", STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, PROCESSNAME) VALUES('SendRTPPACS002', (SELECT max(CAST(STATUSACTION_PK AS INTEGER)) + 1 AS STATUSACTION_PK FROM STATUSACTION), 'PAYMENT', 'REJECTED', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL, 'RFPPain013Req');
INSERT INTO STATUSACTION ("ACTION", STATUSACTION_PK, WORKFLOWCODE, PROCESSSTATUS, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, PROCESSNAME) VALUES('SendRTPPACS002', (SELECT max(CAST(STATUSACTION_PK AS INTEGER)) + 1 AS STATUSACTION_PK FROM STATUSACTION), 'PAYMENT', 'DUPLICATE', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL, 'RFPPain013Req');


INSERT INTO ACTIONDEFINITION (ACTIONNAME, DESCRIPTION, ACTIONDEFINITION_PK, WORKFLOWCODE, ALIASNAME, CLIENTALIASNAME, DUALCONTROLREQUIRED, MANDATENOTE, GROUPACTIONALLOWED, RESTURL, RESTMETHOD, SUCCESSURL, FAILUREURL, FLOWNAME, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('SendNAKRTPPAIN014', 'Send Acknowledgement', (SELECT max(CAST(ACTIONDEFINITION_PK AS INTEGER)) + 1 AS ACTIONDEFINITION_PK FROM ACTIONDEFINITION), 'PAYMENT', 'Send NAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PrepareStatusReportFlow', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL);
INSERT INTO ACTIONDEFINITION (ACTIONNAME, DESCRIPTION, ACTIONDEFINITION_PK, WORKFLOWCODE, ALIASNAME, CLIENTALIASNAME, DUALCONTROLREQUIRED, MANDATENOTE, GROUPACTIONALLOWED, RESTURL, RESTMETHOD, SUCCESSURL, FAILUREURL, FLOWNAME, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('SendRTPPACS002', 'Send Acknowledgement', (SELECT max(CAST(ACTIONDEFINITION_PK AS INTEGER)) + 1 AS ACTIONDEFINITION_PK FROM ACTIONDEFINITION), 'PAYMENT', 'Send NAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NAKPaymentStatusFlow', 'ACTIVE', TO_DATE('2017-05-29','YYYY-MM-DD'), NULL);

INSERT INTO TASKSMASTER (TASKSMASTER_PK, TASKCODE, TASKNAME, TECHNICALFLOWNAME, TASKDESCRIPTION, WORKFLOWCODE) VALUES((SELECT max(CAST(TASKSMASTER_PK AS INTEGER)) + 1 AS TASKSMASTER_PK FROM TASKSMASTER), 'GenerateStatusReport-RFP', 'GenerateStatusReport-RFP', 'GenerateStatusReport', NULL, 'RFP_TXN');
INSERT INTO BUSINESSFUNCTIONMASTER (BUSINESSFUNCTIONMASTER_PK, FUNCTIONCODE, FUNCTIONDESCRIPTION, WORKFLOWCODE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONMASTER_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONMASTER_PK FROM BUSINESSFUNCTIONMASTER), 'GenerateStatusReport-RFP', NULL, 'RFP_TXN');
INSERT INTO BUSINESSFUNCTIONWORKFLOW (BUSINESSFUNCTIONWORKFLOW_PK, FUNCTIONCODE, TASKCODE, ACTIVITYINDEX, TASKCONFIG, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES((SELECT max(CAST(BUSINESSFUNCTIONWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSFUNCTIONWORKFLOW_PK FROM BUSINESSFUNCTIONWORKFLOW), 'GenerateStatusReport-RFP', 'GenerateStatusReport-RFP', 50, NULL, 'ACTIVE', TO_DATE('2016-12-30','YYYY-MM-DD'), NULL);
INSERT INTO BUSINESSPROCESSWORKFLOW (BUSINESSPROCESSWORKFLOW_PK, PROCESSCODE, FUNCTIONCODE, ACTIVITYINDEX, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, BUSINESSRULEHANDLERFLOW)VALUES((SELECT max(CAST(BUSINESSPROCESSWORKFLOW_PK AS INTEGER)) + 1 AS BUSINESSPROCESSWORKFLOW_PK FROM BUSINESSPROCESSWORKFLOW), 'RFPPain013Req', 'GenerateStatusReport-RFP', 50, 'RFP_TXN', 'ACTIVE', TO_DATE('2016-12-30','YYYY-MM-DD'), NULL, NULL);

UPDATE "ROLE" SET ROLETYPE = 'System Admin' WHERE ROLEID = 'Super Admin';

TRUNCATE TABLE LOGCONFIG;
/*Entries for VolPayChannel Log*/
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'org.springframework', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'org.apache.activemq', 'INFO', 'fileAppendarCamel');

INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'volante.runtime', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'com.volante.component', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'org.apache.camel', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'com.volantetech.volante.services', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayChannel', 'com.volantetech.volante.services.camel.routebuilders', 'INFO', 'fileAppendarCamel');

/*Entries for VolPaHub Log*/
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'org.springframework', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'org.apache.activemq', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'volante.runtime', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'com.volante.component', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'org.apache.camel', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'com.volantetech.volante.services', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'com.volantetech.volante.services.camel.routebuilders', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'org.quartz', 'INFO', 'fileAppendarCamel');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'com.tplus.transform.runtime.rest', 'INFO', 'fileAppendar');
INSERT INTO LOGCONFIG (LOGCONFIG_PK, APPLICATIONNAME, CATEGORY, LOGLEVEL, APPENDERS) VALUES((SELECT NVL2(max(CAST(LOGCONFIG_PK AS INTEGER)),max((CAST(LOGCONFIG_PK AS INTEGER))),0) + 1 AS LOGCONFIG_PK FROM LOGCONFIG), 'VolPayHub', 'com.tplus.transform.util', 'INFO', 'fileAppendar');

--Deleting Deprecated fields from Attribute Master
DELETE FROM ATTRIBUTEMASTER WHERE ATTRIBUTEID IN ('014026', '014027', '014033', '014034', '014028', '014029', '014035');

--Entries to configure Attributes for new Entities
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('002','002019','PartyCode','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('002','002020','ServiceCode','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('002','002021','DepartmentCode','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('002','002022','OfficeCode','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('002','002023','BranchCode','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('014','014041','HolidayBulkProfile','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('014','014042','WeekdayBulkProfile','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('014','014043','WeekendBulkProfile','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069002','BulkProfileCode','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069003','MaxPaymentsPerBulkMessage','Integer');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069004','BulkFrequency','Integer');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069005','BulkingStartTime','DateTime');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069006','BulkingEndTime','DateTime');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069007','BulkTriggerCount','Integer');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069008','FixedBulkTimes','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069009','BulkAtMOPCutOffTime','Boolean');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069010','Status','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069011','EffectiveFromDate','DateOnly');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('069','069012','EffectiveTillDate','DateOnly');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070002','MOP','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070003','OverrideType','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070004','OverrideValue','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070005','BulkProfile','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070006','Status','String');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070007','EffectiveFromDate','DateOnly');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('070','070008','EffectiveTillDate','DateOnly');

DELETE FROM ROUTEREGISTRY;
COMMIT;