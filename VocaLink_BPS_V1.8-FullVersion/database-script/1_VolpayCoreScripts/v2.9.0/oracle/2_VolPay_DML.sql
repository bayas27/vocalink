DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/payments/interactions/readall' AND "METHOD" = 'POST' ;
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'POST', 'VolPayHub', '/v2/payments/interactions/readall', '003', 'R', 'This flow will returns the data based on the give query');

UPDATE RESTRESOURCES SET RESOURCEID = '007' WHERE "PATH" IN ('/v2/reports/metainfo','/v2/psa/additionalconfig/metainfo','/v2/actiondefinitions/metainfo','/v2/incidencedefinitions/metainfo','/v2/statusactions/metainfo','/v2/statusdefinitions/metainfo','/v2/logconfig/metainfo','/v2/bankrouting/metainfo','/v2/bankdirectoryplus/metainfo');

DELETE  FROM ATTRIBUTEMASTER WHERE ATTRIBUTEID IN ('001009','001011','001012','068006','068007','068008','066006','066007');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('001', '001011', 'EffectiveFromDate', 'DateOnly');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('001', '001012', 'EffectiveTillDate', 'DateOnly');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('068', '068006', 'Status', 'String');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('068', '068007', 'EffectiveFromDate', 'DateOnly');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('068', '068008', 'EffectiveTillDate', 'DateOnly');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('066', '066006', 'Description', 'String');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('066', '066007', 'ExpiryTime', 'DateTime');

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE = 'PSAERR002';
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('PSAERR002', 'PAYMENT', 'REJECTED', 'msg', 1);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE = 'PSAERR002';
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('PSAERR002', '${msg}', 'Error', 'High', 'User', NULL, NULL, NULL, NULL, 'REJECTED', 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM STATUSDEFINITION WHERE PROCESSSTATUS='CANCELLED' AND workflowcode='PAYMENT';
INSERT INTO STATUSDEFINITION (ALIASNAME, CLIENTALIASNAME, DESCRIPTION, USERCONTROL, PROCESSSTATUS, WORKFLOWCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, COLOURA, COLOURB, OPACITY) VALUES('CANCELLED', NULL, 'Status Changed to Cancelled', NULL, 'CANCELLED', 'PAYMENT', 'ACTIVE',TO_DATE('2017-01-01','YYYY-MM-DD'), NULL, '#8a6d3b', NULL, 100); 
DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='PMNTCNCL01';
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('PMNTCNCL01', 'PAYMENT', 'CANCELLED', NULL, 0); 
DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='PMNTCNCL01' AND processname IS NULL;
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('PMNTCNCL01', 'Status changed to Cancelled', 'payment Status change', 'High', 'User', NULL, NULL, NULL, NULL, 'CANCELLED', 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-01-01','YYYY-MM-DD'), NULL);

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE IN('ACK001','ACK002','ACK003','ACK004');
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ACK001', 'PAYMENT', 'REPAIR', 'partyCode,serviceCode', 1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ACK002', 'PAYMENT', 'REPAIR', 'partyCode,serviceCode', 1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ACK003', 'PAYMENT', 'REPAIR', 'partyCode,serviceCode', 1);
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ACK004', 'PAYMENT', 'REPAIR', NULL, 1);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE IN('ACK001','ACK002','ACK003','ACK004');
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ACK001', 'Unable to Generate ACK: No PSA Found for the Party ${partyCode} and Service ${serviceCode}', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, NULL, 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ACK002', 'Unable to Generate ACK: No PSA with ACK Transport Found for the Party ${partyCode} and Service ${serviceCode} with response required Set', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, NULL, 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ACK003', 'Unable to Generate ACK: Multiple PSA with Ack Transports Found  for the Party ${partyCode} and Service ${serviceCode} with is response required set and is default psa set', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, NULL, 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ACK004', 'Unable to Generate ACK: Either PartyCode or ServiceCode Not Found', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, NULL, 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM MIGRATIONSTEPS WHERE EXECUTABLE='RoleAttributeMigrationUtility'; 
INSERT INTO MIGRATIONSTEPS(INSTALLATIONORDER, VERSION, STEPID, DESCRIPTION, "TYPE", EXECUTABLE, INSTALLEDON, EXECUTIONTIME, STATUS) VALUES(1, '2.9.0', SYS_GUID() , 'Migration of RoleAttributes', 'FLOW', 'RoleAttributeMigrationUtility', NULL, NULL, NULL);

DELETE FROM MIGRATIONSTEPS WHERE EXECUTABLE='UserAttributeMigrationUtility'; 
INSERT INTO MIGRATIONSTEPS(INSTALLATIONORDER, VERSION, STEPID, DESCRIPTION, "TYPE", EXECUTABLE, INSTALLEDON, EXECUTIONTIME, STATUS) VALUES(2, '2.9.0', SYS_GUID() , 'Migration of UserAttributes', 'FLOW', 'UserAttributeMigrationUtility', NULL, NULL, NULL);

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/servicecode/{partyCode}' AND "METHOD" = 'GET' ; 
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'GET', 'VolPayHub', '/v2/servicecode/{partyCode}', '026', 'R', 'API for getting servicecodes from psa'); 
 
DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/inputformat/{partyCode}/{serviceCode}' AND "METHOD" = 'GET' ; 
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'GET', 'VolPayHub', '/v2/inputformat/{partyCode}/{serviceCode}', '009', 'R', 'API for getting inputformat from psa'); 
 
DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/getpsa/{partyCode}/{serviceCode}/{InputFormat}' AND "METHOD" = 'GET' ;
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'GET', 'VolPayHub', '/v2/getpsa/{partyCode}/{serviceCode}/{InputFormat}', '023', 'R', 'API for getting Party Service Association'); 

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/partyserviceassociations/initiatetransaction/querypsa' AND "METHOD" = 'POST' ;
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'POST', 'VolPayHub', '/v2/partyserviceassociations/initiatetransaction/querypsa', '023', 'R', 'API for getting all PSA by Service, Party and Message Type'); 
 
DELETE  FROM ATTRIBUTEMASTER WHERE ATTRIBUTEID IN ('023033','023034','023035','023036','023037');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('023', '023033', 'UI', 'Boolean');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('023', '023034', 'API', 'Boolean');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('023', '023035', 'Transport', 'Boolean');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('023', '023036', 'ResponseRequired', 'Boolean');
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('023', '023037', 'IsDefaultPSA', 'Boolean');

DELETE FROM MSGFIELDINFO WHERE TABLENAME = 'PartyServiceAssociation' AND IMSGNAME = 'PartyServiceAssociation';

INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PartyServiceAssociation_PK','PartyServiceAssociation PK','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PartyServiceAssociationCode','PartyServiceAssociation Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','Association','Association','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PartyCode','Party Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ServiceCode','Service Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ProcessCode','Process Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ProductsSupported','Products Supported','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','BranchCode','Branch Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','DeriveBranchCode','Derive Branch Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','EnforseSettlementInstruction','Enforse Settlement Instruction','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','FileDuplicatecheck','File Duplicate check','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','FDCNumberOfdays','FDC Number Of days','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','FDCParameters','FDC Parameters','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PaymentDuplicatecheck','Payment Duplicate check','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PDCNumberOfdays','PDC Number Of days','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PDCParameters','PDC Parameters','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','RejectionErrThreshold','Rejection Err Threshold','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ReferralErrThreshold','Referral Err Threshold','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PermittedAccountNos','Permitted AccountNos','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PreferredAccount','Preferred Account','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','InputFormat','Input Format','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ResponseStages','Response Stages','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ResponseType','Response Type','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ResponseFormat','Response Format','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PaymentSourceCode','Payment Source Code','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','VDFChooseFastestMOP','VDF Choose Fastest MOP','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','PaymentParallelProcessing','Payment Parallel Processing','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','AdditionalConfig','Additional Config','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','Status','Status','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','EffectiveFromDate','Effective FromDate','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','EffectiveTillDate','Effective TillDate','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','UI','UI','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','API','API','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','Transport','Transport','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ResponseRequired','Response Required','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','ResponseExpected','Response Expected','');
INSERT INTO MSGFIELDINFO (TABLENAME,IMSGNAME,FIELD,FIELDDISPLAYNAME,DESCRIPTION) VALUES ('PartyServiceAssociation','PartyServiceAssociation','IsDefaultPSA','IsDefaultPSA','');   

DELETE FROM MSGFUNCCONFIGMASTER WHERE MSGFUNCCODE='RFP';
INSERT INTO MSGFUNCCONFIGMASTER(MSGFUNCCODE, MSGFUNCNAME, ISONLYCHILD) VALUES('RFP', 'Request for Payment', 0);
DELETE FROM MSGFUNCCONFIGMASTER WHERE MSGFUNCCODE='RRFP';
INSERT INTO MSGFUNCCONFIGMASTER(MSGFUNCCODE, MSGFUNCNAME, ISONLYCHILD) VALUES('RRFP', 'Response to Request for Payment', 1);
DELETE FROM MSGFUNCCONFIGMASTER WHERE MSGFUNCCODE='ROF';
INSERT INTO MSGFUNCCONFIGMASTER(MSGFUNCCODE, MSGFUNCNAME, ISONLYCHILD) VALUES('ROF', 'Return of Funds', 1);
DELETE FROM MSGFUNCCONFIGMASTER WHERE MSGFUNCCODE='RROF';
INSERT INTO MSGFUNCCONFIGMASTER(MSGFUNCCODE, MSGFUNCNAME, ISONLYCHILD) VALUES('RROF', 'Response to Return of Funds', 1);
DELETE FROM MSGFUNCCONFIGMASTER WHERE MSGFUNCCODE='RFI';
INSERT INTO MSGFUNCCONFIGMASTER(MSGFUNCCODE, MSGFUNCNAME, ISONLYCHILD) VALUES('RFI', 'Request for Information', 0);
DELETE FROM MSGFUNCCONFIGMASTER WHERE MSGFUNCCODE='RRFI';
INSERT INTO MSGFUNCCONFIGMASTER(MSGFUNCCODE, MSGFUNCNAME, ISONLYCHILD) VALUES('RRFI', 'Response to Request for Information', 1);

DELETE FROM VOLPAYCONFIGURATION WHERE NAME='TestingConfig';
INSERT INTO VOLPAYCONFIGURATION(NAME, VALUE, "TYPE") VALUES('TestingConfig', 'false', NULL);

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='ATTCHMSG001';
INSERT INTO INCIDENCEMASTER(INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)VALUES('ATTCHMSG001', 'ATTCHMSG_TXN', 'REJECTED', 'msg', 0);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ATTCHMSG001' AND PROCESSNAME IS NULL; 
INSERT INTO INCIDENCEDEFINITION(INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE)
VALUES('ATTCHMSG001', '${msg}', 'Error', 'High', 'User', NULL, NULL, NULL, NULL, 'REJECTED', 'ISSUE', 'ATTCHMSG_TXN', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='ATTCHMSG002';
INSERT INTO INCIDENCEMASTER(INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)VALUES('ATTCHMSG002', 'ATTCHMSG_TXN', 'REJECTED', 'msg', 0);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ATTCHMSG002' AND PROCESSNAME IS NULL;
INSERT INTO INCIDENCEDEFINITION(INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE)
VALUES('ATTCHMSG002', '${msg}', 'Error', 'High', 'User', NULL, NULL, NULL, NULL, 'REJECTED', 'ISSUE', 'ATTCHMSG_TXN', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/userrole/self/readall' AND "METHOD" = 'GET' ; 
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'GET', 'VolPayHub', '/v2/userrole/self/readall', '011', 'R', 'API for getting the userroleassocaition readall for user'); 

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE IN('DPD006','DPD004');
DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE IN('DPD006','DPD004') AND PROCESSNAME IS NULL; 


DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='ACK005';
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ACK005', 'PAYMENT', 'REPAIR', NULL, 1);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ACK005';
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ACK005', 'Unable to Generate ACK: No PSA Found for the Party ${partyCode} and Service ${serviceCode} with response required Set', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, NULL, 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

UPDATE MIGRATIONMANAGER SET MIGRATIONSTATUS ='PENDING';

DELETE FROM VOLPAYCONFIGURATION WHERE NAME='TestingConfig';
INSERT INTO VOLPAYCONFIGURATION(NAME, VALUE, "TYPE") VALUES('AttchMsgRaceCondn', 'false', NULL);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ATTCHMSG001' AND PROCESSNAME IS NULL; 
INSERT INTO INCIDENCEDEFINITION(INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ATTCHMSG001', '${msg} is REJECTED as already another ${msg} for the payment is being processed', 'Error', 'High', 'User', NULL, NULL, NULL, NULL, 'REJECTED', 'ISSUE', 'ATTCHMSG_TXN', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ATTCHMSG002' AND PROCESSNAME IS NULL;
INSERT INTO INCIDENCEDEFINITION(INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ATTCHMSG002', 'Exception occurred while concurrency check: ${msg}', 'Error', 'High', 'User', NULL, NULL, NULL, NULL, 'REJECTED', 'ISSUE', 'ATTCHMSG_TXN', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/initiatetransaction/memebership/{product}' AND "METHOD" = 'POST' ;
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'POST', 'VolPayHub', '/v2/initiatetransaction/memebership/{product}', '036', 'R', 'API for getting Read Membership Based On Partytype'); 

UPDATE ENTITYCONFIGURATION SET ISREFERENCEDATA='YES' WHERE ENTITYNAME='StatusApprovalConfig';

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='FSRJCT002';
INSERT INTO INCIDENCEMASTER(INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP)VALUES('FSRJCT002', 'PAYMENT', 'REJECTED', 'msg', 0);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='FSRJCT002' AND PROCESSNAME IS NULL; 
INSERT INTO INCIDENCEDEFINITION(INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('FSRJCT002', '${msg}', 'Error', 'High', 'User', NULL, NULL, NULL, NULL, 'REJECTED', 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

UPDATE INCIDENCEMASTER SET ALLOWEDPARAMETER = 'msg' WHERE INCIDENCECODE='ATTCHSTSREJECTED';
UPDATE INCIDENCEDEFINITION SET DESCRIPTION = '${msg}' WHERE INCIDENCECODE='ATTCHSTSREJECTED' AND PROCESSNAME IS NULL; 

UPDATE ENTITYCONFIGURATION SET ISREFERENCEDATA='YES' WHERE ENTITYNAME = 'Party';

DELETE FROM PICKLISTMANAGER WHERE PICKLISTTYPE='ISOCode';
DELETE FROM PICKLISTMANAGER WHERE PICKLISTTYPE='NonISOCode';
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC02', 'Debtor account number invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC03', 'Creditor account number invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC04', 'Account number specified has been closed on the bank of account''s books.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC06', 'Account specified is blocked, prohibiting posting of transactions against it.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC07', 'Creditor account number closed');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC10', 'Debtor account currency is invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC11', 'Creditor account currency is invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC13', 'Debtor account type missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AC14', 'Creditor account type missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AG01', 'Transaction forbidden on this type of account (formerly NoAgreement)');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AG03', 'Transaction type not supported / authorized on this account');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AM02', 'Specific transaction/message amount is greater than allowed maximum');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AM04', 'Amount of funds available to cover specified message amount is insufficient.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AM09', 'Amount received is not the amount agreed or expected');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AM11', 'Transaction currency is invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AM12', 'Amount is invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'AM13', 'Transaction amount exceeds limits set by clearing system');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE04', 'Specification of creditor''s address, which is required for payment, is missing/not correct (formerly IncorrectCreditorAddress).');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE06', 'End customer specified is not known at associated Sort/National Bank Code or does no longer exist in the books');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE07', 'Specification of debtor''s address, which is required for payment, is missing/not correct ');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE10', 'Debtor country code is missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE11', 'Creditor country code is missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE13', 'Country code of debtor’s residence is missing or Invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE14', 'Country code of creditor''s residence is missing or Invalid ');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE16', 'Debtor identification code missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'BE17', 'Creditor identification code missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'DS24', 'Waiting time expired due to incomplete order');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'DT04', 'Future date not supported');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'DUPL', 'Payment is a duplicate of another payment');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'DS0H', 'Signer is not allowed to sign for this account');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'FF02', 'Syntax error reason is provided as narrative information in the additional reason information.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'FF03', 'Invalid Payment Type Information');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'FF08', 'End to End Id missing or invalid');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'MD07', 'End customer is deceased.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'NARR', 'Reason is provided as narrative information in the additional reason information.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'RC03', 'Debtor FI identifier is invalid or missing');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'ISOCode', 'RC04', 'Creditor FI identifier is invalid or missing');

INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', 'NOAT', 'Receiving Customer Account does not support/accept this message type.');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', '1100', 'Any Other Reasons');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', '9910', 'Instructed Agent signed-off');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', '9934', 'Instructing Agent signed-off');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', '9946', 'Instructing Agent suspended');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', '9947', 'Instructed Agent suspended');
INSERT INTO PICKLISTMANAGER(PICKLISTMANAGER_PK, PICKLISTTYPE, PICKLISTVALUE, PICKLISTDESCRIPTION) VALUES(SYS_GUID(), 'NonISOCode', '9948', 'Central Switch (RTP) service is suspended');

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/partyserviceassociations/transportinput' AND "METHOD" = 'GET' ; 
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'GET', 'VolPayHub', '/v2/partyserviceassociations/transportinput', '023', 'R', 'API to GetPSABased On Direction is INPUT'); 
  
DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/partyserviceassociations/transportack' AND "METHOD" = 'GET' ; 
INSERT INTO RESTRESOURCES (RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES(SYS_GUID(), 'GET', 'VolPayHub', '/v2/partyserviceassociations/transportack', '023', 'R', 'API to GetPSABased On Direction is ACK'); 
 
DELETE  FROM ATTRIBUTEMASTER WHERE ATTRIBUTEID IN ('028017','028018','028019','028020','028021','028022','028023','028024','028025','064010','064011','064012','060018','060019','014044','014045','069008','069005','069006'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028017', 'AccountDebitAuthorization', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028018', 'AddressLine1', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028019', 'AddressLine2', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028020', 'City', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028021', 'State', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028022', 'PostCode', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028023', 'Country', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028024', 'EmailID', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('028', '028025', 'PhoneNo', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('064', '064010', 'ColourA', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('064', '064011', 'ColourB', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('064', '064012', 'Opacity', 'Integer'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('060', '060018', 'FunctionName', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('060', '060019', 'AdditionalConfig', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('014', '014044', 'SupportedChargeCodes', 'String'); 
INSERT INTO ATTRIBUTEMASTER (RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('014', '014045', 'CashClearingSchemeCode', 'String');

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='ACK006';
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ACK006', 'PAYMENT', 'REPAIR', 'partyCode,serviceCode', 1);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ACK006';
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ACK006', 'Unable to Generate ACK: No PSA with Ack Transports Found  for the Party ${partyCode} and Service ${serviceCode} with is response required set and is default psa set', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL, NULL, 'ISSUE', 'PAYMENT', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);

DELETE FROM INCIDENCEMASTER WHERE INCIDENCECODE='ATTMSGRJCT001';
INSERT INTO INCIDENCEMASTER (INCIDENCECODE, WORKFLOWCODE, ALLOWEDPROCESSSTATUS, ALLOWEDPARAMETER, ALLOWSTP) VALUES('ATTMSGRJCT001', 'PAYMENT', 'REJECTED', 'msg', 0);

DELETE FROM INCIDENCEDEFINITION WHERE INCIDENCECODE='ATTMSGRJCT001';
INSERT INTO INCIDENCEDEFINITION (INCIDENCECODE, DESCRIPTION, INCIDENCERESOLUTION, SEVERITY, INCIDENCEORIGIN, CANCLIENTSETPREFERENCE, OPSSEVERITYPREFERENCE, CLIENTSEVERITYPREFERENCE, REPAIRANDMANUALINPUT, PROCESSSTATUS, INCIDENCETYPE, WORKFLOWCODE, INCIDENCEDEFINITION_PK, PROCESSNAME, PARTYSERVICEASSOCIATIONCODE, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE) VALUES('ATTMSGRJCT001', '${msg}', 'Check the payment', 'High', 'User', NULL, NULL, NULL, NULL,  'REJECTED','ISSUE','ATTCHMSG_TXN', SYS_GUID(), NULL, NULL, 'ACTIVE', TO_DATE('2017-04-25','YYYY-MM-DD'), NULL);  
UPDATE RESTRESOURCES SET RESOURCEID='055' WHERE "METHOD"='GET' AND "PATH"='/v2/volpayidconfigurations/code';

DELETE FROM ENTITYCONFIGURATION WHERE ENTITYNAME = 'VolPayIDConfiguration';
INSERT INTO ENTITYCONFIGURATION(ENTITYNAME, ISCACHERELOAD, ISCAMELRELOAD, ISREFERENCEDATA, ISLRURELOAD) VALUES('VolPayIDConfiguration', 'YES', 'NO', 'NO', NULL);

DELETE FROM VOLPAYCONFIGURATION WHERE NAME = 'AllowedSourceIndicators';
INSERT INTO VOLPAYCONFIGURATION (NAME, VALUE, "TYPE") VALUES('AllowedSourceIndicators', 'volpay-ui', NULL);

COMMIT;