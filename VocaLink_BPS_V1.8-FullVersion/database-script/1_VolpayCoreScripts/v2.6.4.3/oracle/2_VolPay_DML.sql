UPDATE RESTRESOURCES SET METHOD = 'POST' WHERE "PATH" = '/v2/serviceconfig/{InputFormat}';
UPDATE RESTRESOURCES SET METHOD = 'POST' WHERE "PATH" = '/v2/payments/code';

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/roles/count' AND METHOD = 'GET';
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES), 'GET', 'VolPayHub', '/v2/roles/count', '011', 'R', 'API for Getting Role Count');

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/roles/audit/readall' AND METHOD = 'POST';
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES), 'POST', 'VolPayHub', '/v2/roles/audit/readall', '011', 'R', 'API to get Role log audit');

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/roles/{status}/count' AND METHOD = 'GET';
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES), 'GET', 'VolPayHub', '/v2/roles/{status}/count', '011', 'R', 'API for Getting Role Count By Status');

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/roles/suspend' AND METHOD = 'POST';
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES), 'POST', 'VolPayHub', '/v2/roles/suspend', '011', 'R', 'API for suspending Role');

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/roles/activate' AND METHOD = 'POST';
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES), 'POST', 'VolPayHub', '/v2/roles/activate', '011', 'R', 'API for activating Role');

DELETE FROM RESTRESOURCES WHERE "PATH" = '/v2/roles/readall' AND METHOD = 'POST';
INSERT INTO RESTRESOURCES(RESTRESOURCES_PK, "METHOD", APPLICATION, "PATH", RESOURCEID, OPERATION, DESCRIPTION) VALUES((SELECT max(CAST(RESTRESOURCES_PK AS INTEGER)) + 1 AS RESTRESOURCES_PK FROM RESTRESOURCES), 'POST', 'VolPayHub', '/v2/roles/readall', '011', 'R', 'API for Getting Role');

DELETE FROM ATTRIBUTEMASTER WHERE ATTRIBUTEID = '011006' AND RESOURCEID = '011';
DELETE FROM ATTRIBUTEMASTER WHERE ATTRIBUTEID = '011007' AND RESOURCEID = '011';
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('011', '011006', 'EffectiveFromDate', 'DateOnly');
INSERT INTO ATTRIBUTEMASTER(RESOURCEID, ATTRIBUTEID, ATTRIBUTENAME, ATTRIBUTETYPE) VALUES('011', '011007', 'EffectiveTillDate', 'DateOnly');

DELETE FROM TRANSPORT WHERE TRANSPORTCODE = 'OFACINTERFACE_REQ' AND TRANSPORTTYPE = 'QUEUE';
DELETE FROM TRANSPORT WHERE TRANSPORTCODE = 'OFACINTERFACE_REQ' AND TRANSPORTTYPE = 'DIRECT-VM';
INSERT INTO TRANSPORT (TRANSPORT_PK, TRANSPORTCODE, REFERENCECODE, TRANSPORTTYPE, TRANSPORTMODE, CONNECTIONSTRING, ENCRYPTION, DIRECTION, STATUS, EFFECTIVEFROMDATE, EFFECTIVETILLDATE, XML_FIELD) VALUES('601', 'OFACINTERFACE_REQ', 'OFAC', 'DIRECT-VM', 'direct-vm:', 'OFACExternalSystem', 'NONE', 'ASYNC-OUT', 'ACTIVE', TO_DATE('2016-11-18 00:00:00','YYYY-MM-DD HH24:MI:SS'), NULL, HEXTORAW('3c3f786d6c2076657273696f6e3d22312e302220656e636f64696e673d225554462d38223f3e0d0a3c5472616e73706f72743e0d0a093c5472616e73706f7274436f64653e4f464143494e544552464143455f5245513c2f5472616e73706f7274436f64653e0d0a093c446972656374696f6e3e4153594e432d4f55543c2f446972656374696f6e3e0d0a093c5265666572656e6365436f64653e4f4641433c2f5265666572656e6365436f64653e0d0a093c5472616e73706f7274547970653e4449524543542d564d3c2f5472616e73706f7274547970653e0d0a093c5472616e73706f72744d6f64653e6469726563742d766d3a3c2f5472616e73706f72744d6f64653e0d0a093c436f6e6e656374696f6e537472696e673e4f46414345787465726e616c53797374656d3c2f436f6e6e656374696f6e537472696e673e0d0a093c456e6372797074696f6e3e4e4f4e453c2f456e6372797074696f6e3e0d0a093c5374617475733e4143544956453c2f5374617475733e0d0a093c45666665637469766546726f6d446174653e323031362d31312d31383c2f45666665637469766546726f6d446174653e0d0a093c5472616e73706f72745f504b3e3630313c2f5472616e73706f72745f504b3e0d0a3c2f5472616e73706f72743e'));

DELETE FROM VOLPAYCONFIGURATION WHERE NAME = 'OFACExternalSystem';
INSERT INTO VOLPAYCONFIGURATION (NAME, VALUE, "TYPE") VALUES('OFACExternalSystem', 'OFACCheckExternalSystemFlow', 'FLOW_NAME');

DELETE FROM ROLERESOURCEPERMISSION WHERE ROLEID = 'Super Admin' AND RESOURCEID = '011' AND PERMISSION = 'A2';
DELETE FROM ROLERESOURCEPERMISSION WHERE ROLEID = 'Super Admin' AND RESOURCEID = '011' AND PERMISSION = 'A3';
DELETE FROM ROLERESOURCEPERMISSION WHERE ROLEID = 'Super Admin' AND RESOURCEID = '011' AND PERMISSION = 'A4';
DELETE FROM ROLERESOURCEPERMISSION WHERE ROLEID = 'Super Admin' AND RESOURCEID = '011' AND PERMISSION = 'A5';

COMMIT;