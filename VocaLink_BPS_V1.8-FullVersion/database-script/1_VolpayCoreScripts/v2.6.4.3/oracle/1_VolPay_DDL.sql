ALTER TABLE ROLE ADD EFFECTIVEFROMDATE DATE DEFAULT TO_DATE('2017-01-01', 'YYYY-MM-DD') NOT NULL;
ALTER TABLE ROLE ADD EFFECTIVETILLDATE DATE;

COMMIT;