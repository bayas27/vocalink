begin

dbms_aqadm.create_queue(queue_name=>'FILE_ACQUIRED', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'FILE_ACQUIRED');

dbms_aqadm.create_queue(queue_name=>'FILE_REJECT', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'FILE_REJECT');

dbms_aqadm.create_queue(queue_name=>'FILE_HOLD', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'FILE_HOLD');

dbms_aqadm.create_queue(queue_name=>'DEBULK_REJECT', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'DEBULK_REJECT');

dbms_aqadm.create_queue(queue_name=>'PAYMENT_TOBEPROCESSED', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'PAYMENT_TOBEPROCESSED');

dbms_aqadm.create_queue(queue_name=>'PAYMENTLIST_2BEPROCESSED', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'PAYMENTLIST_2BEPROCESSED');

dbms_aqadm.create_queue(queue_name=>'ACK_TOBEGENERATED', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ACK_TOBEGENERATED');

dbms_aqadm.create_queue(queue_name=>'FILE_ACK', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'FILE_ACK');

dbms_aqadm.create_queue(queue_name=>'PAYMENTSTATUS', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'PAYMENTSTATUS');

dbms_aqadm.create_queue(queue_name=>'NOTIGATEQUEUEIN', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'NOTIGATEQUEUEIN');

dbms_aqadm.create_queue(queue_name=>'MAIL_QUEUE_NOTIF', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'MAIL_QUEUE_NOTIF');


dbms_aqadm.create_queue(queue_name=>'VPE_CACHERELOAD', queue_table=>'VolPayTopicTable');
dbms_aqadm.start_queue(queue_name=> 'VPE_CACHERELOAD');

dbms_aqadm.create_queue(queue_name=>'VPE_CAMELRELOAD', queue_table=>'VolPayTopicTable');
dbms_aqadm.start_queue(queue_name=> 'VPE_CAMELRELOAD');

dbms_aqadm.create_queue(queue_name=>'ASYNCDEMOINSTRIFRES', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ASYNCDEMOINSTRIFRES');

dbms_aqadm.create_queue(queue_name=>'ASYNCDEMOINSTRIFREQ', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ASYNCDEMOINSTRIFREQ');


dbms_aqadm.create_queue(queue_name=>'settlement_position_calc', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_position_calc');

dbms_aqadm.create_queue(queue_name=>'PAYMENT_WAITING4RESPONSE', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'PAYMENT_WAITING4RESPONSE');

dbms_aqadm.create_queue(queue_name=>'PAYMENT_REPAIRED', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'PAYMENT_REPAIRED');

dbms_aqadm.create_queue(queue_name=>'BULK_HANDLER', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'BULK_HANDLER');

dbms_aqadm.create_queue(queue_name=>'GROUPEDBULKLIST_HANDLER', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'GROUPEDBULKLIST_HANDLER');

dbms_aqadm.create_queue(queue_name=>'GROUPEDBULK_HANDLER', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'GROUPEDBULK_HANDLER');

dbms_aqadm.create_queue(queue_name=>'INTERFACE_BULK', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'INTERFACE_BULK');

dbms_aqadm.create_queue(queue_name=>'ASYNCOFACINTERFACERESP', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ASYNCOFACINTERFACERESP');

dbms_aqadm.create_queue(queue_name=>'ASYNCOFACINTERFACEREQ', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ASYNCOFACINTERFACEREQ');

dbms_aqadm.create_queue(queue_name=>'volpay_cheques_triggers', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'volpay_cheques_triggers');

dbms_aqadm.create_queue(queue_name=>'FUNDSCTRLIFACERES', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'FUNDSCTRLIFACERES');

dbms_aqadm.create_queue(queue_name=>'FUNDSCTRLIFACEREQ', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'FUNDSCTRLIFACEREQ');

dbms_aqadm.create_queue(queue_name=>'LIQUIDITYCTRLIFACERES', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'LIQUIDITYCTRLIFACERES');

dbms_aqadm.create_queue(queue_name=>'LIQUIDITYCTRLIFACEREQ', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'LIQUIDITYCTRLIFACEREQ');


dbms_aqadm.create_queue(queue_name=>'ACCTPOSTINGIFACERES', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ACCTPOSTINGIFACERES');

dbms_aqadm.create_queue(queue_name=>'ACCTPOSTINGIFACEREQ', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'ACCTPOSTINGIFACEREQ');

dbms_aqadm.create_queue(queue_name=>'settlement_ct_in', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_ct_in');

dbms_aqadm.create_queue(queue_name=>'settlement_ct_out', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_ct_out');

dbms_aqadm.create_queue(queue_name=>'settlement_cheque_in', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_cheque_in');

dbms_aqadm.create_queue(queue_name=>'settlement_cheque_out', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_cheque_out');

dbms_aqadm.create_queue(queue_name=>'settlement_pw_ct_in', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_pw_ct_in');

dbms_aqadm.create_queue(queue_name=>'settlement_pw_ct_out', queue_table=>'VolPayQueueTable');
dbms_aqadm.start_queue(queue_name=> 'settlement_pw_ct_out');

end;
/
Commit;
