INSERT INTO USERPROFILE
(USERID, LASTNAME, FIRSTNAME, EMAILADDRESS, ROLEID, PASSWORD, ISFORCERESET, STATUS, ADDITIONALDETAILS, TIMEZONE, EFFECTIVEFROMDATE, EFFECTIVETILLDATE)
VALUES('user3', 'user3', 'user3', 'user3@gmail.com', 'VocalinkOperator', '$2a$10$r8m6bjOn0kpby8y904Og/e4CVVqT7tiNRev9MigptLWUamq1p/sKi', 0, 'ACTIVE', 'LastFivePasswords :{
    "Records":[
        {
            "date":"2019-01-18T17:19:09.122",
            "password":"$2a$10$n1H573hMa7qAoCOxl.J62u2n9yubiwmvEUdzleichDKaK6oT4lgGy"
        },
        {
            "date":"2019-01-18T17:29:56.024",
            "password":"$2a$10$r8m6bjOn0kpby8y904Og/e4CVVqT7tiNRev9MigptLWUamq1p/sKi"
        }
    ]
},', 'IST', TIMESTAMP '2019-01-18 00:00:00.000000', NULL);
INSERT INTO USERPROFILE
(USERID, LASTNAME, FIRSTNAME, EMAILADDRESS, ROLEID, PASSWORD, ISFORCERESET, STATUS, ADDITIONALDETAILS, TIMEZONE, EFFECTIVEFROMDATE, EFFECTIVETILLDATE)
VALUES('user1', 'user1', 'user1', 'user1@gmail.com', 'VocalinkOperator', '$2a$10$hfOaXd7M9EZ6waIfx2UUueJgDqcwC.0KfgXNAphzOu/CMR70/iVAS', 0, 'ACTIVE', 'LastFivePasswords :{
    "Records":[
        {
            "date":"2019-01-18T17:17:13.184",
            "password":"$2a$10$SS7LC.FTNNC4jM7q8nnGie9tMB4JPCN99/X5vczvyzXlDVu4V4UqS"
        },
        {
            "date":"2019-01-18T17:28:38.941",
            "password":"$2a$10$hfOaXd7M9EZ6waIfx2UUueJgDqcwC.0KfgXNAphzOu/CMR70/iVAS"
        }
    ]
},', 'IST', TIMESTAMP '2019-01-18 00:00:00.000000', NULL);
INSERT INTO USERPROFILE
(USERID, LASTNAME, FIRSTNAME, EMAILADDRESS, ROLEID, PASSWORD, ISFORCERESET, STATUS, ADDITIONALDETAILS, TIMEZONE, EFFECTIVEFROMDATE, EFFECTIVETILLDATE)
VALUES('user2', 'user2', 'user2', 'user2@gmail.com', 'VocalinkOperator', '$2a$10$b3EfxalU0.3edOeB8v/qxO4/DViJegfxGdikoxFqC6qLsi4tNwd6q', 0, 'ACTIVE', 'LastFivePasswords :{
    "Records":[
        {
            "date":"2019-01-18T17:18:25.103",
            "password":"$2a$10$gPYGIEjBmLpG.8t35lps3OfKnlFxqWTVeMCMVldkV1BOUzJS0gT.m"
        },
        {
            "date":"2019-01-18T17:29:19.616",
            "password":"$2a$10$b3EfxalU0.3edOeB8v/qxO4/DViJegfxGdikoxFqC6qLsi4tNwd6q"
        }
    ]
},', 'IST', TIMESTAMP '2019-01-18 00:00:00.000000', NULL);
