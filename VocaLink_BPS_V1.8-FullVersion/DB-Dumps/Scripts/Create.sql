begin
dbms_aqadm.create_queue_table(
     queue_table=>'VolPayQueueTable',
     queue_payload_type=>'sys.aq$_jms_text_message',
     multiple_consumers=>false
);

dbms_aqadm.create_queue_table(
     queue_table=>'VolPayTopicTable',
     queue_payload_type=>'sys.aq$_jms_text_message',
     multiple_consumers=>TRUE
);
end;
/
Commit;