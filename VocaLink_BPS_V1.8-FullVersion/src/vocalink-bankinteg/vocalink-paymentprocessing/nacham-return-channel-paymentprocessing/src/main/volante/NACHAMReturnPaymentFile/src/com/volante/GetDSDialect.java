package com.volante;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import com.volante.component.server.jdbc.ManagedConnectionPool;
import com.tplus.transform.util.sql.connection.ConnectionInfo;
import javax.naming.NamingException;

import com.tplus.transform.util.sql.connection.ConnectionPool;


import com.tplus.transform.util.log.Log;

public class GetDSDialect implements IInvokable {
	
	    static com.tplus.transform.util.log.Log logger = com.tplus.transform.util.log.LogFactory.getLog(GetDSDialect.class);

		
    public Object run(Object[] args, TransformContext cxt) throws TransformException {

    	String DSName=null;
    	if(args.length == 1)
    		DSName = (String)args[0];
        ManagedConnectionPool obj  = null;
	    ConnectionPool connectionPool = null;

        String Dialect = "";

        try {
        LookupContext lcxt = LookupContextFactory.getLookupContext();
		
       	connectionPool = LookupContextFactory.getLookupContext().lookupDataSource(DSName);
		ConnectionInfo info = connectionPool.getConnectionInfo();
		
		//  obj = (ManagedConnectionPool)lcxt.lookup( DSName );
        //ConnectionInfo info = obj.getConnectionInfo();
        Dialect=info.getProperty("dialect");
        } 
		
		catch (NamingException e) {
			e.printStackTrace();
            }
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("the Error in GetDSDialect is :"+e.getMessage());
		}
       return Dialect;
   }
}
