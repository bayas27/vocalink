package com.volantetech.volante.services;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.tplus.transform.runtime.TransformException;

/**
 * Created by mukund.g@volantetech.net on 07-Dec-19.
 */
public class VocalinkAuthenticationHandler extends VolPayRestAuthenticationHandler {
	private final static String TIME_ZONE_LIST = "GetTimeZoneList";
	private final static String CONFIGURATION = "GetUIConfiguration";
	private final static String APPLICATIONINFO = "GetApplicationInfo";

	public VocalinkAuthenticationHandler() {
		ArrayList<String> toAdd = new ArrayList<String>();

		toAdd.add(TIME_ZONE_LIST);
		toAdd.add(CONFIGURATION);
		toAdd.add(APPLICATIONINFO);
		super.addAllFlowsToIgnore(toAdd);
		super.setInternalAuthentication(false);
	}

	@Override
	public String getUserInfo(final HttpServletRequest request) throws TransformException {

		String userID = request.getRemoteUser() == null ? "" : request.getRemoteUser();
		return userID;
	}
}
