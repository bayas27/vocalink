package com.volantetech.volante.services.fileupload;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.HashMap;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;

import com.tplus.transform.runtime.LookupContext;
import com.tplus.transform.runtime.LookupContextFactory;
import com.tplus.transform.runtime.MessageFlow;
import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformContextImpl;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.util.log.Log;
import com.tplus.transform.util.log.LogFactory;
import com.volantetech.volante.services.fileupload.common.FileUploadConstants;

public class FileUploadServlet extends HttpServlet {
	/**
	 * <h1>FileUploadServlet</h1> This is a servlet configured in VolPay to
	 * facilitate file upload
	 * <p>
	 * This is configured in VolPay Channel to allow users to upload a file as
	 * an octet-stream. This accepts all incoming files to VolPay and
	 * distributes them to the appropriate process based on the PSACode.
	 * </p>
	 */
	final static Log log = LogFactory.getLog(FileUploadServlet.class);

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FileUploadHelper fileUploadHelper = new FileUploadHelper();
		PrintWriter out = response.getWriter();
		response.setContentType(FileUploadConstants.CONTENT_TYPE);
		response.setCharacterEncoding("UTF-8");

		HashMap<String, String> queryParamsMap = new HashMap<String, String>();
		String instructionFileName = "";
		String psaCode = "";
		String participantId = "";
		String receivedAt = "";
		String mftId = "";
		String rawResponse = "";
		String strResponse = "";

		try {
			ServletInputStream sis = request.getInputStream();
			byte[] data = IOUtils.toByteArray(sis);
			String queryString = request.getQueryString();

			queryParamsMap = fileUploadHelper.extractQueryParams(queryString);
			
			psaCode = queryParamsMap.get(FileUploadConstants.queryParamPSACode);
			instructionFileName = queryParamsMap.get(FileUploadConstants.queryParamInstructionFileName);
			participantId = queryParamsMap.get(FileUploadConstants.queryParamParticipantId) == null ? "" : queryParamsMap.get(FileUploadConstants.queryParamParticipantId);
			receivedAt = queryParamsMap.get(FileUploadConstants.queryParamReceivedTime) == null ? "" : queryParamsMap.get(FileUploadConstants.queryParamReceivedTime);
			mftId = queryParamsMap.get(FileUploadConstants.queryParamMFTId) == null ? "" : queryParamsMap.get(FileUploadConstants.queryParamMFTId);

			if (psaCode.equalsIgnoreCase(FileUploadConstants.bcrpPSA)) {
				rawResponse = processBCRPConfirmation(data);
				if (rawResponse.contains(FileUploadConstants.statusSuccess)) {
					strResponse = fileUploadHelper.extractBCRPGenerationId(rawResponse);
				}
			} else {
				rawResponse = processInstruction(instructionFileName, data, psaCode, participantId, receivedAt, mftId);

				log.error("RawResponse --> " +  rawResponse);
				strResponse = fileUploadHelper.extractInstructionId(rawResponse);
			}
			if (strResponse != null && strResponse.length() > 0) {

				response.setStatus(200);
				out.print(fileUploadHelper.generateJSONResponse(strResponse));
			} else {
				response.setStatus(400);
				out.print(fileUploadHelper.generateFailureResponse());
			}
		} catch (Exception ex) {
			log.error("Error uploading files to server", ex);
			response.setStatus(400);
			try {
				out.print(fileUploadHelper.generateFailureResponse());
			} catch (JSONException e) {
				log.error("Error uploading files to server", e);
			}
		}
	}

	private String processInstruction(final String strFileName, final byte[] byteContent, final String psaCode) throws NamingException, TransformException, RemoteException {

		log.error("strFileName -->" + strFileName);
		log.error("ByteContent -->" + byteContent);
		log.error("psaCode -->" + psaCode);

		LookupContext lcxt = LookupContextFactory.getLookupContext();
		// Lookup message flow (defined in the cartridge)
		MessageFlow messageFlow = lcxt.lookupMessageFlow(FileUploadConstants.fileUploadFlowName);

		// Create a TransformContext. We have no special properties to set
		// in the context.
		TransformContext cxt = new TransformContextImpl();

		// Prepare the input for the message flow.
		Object[] messageFlowArgs = new Object[] { strFileName, byteContent, psaCode,  };
		// Execute the message flow.
		Object[] output = messageFlow.run(messageFlowArgs, cxt);

		RawMessage rawMessage = (RawMessage) output[0];

		return rawMessage.toString();
	}

	private String processInstruction(final String strFileName, final byte[] byteContent, final String psaCode, final String participantId, final String receivedAt, final String mftId) throws NamingException, TransformException, RemoteException {

		log.error("strFileName -->" + strFileName);
		log.error("ByteContent -->" + byteContent);
		log.error("psaCode -->" + psaCode);
		log.error("participantId -->" + participantId);
		log.error("receivedAt -->" + receivedAt);
		log.error("mftId -->" + mftId);

		LookupContext lcxt = LookupContextFactory.getLookupContext();
		// Lookup message flow (defined in the cartridge)
		MessageFlow messageFlow = lcxt.lookupMessageFlow(FileUploadConstants.fileUploadFlowName);

		// Create a TransformContext. We have no special properties to set
		// in the context.
		TransformContext cxt = new TransformContextImpl();

		// Prepare the input for the message flow.
		Object[] messageFlowArgs = new Object[] { strFileName, byteContent, psaCode, participantId, receivedAt, mftId};
		// Execute the message flow.
		Object[] output = messageFlow.run(messageFlowArgs, cxt);

		RawMessage rawMessage = (RawMessage) output[0];

		return rawMessage.toString();
	}

	private String processBCRPConfirmation(final byte[] byteContent) throws NamingException, TransformException, RemoteException {
		
		log.error("ByteContent -->" + byteContent);

		LookupContext lcxt = LookupContextFactory.getLookupContext();
		// Lookup message flow (defined in the cartridge)
		MessageFlow messageFlow = lcxt.lookupMessageFlow(FileUploadConstants.bcrpConfirmationFile);

		// Create a TransformContext. We have no special properties to set
		// in the context.
		TransformContext cxt = new TransformContextImpl();

		// Prepare the input for the message flow.
		Object[] messageFlowArgs = new Object[] { byteContent };
		// Execute the message flow.
		Object[] output = messageFlow.run(messageFlowArgs, cxt);

		RawMessage rawMessage = (RawMessage) output[0];
		return rawMessage.toString();
	}
}