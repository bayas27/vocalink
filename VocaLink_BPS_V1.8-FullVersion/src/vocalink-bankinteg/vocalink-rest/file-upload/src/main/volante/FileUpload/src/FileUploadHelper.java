package com.volantetech.volante.services.fileupload;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.volantetech.volante.services.fileupload.common.FileUploadConstants;

public class FileUploadHelper {
	String extractInstructionId(final String response) {
		String instructionId = "";
		final Pattern pattern = Pattern.compile("<InstructionID>\\[(.+?)\\]</InstructionID>");
		final Matcher matcher = pattern.matcher(response);
		try {
			matcher.find();
			instructionId = matcher.group(1);
		} catch (Exception e) {
			instructionId = "";
		}
		return instructionId;
	}

	String extractBCRPGenerationId(final String response) {
		String instructionId = "";
		final Pattern pattern = Pattern.compile("<generationId>(.+?)</generationId>");
		final Matcher matcher = pattern.matcher(response);
		try {
			matcher.find();
			instructionId = matcher.group(1);
		} catch (Exception e) {
			instructionId = "";
		}
		return instructionId;
	}

	JSONObject generateJSONResponse(final String instructionID) throws JSONException {

		JSONObject responseJsonObject = new JSONObject();

		JSONObject instructionJsonObject = new JSONObject();
		instructionJsonObject.put("Name", "InstructionID");
		instructionJsonObject.put("Value", instructionID);

		JSONArray jsonArray = new JSONArray();
		responseJsonObject.put("ResponseMessage", "Instruction successfully added");
		responseJsonObject.put("BusinessPrimaryKey", jsonArray.put(0, instructionJsonObject));
		responseJsonObject.put("Status", "FILE UPLOADED SUCCESFULLY");
		return responseJsonObject;
	}

	JSONObject generateFailureResponse() throws JSONException {

		JSONObject responseJsonObject = new JSONObject();

		JSONObject instructionJsonObject = new JSONObject();
		instructionJsonObject.put("Name", "InstructionID");
		instructionJsonObject.put("Value", "");

		JSONArray jsonArray = new JSONArray();
		responseJsonObject.put("ResponseMessage", "Failed to add instruction");
		responseJsonObject.put("BusinessPrimaryKey", jsonArray.put(0, instructionJsonObject));
		responseJsonObject.put("Status", "FILE UPLOAD FAILED");
		return responseJsonObject;
	}

	HashMap<String, String> extractQueryParams(String queryString) {
		String[] queryParams = queryString.split("&");
		HashMap<String, String> queryParamMap = new HashMap<String, String>();
		for (int i = 0; i < queryParams.length; i++) {
			if (queryParams[i].startsWith(FileUploadConstants.queryParamInstructionFileName)) {
				queryParamMap.put(FileUploadConstants.queryParamInstructionFileName, (queryParams[i].split("="))[1]);
			} else if (queryParams[i].startsWith(FileUploadConstants.queryParamPSACode)) {
				queryParamMap.put(FileUploadConstants.queryParamPSACode, (queryParams[i].split("="))[1]);
			} else if (queryParams[i].startsWith(FileUploadConstants.queryParamParticipantId)) {
				queryParamMap.put(FileUploadConstants.queryParamParticipantId, (queryParams[i].split("="))[1]);
			} else if (queryParams[i].startsWith(FileUploadConstants.queryParamReceivedTime)) {
				queryParamMap.put(FileUploadConstants.queryParamReceivedTime, (queryParams[i].split("="))[1]);
			} else if (queryParams[i].startsWith(FileUploadConstants.queryParamMFTId)) {
				queryParamMap.put(FileUploadConstants.queryParamMFTId, (queryParams[i].split("="))[1]);
			}
		}
		return queryParamMap;
	}
}
