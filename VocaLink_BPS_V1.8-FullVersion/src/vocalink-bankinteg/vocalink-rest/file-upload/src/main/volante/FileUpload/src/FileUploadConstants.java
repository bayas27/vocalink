package com.volantetech.volante.services.fileupload.common;

public final class FileUploadConstants {

	private FileUploadConstants() {

	}

	public static final String CONTENT_TYPE = "application/json; charset=windows-1252";
	public static final String queryParamInstructionFileName = "InstructionFileName";
	public static final String queryParamPSACode = "PSACode";
	public static final String queryParamParticipantId = "Participant";
	public static final String queryParamReceivedTime = "Received";
	public static final String queryParamMFTId = "MFTID";
	public static final String fileUploadFlowName = "VolpayFileUpload";
	public static final String bcrpConfirmationFile = "NachamSettlementFlows";
	public static final String bcrpPSA = "00010001_BCRPCONFIRMATION_NACHAMCHANNELInput";
	public static final String statusSuccess = "SUCCESS";
}
