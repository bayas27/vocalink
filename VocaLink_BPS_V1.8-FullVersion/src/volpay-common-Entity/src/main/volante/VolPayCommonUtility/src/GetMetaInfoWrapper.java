package com.volantetech.volante.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class GetMetaInfoWrapper {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow()  throws javax.naming.NamingException {
        if(messageFlow == null) {
			LookupContext lcxt = LookupContextFactory.getLookupContext();
			messageFlow = lcxt.lookupMessageFlow("GetMetaInfo");
        }
        return messageFlow;
    }

    public static void process(String fileName) {
	    try {
	        MessageFlow messageFlow = getMessageFlow();
			TransformContext cxt = new TransformContextImpl();
	      	Object[] messageFlowArgs = new Object[] {  };
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);
            String fdcObjString = (String)output[0];
	    }
	    catch(TransformException e) {
	      System.err.println(e.toXMLString());
	    }
	    catch(javax.naming.NamingException e) {
	      e.printStackTrace();
	    }
	    catch(java.rmi.RemoteException e) {
	      e.printStackTrace();
	    }
	    catch(java.io.IOException e) {
	      e.printStackTrace();
	    }
	}
}