package com.volante.volantetech.membershiputility;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class MembershipUtility {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow()  throws javax.naming.NamingException {
        // look up message flow and cache
        if(messageFlow == null) {
			// Get the lookup context for the current environment
			LookupContext lcxt = LookupContextFactory.getLookupContext();

			// Lookup message flow (defined in the cartridge)
			messageFlow = lcxt.lookupMessageFlow("PersistMembershipUtilityFile");
        }
        return messageFlow;
    }

    public static void process(String uploadType, String rawInFile) {
	    try {
	        MessageFlow messageFlow = getMessageFlow();

			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
			
	      	RawMessage rawIn  = new FileInputSource(rawInFile);
	      	Object[] messageFlowArgs = new Object[] {  uploadType,  rawInFile, rawIn  };

	      	// Execute the message flow.
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);

			// Use output
            DataObjectSection destObj = (DataObjectSection)output[0];

	    }
	    catch(TransformException e) {
	      System.err.println(e.toXMLString());
	    }
	    catch(javax.naming.NamingException e) {
	      e.printStackTrace();
	    }
	    catch(java.rmi.RemoteException e) {
	      e.printStackTrace();
	    }
	    catch(java.io.IOException e) {
	      e.printStackTrace();
	    }
	}
    public static void main(String[] args) {
        com.tplus.transform.util.LoggingUtil.enableLogging("log.xml");
        String uploadType = null;
        String rawInFile = null;
        if(args.length > 0) {
           uploadType = args[0];
            rawInFile = args[1];

        }
       process(uploadType,rawInFile);
    }
}