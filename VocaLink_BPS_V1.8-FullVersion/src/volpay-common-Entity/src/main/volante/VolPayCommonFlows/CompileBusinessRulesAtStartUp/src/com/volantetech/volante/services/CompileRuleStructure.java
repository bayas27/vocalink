package com.volantetech.volante.services;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.*;
import java.util.HashMap;

/**
 * Created by administrator on 22/3/18.
 */
public class CompileRuleStructure extends VPERuleEngine implements IInvokable{
   
    @Override
    public Object run(Object[] objects, TransformContext transformContext) throws TransformException {
        VPERuleEngine.initEngine();
        VPERuleEngine.getCompiledRule((DataObject) objects[0]);
        return null;
    }
}
