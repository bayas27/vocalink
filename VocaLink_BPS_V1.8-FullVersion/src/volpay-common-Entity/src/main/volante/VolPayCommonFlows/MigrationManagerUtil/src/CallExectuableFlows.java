package com.volantetech.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;
import java.util.Collections;
import org.apache.maven.artifact.versioning.ComparableVersion;


public class CallExectuableFlows {

	private static com.tplus.transform.util.log.Log logger = com.tplus.transform.util.log.LogFactory.getLog(CallExectuableFlows.class);

    public static void process(String flowName) {
	    try {
	    	LookupContext lcxt = LookupContextFactory.getLookupContext();

	        MessageFlow messageFlow = lcxt.lookupMessageFlow(flowName);

			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
	      	Object[] messageFlowArgs = new Object[] {  };

	      	// Execute the message flow.
	      	try {
	      		Object[] output = messageFlow.run(messageFlowArgs, cxt);
	      	} catch (Throwable e) {
	      		throw new TransformRuntimeException("Unrecoverable exception thrown!", e);
			}
			}
			catch (Throwable e) {
	      		throw new TransformRuntimeException("Unrecoverable exception thrown!", e);
			}
	    finally {
	    	logger.info(flowName + " has been completed");
	    }
	}
    public static StringList sortMigrationVersion(StringList toBeSortedVerList, String currentVersion, String latestVersion) {
        Collections.sort(toBeSortedVerList, new VersionComparator());
        StringList migrationStepList = new StringList();
        int currentVersionIndex = toBeSortedVerList.lastIndexOf(currentVersion);
        int latestVersionIndex = toBeSortedVerList.indexOf(latestVersion);
        for(int i = 0; i < toBeSortedVerList.size(); i++) {
        	if(i > currentVersionIndex && i <= latestVersionIndex){
        		migrationStepList.add(toBeSortedVerList.get(i));
        	}
        }
        return migrationStepList;
    }

    public static int at(StringList list, String stringToSearch) {
        return list.indexOf(stringToSearch);
    }

    public static String get(StringList list, int index) {
        return (String) list.get(index);
    }

    public static DataObjectSection sortMigrationStepsObj(DataObjectSection toBeSortedDataobject) {
        DataObject temp = null;
        for (int i = 0; i < toBeSortedDataobject.getElementCount() - 1; i++) {
            for (int j = i + 1; j < toBeSortedDataobject.getElementCount(); j++) {
                if (new ComparableVersion((String) toBeSortedDataobject.getElement(i).getField("Version")).compareTo(new ComparableVersion((String) toBeSortedDataobject.getElement(j).getField("Version"))) == 1) {
                    temp = toBeSortedDataobject.getElement(j);
                    toBeSortedDataobject.set(j, toBeSortedDataobject.getElement(i));
                    toBeSortedDataobject.set(i, temp);
                }
                if((toBeSortedDataobject.getElement(i).getField("Version").toString()).equalsIgnoreCase((toBeSortedDataobject.getElement(j).getField("Version").toString()))){
					if (new ComparableVersion((String) toBeSortedDataobject.getElement(i).getField("InstallationOrder").toString()).compareTo(new ComparableVersion((String) toBeSortedDataobject.getElement(j).getField("InstallationOrder").toString())) == 1) {
                    	temp = toBeSortedDataobject.getElement(j);
                    	toBeSortedDataobject.set(j, toBeSortedDataobject.getElement(i));
                    	toBeSortedDataobject.set(i, temp);
                	}
				}
            }
        }
        return toBeSortedDataobject;
    }

}