package com.volantetech.volante.services;

import com.tplus.transform.runtime.formula.ObjectFunctions;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import java.text.ParseException;
import javax.script.*;
import java.lang.*;
import java.math.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.tplus.transform.runtime.formula.SectionTransformFunctions;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.runtime.formula.SectionFunctions;
import com.tplus.transform.runtime.formula.DataObjectFunctions;

public class VPEParams {
    public DataObject cd;
    List<Object> nullList;

   public VPEParams(DataObject cd) {
        this.cd = cd;
       nullList=new ArrayList<Object>();
       nullList.add("");
       nullList.add(new BigDecimal(-1));
       nullList.add(new Integer(-1));
       nullList.add(new java.util.Date(0,0,0));
       nullList.add(new Boolean(false));
    }

    public Object getString(String section) {

        if (section.indexOf(".") == -1) {
            try {
                return cd.getFieldCheckNull(section);
            }
            catch (FieldNullException e) {
                return  "";
            }

        }
        else {
            String str[] = section.split("\\.");
            try {
                DataObjectSection sectionObjs = cd.getSection(str[0]);
                if (sectionObjs.getElementCount() == 0) {
                    return   "";
                }
                else  {
                    DataObject sectionObj = sectionObjs.getElement(0);
                    return sectionObj.getFieldCheckNull(str[1]);
                }
            }
            catch (FieldNullException e) {
                return  "";
            }
        }
    }
	public boolean PatternContains(String field,String pattern){
		Pattern p=Pattern.compile(pattern);
		Matcher match=p.matcher(field);	
		return match.find();
	}
	public boolean PatternContainsIgnoreCase(String field,String pattern){
		Pattern p=Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
		Matcher match=p.matcher(field);
		return match.find();	
	}
    public boolean IsNullCheck(Object obj){
       if(nullList.contains(obj)){
           return true;
       }
       return false;
    }
    public boolean IsNotNullCheck(Object obj){
        if(nullList.contains(obj)){
            return false;
        }
        return true;
    }
    public Date now() {  
    	Date date = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    	Date gmt = new Date(sdf.format(date));
    	return gmt;
    }
    public Date today(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date today = dateFormat.parse(dateFormat.format(new Date()));
            return today;
        }catch(ParseException p){
            p.printStackTrace();
            return new java.util.Date(0,0,0);
        }
    }
    public Object getBigDecimal(String section) {

        if (section.indexOf(".") == -1) {
            try {
                return cd.getFieldCheckNull(section);
            }
            catch (FieldNullException e) {
                return  new BigDecimal(-1);
            }

        }
        else {
            String str[] = section.split("\\.");
            try {
                DataObjectSection sectionObjs = cd.getSection(str[0]);
                if (sectionObjs.getElementCount() == 0) {
                    return  new BigDecimal(-1);
                }
                else  {
                    DataObject sectionObj = sectionObjs.getElement(0);
                    return sectionObj.getFieldCheckNull(str[1]);
                }
            }
            catch (FieldNullException e) {
                return  new BigDecimal(-1);
            }
        }
    }
    public boolean notEquals(String a,String b){
        return !(a.equals(b));
    }
    public boolean notContains(String a,String b){
        return !(a.contains(b));
    }
    public boolean gt(int a,int b){
        return a>b;
    }
    public boolean lt(int a,int b){
        return a<b;
    }
    public boolean lte(int a,int b){
        return a<=b;
    }
    public boolean eq(int a,int b){
        return a==b;
    }
    public boolean neq(int a,int b){
        return a!=b;
    }
    public int length(String str){
        return str.length();
    }
    public boolean gte(int a,int b) {return a>=b;}
    public Object getInteger(String section) {

        if (section.indexOf(".") == -1) {
            try {
                return cd.getFieldCheckNull(section);
            }
            catch (FieldNullException e) {
                return new Integer(-1);
            }

        }
        else {
            String str[] = section.split("\\.");
            try {
                DataObjectSection sectionObjs = cd.getSection(str[0]);
                if (sectionObjs.getElementCount() == 0) {
                    return new Integer(-1);
                }
                else  {
                    DataObject sectionObj = sectionObjs.getElement(0);
                    return sectionObj.getFieldCheckNull(str[1]);
                }
            }
            catch (FieldNullException e) {
                return new Integer(-1);
            }
        }
    }
    public Object getDate(String section) {

        if (section.indexOf(".") == -1) {
            try {
                return cd.getFieldCheckNull(section);
            }
            catch (FieldNullException e) {
                return  new java.util.Date(0,0,0);
            }
        }
        else {
            String str[] = section.split("\\.");
            try {
                DataObjectSection sectionObjs = cd.getSection(str[0]);
                if (sectionObjs.getElementCount() == 0) {
                    return  new java.util.Date(0,0,0);
                }
                else  {
                    DataObject sectionObj = sectionObjs.getElement(0);
                    return sectionObj.getFieldCheckNull(str[1]);
                }
            }
            catch (FieldNullException e) {
                return  new java.util.Date(0,0,0);
            }
        }
    }
    public Object getBoolean(String section) {

        if (section.indexOf(".") == -1) {
            try {
                return cd.getFieldCheckNull(section);
            }
            catch (FieldNullException e) {
                return  new Boolean(false);
            }

        }
        else {
            String str[] = section.split("\\.");
            try {
                DataObjectSection sectionObjs = cd.getSection(str[0]);
                if (sectionObjs.getElementCount() == 0) {
                    return   new Boolean(false);
                }
                else  {
                    DataObject sectionObj = sectionObjs.getElement(0);
                    return sectionObj.getFieldCheckNull(str[1]);
                }
            }
            catch (FieldNullException e) {
                return  new Boolean(false);
            }
        }
    }
    public void setString(String section,String value)
    {
        if (section.indexOf(".") == -1) {
            try {
                cd.setField(section, value);
            }
            catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        } else {
            try {
                String str[] = section.split("\\.");

                getSection(str[0]).getElement(0).setField(str[1],value);
            }catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        }
    }
    public void setBigDecimal(String section,BigDecimal value)
    {
        if (section.indexOf(".") == -1) {
            try {
                cd.setField(section,value);

            }
            catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        } else {
            try {
                String str[] = section.split("\\.");

                getSection(str[0]).getElement(0).setField(str[1],value);
            }catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        }
    }
    public void setBoolean(String section,Boolean value)
    {
        if (section.indexOf(".") == -1) {
            try {
                cd.setField(section, value);
            }
            catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        }  else {
            try {
                String str[] = section.split("\\.");

                 getSection(str[0]).getElement(0).setField(str[1],value);
            }catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        }


    }
    public void setDate(String section,Date value){
        if (section.indexOf(".") == -1) {
            try {
                cd.setField(section,value);
            }
            catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        }  else {
            try {
                String str[] = section.split("\\.");
                getSection(str[0]).getElement(0).setField(str[1],value);
            }catch (NoSuchFieldError  e) {
                throw new NoSuchFieldError("");
            }
        }


    }
    public void setInteger(String section,Integer value){
        if (section.indexOf(".") == -1) {
            try {
                cd.setField(section,value);
            }
            catch (NoSuchFieldError e) {
                throw new NoSuchFieldError("");
            }
        }  else {
            try {
                String str[] = section.split("\\.");
                getSection(str[0]).getElement(0).setField(str[1],value);
            }catch (NoSuchFieldError  e) {
                throw new NoSuchFieldError("");
            }
        }


    }

    public DataObjectSection getSection(String section) {

        DataObjectSection sectionObjs = cd.getSection(section);
        if (sectionObjs.getElementCount() == 0) {
            sectionObjs.addElement(sectionObjs.createElement());
        }
        return sectionObjs;
    }
}