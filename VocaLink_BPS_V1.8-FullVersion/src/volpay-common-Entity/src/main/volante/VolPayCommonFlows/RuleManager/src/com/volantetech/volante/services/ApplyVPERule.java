package com.volantetech.volante.services;

import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import javax.script.*;
import java.util.Map;


public class ApplyVPERule implements IInvokable {
   public Object run(Object[] args, TransformContext cxt) throws TransformException {
        assertSyntax (args);
        DataObject cd = (DataObject) args[0];
        DataObject alert = (DataObject) args[1];
        DataObject incidence = (DataObject) args[2];
        DataObject ruleElm = (DataObject) args[3];

        VPERuleEngine.initEngine ();
       	SimpleBindings bindings = VPERuleEngine.updateBindings (cd,alert, incidence);
       	DataObject bindingAlertObjAtStart = (DataObject)bindings.get("alert");
        bindingAlertObjAtStart.reset();
		
		DataObject bindingIncidenceObjAtStart = (DataObject)bindings.get("incidence");
        bindingIncidenceObjAtStart.reset();
        
        try {
            CompiledScript script = VPERuleEngine.getCompiledRule (cd, ruleElm);
            script.eval (bindings);
            DataObject bindingAlertObj = (DataObject)bindings.get("alert");
            if(!bindingAlertObj.isEmpty()) {
                for(int index = 0; index < bindingAlertObj.getFieldCount(); index += 1) {
                    alert.setField(bindingAlertObj.getFieldName(index), bindingAlertObj.getField(bindingAlertObj.getFieldName(index)));
                }
            }
            DataObject bindingIncidenceObj = (DataObject)bindings.get("incidence");
            if(!bindingIncidenceObj.isEmpty()) {
                for(int index = 0; index < bindingIncidenceObj.getFieldCount(); index += 1) {
                    incidence.setField(bindingIncidenceObj.getFieldName(index), bindingIncidenceObj.getField(bindingIncidenceObj.getFieldName(index)));
                }
            }
        }catch (ScriptException exc) {
            throw new TransformException ("Unable to execute script. Reason - " + exc.getMessage(), exc);
        }
        return new Object();
   }

   private void assertSyntax (Object[] args) throws TransformException {
       if ((args.length != 4) || (!(args[0] instanceof DataObject)) || (!(args[1] instanceof DataObject)) || (!(args[2] instanceof DataObject)) || (!(args[3] instanceof DataObject)))
           throw new TransformException ("Exactly three arguments needed to apply vpe rule - CD, Alert, Incidence, ruleObj.");
   }
}
