package com.volantetech.volante.services;

import com.tplus.transform.runtime.*;
import javax.script.*;
import java.util.HashMap;
import java.util.Map;


public class VPERuleEngine {
	static ScriptEngine scriptEngine;
	static Map<String, CompiledScript> compiledRulesMap;
    //static com.tplus.transform.util.log.Log log = com.tplus.transform.runtime.log.LogFactory.getRuntimeLog();

    private static SimpleBindings createBindings (DataObject cd, DataObject alert, DataObject incidence) {
        SimpleBindings bindings = new SimpleBindings();
        bindings.put ("cd", cd);
        bindings.put ("alert", alert);
        bindings.put("incidence", incidence);
		/*Object hashMapObj = cd.getProperty("HASHMAP_PROP");
		if (hashMapObj != null) {
			HashMap<String, Object> cdHashMap = (HashMap) hashMapObj;
			for (Map.Entry<String, Object> entry : cdHashMap.entrySet()) {
				Object val = entry.getValue();

                String key = entry.getKey();
                if (val instanceof DataObject)
                    bindings.put (key, (DataObject) val);
				else if (val instanceof DataObjectSection)
                    bindings.put (key, (DataObjectSection) val);
				else
                    bindings.put (key, val);
			}
		}*/
        return bindings;
    }


    static SimpleBindings updateBindings (DataObject cd, DataObject alert, DataObject incidence) {
        SimpleBindings bindings;
        String BINDINGS_PROPERTY = "Bindings";
        if (cd.hasProperty (BINDINGS_PROPERTY)){
            bindings = (SimpleBindings) cd.getProperty(BINDINGS_PROPERTY);
        }
        else
            bindings = createBindings(cd,alert, incidence);
		VPEParams params = new VPEParams (cd);
        bindings.put ("br", params);

        cd.setProperty (BINDINGS_PROPERTY, bindings);
        scriptEngine.setBindings (bindings, ScriptContext.ENGINE_SCOPE);
        return bindings;
    }

	static CompiledScript getCompiledRule (DataObject ruleElm) throws TransformException {
		return getCompiledRule (null,ruleElm);
	}
	
    static CompiledScript getCompiledRule (DataObject cd, DataObject ruleElm) throws TransformException {
        String mapKey = cd != null ? getMapKey(cd, ruleElm) : getMapKey(ruleElm);
        String scriptStr = (String) ruleElm.getField ("Rule");

        synchronized (VPERuleEngine.class) {
            instantiateRulesMap();
            CompiledScript script = compiledRulesMap.get(mapKey);
            if (script == null) {
                try {
                    Compilable engine = (Compilable) scriptEngine;
                    script = engine.compile (scriptStr);
                }
                catch (ScriptException exc) {
                    throw new TransformException("Unable to compile script. Reason - " + exc.getMessage(), exc);
                }

                compiledRulesMap.put(mapKey, script);
            }

            return script;
        }
    }
    private static String getMapKey(DataObject ruleElm) {
        final String OfficeCode = (String) ruleElm.getField ("OfficeCode");
        final String RuleCode = (String) ruleElm.getField ("RuleCode");
        return OfficeCode + "_" + RuleCode;
    }

    private static String getMapKey(DataObject cd, DataObject ruleElm) {
        final String OfficeCode = (String) cd.getField ("OfficeCode");
        final String RuleCode = (String) ruleElm.getField ("RuleCode");
        return OfficeCode + "_" + RuleCode;
    }

    private static void instantiateRulesMap() {
		if (compiledRulesMap == null)
			compiledRulesMap = new HashMap<String, CompiledScript>();
	}

	public static void clearRulesMap() {
		if (compiledRulesMap != null)
			compiledRulesMap.clear();
		compiledRulesMap = new HashMap<String, CompiledScript>();
	}
	


	static void initEngine () throws TransformException {
		synchronized (VPERuleEngine.class) {
            if (scriptEngine == null) {
                ScriptEngineManager factory = new ScriptEngineManager();
                String scriptEngineName = "JavaScript"; // groovy
                scriptEngine = factory.getEngineByName(scriptEngineName);
                if (scriptEngine == null)
                    throw new TransformException("Unable to instantiate '" + scriptEngineName + "' script engine");
            }
        }
	}
}
