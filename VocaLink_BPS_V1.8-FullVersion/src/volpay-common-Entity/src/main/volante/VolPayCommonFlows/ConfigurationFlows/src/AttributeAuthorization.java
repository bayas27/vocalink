package com.volantetech.volante.services;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AttributeAuthorization implements IInvokable {
	int attributeIntValue;
	int bodyDataIntValue;
	boolean isInteger;
	boolean isDate;
	boolean isString;
	boolean isBoolean;
	boolean isBigDecimal;
	String attributeValue;
	String bodyDataValue;
	Date attributeDateValue;
	Date bodyDataDateValue;
	double attributeDoubleValue;
	double bodyDataDoubleValue;

	public Object run(Object[] args, TransformContext cxt) throws TransformException {
    	com.tplus.transform.util.log.Log log = (com.tplus.transform.util.log.Log)com.tplus.transform.runtime.log.LogFactory.getRuntimeLog( "" );
		if ((args.length != 2) || !(args[0] instanceof DataObjectSection) || !(args[1] instanceof DataObject)) {
			throw new TransformException ("Add to cache needs to be invoked with exactly two arguments (Cache Name - type: String, Object to be cached - type: Data Object)");
		}
		DataObjectSection AttributesDataObjectSection = (DataObjectSection) args[0];
		DataObject bodyDataObject = (DataObject) args[1];
		for(int i = 0; i < AttributesDataObjectSection.getElementCount(); i++){
		    isInteger=false;
		    isDate=false;
		    isString=false;
		    isBoolean=false;
		    isBigDecimal=false;
		    for(int j = 0; j < bodyDataObject.getFieldCount(); j++){
				DataObject AttributesDataObject = AttributesDataObjectSection.getElement(i);
				String attributeColumnName = String.valueOf(AttributesDataObject.getField(0));
				String attributeOperator = String.valueOf(AttributesDataObject.getField(2));
				if(attributeColumnName.equalsIgnoreCase(bodyDataObject.getFieldName(j))){
					if(bodyDataObject.getField(j)==null)
					{ if(attributeOperator.equalsIgnoreCase("!="))
						return true;
					else
						return false;
					}
				}
				if(attributeColumnName.equalsIgnoreCase(bodyDataObject.getFieldName(j))){
					if(bodyDataObject.getFieldType(j).equalsIgnoreCase("Integer")){
						if(attributeOperator.equalsIgnoreCase("IN")){
							attributeValue = String.valueOf(AttributesDataObject.getField(1));
						} else {
							attributeIntValue = Integer.parseInt(String.valueOf(AttributesDataObject.getField(1)));
						}
						bodyDataIntValue = Integer.parseInt(String.valueOf(bodyDataObject.getField(j)));
						isInteger = true;
					}
 else if(bodyDataObject.getFieldType(j).equalsIgnoreCase("BigDecimal")){
						if(attributeOperator.equalsIgnoreCase("IN")){
							attributeValue = String.valueOf(AttributesDataObject.getField(1));
						} else {
							attributeDoubleValue = Double.parseDouble(String.valueOf(AttributesDataObject.getField(1)));
						}
						bodyDataDoubleValue = Double.parseDouble(String.valueOf(bodyDataObject.getField(j)));
						isBigDecimal = true;
					} else if(bodyDataObject.getFieldType(j).equalsIgnoreCase("DateOnly")){
						attributeValue = String.valueOf(AttributesDataObject.getField(1));
						bodyDataValue = String.valueOf(bodyDataObject.getField(j));
						try {
							attributeDateValue = new SimpleDateFormat("yyyy-MM-dd").parse(attributeValue);
            				bodyDataDateValue = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy").parse(bodyDataValue);
            			} catch (ParseException e){
            					throw new TransformException("Attribute configuration issue: "+ e.getMessage());
            					//return false;
            			}
						isDate = true;
					} else if(bodyDataObject.getFieldType(j).equalsIgnoreCase("Boolean")){
						attributeValue = String.valueOf(AttributesDataObject.getField(1));
						bodyDataValue = String.valueOf(bodyDataObject.getField(j));
						if(bodyDataValue != null && !bodyDataValue.equals("")){
							if(!attributeOperator.equalsIgnoreCase("IN")){
								attributeIntValue = Integer.parseInt(attributeValue);
							}
							if(bodyDataValue.equalsIgnoreCase("true")){
            					bodyDataIntValue = 1;
            				} else if(bodyDataValue.equalsIgnoreCase("false")){
            					bodyDataIntValue = 0;
            				} else {
            					bodyDataIntValue = Integer.parseInt(bodyDataValue);
            				}
            				isBoolean = true;
            			} else {
            				return false;
            			}
					} else {
						attributeValue = String.valueOf(AttributesDataObject.getField(1));
						bodyDataValue = String.valueOf(bodyDataObject.getField(j));
						isString = true;
					}

					if(attributeOperator.equalsIgnoreCase("=")){
						if(isInteger){
							if(bodyDataIntValue != attributeIntValue){
								return false;
							}
						} else if(isBigDecimal){
							if(bodyDataDoubleValue != attributeDoubleValue){
								return false;
							}
						} else if(isDate){
							if(bodyDataDateValue.compareTo(attributeDateValue) != 0){
								return false;
							}
						} else if(isBoolean){
							if(bodyDataIntValue != attributeIntValue){
								return false;
							}
						} else {
							if(!bodyDataValue.equals(attributeValue)){
								return false;
							}
						}
					}else if(attributeOperator.equalsIgnoreCase("!=")){
						if(isInteger){
							if(bodyDataIntValue == attributeIntValue){
								return false;
							}
						} else if(isBigDecimal){
							if(bodyDataDoubleValue == attributeDoubleValue){
								return false;
							}
						} else if(isDate){
							if(bodyDataDateValue.compareTo(attributeDateValue) == 0){
								return false;
							}
						} else if(isBoolean){
							if(bodyDataIntValue == attributeIntValue){
								return false;
							}
						} else {
							if(bodyDataValue.equals(attributeValue)){
								return false;
							}
						}
					}else if(attributeOperator.equalsIgnoreCase("<")){
						if(isInteger){
							if(bodyDataIntValue >= attributeIntValue){
								return false;
							}
						} else if(isBigDecimal){
							if(bodyDataDoubleValue >= attributeDoubleValue){
								return false;
							}
						} else if(isDate){
							if((bodyDataDateValue.compareTo(attributeDateValue) == 0) || (bodyDataDateValue.compareTo(attributeDateValue) > 0)){
								return false;
							}
						} else {
							throw new TransformException("Operator '<' is supported only for Integer and Date");

						}
					}else if(attributeOperator.equalsIgnoreCase(">")){
						if(isInteger){
							if(bodyDataIntValue <= attributeIntValue){
								return false;
							}
						} else if(isBigDecimal){
							if(bodyDataDoubleValue <= attributeDoubleValue){
								return false;
							}
						} else if(isDate){
							if((bodyDataDateValue.compareTo(attributeDateValue) == 0) || (bodyDataDateValue.compareTo(attributeDateValue) < 0)){
								return false;
							}
						} else {
							throw new TransformException("Operator '>' is supported only for Integer and Date");
						}
					}else if(attributeOperator.equalsIgnoreCase("<=")){
						if(isInteger){
							if(bodyDataIntValue > attributeIntValue){
								return false;
							}
						} else if(isBigDecimal){
							if(bodyDataDoubleValue > attributeDoubleValue){
								return false;
							}
						} else if(isDate){
							if(bodyDataDateValue.compareTo(attributeDateValue) > 0){
								return false;
							}
						} else {
							throw new TransformException("Operator '<=' is supported only for Integer and Date");
						}
					}else if(attributeOperator.equalsIgnoreCase(">=")){
						if(isInteger){
							if(bodyDataIntValue < attributeIntValue){
								return false;
							}
						} else if(isBigDecimal){
							if(bodyDataDoubleValue < attributeDoubleValue){
								return false;
							}
						} else if(isDate){
							if(bodyDataDateValue.compareTo(attributeDateValue) < 0){
								return false;
							}
						} else {
							throw new TransformException("Operator '>=' is supported only for Integer and Date");
						}
					}
else if(attributeOperator.equalsIgnoreCase("IN")){
						boolean valueAvailable = false;
						attributeValue = attributeValue.replace("'", "");
						String attributeValueArg[] = attributeValue.split(",");
						if(isString){
							for(String value : attributeValueArg){
								if(bodyDataValue.equals(value.trim())){
									valueAvailable = true;
								}
							}
							if(!valueAvailable){
								return false;
							}
						} 
else if(isInteger){
							for(String value : attributeValueArg){
								if(bodyDataIntValue ==  Integer.parseInt(value.trim())){
									valueAvailable = true;
								}
							}
							if(!valueAvailable){
								return false;
							}
						} else if(isBigDecimal){
							for(String value : attributeValueArg){
								if(bodyDataDoubleValue ==  Double.parseDouble(value.trim())){
									valueAvailable = true;
								}
							}
							if(!valueAvailable){
								return false;
							}
						} else if(isBoolean){
							for(String value : attributeValueArg){
								if(value.equalsIgnoreCase("true")){
            						bodyDataIntValue = 1;
            					} else if(value.equalsIgnoreCase("false")){
            						bodyDataIntValue = 0;
            					}
            					try {
									if(bodyDataIntValue ==  Integer.parseInt(value.trim())){
										valueAvailable = true;
									}
								} catch (NumberFormatException e) {
									throw new TransformException("For Boolean Attribute , only 'true' or 'false' value is allowed");
								}
							}
							if(!valueAvailable){
								return false;
							}
						} else {
							throw new TransformException("Operator 'IN' is supported only for Integer, Boolean and String");
						}
					}else{
						return false;
					}

				
                                  
				}
			}
		}
			
       return true;
   }
}
