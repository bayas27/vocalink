package com.volantetech.volante.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class LogObjectError {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow()  throws javax.naming.NamingException {
        // look up message flow and cache
        if(messageFlow == null) {
			// Get the lookup context for the current environment
			LookupContext lcxt = LookupContextFactory.getLookupContext();

			// Lookup message flow (defined in the cartridge)
			messageFlow = lcxt.lookupMessageFlow("Log_Object_Error");
        }
        return messageFlow;
    }

    public static void process(DataObject LoggingObj) {
	    try {
	        MessageFlow messageFlow = getMessageFlow();

			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
			//DataObject LoggingObj = ...;
	      	Object[] messageFlowArgs = new Object[] {  LoggingObj,  };

	      	// Execute the message flow.
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);

			// Use output

	    }
	    catch(TransformException e) {
	      System.err.println(e.toXMLString());
	    }
	    catch(javax.naming.NamingException e) {
	      e.printStackTrace();
	    }
	    catch(java.rmi.RemoteException e) {
	      e.printStackTrace();
	    }
	    catch(java.io.IOException e) {
	      e.printStackTrace();
	    }
	}
  /*  public static void main(String[] args) {
        com.tplus.transform.util.LoggingUtil.enableLogging("log.xml");
        String fileName = "data.txt";
        if(args.length > 0) {
            fileName = args[0];
        }
        process(fileName);
    }*/
}