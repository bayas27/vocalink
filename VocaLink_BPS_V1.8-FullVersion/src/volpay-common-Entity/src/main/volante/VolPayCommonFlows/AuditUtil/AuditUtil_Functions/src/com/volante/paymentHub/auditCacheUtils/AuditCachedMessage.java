package com.volante.paymentHub.auditCacheUtils;

import com.tplus.transform.runtime.Message;
import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.formula.MessageFunctions;
import com.tplus.transform.runtime.handler.IInvokable;

/**
 * Created by Ayyanar on 29-03-2017.
 */
public class AuditCachedMessage {
    private static RawMessage rawMessage  = null;
    private AuditCachedMessage(){

    }

    public static RawMessage getInstance() throws TransformException {
        if(rawMessage==null)
            rawMessage= MessageFunctions.newCachedMessage();
        return rawMessage;
    }

    public static void addAuditcache(RawMessage rawMessageContent) throws TransformException {
        RawMessage rawMessage =getInstance();
        rawMessage.append(rawMessageContent);
    }


    public static  RawMessage getauditContents(){
        return rawMessage;
    }

    public static void close() throws TransformException {
        if (rawMessage != null) {
            rawMessage.dispose();
        }
    }

}
