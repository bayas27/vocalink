package com.volante.paymentHub.auditCacheUtils;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;


public class AuditCacheUtilFlushCache implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 0)
			throw new TransformException ("Flush cache should be invoked with no arguments");

		DataObjectSection cache = AuditCacheUtil.getCache ();
		return cache;
	}
}
