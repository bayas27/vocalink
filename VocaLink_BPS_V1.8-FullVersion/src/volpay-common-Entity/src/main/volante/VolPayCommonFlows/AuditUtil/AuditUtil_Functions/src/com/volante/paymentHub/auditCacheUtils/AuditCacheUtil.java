package com.volante.paymentHub.auditCacheUtils;

import com.tplus.transform.runtime.*;

public class AuditCacheUtil {

	static DataObjectCollectionImpl auditColl;
	
	
	public synchronized static void addToCache (DataObject auditElm) {
		if (auditColl == null)
			auditColl = new DataObjectCollectionImpl();

		auditColl.addElement (auditElm);
	}

	public synchronized static DataObjectCollectionImpl flushCache() {
		DataObjectCollectionImpl auditCollLocal = auditColl.clone();
		auditColl.clear();
		return auditCollLocal;
	}
	
	public synchronized static int getLength() {
		return auditColl.getElementCount();
	}

}
