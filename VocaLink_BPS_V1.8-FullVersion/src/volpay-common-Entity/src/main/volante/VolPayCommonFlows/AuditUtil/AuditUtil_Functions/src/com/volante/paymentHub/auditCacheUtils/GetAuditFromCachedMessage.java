package com.volante.paymentHub.auditCacheUtils;

import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;

/**
 * Created by Ayyanar on 29-03-2017.
 */
public class GetAuditFromCachedMessage implements IInvokable {

    public Object run(Object[] objects, TransformContext transformContext) throws TransformException {
        RawMessage rawMessage = AuditCachedMessage.getauditContents();
        return rawMessage;
    }
}
