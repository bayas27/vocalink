package com.volante.paymentHub.auditCacheUtils;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;


public class AuditCacheUtilAddToCache implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 1) || !(args[0] instanceof DataObject))
			throw new TransformException ("Add to cache needs to be invoked with exactly one argument of type Data Object");

		DataObject elm = (DataObject) args[0];
		AuditCacheUtil.addToCache (elm);

		return new Object();
	}

}
