package com.volante.paymentHub.auditCacheUtils;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;


public class AuditCacheUtilGetCacheLength implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 0)
			throw new TransformException ("Get cache length should be invoked with no arguments");

		return new Integer (AuditCacheUtil.getLength ());
	}
}
