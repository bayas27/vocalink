package com.volante.services;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;

/**
 * Created by Ayyanar on 29-03-2017.
 */
public class CloseAuditCache implements IInvokable {
    public Object run(Object[] objects, TransformContext transformContext) throws TransformException {
        AuditCachedMessage.close();
        return new Object();
    }
}
