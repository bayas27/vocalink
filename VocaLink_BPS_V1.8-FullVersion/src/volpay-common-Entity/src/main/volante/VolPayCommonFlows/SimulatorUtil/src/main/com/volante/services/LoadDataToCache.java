package com.volante.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class LoadDataToCache {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow()  throws javax.naming.NamingException {
        // look up message flow and cache
        if(messageFlow == null) {
			// Get the lookup context for the current environment
			LookupContext lcxt = LookupContextFactory.getLookupContext();

			// Lookup message flow (defined in the cartridge)
			messageFlow = lcxt.lookupMessageFlow("LoadInputDataToCache");
        }
        return messageFlow;
    }

    public static void process(String fileName) throws TransformException {
	    try {
	        MessageFlow messageFlow = getMessageFlow();

			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
	      	RawMessage rawIn  = new FileInputSource(fileName);
	      	Object[] messageFlowArgs = new Object[] {  rawIn,  };

	      	// Execute the message flow.
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);

			// Use output

	    }
	    catch(Exception e) {
	   throw  new TransformException(e.getMessage());
	    }

	}
    public static void main(String[] args) throws TransformException {
        com.tplus.transform.util.LoggingUtil.enableLogging("log.xml");
        String fileName = "data.txt";
        if(args.length > 0) {
            fileName = args[0];
        }
        process(fileName);
    }
}