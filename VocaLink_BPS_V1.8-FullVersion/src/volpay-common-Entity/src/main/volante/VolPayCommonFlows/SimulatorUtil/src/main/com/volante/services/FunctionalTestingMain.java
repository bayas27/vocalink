package com.volante.services;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.FileInputSource;
import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformException;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

/**
 * Created by Ayyanar on 3/29/2017.
 */
public class FunctionalTestingMain {
    static String task = null;
    static TreeSet<String> treeSet = new TreeSet<String>();
    static String cacheDataFileName = null;
    static String inputPath = null;
    static String outputPath = null;
    static String outputFileName = null;


    public static void main(String[] args) throws Exception {
        if (args.length != 3)
            throwException("Invalid argument - Argument must containg <TaskName InputLocation OutPutLocation>");
        task = args[0];
        inputPath = args[1];
        outputPath = args[2];
        readAllFile(inputPath);
    }

    /**
     *
     * @param inputPath
     * @throws Exception
     */
    //go to paths
    public static void readAllFile(String inputPath) throws Exception {
        File inputFile = new File(inputPath);
        if (inputFile.exists() && inputFile.isDirectory()) {
            File[] listOfFiles = inputFile.listFiles();
            for (File readFile : listOfFiles) {
                if (readFile.isFile()) {
                    cacheDataFileName = getFileName(readFile.getName());
                    loadCachedEntry(readFile.getPath());
                    System.out.println("Top file  " + cacheDataFileName + " Path -- > " + readFile.getPath());
                    loadEachFolder(readFile.getParent() + File.separator + cacheDataFileName);
                }
            }
        } else {
            throwException("Entered location not exists!!!!");

        }

    }

    /**
     *
     * @param fileNameWithExtension
     * @return
     */
    public static String getFileName(String fileNameWithExtension) {
        if (fileNameWithExtension == null) return null;
        int pos = fileNameWithExtension.indexOf(".");
        if (pos == -1) return fileNameWithExtension;
        return fileNameWithExtension.substring(0, pos);

    }

    public static void loadEachFolder(String subFolderPath) throws Exception {
        File subFolder = new File(subFolderPath);
        if (subFolder.exists() && subFolder.isDirectory()) {
            addFileNameToSet(subFolder.listFiles());
            processFileForInput(subFolderPath);
        } else {
            throwException("unknown folder location !! " + subFolderPath);
        }
    }

    public static int validateFileCountAndProcess(File[] listOfFile) throws Exception {
        int fileCount = 0;
        String[] fileLocations = new String[3];
        for (File listOfFiles : listOfFile) {
            if (listOfFiles.isFile()) {
                String currentFileName = listOfFiles.getName();
                outputFileName = getFileName(listOfFiles.getName());
                if (currentFileName.toUpperCase().endsWith(".PFD") || currentFileName.toUpperCase().endsWith(".PCD") || currentFileName.toUpperCase().endsWith(".PMD")) {
                    fileLocations[fileCount] = listOfFiles.getPath();
                    fileCount = fileCount + 1;

                }
            }
        }
        if (fileCount == 3 && fileLocations.length == 3) {
            processFileForInput(fileLocations);
        }
        return fileCount;
    }

    public static void addFileNameToSet(File[] listOfFile) {
        for (File listOfFiles : listOfFile) {
            if (listOfFiles.isFile()) {
                treeSet.add(getFileName(listOfFiles.getName()));
            }
        }
    }

    public static void processFileForInput(String[] listOfPath) throws Exception {
        getFlowsInput(listOfPath, task);
    }

    public static void processFileForInput(String directory) throws Exception {
        if (!treeSet.isEmpty()) {
            Iterator<String> iterator = treeSet.iterator();
            while (iterator.hasNext()) {
                final String fileName = iterator.next();
                File fOBj = new File(directory);
                if (fOBj.exists() && fOBj.isDirectory()) {
                    File[] foundFiles = fOBj.listFiles(new FilenameFilter() {
                        public boolean accept(File dir, String name) {
                            System.out.println("Filter Name " + name);
                            return getFileName(name).equals(fileName);

                        }
                    });

                    validateFileCountAndProcess(foundFiles);
                }
            }

        }
    }

    public static void throwException(String exMsg) throws Exception {
        throw new Exception(exMsg);
    }

    public static void loadCachedEntry(String cachedEntryFile) throws TransformException {
        LoadDataToCache.process(cachedEntryFile);
    }

    public static void genericCallTaskFlow(String pfdLoc, String pcdLoc, String pmdLoc, String taskName) throws Exception {
        Object[] process = GenericTaskTestMain.process(pcdLoc, pfdLoc, pmdLoc, taskName);
        for (int i = 0; i < process.length; i++) {
            String extension = null;
            if (i == 0) {
                //Payment Control Data
                extension = "PCD";
            } else if (i == 1) {
                //Payment Field Data
                extension = "PFD";
            } else if (i == 2) {
                //Payment Meta Data
                extension = "PMD";
            } else if (i == 3) {
                //General Error Information
                extension = "GEI";
            } else if (i == 4) {
                extension = "AUD";
            } else if (i == 5) {
                //Payment Error Information
                extension = "PEI";
            }
            if (process[i] != null) {
                String fileContent = null;
                if (process[i] instanceof DataObject) {
                    fileContent = ((DataObject) process[i]).toXMLString();
                } else if (process[i] instanceof RawMessage) {
                    fileContent = ((RawMessage) process[i]).getAsString();
                }
                writeOutput(outputPath + File.separator + cacheDataFileName, fileContent, extension);
            }
        }
    }

    public static void getFlowsInput(String[] listLocation, String taskName) throws Exception {
        String pfdLoc = null;
        String pcdLoc = null;
        String pmdLoc = null;
        for (int i = 0; i < listLocation.length; i++) {
            if (listLocation[i].toUpperCase().endsWith(".PFD")) {
                pfdLoc = listLocation[i];
            } else if (listLocation[i].toUpperCase().endsWith(".PCD")) {
                pcdLoc = listLocation[i];
            } else if (listLocation[i].toUpperCase().endsWith(".PMD")) {
                pmdLoc = listLocation[i];
            }
        }
        genericCallTaskFlow(pfdLoc, pcdLoc, pmdLoc, taskName);

    }

    public static void writeOutput(String outputLocation, String outObj, String fileExtenstion) throws Exception {
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        RawMessage a = new FileInputSource("a");
        try {
            File fObj = new File(outputLocation);
            if (!fObj.exists()) {
                fObj.mkdirs();
            }
            fObj = new File(fObj.getPath() + File.separator + outputFileName + "." + fileExtenstion);
            if (!fObj.exists()) {
                fObj.createNewFile();
            }
            fileWriter = new FileWriter(fObj.getAbsoluteFile(), true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(outObj);
        } catch (Exception e) {
            throwException(e.getMessage());
        } finally {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
    }

    public static  void procesTaskTesting(String taskName,String inputLoc,String outputLoc) throws Exception {
        task = taskName;
        inputPath= inputLoc;
        outputPath= outputLoc;
        readAllFile(inputLoc);
    }
}
