package com.volante.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;

import javax.xml.crypto.dsig.Transform;
import java.io.IOException;

public class GenericTaskTestMain {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow()  throws javax.naming.NamingException {
        // look up message flow and cache
        if(messageFlow == null) {
			// Get the lookup context for the current environment
			LookupContext lcxt = LookupContextFactory.getLookupContext();

			// Lookup message flow (defined in the cartridge)
			messageFlow = lcxt.lookupMessageFlow("GenericTaskTestUtil");
        }
        return messageFlow;
    }

    public static Object[] process(String pcdfile,String pfdFile,String pmdFile,String taskname) throws TransformException {
	    try {
	        MessageFlow messageFlow = getMessageFlow();

			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
	      	RawMessage pcd  = new FileInputSource(pcdfile);
	      	RawMessage pfd  = new FileInputSource(pfdFile);
	      	RawMessage pmd  = new FileInputSource(pmdFile);

	      	Object[] messageFlowArgs = new Object[] {  pcd,  pfd,  pmd,  taskname,  };

	      	// Execute the message flow.
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);

			// Use output
            DataObject pcdObj = (DataObject)output[0];
            DataObject pfdObj = (DataObject)output[1];
            DataObject pmdObj = (DataObject)output[2];
            DataObject exObj = (DataObject)output[3];
            RawMessage audit = (RawMessage) output[4];
			//System.out.println(pcdObj.toXMLString());
            return output;

	    }
	    catch(Exception e) {
            e.printStackTrace();
	        throw new TransformException(e.getMessage());
	    }

	}
    public static void main(String[] args) throws TransformException {
        com.tplus.transform.util.LoggingUtil.enableLogging("log.xml");
        if(args.length!=4) {
            throw  new TransformException("Invalid aruguments");
        }
        process(args[0],args[1],args[2],args[3]);
    }
}