package com.volante.services;

import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.formula.MessageFunctions;

/**
 * Created by Ayyanar on 29-03-2017.
 */
public class PaymentErrorCachedMessage {
    private static RawMessage rawMessage  = null;
    private PaymentErrorCachedMessage(){

    }

    public static RawMessage getInstance() throws TransformException {
        if(rawMessage==null)
            rawMessage= MessageFunctions.newCachedMessage();
        return rawMessage;
    }

 /*   public static void addAuditcache(RawMessage rawMessageContent) throws TransformException {
        RawMessage rawMessage =getInstance();
        rawMessage.append(rawMessageContent);
    }*/


   /* public static  RawMessage getauditContents(){
        return rawMessage;
    }*/

    public static void close() throws TransformException {
        if (rawMessage != null) {
            rawMessage.dispose();
            rawMessage = null;
        }
    }

}
