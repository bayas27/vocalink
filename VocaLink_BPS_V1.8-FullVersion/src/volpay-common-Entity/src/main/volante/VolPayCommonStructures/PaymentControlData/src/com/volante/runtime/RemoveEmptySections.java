package com.volante.runtime;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;

public class RemoveEmptySections implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
       DataObject obj = (DataObject) args[0];
       // perform the operation here
       removeEmptyFields(obj);
       return null;
    }
   public static void removeEmptyFields(DataObject obj) {
        DataObjectMetaInfo metaInfo = obj.getMetaInfo();
        int count = metaInfo.getFieldCount();
        //Iterate through all the fields
        for (int i = 0; i < count; ++i) {
            FieldMetaInfo fieldInfo = metaInfo.getFieldMetaInfo(i);
            if (fieldInfo.isSection()) {
                Object secObj = obj.getField(i);
                if (secObj instanceof DataObjectSection) {
                    DataObjectSection sec = (DataObjectSection) secObj;
                    for (int j = 0; j < sec.size(); j++) {
                        DataObject secElm = (DataObject) sec.get(j);
                        // Recursively descent to the next level
                        removeEmptyFields(secElm);
                        // If all the sub-fields of the section are null
                        if(secElm.isEmpty()) {
                            // Remove the element from the section
                            sec.remove(j);
                            --j;
                        }
                    }
                }
                if (secObj instanceof DataObject) {
                    DataObject subObj = (DataObject) secObj;
                    // Recursively descent to the next level
                    removeEmptyFields(subObj);
                }
            }
            else if (fieldInfo.getDesignerType() == DesignerTypes.STRING_TYPE) {
                String fieldValue = (String) obj.getField(i);
                // If the field's value is empty string
                //if (fieldValue != null && fieldValue.length() == 0) { ==> Original code..
                if (fieldValue != null && fieldValue.trim().length() == 0) {
                    //set field to null
                    obj.setNull(i);
                }
            }
        }
    }
}