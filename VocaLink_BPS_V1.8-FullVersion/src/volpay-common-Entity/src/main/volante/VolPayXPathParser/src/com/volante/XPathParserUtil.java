package com.volante;

import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


public class XPathParserUtil implements IInvokable {
	
	
	static com.tplus.transform.util.log.Log log = com.tplus.transform.runtime.log.LogFactory.getRuntimeLog("XPathParserUtil");

	public Object run(Object[] args, TransformContext cxt)
			throws TransformException {

		DocumentBuilderFactory documentBuilderFactory = null;
		DocumentBuilder documentBuilder = null;
		Document doc = null;
		XPathFactory xpathFactory = null;
		XPath xpath = null;
		String expression = null;
		ByteArrayInputStream input = null;

		try {

			input = new ByteArrayInputStream(args[0].toString().getBytes("UTF-8"));
			expression = getXPathExpression((String) args[1]);

			documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(true);
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			doc = documentBuilder.parse(input);
			doc.getDocumentElement().normalize();
			
			xpathFactory = XPathFactory.newInstance();
			xpath = xpathFactory.newXPath();

			return (String) xpath.compile(expression).evaluate(doc,	XPathConstants.STRING);
			
		} catch (ParserConfigurationException e) {
			 log.error("[Exception: " + e.toString() + " ], [Root Cause: " + e.getCause() + "], [Stack Trace: " + getStackTrace(e) + "]");
		} catch (SAXException e) {
			 log.error("[Exception: " + e.toString() + " ], [Root Cause: " + e.getCause() + "], [Stack Trace: " + getStackTrace(e) + "]");
		} catch (IOException e) {
			 log.error("[Exception: " + e.toString() + " ], [Root Cause: " + e.getCause() + "], [Stack Trace: " + getStackTrace(e) + "]");
		} catch (XPathExpressionException e) {
			 log.error("[Exception: " + e.toString() + " ], [Root Cause: " + e.getCause() + "], [Stack Trace: " + getStackTrace(e) + "]");
		} catch (Exception e) {
			 log.error("[Exception: " + e.toString() + " ], [Root Cause: " + e.getCause() + "], [Stack Trace: " + getStackTrace(e) + "]");
		}
		return null;
	}

	public String getXPathExpression(String xpathStr) throws Exception{

		String[] strArray = null;
		String xPathParse = null;
		
		strArray = xpathStr.split("\\.");
		for (int i = 0; i < strArray.length; i++) {
			if (i == 0)
				xPathParse = "//*[local-name()='" + strArray[i] + "']";
			else
				xPathParse = xPathParse + "//*[local-name()='" + strArray[i] + "']";

		}
		return xPathParse+"/text()";
	}
	
	private static String getStackTrace(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

}