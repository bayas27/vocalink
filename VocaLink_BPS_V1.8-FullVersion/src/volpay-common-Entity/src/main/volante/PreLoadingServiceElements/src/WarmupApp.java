package com.volantetech.services;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.formula.LookupHelper;
import com.tplus.transform.util.LoggingUtil;
import com.tplus.transform.util.log.Log;
import com.tplus.transform.util.log.LogFactory;

import javax.naming.NamingException;
import java.rmi.RemoteException;

public class WarmupApp {

    private static Log logger = LogFactory.getLog(WarmupApp.class);

    public static void warmup() throws RemoteException, NamingException {
        warmup(LookupHelper.getLookupContext());
    }

    public static void warmup(LookupContext lookupContext) throws RemoteException, NamingException {

        logger.info("[ApplicationStartup] Starting warm up!");
        final String[] messages = lookupContext.getMessages();
        for (int i = 0; i < messages.length; i++) {
            String messageName = messages[i];
            final Message message = lookupContext.lookupMessage(messageName);
            logger.debug("[ApplicationStartup] Warming up message:" + messageName);
            try {
                message.createObject();
            } catch (Throwable e) {
                logger.error("[ApplicationStartup] Error warming up message: " + messageName);
                logger.error(e);
            }
        }
        final String[] mappings = lookupContext.getMessageMappings();
        for (int i = 0; i < mappings.length; i++) {
            String mapping = mappings[i];
            final MessageMapping messageMapping = lookupContext.lookupMessageMapping(mapping);
            logger.debug("[ApplicationStartup] Warming up mapping:" + mapping);
            try {
                final MappingInfo mappingInfo = messageMapping.getMappingInfo();
            } catch (Throwable e) {
                logger.error("[ApplicationStartup] Error warming up mapping: " + mapping);
                logger.error(e);
            }
        }

        final String[] flows = lookupContext.getMessageFlows();
        for (int i = 0; i < flows.length; i++) {
            String flowName = flows[i];
            final MessageFlow flow = lookupContext.lookupMessageFlow(flowName);
            logger.debug("[ApplicationStartup] Warming up flow:" + flowName);
            try {
                flow.createInputDataObject();
            } catch (Throwable e) {
                logger.error("[ApplicationStartup] Error warming up flow: " + flowName);
                logger.error(e);
            }
        }
        logger.info("[ApplicationStartup] Completed warm up!");
    }
}