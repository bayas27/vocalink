package com.volantetech.services;

import com.tplus.transform.runtime.*;
import com.tplus.transform.util.log.Log;
import com.tplus.transform.util.log.LogFactory;

import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.rmi.RemoteException;

public class ApplicationWarmupListener implements ServletContextListener {
    private static Log logger = LogFactory.getLog("ApplicationWarmupListener");
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            LookupContext lcxt = LookupContextFactory.getLookupContext();
            MessageFlow messageFlow = lcxt.lookupMessageFlow("WarmupFlow");
            TransformContext cxt = new TransformContextImpl();
            Object[] messageFlowArgs = new Object[] {};
            // Execute the message flow.
            Object[] output = messageFlow.run(messageFlowArgs, cxt);
        } catch (NamingException e) {
            logger.error(e);
        } catch (TransformException e) {
            logger.error(e);
        } catch (RemoteException e) {
            logger.error(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
