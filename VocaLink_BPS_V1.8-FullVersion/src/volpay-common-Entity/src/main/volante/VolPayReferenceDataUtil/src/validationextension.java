package com.volante.validation;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;

public class validationextension implements IInvokable {
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
        // perform the operation here
        try {
        LookupContext lcxt = LookupContextFactory.getLookupContext();
		String rawIn=(String)args[0];
		String flowName=(String)args[1];
		// Lookup message flow (defined in the cartridge)
		MessageFlow messageFlow = lcxt.lookupMessageFlow(flowName);


		// Create a TransformContext. We have no special properties to set in the context.

      	// Prepare the input for the message flow.
      	Object[] messageFlowArgs = new Object[] { rawIn };

      	// Execute the message flow.
      	Object[] output = messageFlow.run(messageFlowArgs, cxt);
      	return (boolean)output[0];
      	}
      	 catch(TransformException e){
	    	throw(e);
	    }
	    catch(javax.naming.NamingException e) {
	      e.printStackTrace();
	    }
	    catch(java.rmi.RemoteException e) {
	      e.printStackTrace();
	    }
	    catch(java.io.IOException e) {
	      e.printStackTrace();
	    }
	    return false;

			// Use output
            
   }
}
