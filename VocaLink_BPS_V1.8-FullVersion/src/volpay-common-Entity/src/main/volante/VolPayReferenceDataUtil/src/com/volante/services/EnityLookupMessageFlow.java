package com.volante.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class EnityLookupMessageFlow {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow(String internalMessageName)  throws javax.naming.NamingException {
        // look up message flow and cache
        if(messageFlow == null) {
			// Get the lookup context for the current environment
			LookupContext lcxt = LookupContextFactory.getLookupContext();

			// Lookup message flow (defined in the cartridge)
			messageFlow = lcxt.lookupMessageFlow("Load"+internalMessageName+"Table");
			
		}
        return messageFlow;
    }

    public static DataObjectSection process(String internaleMesageName,Boolean forceLoad) throws TransformException {
		DataObjectSection dataObjectSectionOut = null;
	    try {
	        MessageFlow messageFlow = getMessageFlow(internaleMesageName);
			if(messageFlow==null){
				throw new TransformException ("Unable to look up internal message " + internaleMesageName);
			}
			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
			Object[] messageFlowArgs = new Object[] {  forceLoad,  };

	      	// Execute the message flow.
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);

			// Use output
             dataObjectSectionOut = (DataObjectSection)output[0];

	    }
	    catch(TransformException e) {
	      System.err.println(e.toXMLString());
	    }
	    catch(javax.naming.NamingException e) {
	      throw new TransformException(e.getMessage(),e);
	    }
	    catch(java.rmi.RemoteException e) {
			throw new TransformException(e.getMessage(),e);

		}
	    catch(IOException e) {
	      e.printStackTrace();
	    }
		return dataObjectSectionOut;
	}
   
}