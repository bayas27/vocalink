package com.volante.services;

import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;

/**
 * Created by Ayyanar on 20-03-2017.
 */
public class LoadEntityByEntityName  implements IInvokable{

    @Override
    public Object run(Object[] args, TransformContext transformContext) throws TransformException {
        if ((args.length != 2) || !(args[0] instanceof String) || !(args[1] instanceof Boolean)) {
            throw new TransformException ("Can't able to load entity two  arguments required (Internal Message Name and force Load flag");
        }
        String internalMesageName = ((String) args[0]);
        Boolean forceLoadFlag = ((Boolean) args[1]);

        DataObjectSection dataObjectSection = EnityLookupMessageFlow.process(internalMesageName,forceLoadFlag);
        return dataObjectSection;
    }
}
