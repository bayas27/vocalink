package com.volante.vpe.vpeThreadPool.config;

/**
 * Created by Partha on 15-10-2015.
 */
public class VPEThreadPoolConfig {

    String poolName;
    int maxThreads;
    boolean isScheduled;


    public VPEThreadPoolConfig(String poolName, int maxThreads, boolean isScheduled) {
        this.poolName = poolName;
        this.maxThreads = maxThreads;
        this.isScheduled = isScheduled;
    }

    public String getPoolName() {
        return poolName;
    }

    public int getMaxThreads() {
        return maxThreads;
    }


    public boolean isScheduled() {
        return isScheduled;
    }

    public String toString() {
        return "PoolName: " + poolName + ", MaxThreads: " + maxThreads + ", IsScheduled: " + isScheduled;
    }

}
