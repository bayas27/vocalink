package com.volante.paymentHub.cacheUtils.cachers;

import com.tplus.transform.runtime.*;

import java.util.HashMap;

public class VPECacherUtil {

	public static void throwNotSupported (String methodName, String cacheName) throws TransformException {
		throw new TransformException (methodName + " not supported for cache " + cacheName);
	}

    public static DataObjectSection flushCache (DataObjectSection coll) {
        DataObjectSection collLocal = null;
        if (coll != null) {
            collLocal = (DataObjectSection) coll.clone();
            coll.clear();
        }
        else
            collLocal = new DataObjectSectionImpl();

        return collLocal;
    }
    
    public static DataObjectSection clearCache (DataObjectSection coll) {
        DataObjectSection collLocal = null;
        if (coll != null) {
            collLocal = (DataObjectSection) coll.clone();
            coll.clear();
        }
        else
            collLocal = new DataObjectSectionImpl();

        return collLocal;
    }



    public static void addNewCacheEntry(String cacheName, java.util.concurrent.ConcurrentHashMap<String, DataObjectSection> allCache) throws TransformException {
        if (!allCache.containsKey (cacheName))
            allCache.put (cacheName, new DataObjectSectionImpl());
    }


    public static DataObjectSection getThisCacheObj (String cacheName, java.util.concurrent.ConcurrentHashMap<String, DataObjectSection> allCache) throws TransformException {
        DataObjectSection curCache = allCache.get(cacheName);
        /*if (curCache == null)
            throw new TransformException("Unknown cacher - " + cacheName);*/
        return curCache;
    }

    public static void setCacheObj (String cacheName, java.util.concurrent.ConcurrentHashMap<String, DataObjectSection> allCache, DataObjectSection coll) throws TransformException {
        //DataObjectSection curCache = allCache.get(cacheName);
        allCache.put(cacheName, coll);
        /*if (curCache == null)
            throw new TransformException("Unknown cacher - " + cacheName);*/
        //return curCache;
    }

}
