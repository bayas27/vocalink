package com.volante.paymentHub.cacheUtils;

import com.tplus.transform.runtime.*;
import com.volante.paymentHub.cacheUtils.iinvokable.*;

/**
 * Created with IntelliJ IDEA.
 * User: Partha
 * Date: 12/18/14
 * Time: 9:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class CacherTest {

    public static void main (String args[]) throws TransformException {
        InitUserDataCache obj = new InitUserDataCache();
        final TransformContextImpl transformContext = new TransformContextImpl();
        obj.run (new Object[] {args[0]}, transformContext);

        DataObjectSection section = new DataObjectSectionImpl();
        final SimpleGenericDataObject dataObject = getSimpleObject();
        section.add (dataObject);

        final String cacheName = "AUDIT";
        final Object[] runParams = {cacheName, dataObject};
        final AddToCache addToCacheObj = new AddToCache();
        ////System.out.println ("dataObj: " + dataObject.toXMLString() + ", " + dataObject.getFieldCount());

        addToCacheObj.run (runParams, transformContext);
        getAndPrint (cacheName);
        flushAndPrint(cacheName);
    }

    private static SimpleGenericDataObject getSimpleObject() {
        String[] fldNames = new String[]{"F1", "F2", "F3"};
        String[] values = new String[]{"abc", "pqr", "xyz"};
        DesignerType[] types = new DesignerType[]{DesignerType.DESIGNER_STRING_TYPE, DesignerType.DESIGNER_STRING_TYPE, DesignerType.DESIGNER_STRING_TYPE};
        SimpleGenericDataObject simpleGenericDataObject = new SimpleGenericDataObject (new DataObjectMetaInfoImpl(fldNames, types));

        for (int i=0; i<simpleGenericDataObject.getFieldCount(); i++)
            simpleGenericDataObject.setField(i, values[i]);

        ////System.out.println("simpleGenericDataObject:" + simpleGenericDataObject.toString() + ", " + simpleGenericDataObject.getFieldCount());

        return simpleGenericDataObject;
    }

    private static void flushAndPrint(String cacheName) throws TransformException {
        final TransformContextImpl transformContext = new TransformContextImpl();
        DataObjectSection runObjOutput = (DataObjectSection) new FlushCache().run(new Object[]{cacheName}, transformContext);
        //System.out.println ("Flush output");
        printRetVal(runObjOutput);
    }

    private static void getAndPrint(String cacheName) throws TransformException {
        final TransformContextImpl transformContext = new TransformContextImpl();
        DataObjectSection runObjOutput = (DataObjectSection) new GetCache().run(new Object[]{cacheName}, transformContext);
        //System.out.println("Get output");
        printRetVal(runObjOutput);
    }

    private static void printRetVal (DataObjectSection runObjOutput) {
        final int elementCount = runObjOutput.getElementCount();
        if (!((runObjOutput == null) || (elementCount == 0))) {
            //System.out.println("Total obj count: " + elementCount);
            for (int i=0; i<elementCount; i++) {
                DataObject elm = runObjOutput.getElement(i);
                //for (int j=0; j<elm.getFieldCount(); j++)
                    //System.out.println (elm.getFieldName(j) + ":" + elm.getField(j));
                //System.out.println();
            }
        }
    }
}
