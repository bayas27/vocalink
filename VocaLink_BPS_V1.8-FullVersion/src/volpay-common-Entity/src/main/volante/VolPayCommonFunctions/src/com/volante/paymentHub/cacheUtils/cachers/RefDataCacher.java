package com.volante.paymentHub.cacheUtils.cachers;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.DataObjectSectionImpl;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.collection.StringList;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RefDataCacher extends AbstractCacher {

    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    static java.util.concurrent.ConcurrentHashMap<String, DataObjectSection> allRefDataCache = new java.util.concurrent.ConcurrentHashMap<String, DataObjectSection>();

    public RefDataCacher(String cacheName) {
        super(cacheName);
    }

    public void addToCache(DataObject auditElm) throws TransformException {
        VPECacherUtil.throwNotSupported("AddToCache", cacheName);
    }

    public /*synchronized*/ DataObjectSection flushCache() throws TransformException {
        w.lock();
        try {
            DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
            return VPECacherUtil.flushCache(curCache);
        } finally {
            w.unlock();
        }
    }
    
    public /*synchronized*/ boolean clearCache() throws TransformException {
        w.lock();
        try {
            /*DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
            curCache.clear();*/
            DataObjectSection clearedCache = new DataObjectSectionImpl();
            setCache(cacheName,clearedCache);
            return true;
        } finally {
            w.unlock();
        }
    }


    public /*synchronized*/ void setCache(DataObjectSection coll) throws TransformException {
        w.lock();
        try {
            DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
            curCache.clear();
            curCache.addAll(coll);
        } finally {
            w.unlock();
        }
    }

    public /*synchronized*/ void setCache(String cacheName, DataObjectSection coll) throws TransformException {
        w.lock();
        try {
            /*System.out.println("Enter");
            DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
            System.out.println("Collection "+ coll.size()+ " : " + coll.toXMLString());
            curCache.reset();
            System.out.println("Enter clear");
            System.out.println("Cache Data after clearing " + curCache.toXMLString());*/
//            curCache.addAll(coll);
            /*for (int i = 0; i < coll.getElementCount(); i++) {
                DataObject dataObject = coll.getElement(i);
                System.out.println("Current  " + curCache.toXMLString());
                curCache.addElement(dataObject);
                System.out.println("after adding  " + curCache.toXMLString());
            }*/
           /* VPECacherUtil.setCacheObj(cacheName, allRefDataCache, coll);

            DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);

            //System.out.println("After Adding : " + curCache.toXMLString());*/
            DataObjectSection updated_value = (DataObjectSection) coll.clone();
            VPECacherUtil.setCacheObj(cacheName, allRefDataCache, updated_value );
        } finally {
            w.unlock();
        }
    }

    public /*synchronized*/ DataObjectSection getCache() throws TransformException {
        r.lock();
        try {
            return VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
        } finally {
            r.unlock();
        }
    }

    public /*synchronized*/ DataObjectSection getCache(String cacheName) throws TransformException {
        r.lock();
        try {
            return VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
        } finally {
            r.unlock();
        }
    }

    public /*synchronized*/ int getLength() throws TransformException {
        r.lock();
        try {
            DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allRefDataCache);
            return curCache.getElementCount();
        } finally {
            r.unlock();
        }
    }


    public static void initRefDataCacher(StringList cacheNames) throws TransformException {
        //synchronized not needed as caller is synchronized
        //synchronized (RefDataCacher.class) {
        for (int i = 0; i < cacheNames.size(); i++) {
            VPECacherUtil.addNewCacheEntry(cacheNames.getValue(i), allRefDataCache);
        }
    }

    public static void initRefDataCacher(String cacheNames) throws TransformException {
        //synchronized not needed as caller is synchronized
        //synchronized (RefDataCacher.class) {
        VPECacherUtil.addNewCacheEntry(cacheNames, allRefDataCache);

        //}
    }

}
