package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.uriListCache.NormalPriorityURIListCache;


public class AddURIToNormalPriorityCache implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 1) || !(args[0] instanceof String)) {
			throw new TransformException ("AddURIToNormalPriorityCache needs to be invoked with exactly one argument (URI - type: String)");
		}

        final String uri = (String) args[0];
		NormalPriorityURIListCache.append (uri);

		return new Object();
	}

}
