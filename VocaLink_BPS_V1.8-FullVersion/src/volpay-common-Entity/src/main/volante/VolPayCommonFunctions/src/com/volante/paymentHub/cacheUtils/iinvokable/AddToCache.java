package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.cachers.VPECacherUtil;


public class AddToCache implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 2) || !(args[0] instanceof String) || !(args[1] instanceof DataObject)) {
			throw new TransformException ("Add to cache needs to be invoked with exactly two arguments (Cache Name - type: String, Object to be cached - type: Data Object)");
		}

        final String cacheName = (String) args[0];
        VPECacher cacher = VPECacheManager.getCacher (cacheName);

		DataObject elm = (DataObject) args[1];
		////System.out.println ("AddToCache (" + cacheName + "): " + System.identityHashCode(cacher) +
		//	", ID:" + Thread.currentThread().getId());
		//synchronized (VPECacheManager.class) {
		synchronized (cacher) {
			cacher.addToCache (elm);
		}

		return new Object();
	}

}
