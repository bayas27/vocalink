package com.volante.paymentHub;

import com.tplus.transform.runtime.collection.RawMessageList;
import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;

import java.lang.String;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileOutputStream;

public class WriteRawMessageToFile implements IInvokable {

	public Object run(Object[] args, TransformContext cxt) throws TransformException {
        String path = (String) args[0];
        RawMessage msg = (RawMessage) args[1];
        byte[] bytesToWrite = msg.getAsBytes();

        try {
	        FileOutputStream out = new FileOutputStream (path);
			out.write(bytesToWrite);
			out.close();
        }
        catch (FileNotFoundException e) {
        	throw new TransformException ("Unable to write to sample", e);
        }
        catch (IOException e) {
        	throw new TransformException ("Unable to write to sample", e);
        }
		return new Object();
	}
}
