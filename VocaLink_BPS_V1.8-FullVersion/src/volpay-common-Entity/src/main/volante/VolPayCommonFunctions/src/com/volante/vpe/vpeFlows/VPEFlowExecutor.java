package com.volante.vpe.vpeFlows;

import com.volante.vpe.vpeFlows.config.VPEFlowConfig;
import com.volante.vpe.vpeFlows.config.VPEFlowConfigManager;
import com.volante.vpe.vpeThreadPool.VPEThreadPoolManager;
import java.util.List;

/**
 * Created by partha on 10/26/2015.
 */
public class VPEFlowExecutor {

    static VPEThreadPoolManager vpeThreadPoolManager;

    public static void init (VPEThreadPoolManager vpeThreadPoolManagerIn){
        vpeThreadPoolManager = vpeThreadPoolManagerIn;
        vpeThreadPoolManager.init();
    }

    public static void runScheduledFlows () throws Exception {
        List vpeScheduledFlowConfigs = VPEFlowConfigManager.getVpeScheduledFlowConfigs();
        if (vpeThreadPoolManager == null)
            throw new Exception ("VPE Thread Pool Manager not initialized.");


        for (int i = 0; i < vpeScheduledFlowConfigs.size(); i++) {
            VPEFlowConfig vpeFlowConfig = (VPEFlowConfig) vpeScheduledFlowConfigs.get(i);
            VPEFlowInvoker vpeFlowInvoker = new VPEFlowInvoker(vpeFlowConfig.getFlowName());
            try {
                vpeThreadPoolManager.scheduleRunnableAtFixedRate(vpeFlowInvoker, vpeFlowConfig.getPoolName(), vpeFlowConfig.getRepeatInterval());
            } catch (Exception e) {
                //System.out.println("Unable to schedule flow " + vpeFlowConfig.getFlowName() + ".\n" + e.getMessage());
            }
        }
    }


    public static void submitFlowToPool (String flowName, String poolName) throws Exception {
        VPEFlowInvoker vpeFlowInvoker = new VPEFlowInvoker (flowName);
        vpeThreadPoolManager.submitRunnable (vpeFlowInvoker, poolName);
    }


    public static void stopScheduledFlows() throws Exception {
        if (vpeThreadPoolManager == null)
            throw new Exception ("VPE Thread Pool Manager not initialized.");

        vpeThreadPoolManager.stopScheduledProcesses();
    }
}
