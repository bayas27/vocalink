package com.volante.paymentHub.cacheUtils.cachers;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.DataObjectSectionImpl;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.util.ObjectInputStreamWithLoader;

import java.io.*;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class UserDataCacher extends AbstractCacher {

    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    static java.util.concurrent.ConcurrentHashMap<String, DataObjectSection> allUserDataCache = new java.util.concurrent.ConcurrentHashMap<String, DataObjectSection>();
    private static String cacheBackupDir;

    public UserDataCacher(String cacheName) {
        super(cacheName);
    }

    public /*synchronized*/ void addToCache(DataObject elm) throws TransformException {
        DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allUserDataCache);
        curCache.add(elm);
        backupOnDrive(cacheName, curCache);
    }

    public /*synchronized*/ DataObjectSection flushCache() throws TransformException {
        DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allUserDataCache);
        final DataObjectSection dataObjectSection = VPECacherUtil.flushCache(curCache);
        ////System.out.println ("SZ:" + dataObjectSection.getElementCount());

        final DataObjectSection dataObjectSection2 = VPECacherUtil.flushCache(curCache);
        ////System.out.println ("SZ after flush:" + dataObjectSection2.getElementCount());        

        removeBackupOnDrive(cacheName);
        return dataObjectSection;
    }

    public void setCache(DataObjectSection coll) throws TransformException {
        VPECacherUtil.throwNotSupported("SetCache", cacheName);
    }

    public void setCache(String cacheName, DataObjectSection coll) throws TransformException {
        VPECacherUtil.throwNotSupported("SetCache", cacheName);
    }

    public /*synchronized*/ DataObjectSection getCache() throws TransformException {
        DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allUserDataCache);
        return curCache;
    }

    public DataObjectSection getCache(String cacheName) throws TransformException {
        DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allUserDataCache);
        return curCache;
    }

    public /*synchronized*/ int getLength() throws TransformException {
        DataObjectSection curCache = VPECacherUtil.getThisCacheObj(cacheName, allUserDataCache);
        return curCache.getElementCount();
    }

    private static String setUpCacheBackUpDir(String cacheBackupDir) {
        StringBuilder toRet = new StringBuilder();
        try {
            Class WorkDirectoryIntializer = Class.forName("com.volantetech.volante.services.ApplicationStartupListener");
            Method method1 = WorkDirectoryIntializer.getDeclaredMethod("getWorkDir");
            String workdir = (String) method1.invoke(method1);

            toRet.append(workdir);
            toRet.append(File.separator);

            Class CacheInitiator = Class.forName("com.volantetech.volante.services.ApplicationStartupListener");
            Method method = CacheInitiator.getDeclaredMethod("getApplicationName");
            String appName = (String) method.invoke(method);

            toRet.append(appName);
            toRet.append(File.separator);

            toRet.append(cacheBackupDir);
        } catch (Exception e) {
//            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return toRet.toString();
    }

    public static void initUserDataCacher(StringList cacheNames, String cacheBackupDir) throws TransformException {
        //synchronized not needed as caller is synchronized
        //synchronized (UserDataCacher.class) {
        if ((cacheBackupDir == null) || (cacheBackupDir.length() == 0))
            throw new TransformException("Invalid cache backup directory provided - " + cacheBackupDir);

        UserDataCacher.cacheBackupDir = setUpCacheBackUpDir(cacheBackupDir);

        for (int i = 0; i < cacheNames.size(); i++) {
            final String cacheName = cacheNames.getValue(i);
            VPECacherUtil.addNewCacheEntry(cacheName, allUserDataCache);
            DataObjectSection restoredCache = restoreBackupOnDrive(cacheName);
            allUserDataCache.put(cacheName, restoredCache);
        }
        //}
    }


    private static File getBackupFile(String cacheName) throws TransformException {
        final boolean cacheBackupDirLocationPresent = cacheBackupDir != null;
        if (cacheBackupDirLocationPresent) {
            String cacheBackupPath = cacheBackupDir + File.separator + cacheName + ".ser";
            final File backupFile = new File(cacheBackupDir);
            backupFile.mkdirs();
            return new File(cacheBackupPath);
        } else
            throw new TransformException("PaymentDataCacher not intialized.");
    }


    private void backupOnDrive(String cacheName, DataObjectSection coll) throws TransformException {
        OutputStream file = null;
        BufferedOutputStream buffer = null;
        ObjectOutputStream output = null;
        try {
            file = new FileOutputStream(getBackupFile(cacheName));
            buffer = new BufferedOutputStream(file);
            output = new ObjectOutputStream(buffer);
            output.writeObject(coll);
        } catch (IOException ex) {
            throw new TransformException("Unable to backup.", ex);
        } finally {
            try {
                if (output != null)
                    output.close();
                if (buffer != null) {
                    buffer.flush();
                    buffer.close();
                }
                if (file != null)
                    file.close();
            } catch (IOException ex) {

            }
        }
    }

    private void removeBackupOnDrive(String cacheName) throws TransformException {
        File file = getBackupFile(cacheName);
        file.delete();
    }

    private static DataObjectSection restoreBackupOnDrive(String cacheName) throws TransformException {
        final String errMsg = "Unable to restore backup.";
        FileInputStream file = null;
        ObjectInput input = null;

        DataObjectSection retSec = new DataObjectSectionImpl();

        try {
            file = new FileInputStream(getBackupFile(cacheName));
            // The following way ensures that the audit obj is loaded. If we use normal ObjectInputStream, class not found exception
            input = new ObjectInputStreamWithLoader(file, UserDataCacher.class.getClassLoader());
            retSec = (DataObjectSection) input.readObject();
        } catch (EOFException ex) {

        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
            throw new TransformException(errMsg, ex);
        } catch (ClassNotFoundException ex) {
            throw new TransformException(errMsg, ex);
        } finally {
            if (file != null) {
                try {
                    file.close();
                    if (input != null)
                        input.close();
                } catch (IOException ex) {

                }
            }
        }
        return retSec;
    }

}