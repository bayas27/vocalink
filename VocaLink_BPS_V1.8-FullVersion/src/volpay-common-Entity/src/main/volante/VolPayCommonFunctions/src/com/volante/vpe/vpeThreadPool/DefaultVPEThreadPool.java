package com.volante.vpe.vpeThreadPool;

import com.volante.vpe.vpeThreadPool.config.VPEThreadPoolConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Created by Partha on 15-10-2015.
 */
public class DefaultVPEThreadPool extends AbstractVPEThreadPool {


    public DefaultVPEThreadPool(VPEThreadPoolConfig vpeThreadPoolConfig, ExecutorService executorService) {
        super (vpeThreadPoolConfig, executorService);
    }

    public Future scheduleRunnableAtFixedRate (Runnable runnable, long repeatInterval) throws Exception {
        throw new Exception (vpeThreadPoolConfig.getPoolName() + " is not a pool for scheduling threads.");
    }

    public void stopScheduledProcesses() throws Exception {
        throw new Exception (vpeThreadPoolConfig.getPoolName() + " is not a pool for scheduling threads.");
    }

}
