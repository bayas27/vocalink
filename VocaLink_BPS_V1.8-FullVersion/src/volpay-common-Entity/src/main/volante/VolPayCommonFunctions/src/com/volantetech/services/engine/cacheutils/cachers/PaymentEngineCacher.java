package com.volantetech.services.engine.cacheutils.cachers;

import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.TransformException;

/**
 * Created by Sivaranjani on 3/8/17.
 */
public interface PaymentEngineCacher {

    public void setCache (String cacheName,DataObjectSection coll) throws TransformException;
    public DataObjectSection getCache(String cacheName) throws TransformException;
    public void clearCache( String cacheName) throws TransformException;
    public int getLength(String cacheName) throws TransformException;


}
