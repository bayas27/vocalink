package com.volante.vpe.vpeFlows.config;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;

/**
 * Created by Partha on 20-10-2015.
 */
public class VPEFlowConfigReader {

    static String POOL_TAG = "Pool";
    static String FLOW_TAG = "Flow";
    static String POOL_ATTR_NAME = "poolName";
    static String FLOW_ATTR_NAME = "flowName";
    static String FLOW_ATTR_REPEATINTERVAL = "repeatInterval";


    static void readVPEFlowConfigs (InputStream flowConfigStream) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document configFileDoc = dBuilder.parse (flowConfigStream);

        configFileDoc.getDocumentElement().normalize();
        NodeList nList = configFileDoc.getElementsByTagName (POOL_TAG);

        if (nList.getLength() == 0)
            throw new Exception ("Invalid config. No entries found");

        for (int i=0; i<nList.getLength(); i++) {
            Node nNode = nList.item (i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element flowNode = (Element) nNode;
                String poolName = flowNode.getAttribute(POOL_ATTR_NAME);
                if (poolName == null)
                    throw new Exception ("Invalid entry. Pool name cannot be null.");

                NodeList flowChildren = flowNode.getChildNodes();

                for (int j=0; j<flowChildren.getLength(); j++) {
                    Node nameNode = flowChildren.item(j);
                    if (nameNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element nameElm = (Element) nameNode;

                        String flowName = nameElm.getAttribute (FLOW_ATTR_NAME);
                        if (flowName == null)
                            throw new Exception ("Invalid entry. Flow name cannot be null.");

                        String repeatIntervalStr = nameElm.getAttribute (FLOW_ATTR_REPEATINTERVAL);
                        long repeatInterval = -1;
                        if ((repeatIntervalStr != null) && (repeatIntervalStr.trim().length() != 0))
                            repeatInterval = Long.parseLong (repeatIntervalStr);

                        VPEFlowConfig vpeFlowConfig = new VPEFlowConfig (flowName, poolName, repeatInterval);
                        if (repeatInterval == -1)
                            VPEFlowConfigManager.addToOtherFlowConfig (vpeFlowConfig);
                        else
                            VPEFlowConfigManager.addToScheduledFlowConfig(vpeFlowConfig);
                    }
                }

            }
        }

    }


}
