package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.PaymentEngineCacher;
import com.volantetech.services.engine.cacheutils.cachers.ReferenceDataCacher;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */
public class GetCacheLength implements IInvokable {
	/**
	 * To get no of data object section  in a cache
	 * @param args cache name
	 * @param cxt
	 * @return collection size
	 * @throws TransformException
     */

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 1)
			throw new TransformException ("Get cache length should be invoked with only one argument - cache name");

        final String cacheName = (String) args[0];
		Integer length = null;
		PaymentEngineCacher cacher=null;

		if(UserDataCacher.isUserDataCache(cacheName))
		{
			cacher=UserDataCacher.getInstance();
		}
		else {
			cacher= ReferenceDataCacher.getInstance();
		}
		synchronized (cacher) {
			length = new Integer (cacher.getLength (cacheName));
		}

		return length;
	}
}
