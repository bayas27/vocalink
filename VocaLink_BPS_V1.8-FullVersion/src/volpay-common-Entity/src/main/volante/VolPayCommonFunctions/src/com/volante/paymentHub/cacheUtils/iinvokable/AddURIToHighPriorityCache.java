package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volante.paymentHub.cacheUtils.uriListCache.HighPriorityURIListCache;
import com.volante.paymentHub.cacheUtils.uriListCache.NormalPriorityURIListCache;


public class AddURIToHighPriorityCache implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 1) || !(args[0] instanceof String)) {
			throw new TransformException ("AddURIToHighPriorityCache needs to be invoked with exactly one argument (URI - type: String)");
		}

        final String uri = (String) args[0];
		HighPriorityURIListCache.append(uri);

		return new Object();
	}

}
