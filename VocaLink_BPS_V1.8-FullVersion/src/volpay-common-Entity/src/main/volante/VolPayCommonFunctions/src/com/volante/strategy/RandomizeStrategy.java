package com.volante.strategy;
import java.util.HashMap;
import java.security.SecureRandom;


import java.util.Map;
/**
 * Created by DV15 on 9/21/2016.
 */
public class RandomizeStrategy implements GetNextElementStrategy {
    static HashMap<String,Integer> randomEntry = new HashMap<String,Integer>();
    public int doOperation(String name, int count) {
        initialize(name, count);
        /*for (Map.Entry<String, Integer> entry : randomEntry.entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}*/
        if(count == 1)
        {
            return 1;
        } else {
            int range = (int) randomEntry.get(name);
            int min = 1;
            return new SecureRandom().nextInt((range - min) + 1) + min;
        }

    }

    public void initialize(String name, int count) {
        randomEntry.put(name,count);
    }

    public void remove(String name,int count) {
        int range = randomEntry.get(name);
        range -= count;
        randomEntry.remove(name);
        randomEntry.put(name,range);
    }

    public void add(String name, int count) {
        int range = randomEntry.get(name);
        range -= count;
        randomEntry.remove(name);
        randomEntry.put(name,range);
    }
}
