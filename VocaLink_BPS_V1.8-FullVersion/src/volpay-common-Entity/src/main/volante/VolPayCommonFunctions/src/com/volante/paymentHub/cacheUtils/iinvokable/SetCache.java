package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.util.LoggingUtil;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.cachers.VPECacherUtil;


public class SetCache implements IInvokable {
	com.tplus.transform.util.log.Log log = (com.tplus.transform.util.log.Log)com.tplus.transform.runtime.log.LogFactory.getRuntimeLog( "SetCache" );
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 2) || !(args[0] instanceof String) || !(args[1] instanceof DataObjectSection)) {
			throw new TransformException ("Set Cache needs to be invoked with exactly two arguments (Cache Name - type: String, Object to be cached - type: Data Object Collection)");
		}

        final String cacheName = (String) args[0];
        VPECacher cacher = VPECacheManager.getOrCreateCacher  (cacheName,true);

		DataObjectSection coll = (DataObjectSection) args[1];
		//System.out.println(coll.toString());
		synchronized (cacher) {
			log.debug("CACHE_MANAGER: Setting cache for " + cacheName);
			cacher.setCache (cacheName,coll);
		}
		//System.out.println("after set "+cacher.getCache (cacheName).toString());
		return new Object();
	}
}
