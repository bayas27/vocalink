package com.volante.paymentHub.cacheUtils.cachers;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.StringList;
import java.util.*;

public class VPECacheManager {

    static StringList userDataCacheNames;
    static ArrayList<UserDataCacher> userDataCachers = new ArrayList<UserDataCacher>();

    static StringList refDataCacheNames;
    static ArrayList<RefDataCacher> refDataCachers = new ArrayList<RefDataCacher>();

	public VPECacheManager() {
	}


  /*  public static void addNewRefDataCache (String cacheName) throws TransformException {
        synchronized (VPECacheManager.class) {
            StringList cacheNames = new StringList().add(cacheName);
            createRefDateCacheNameList (cacheNames);
            RefDataCacher.initRefDataCacher (cacheNames);
        }
    }*/


    public static void initRefDataCache (StringList cacheNames) throws TransformException {
        synchronized (VPECacheManager.class) {
            createRefDateCacheNameList (cacheNames);
            for (int i=0; i<cacheNames.size(); i++)
            	refDataCachers.add (new RefDataCacher ((String) cacheNames.get (i)));

            RefDataCacher.initRefDataCacher (cacheNames);
        }
    }

 public static void initRefDataCache (String cacheNames) throws TransformException {
        synchronized (VPECacheManager.class) {
            createRefDateCacheNameList (cacheNames);

            	refDataCachers.add (new RefDataCacher ( cacheNames));

            RefDataCacher.initRefDataCacher (cacheNames);
        }
    }


    private static void createRefDateCacheNameList (StringList cacheNames) throws TransformException {
        if (refDataCacheNames == null)
            refDataCacheNames = new StringList();

        refDataCacheNames.addAll (cacheNames);
    }

    private static void createRefDateCacheNameList (String cacheNames) throws TransformException {
        if (refDataCacheNames == null)
            refDataCacheNames = new StringList();

        refDataCacheNames.add (cacheNames);
    }


    public static void initUserDataCache (StringList cacheNames, String userDataBackupLocation) throws TransformException {
        synchronized (VPECacheManager.class) {
            userDataCacheNames = cacheNames;
            for (int i=0; i<cacheNames.size(); i++)
            	userDataCachers.add (new UserDataCacher ((String) cacheNames.get (i)));

            UserDataCacher.initUserDataCacher (userDataCacheNames, userDataBackupLocation);
        }
    }


	public static VPECacher getCacher (String cacheName) throws TransformException {
        ////System.out.println ("Trying to obtain cacher:" + cacheName);
        if ((userDataCacheNames == null) && (refDataCacheNames == null))
            throw new TransformException ("VPE Cache Manager not initialized.");

        VPECacher cacher = getCacherInList (cacheName);

        if (cacher == null)
            throw new TransformException ("Unknown cache - " + cacheName);

		////System.out.println ("Obtained cacher:" + cacher.getCacheName());
        return cacher;
	}


	public static VPECacher getOrCreateCacher (String cacheName) throws TransformException {
        VPECacher cacher = getCacherInList (cacheName);
        //if (cacher == null)
            //addNewRefDataCache (cacheName);

        return getCacherInList (cacheName);
	}
    public static VPECacher getOrCreateCacher (String cacheName,boolean bool) throws TransformException {
        VPECacher cacher =null;
        //if (cacher == null)
            //addNewRefDataCache (cacheName);
            initRefDataCache(cacheName);

        if ((cacher == null) && (refDataCacheNames != null)) {
            int refCacheIndex = refDataCacheNames.indexOf (cacheName);
            if (refCacheIndex != -1)
                cacher = refDataCachers.get (refCacheIndex);
        }

        return cacher;
	}


	private static VPECacher getCacherInList (String cacheName) throws TransformException {
		VPECacher cacher = null;
		if (userDataCacheNames != null) {
            int userCacheIndex = userDataCacheNames.indexOf (cacheName);
            if (userCacheIndex != -1)
                cacher = userDataCachers.get (userCacheIndex);
        }

        if ((cacher == null) && (refDataCacheNames != null)) {
            int refCacheIndex = refDataCacheNames.indexOf (cacheName);
            if (refCacheIndex != -1)
                cacher = refDataCachers.get (refCacheIndex);
        }

        return cacher;
	}

}
