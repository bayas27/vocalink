package com.volante.vpe.vpeThreadPool;

import java.util.concurrent.Future;

/**
 * Created by partha on 10/27/2015.
 */
public interface VPEThreadPool {
    public String getPoolName ();
    public Future submitRunnable(Runnable runnable);
    public Future scheduleRunnableAtFixedRate (Runnable runnable, long repeatInterval) throws Exception;
    public void stopScheduledProcesses() throws Exception;

}
