package com.volante.vpe.vpeThreadPool;

import java.util.concurrent.Future;

/**
 * Created by partha on 10/27/2015.
 */
public interface VPEThreadPoolManager {
    public void init();
    public Future submitRunnable (Runnable runnable, String poolName) throws Exception;
    public Future scheduleRunnableAtFixedRate (Runnable runnable, String poolName, long repeatInterval) throws Exception;
    public void stopScheduledProcesses() throws Exception;
}
