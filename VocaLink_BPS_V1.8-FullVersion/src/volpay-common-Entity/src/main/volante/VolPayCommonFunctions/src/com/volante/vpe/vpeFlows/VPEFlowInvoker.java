package com.volante.vpe.vpeFlows;

import com.tplus.transform.runtime.*;

/**
 * Created by partha on 10/26/2015.
 */
public class VPEFlowInvoker implements Runnable {

    static final String UNABLE_TO_RUN_FLOW = "Unable to run flow ";
    MessageFlow messageFlow;
    String flowName;


    public VPEFlowInvoker (String flowName) {
        this.flowName = flowName;
        init();
    }

    private void init() {
        try {
            if (messageFlow == null) {
                LookupContext lcxt = LookupContextFactory.getLookupContext();
                messageFlow = lcxt.lookupMessageFlow(flowName);
            }
        }
        catch (javax.naming.NamingException exc) {
            System.err.println (UNABLE_TO_RUN_FLOW + flowName + ".\n" + exc.toString());
        }
    }


    public void run() {
        try {
            init();
            //System.out.println ("Executing flow - " + flowName + " in thread " + Thread.currentThread().getName());
            TransformContext cxt = new TransformContextImpl();
            Object[] messageFlowArgs = new Object[] {};
            Object[] output = messageFlow.run (messageFlowArgs, cxt);
        }

        catch(TransformException exc) {
            System.err.println (UNABLE_TO_RUN_FLOW + flowName + ".\n" + exc.toString());
        }
        catch(java.rmi.RemoteException exc) {
            System.err.println (UNABLE_TO_RUN_FLOW + flowName + ".\n" + exc.toString());
        }
        catch(java.io.IOException exc) {
            System.err.println (UNABLE_TO_RUN_FLOW + flowName + ".\n" + exc.toString());
        }
    }
}
