package com.volante.paymentHub.cacheUtils.iinvokable;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.cachers.VPECacherUtil;
import com.tplus.transform.runtime.formula.DataObjectFunctions;

public class ClearCache implements IInvokable {
    com.tplus.transform.util.log.Log log = (com.tplus.transform.util.log.Log)com.tplus.transform.runtime.log.LogFactory.getRuntimeLog( "SetCache" );
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
        if (args.length != 1)
			throw new TransformException ("Get cache should be invoked with only one argument - cache name");

        final String cacheName = (String) args[0];
        VPECacher cacher = VPECacheManager.getOrCreateCacher(cacheName,true);
        boolean cacheClearStatus = false;
		////System.out.println ("FlushCache (" + cacheName + "):" + System.identityHashCode(cacher) +
		//	", ID:" + Thread.currentThread().getId());
		//synchronized (VPECacheManager.class) {
		synchronized (cacher) {
            log.debug("CACHE_MANAGER: Clearing cache for " + cacheName);
			cacheClearStatus = cacher.clearCache();
		}
       return cacheClearStatus;
   }

}
