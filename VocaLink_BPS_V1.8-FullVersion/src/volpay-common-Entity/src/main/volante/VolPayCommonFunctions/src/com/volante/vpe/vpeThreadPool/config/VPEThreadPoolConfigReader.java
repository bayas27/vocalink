package com.volante.vpe.vpeThreadPool.config;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Partha on 20-10-2015.
 */
public class VPEThreadPoolConfigReader {

    static String POOL_TAG = "Pool";
    static String POOL_ATTR_NAME = "poolName";
    static String POOL_ATTR_MAXTHREADS = "maxThreads";
    static String POOL_ATTR_ISSCHEDULED = "isScheduled";


    static List readThreadPoolConfig (InputStream threadPoolConfigStream) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document configFileDoc = dBuilder.parse (threadPoolConfigStream);
        configFileDoc.getDocumentElement().normalize();

        NodeList nList = configFileDoc.getElementsByTagName(POOL_TAG);
        if (nList.getLength() == 0)
            throw new Exception ("Invalid config. No entries found");

        List vpeThreadConfigs = new ArrayList();

        for (int i=0; i<nList.getLength(); i++) {
            Node nNode = nList.item (i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element taskNode = (Element) nNode;
                String poolName = taskNode.getAttribute(POOL_ATTR_NAME);
                String maxThreadStr = taskNode.getAttribute(POOL_ATTR_MAXTHREADS);


                if ((poolName == null) || (maxThreadStr == null))
                    throw new Exception ("Invalid config. Category and / or maxThreads null.");

                int maxThreads = Integer.parseInt (maxThreadStr);

                boolean isScheduled = false;
                String isScheduledStr = taskNode.getAttribute(POOL_ATTR_ISSCHEDULED);
                if (isScheduledStr != null)
                    isScheduled = Boolean.parseBoolean (isScheduledStr);

                VPEThreadPoolConfig vpeThreadPool = new VPEThreadPoolConfig (poolName, maxThreads, isScheduled);
                vpeThreadConfigs.add (vpeThreadPool);
            }
        }

        return vpeThreadConfigs;
    }


}
