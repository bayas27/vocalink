package com.volantetech.volante.services;

import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.log.LogFactory;
import com.tplus.transform.util.log.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

//import com.tplus.transform.runtime.TransformContext;

/**
 * Stream ZIP performs zipping of raw message and returns zip file in byte[]
 */
public class StreamZip {


    public static RawMessage run(RawMessage rawMessage) throws TransformException {
        Log log = LogFactory.getRuntimeLog();

        //init buffer and compression level
        byte[] buffer = new byte[1024 * 10];
        byte[] fileInByte = null;
        int level = 9;

        //init
        String fileName = UUID.randomUUID().toString();

        try {
            log.debug("Compressing message starts..");
            //init streams
            com.tplus.transform.io.MemoryCacheSeekableStream inputStream = new com.tplus.transform.io.MemoryCacheSeekableStream(rawMessage.getAsStream());
            inputStream.seek(0);
            Class WorkDirectoryIntializer = Class.forName("com.volantetech.volante.services.ApplicationStartupListener");
            Method method1 = WorkDirectoryIntializer.getDeclaredMethod("getWorkDir");
            String workdir = (String) method1.invoke(method1);
            File tempFile = File.createTempFile(fileName, ".temp", new File(workdir + File.separator));
            log.debug("tempFile : " + tempFile.getAbsolutePath());

            FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
            ZipEntry ze = new ZipEntry(fileName);
            zipOutputStream.setLevel(level);

            //do stream zip
            zipOutputStream.putNextEntry(ze);
            int bufferSize = 0;
            while ((bufferSize = inputStream.read(buffer, 0, buffer.length)) != -1) {
                zipOutputStream.write(buffer, 0, bufferSize);
            }
            //Return byte[] of zip file
            if (tempFile.length() > 0) {
                zipOutputStream.closeEntry();
                zipOutputStream.close();
                fileOutputStream.close();
                fileInByte = Files.readAllBytes(new File(tempFile.getAbsolutePath()).toPath());
            }
            log.debug("Compressing message ends..");
            com.tplus.transform.runtime.CachedMessage toRet = new com.tplus.transform.runtime.CachedMessage(System.getProperty("file.encoding"), workdir, 262144);
            toRet.append(fileInByte);
            tempFile.delete();
            inputStream.close();
            return toRet;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new TransformException("Unable to stream zip message");
        }

    }
}
