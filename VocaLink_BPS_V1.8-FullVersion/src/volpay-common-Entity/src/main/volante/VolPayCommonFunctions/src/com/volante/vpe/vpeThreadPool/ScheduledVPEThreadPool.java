package com.volante.vpe.vpeThreadPool;

import com.volante.vpe.vpeThreadPool.config.VPEThreadPoolConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Partha on 15-10-2015.
 */
public class ScheduledVPEThreadPool extends AbstractVPEThreadPool {
    ScheduledExecutorService scheduledExecutorService;

    public ScheduledVPEThreadPool(VPEThreadPoolConfig vpeThreadPoolConfig, ExecutorService executorService) {
        super (vpeThreadPoolConfig, executorService);
        scheduledExecutorService = (ScheduledExecutorService) executorService;
    }


    public Future scheduleRunnableAtFixedRate (Runnable runnable, long repeatInterval) throws Exception {
        return scheduledExecutorService.scheduleAtFixedRate(runnable, 0, repeatInterval, TimeUnit.MILLISECONDS);
    }

    public void stopScheduledProcesses() throws Exception {
        scheduledExecutorService.shutdown();
    }

}
