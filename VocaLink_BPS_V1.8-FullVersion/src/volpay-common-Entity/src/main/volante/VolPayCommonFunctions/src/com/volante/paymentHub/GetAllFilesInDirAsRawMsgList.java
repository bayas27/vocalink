package com.volante.paymentHub;

import com.tplus.transform.runtime.collection.RawMessageList;
import com.tplus.transform.runtime.RawMessage;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.tplus.transform.runtime.FileInputSource;

import java.lang.String;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FilenameFilter;

public class GetAllFilesInDirAsRawMsgList implements IInvokable {

    ArrayList<File> fileArrayList;

	public Object run(Object[] args, TransformContext cxt) throws TransformException {
        fileArrayList = new ArrayList();
        String path = (String) args[0];

        getFileList (path);
        RawMessageList listToRet = new RawMessageList();
        for (int i=0; i<fileArrayList.size(); i++) {
        	try {
	            listToRet.add (new FileInputSource (fileArrayList.get(i).getAbsolutePath()));
	        }
	        catch (FileNotFoundException exc) {
	        	throw new TransformException ("Unable to open sample.", exc);
	        }
	        catch (IOException exc) {
	        	throw new TransformException ("Unable to open sample.", exc);
	        }
        }

        return listToRet;
	}



    void getFileList (String path) {
        File file = new File (path);
        File[] children = file.listFiles ();

        for (int i=0; i<children.length; i++) {
            if (children[i].isDirectory())
                getFileList (children[i].getAbsolutePath());
            else
                fileArrayList.add (children[i]);
        }
    }
}
