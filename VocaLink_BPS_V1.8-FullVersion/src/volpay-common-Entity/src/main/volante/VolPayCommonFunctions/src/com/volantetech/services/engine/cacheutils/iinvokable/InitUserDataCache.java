package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */
public class InitUserDataCache implements IInvokable {

    static boolean initialized = false;

    /**
     * setting up backup directory for user data cache
     * @param args cache names , path for cache backup directory
     * @param cxt
     * @return null
     * @throws TransformException
     */

    public Object run (Object[] args, TransformContext cxt) throws TransformException {
        assertSyntax(args);

        final StringList cacheNames = (StringList) args[0];
        String backupDir = (String) args[1];

        synchronized (InitUserDataCache.class) {
        	if (!initialized)
        		UserDataCacher.initUserDataCacher (cacheNames, backupDir);
        	initialized = true;
        }
		return new Object();
	}


    private void assertSyntax (Object[] args) throws TransformException {
        boolean invalid = (args.length != 2) || !(args[0] instanceof StringList) || !(args[1] instanceof String);
        if (invalid)
            throw new TransformException ("Init user data cache needs to be invoked with 2 arguments: Cache names - String list; Path to use for user cache backup - String");
    }

}
