package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.PaymentEngineCacher;
import com.volantetech.services.engine.cacheutils.cachers.ReferenceDataCacher;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */
public class SetPaymentEngineCache implements IInvokable {
    /**
     * To set elements in reference data cache
     * @param args cache name  , object to be cached
     * @param cxt
     * @return null
     * @throws TransformException
     */

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
        if ((args.length != 2) || !(args[0] instanceof String) || !(args[1] instanceof DataObjectSection)) {
            throw new TransformException("Set Cache needs to be invoked with exactly two arguments (Cache Name - type: String, Object to be cached - type: Data Object Collection)");
        }

        final String cacheName = (String) args[0];

        DataObjectSection coll = (DataObjectSection) args[1];
        PaymentEngineCacher cacher = null;

        if (UserDataCacher.isUserDataCache(cacheName)) {
            cacher = UserDataCacher.getInstance();
        } else {
            cacher = ReferenceDataCacher.getInstance();
        }


        cacher.setCache(cacheName, coll);

        return new Object();
    }
}
