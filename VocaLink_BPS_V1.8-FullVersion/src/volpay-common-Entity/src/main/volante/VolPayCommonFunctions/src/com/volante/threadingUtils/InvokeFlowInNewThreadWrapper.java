package com.volante.threadingUtils;

import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class InvokeFlowInNewThreadWrapper implements IInvokable {

    static ExecutorService executor;

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 2) {
			throw new TransformException ("Exactly 2 parameters expected ");
		}

		createPool();
		Runnable flowthread = new InvokeFlowInNewThread (args[0],args[1], cxt);
		executor.execute (flowthread);

		return null;
    }


    private void createPool() {
    	if (executor == null)
    		executor = Executors.newFixedThreadPool(4);
    }
}
