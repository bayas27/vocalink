    package com.tplus.transform.runtime.keygen;

    import com.tplus.transform.runtime.log.LogFactory;
    import com.tplus.transform.util.log.Log;
    import com.tplus.transform.util.sql.connection.ConnectionPool;

    import java.sql.Connection;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.util.concurrent.locks.ReentrantLock;
    import java.util.concurrent.locks.ReentrantReadWriteLock;

    /**
     * Created by Ayyanar on 22-02-2017.
     */
    public class VolPayTableKeyGenerator implements KeyGenerator {
        static final int BLOCK_SIZE = 100;
        String tableName = "UniqueKeyGenTable";

        long currentKey = 0L;
        long blockedTill = 0L;
        ConnectionPool connectionPool;
        boolean tableAddAttempted = false;
        private final String updateCurrentKey;
        private final String selectCurrentKey;
        protected Log log;
        private ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock(true);
        private ReentrantReadWriteLock.ReadLock r = reentrantReadWriteLock.readLock();
        private ReentrantReadWriteLock.WriteLock w = reentrantReadWriteLock.writeLock();

        public VolPayTableKeyGenerator(ConnectionPool connectionPool, String tableName) {
            this.tableName = tableName;
            this.connectionPool = connectionPool;
            this.updateCurrentKey = ("update " + tableName + " set CurrentKey = ? where CurrentKey = ?");
            this.selectCurrentKey = ("select CurrentKey from " + tableName);
            this.log = LogFactory.getRuntimeLog("KeyGenerator");
        }

        public String getNextString(KeyGeneratorInfo info) throws KeyGenerationException {
            return String.valueOf(getNextLong(info));
        }

        public synchronized long getNextLong(KeyGeneratorInfo info) throws KeyGenerationException {
            w.lock();
            try {
                if (!isKeysAvailable()) {
                    block(BLOCK_SIZE);
                }
                this.currentKey += 1L;
                return this.currentKey;
            } catch (SQLException e) {
                KeyGenerationException generationException = KeyGenerationException.createKeyGenerationExceptionFormatted("SRT631", new Object[]{e});
                generationException.setDetail(e);
                throw generationException;
            }
            finally {
                w.unlock();
            }
        }

        private boolean isKeysAvailable() {
            return this.currentKey + 1L <= this.blockedTill;
        }

        public int getNextInt(KeyGeneratorInfo info) throws KeyGenerationException {
            return (int) getNextLong(info);
        }

        public boolean hasFreeKeys(KeyGeneratorInfo keyGeneratorInfo) {
            return isKeysAvailable();
        }

        private void block(int keys) throws SQLException {
            for (int i = 0; i < 10; i++) {
                if (block0(keys)) {
                    return;
                }
                try {
                    Thread.sleep(i * i * 100);
                } catch (InterruptedException localInterruptedException) {
                }
            }
            throw new SQLException("Error allocating Unique Key");
        }

        private boolean block0(int keys) throws SQLException {
            Connection con = this.connectionPool.getConnection();
            try {
                this.currentKey = getCurrentKey(con);
                this.blockedTill = this.currentKey;
                long tryBlockTill = this.currentKey + keys;
                if (!modifyCurrentKey(tryBlockTill, this.currentKey, con)) {
                    return false;
                }

                this.blockedTill = tryBlockTill;
                this.connectionPool.commit(con);
                return true;
            } catch (SQLException e) {
                this.connectionPool.rollback(con);

                throw e;
            } finally {
                this.connectionPool.releaseConnection(con);
            }
        }

        public long getCurrentKey(Connection con) throws SQLException {
            PreparedStatement stmt = prepareStatement(con, this.selectCurrentKey);
            try {
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    return rs.getLong(1);
                }
                throw new SQLException("SQL error while executing statement [" + this.selectCurrentKey + "]");
            } finally {
                stmt.close();
            }
        }

        private PreparedStatement prepareStatement(Connection con, String stmt) throws SQLException {
            this.log.trace("Executing " + stmt);
            return con.prepareStatement(stmt);
        }

        public boolean modifyCurrentKey(long newKeyValue, long oldKeyValue, Connection con) throws SQLException {
            PreparedStatement stmt = prepareStatement(con, this.updateCurrentKey);
            try {
                stmt.setLong(1, newKeyValue);
                stmt.setLong(2, oldKeyValue);
                int ret = stmt.executeUpdate();
                return ret == 1;
            } finally {
                stmt.close();
            }
        }
    }
