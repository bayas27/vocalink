package com.volante.paymentHub;


import com.tplus.transform.runtime.*;
import java.io.IOException;

public class InitializeVPEDB {

	public static void main(String[] args) {
		com.tplus.transform.util.LoggingUtil.enableLogging("log.xml");
	    try {
			LookupContext lcxt = LookupContextFactory.getLookupContext();
			MessageFlow messageFlow = lcxt.lookupMessageFlow("InitializeVPEDB");
			TransformContext cxt = new TransformContextImpl();
	      	Object[] messageFlowArgs = new Object[] {args[0]};
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);
	    }
	    catch(TransformException e) {
	      System.err.println(e.toXMLString());
	    }
	    catch(javax.naming.NamingException e) {
	      e.printStackTrace();
	    }
	    catch(java.rmi.RemoteException e) {
	      e.printStackTrace();
	    }
	    catch(java.io.IOException e) {
	      e.printStackTrace();
	    }
	}
}