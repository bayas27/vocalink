package com.volante.paymentHub.cacheUtils.cachers;

import com.tplus.transform.runtime.*;
import com.volante.paymentHub.cacheUtils.cachers.*;

public interface VPECacher {
	public void addToCache (DataObject elm) throws TransformException;
	public void setCache (DataObjectSection coll) throws TransformException;
	public void setCache (String cacheName,DataObjectSection coll) throws TransformException;
	public DataObjectSection getCache () throws TransformException;
	public DataObjectSection getCache (String cacheName) throws TransformException;
	public DataObjectSection flushCache () throws TransformException;
	public int getLength () throws TransformException;
    public String getCacheName () throws TransformException;
    public boolean clearCache() throws TransformException;
}
