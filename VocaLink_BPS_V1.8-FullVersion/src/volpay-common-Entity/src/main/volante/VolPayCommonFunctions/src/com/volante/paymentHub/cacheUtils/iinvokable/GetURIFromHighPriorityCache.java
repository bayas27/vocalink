package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volante.paymentHub.cacheUtils.uriListCache.HighPriorityURIListCache;
import com.volante.paymentHub.cacheUtils.uriListCache.NormalPriorityURIListCache;


public class GetURIFromHighPriorityCache implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 0)) {
			throw new TransformException ("GetURIFromHighPriorityCache needs no arguments");
		}

		return HighPriorityURIListCache.getTop ();
	}

}
