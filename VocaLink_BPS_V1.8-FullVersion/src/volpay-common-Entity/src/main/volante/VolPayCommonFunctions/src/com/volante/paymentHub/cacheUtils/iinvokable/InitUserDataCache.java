package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.runtime.handler.*;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;


public class InitUserDataCache implements IInvokable {

    static boolean initialized = false;

    public Object run (Object[] args, TransformContext cxt) throws TransformException {
        assertSyntax(args);

        final StringList cacheNames = (StringList) args[0];
        String backupDir = (String) args[1];

        synchronized (InitUserDataCache.class) {
        	if (!initialized)
        		VPECacheManager.initUserDataCache (cacheNames, backupDir);
        	initialized = true;
        }
		return new Object();
	}


    private void assertSyntax (Object[] args) throws TransformException {
        boolean invalid = (args.length != 2) || !(args[0] instanceof StringList) || !(args[1] instanceof String);
        if (invalid)
            throw new TransformException ("Init user data cache needs to be invoked with 2 arguments: Cache names - String list; Path to use for user cache backup - String");
    }

}
