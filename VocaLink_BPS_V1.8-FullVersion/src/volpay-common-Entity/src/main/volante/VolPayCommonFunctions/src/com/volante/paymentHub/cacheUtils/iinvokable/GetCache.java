package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.cachers.VPECacherUtil;
import 
com.tplus.transform.runtime.formula.DataObjectFunctions;



public class GetCache implements IInvokable {
	com.tplus.transform.util.log.Log log = (com.tplus.transform.util.log.Log)com.tplus.transform.runtime.log.LogFactory.getRuntimeLog( "SetCache" );
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 1) || !(args[0] instanceof String)) {
			throw new TransformException("Get Cache needs to be invoked with exactly one argument (Cache Name - type: String)");
		}

        final String cacheName = (String) args[0];
       // //System.out.println ("Trying to obtain:" + cacheName);
        VPECacher cacher = VPECacheManager.getOrCreateCacher (cacheName,true);
		////System.out.println ("CN:" + cacher.getCacheName());
		Object cache = null;
		//synchronized (VPECacheManager.class) {
		synchronized (cacher) {
			log.debug("CACHE_MANAGER: Getting cached data for " + cacheName);
			cache = cacher.getCache (cacheName);
		}

		return cache;
	}
}
