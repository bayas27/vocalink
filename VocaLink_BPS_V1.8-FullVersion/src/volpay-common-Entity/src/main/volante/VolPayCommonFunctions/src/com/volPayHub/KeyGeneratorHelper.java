package com.volPayHub;

import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import com.tplus.transform.util.sql.connection.ConnectionPool;
import com.tplus.transform.runtime.keygen.VolPayTableKeyGenerator;
import com.tplus.transform.runtime.keygen.KeyGeneratorInfo;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.runtime.volante.Task;
import com.tplus.transform.runtime.volante.TransactionalExecutor;
import com.tplus.transform.runtime.volante.DeclarativeTransactionalExecutor;

import java.util.*;
import javax.transaction.TransactionManager;
import javax.naming.NamingException;
import java.rmi.RemoteException;


public class KeyGeneratorHelper implements IInvokable {

    static LookupContext lcxt;
    static HashMap<String, VolPayTableKeyGenerator> keyGenMap;

    public synchronized Object run(Object[] args, TransformContext cxt) throws TransformException {
        if (args.length == 3) {
            if (keyGenMap != null) {
                boolean clearObject = (boolean) args[2];
                if (clearObject) {
                    keyGenMap.remove((String) args[1]);
                }
            }
            return 0;
        }
        assertSyntax(args);

        if (lcxt == null) {
            try {
                lcxt = LookupContextFactory.getLookupContext(getClass(), new java.util.Hashtable());
            } catch (NamingException e) {
                throw new TransformException("Unable to initialize generator.", e);
            }
        }

        String dataSourceName = (String) args[0];
        String tableName = (String) args[1];


        KeyGeneratorInfo keyGenInfo = new KeyGeneratorInfo(tableName, "CurrentKey", dataSourceName);
        // Column name hard coded here as it is hard coded inside DESIGNER Runtime - see constructor of class TableKeyGenerator
        VolPayTableKeyGenerator keyGen = getKeyGenerator(dataSourceName, tableName);
        GetNextKeyTask getNextKeyTask = new GetNextKeyTask(keyGen, keyGenInfo);

        DeclarativeTransactionalExecutor dte = new DeclarativeTransactionalExecutor(getTransactionManager(), TransactionalExecutor.TX_REQUIRED_NEW);
        try {
            return dte.start(getNextKeyTask);
        } catch (RemoteException e) {
            throw new TransformException("Unable to get next key", e);
        }
        // Running the get next key in this manner in order to run it in a new transaction
        // If not run in new transaction, on exception and roll-back, unique generation guarantee will be lost
    }


    private void assertSyntax(Object[] args) throws TransformException {
        if ((args.length != 2) || !(args[0] instanceof String) || !(args[1] instanceof String))
            throw new TransformException("Invalid call to KeyGeneratorHelper. Pass exactly two parameters - dataSourceName (String), tableName (String).");
    }


    private synchronized static VolPayTableKeyGenerator getKeyGenerator(String dataSourceName, String tableName) throws TransformException {
        if (keyGenMap == null)
            keyGenMap = new HashMap();

        Object keyGenObj = keyGenMap.get(tableName);
        if (keyGenObj != null)
            return (VolPayTableKeyGenerator) keyGenObj;
        try {
            ConnectionPool cPool = lcxt.lookupDataSource(dataSourceName);
            VolPayTableKeyGenerator keyGen = new VolPayTableKeyGenerator(cPool, tableName);
            keyGenMap.put(tableName, keyGen);
            return keyGen;
        } catch (javax.naming.NamingException e) {
            throw new TransformException("Unable to get next key.", e);
        } catch (java.rmi.RemoteException e) {
            throw new TransformException("Unable to next key.", e);
        }

    }


    private static TransactionManager getTransactionManager() throws TransformException {
        TransactionManager tm;
        try {
            tm = (TransactionManager) LookupContextFactory.getLookupContext().getTransactionManager();
        } catch (NamingException e) {
            throw new TransformException("Error looking up transaction manager", e);
        }
        return tm;
    }

}


class GetNextKeyTask implements Task {

    VolPayTableKeyGenerator keyGen;
    KeyGeneratorInfo keyGenInfo;

    public GetNextKeyTask(VolPayTableKeyGenerator keyGen, KeyGeneratorInfo keyGenInfo) {
        this.keyGen = keyGen;
        this.keyGenInfo = keyGenInfo;
    }

    public Object run() throws TransformException {
        return keyGen.getNextLong(keyGenInfo);
    }

}

