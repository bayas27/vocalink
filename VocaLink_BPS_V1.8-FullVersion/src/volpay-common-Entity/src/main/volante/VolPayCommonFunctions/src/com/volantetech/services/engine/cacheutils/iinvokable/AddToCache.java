package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */
public class AddToCache implements IInvokable {

	/**
	 * To add elements to the user data cache
	 * @param args cache name ,object to be cached
	 * @param cxt
	 * @return   null
	 * @throws TransformException
     */

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 2) || !(args[0] instanceof String) || !(args[1] instanceof DataObject)) {
			throw new TransformException ("Add to cache needs to be invoked with exactly two arguments (Cache Name - type: String, Object to be cached - type: Data Object)");
		}

        final String cacheName = (String) args[0];

		DataObject elm = (DataObject) args[1];
        UserDataCacher cacher= UserDataCacher.getInstance();

		synchronized (cacher) {
			cacher.addToCache (cacheName,elm);
		}

		return new Object();
	}

}
