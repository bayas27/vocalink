package com.volante.threadingUtils;

import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;

public class UnblockObject implements IInvokable {
	
	public Object run(Object[] args, TransformContext cxt) throws TransformException {
		LockManager.unblockObject ((DataObject) args[0]);
		return new Object(); // dummy
	}
}
