package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */

public class FlushCache implements IInvokable {
	/**
	 * clear and return object in user data cache
	 * @param args cache name
	 * @param cxt
	 * @return cached object
	 * @throws TransformException
     */

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 1)
			throw new TransformException ("Get cache should be invoked with only one argument - cache name");

        final String cacheName = (String) args[0];

		UserDataCacher cacher= UserDataCacher.getInstance();
		DataObjectSection cache = null;

		synchronized (cacher) {
			cache = cacher.flushCache (cacheName);
		}
		return cache;
	}
}
