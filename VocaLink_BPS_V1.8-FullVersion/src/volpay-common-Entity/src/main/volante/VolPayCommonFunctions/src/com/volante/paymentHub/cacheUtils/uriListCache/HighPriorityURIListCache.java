package com.volante.paymentHub.cacheUtils.uriListCache;

import com.tplus.transform.runtime.collection.StringList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by partha on 11/5/2015.
 */
public class HighPriorityURIListCache  {

    static ConcurrentLinkedQueue uriHighPriorityCache;


    public static void init() {
        uriHighPriorityCache = new ConcurrentLinkedQueue();
    }

    public static void append (String uri) {
        ////System.out.println("Inserting uri into high priority cache - " + uri);
        if(uriHighPriorityCache == null)
    		init();
    		
       	if (uri != null){
	        uriHighPriorityCache.add (uri);
        }
    }

    public static String getTop() {
        String uri = null;
        if (uriHighPriorityCache != null && !uriHighPriorityCache.isEmpty()) {
            uri = (String) uriHighPriorityCache.remove();
            ////System.out.println("Getting uri from high priority cache - " + uri);
        }
        //else
            ////System.out.println("Top uri in high priority cache is null");

        return uri;
    }

    public static StringList getAll() {
        StringList list  = new StringList();
        ////System.out.println ("Getting all from high priority cache");
        if (uriHighPriorityCache != null) {
            uriHighPriorityCache.removeAll (list);
            ////System.out.println("High priority cache - " + list.toString());
        }

        return list;
    }
}
