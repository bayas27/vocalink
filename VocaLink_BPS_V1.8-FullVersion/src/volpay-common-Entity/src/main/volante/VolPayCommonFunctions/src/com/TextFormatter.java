package com.volante.volpay.utils;
import com.tplus.transform.runtime.collection.StringList;

import java.util.LinkedHashMap;
import java.util.Map;

public class TextFormatter {

    public static StringList getPropertyNames(String formattedString) {
        StringList propertyNames = new StringList();
        int start = 0;
        while (true) {
            int startIndex = formattedString.indexOf("${", start);
            if (startIndex != -1) {
                start = startIndex + 2;
                int endIndex = formattedString.indexOf('}', start);
                if (endIndex != -1) {
                    String propName = formattedString.substring(start, endIndex);
                    propertyNames.add(propName);
                }
            }
            else break;
        }
        return propertyNames;
    }

    public static String substituteProperties(String formattedString, StringList nameValues) {
        return substituteProperties(formattedString, nameValues, false);
    }

    public static String substituteProperties(String formattedString, StringList nameValues, boolean strict) {
        if (formattedString == null) {
            return null;
        }
        Map props = toProperties(nameValues);
        int start = 0;
        while (true) {
            int startIndex = formattedString.indexOf("${", start);
            if (startIndex != -1) {
                start = startIndex + 2;
                int endIndex = formattedString.indexOf('}', start);
                if (endIndex != -1) {
                    String propName = formattedString.substring(start, endIndex);
                    String value = getProperty(props, propName);
                    if (value != null) {
                        String toRet = formattedString.substring(0, startIndex);
                        toRet += value;
                        start = toRet.length();
                        toRet += formattedString.substring(endIndex + 1);
                        //return replacePropertyInString(toRet, props);
                        formattedString = toRet;
                    }
                    else {
                        if (strict) {
                            throw new IllegalArgumentException("Unrecognized property '" + propName + "' in " + formattedString);
                        }
                    }
                }
            }
            else {
                break;
            }
        }
        return formattedString;

    }

    private static Map toProperties(StringList nameValues) {
        Map toRet = new LinkedHashMap();
        for (int i = 0; i < nameValues.size(); i += 2) {
            String name = (String) nameValues.get(i);
            String value = (String) nameValues.get(i + 1);
            toRet.put(name, value);
        }
        return toRet;
    }

    private static String getProperty(Map props, String propName) {
        return (String) props.get(propName);
    }

}