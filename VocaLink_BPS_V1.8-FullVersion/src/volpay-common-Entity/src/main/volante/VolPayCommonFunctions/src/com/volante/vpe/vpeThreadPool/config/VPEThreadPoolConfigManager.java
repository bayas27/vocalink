package com.volante.vpe.vpeThreadPool.config;

import java.io.InputStream;
import java.util.List;

/**
 * Created by partha on 10/26/2015.
 */
public class VPEThreadPoolConfigManager {

    static List vpeThreadPoolConfigs;

    public static void init (InputStream threadPoolConfigStream) throws Exception {
        vpeThreadPoolConfigs = VPEThreadPoolConfigReader.readThreadPoolConfig (threadPoolConfigStream);
    }


    public static List getVpeThreadPoolConfigs() {
        return vpeThreadPoolConfigs;
    }

    static int searchAndGetMaxThreads (String name) {
        VPEThreadPoolConfig curVPEThreadPool = searchAndGetThreadConfig(name);
        if (curVPEThreadPool != null)
            return curVPEThreadPool.getMaxThreads();

        return -1;
    }

    static VPEThreadPoolConfig searchAndGetThreadConfig (String name) {
        for (int i=0; i<vpeThreadPoolConfigs.size(); i++) {
            VPEThreadPoolConfig curVPEThreadPool = (VPEThreadPoolConfig) vpeThreadPoolConfigs.get(i);
            if (curVPEThreadPool.getPoolName().equalsIgnoreCase (name))
                return curVPEThreadPool;
        }

        return null;
    }


    private static void dumpConfigs() {
        for (int i=0; i<vpeThreadPoolConfigs.size(); i++) {
            VPEThreadPoolConfig vpeThreadPoolConfig = (VPEThreadPoolConfig) vpeThreadPoolConfigs.get(i);
            //System.out.println (vpeThreadPoolConfig.toString());
        }
    }


    public static void main (String args[]) throws Exception {
        InputStream threadPoolConfigFileStream = VPEThreadPoolConfigManager.class.getClassLoader().getResourceAsStream (args[0]);
        VPEThreadPoolConfigManager.init (threadPoolConfigFileStream);
        VPEThreadPoolConfigManager.dumpConfigs();
    }
}
