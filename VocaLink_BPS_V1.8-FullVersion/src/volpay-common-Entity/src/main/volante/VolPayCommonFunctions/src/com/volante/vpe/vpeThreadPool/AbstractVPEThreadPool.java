package com.volante.vpe.vpeThreadPool;

import com.volante.vpe.vpeThreadPool.config.VPEThreadPoolConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Created by Partha on 15-10-2015.
 */
public abstract class AbstractVPEThreadPool implements VPEThreadPool {

    VPEThreadPoolConfig vpeThreadPoolConfig;
    ExecutorService executorService;

    public AbstractVPEThreadPool(VPEThreadPoolConfig vpeThreadPoolConfig, ExecutorService executorService) {
        this.vpeThreadPoolConfig = vpeThreadPoolConfig;
        this.executorService = executorService;
    }

    public String getPoolName () {
        return vpeThreadPoolConfig.getPoolName();
    }

    public Future submitRunnable(Runnable runnable) {
        return executorService.submit(runnable);
    }

    public boolean isScheduled () {
        return vpeThreadPoolConfig.isScheduled();
    }

}
