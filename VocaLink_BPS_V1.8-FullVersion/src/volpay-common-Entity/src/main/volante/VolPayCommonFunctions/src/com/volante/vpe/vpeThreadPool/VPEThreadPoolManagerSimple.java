package com.volante.vpe.vpeThreadPool;

import com.volante.vpe.vpeThreadPool.config.VPEThreadPoolConfig;
import com.volante.vpe.vpeThreadPool.config.VPEThreadPoolConfigManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by partha on 10/26/2015.
 */
public class VPEThreadPoolManagerSimple implements VPEThreadPoolManager {
    List vpeThreadPools;

    public void init () {
        vpeThreadPools = new ArrayList();
        createThreadPools();
    }


    private void createThreadPools() {
        List vpeThreadConfigs = VPEThreadPoolConfigManager.getVpeThreadPoolConfigs();

        for (int i=0; i< vpeThreadConfigs.size(); i++) {
            VPEThreadPoolConfig vpeThreadPoolConfig = (VPEThreadPoolConfig) vpeThreadConfigs.get(i);
            ExecutorService executorService;
            VPEThreadPool vpeThreadPool;
            if (vpeThreadPoolConfig.isScheduled()) {
                executorService = Executors.newScheduledThreadPool(vpeThreadPoolConfig.getMaxThreads());
                vpeThreadPool = new ScheduledVPEThreadPool (vpeThreadPoolConfig, executorService);
            }
            else {
                executorService = Executors.newFixedThreadPool(vpeThreadPoolConfig.getMaxThreads());
                vpeThreadPool = new DefaultVPEThreadPool (vpeThreadPoolConfig, executorService);
            }

            vpeThreadPools.add (vpeThreadPool);
        }
    }


    public Future submitRunnable (Runnable runnable, String poolName) throws Exception {
        VPEThreadPool vpeThreadPool = searchAndGetThreadPool(poolName);
        if (vpeThreadPool == null)
            throw new Exception ("Invalid category - " + poolName);

        return vpeThreadPool.submitRunnable(runnable);
    }



    public Future scheduleRunnableAtFixedRate (Runnable runnable, String poolName, long repeatInterval) throws Exception {
        VPEThreadPool vpeThreadPool = searchAndGetThreadPool (poolName);
        if (vpeThreadPool == null)
            throw new Exception ("Invalid category - " + poolName);

        return vpeThreadPool.scheduleRunnableAtFixedRate(runnable, repeatInterval);
    }


    VPEThreadPool searchAndGetThreadPool (String name) {
        for (int i=0; i<vpeThreadPools.size(); i++) {
            VPEThreadPool vpeThreadPool = (VPEThreadPool) vpeThreadPools.get(i);
            if (vpeThreadPool.getPoolName().equalsIgnoreCase (name))
                return vpeThreadPool;
        }

        return null;
    }

    public void stopScheduledProcesses() throws Exception {
        for (int i=0; i<vpeThreadPools.size(); i++) {
            AbstractVPEThreadPool vpeThreadPool = (AbstractVPEThreadPool) vpeThreadPools.get(i);
            if (vpeThreadPool.isScheduled()) {
                vpeThreadPool.stopScheduledProcesses();
            }
        }
    }
}
