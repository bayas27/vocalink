package com.volante.threadingUtils;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class InvokeFlowInNewThread implements Runnable {

    static MessageFlow messageFlow;
    String flowName;
    Object bitObj;
    Object domainInObj;
    TransformContext cxt;
    public InvokeFlowInNewThread (Object flowName, Object domainInObj,TransformContext cxt) {
    	this.flowName = (String) flowName;
    	this.domainInObj = domainInObj;
    	this.cxt = cxt;
    }

    public void run () {
		final String errStr = "Unable to run flow '" + flowName + "'.";
		try {
			LookupContext lcxt = LookupContextFactory.getLookupContext();
			messageFlow = lcxt.lookupMessageFlow (flowName);

			//TransformContext cxt = new TransformContextImpl();

		Object[] messageFlowArgs = new Object[] {domainInObj};
		Object[] output = messageFlow.run (messageFlowArgs, cxt);
		}
		catch (javax.naming.NamingException exc) {
			System.err.println (errStr + "\n" + exc.toString());
		}
	    catch(TransformException exc) {
			System.err.println (errStr + "\n" + exc.toString());
	    }
	    catch(java.rmi.RemoteException exc) {
			System.err.println (errStr + "\n" + exc.toString());
	    }
	    catch(java.io.IOException exc) {
			System.err.println (errStr + "\n" + exc.toString());
	    }
    }
}
