package com.volantetech.volante.services;

import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.Variant;
import java.util.HashMap;

/**
 * Created by Ayyanar on 19/10/2016.
 */
public class ServiceRegistryCacher {
    private static ServiceRegistryCacher serviceRegistryCacher = new ServiceRegistryCacher();
  //  public static HashMap<String, Object> serviceRegistryColl = new HashMap<String, Object>();
   public static HashMap<String, HashMap<String, Object>> serviceRegistry = new HashMap<String, HashMap<String, Object>>();
    public static com.tplus.transform.util.log.Log log = (com.tplus.transform.util.log.Log)com.tplus.transform.runtime.log.LogFactory.getRuntimeLog( "" );
     final static Long RELOADTIME = 300000L;
    private ServiceRegistryCacher() {

    }

    public static ServiceRegistryCacher getInstance() {
        if (serviceRegistryCacher == null)
            serviceRegistryCacher = new ServiceRegistryCacher();
        return serviceRegistryCacher;
    }

    public static void addServiceRegToCache(String serviceRegistryName,Object serviceRegistryCollObj){
        log.debug("Service Registry started adding to Cache");
        synchronized (serviceRegistry) {
            HashMap<String, HashMap<String, Object>> toAdd= (HashMap<String, HashMap<String, Object>>) serviceRegistry.clone();
            Long currentTime = java.lang.System.currentTimeMillis();
            HashMap<String, Object> serviceRegistryColl = new HashMap<String, Object>();
            serviceRegistryColl.put("serviceRegistry", serviceRegistryCollObj);
            serviceRegistryColl.put("currentTime", currentTime);
            toAdd.put(serviceRegistryName,serviceRegistryColl);
            serviceRegistry=toAdd;
         //   serviceRegistry.put(serviceRegistryName, serviceRegistryColl);
            log.debug("Service Registry added to cache");
        }
          
    }

    public static  DataObjectSection  getServiceRegByServiceName(String serviceRegistryName){
       DataObjectSection dataObjectSection = null;
        synchronized (serviceRegistry) {
            Long currentTime = System.currentTimeMillis();
            log.debug("Getting Service Registry from cache");
            if (serviceRegistry != null && serviceRegistry.get(serviceRegistryName) != null && serviceRegistry.get(serviceRegistryName).get("currentTime") != null) {
                log.debug("Service Registry exist in the cache");
                if (currentTime - (Long) serviceRegistry.get(serviceRegistryName).get("currentTime") > RELOADTIME) {
                    log.debug("Flush Service Registry from  cache");
                    return dataObjectSection;
                } else {
                    dataObjectSection = (DataObjectSection) serviceRegistry.get(serviceRegistryName).get("serviceRegistry");
                    //  System.out.println(serviceRegistry.toString());
                }
            }
            return dataObjectSection;
        }
    }
    
    public static void clearServiceRegistryCache(String serviceRegistryName){
    log.debug("Clear Service Registry from the cache");
        synchronized (serviceRegistry) {
            HashMap<String, HashMap<String, Object>> toRemove= (HashMap<String, HashMap<String, Object>>) serviceRegistry.clone();
            toRemove.remove(serviceRegistryName);
            serviceRegistry=toRemove;
        }
    }


}
