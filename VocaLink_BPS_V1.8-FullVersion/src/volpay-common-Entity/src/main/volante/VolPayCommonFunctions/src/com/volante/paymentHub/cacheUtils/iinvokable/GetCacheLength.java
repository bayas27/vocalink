package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.cachers.VPECacherUtil;


public class GetCacheLength implements IInvokable {

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 1)
			throw new TransformException ("Get cache length should be invoked with only one argument - cache name");

        final String cacheName = (String) args[0];
        VPECacher cacher = VPECacheManager.getCacher(cacheName);
		Integer length = null;
		//synchronized (VPECacheManager.class) {
		synchronized (cacher) {
			length = new Integer (cacher.getLength ());
		}

		return length;
	}
}
