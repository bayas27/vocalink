package com.volante.strategy;

/**
 * Created by DV15 on 9/21/2016.
 */

public interface GetNextElementStrategy {
    int doOperation(String name, int count);
    void initialize(String name, int length);
   /* void remove(String name,int id);
    void add(String name, int id);*/
}
