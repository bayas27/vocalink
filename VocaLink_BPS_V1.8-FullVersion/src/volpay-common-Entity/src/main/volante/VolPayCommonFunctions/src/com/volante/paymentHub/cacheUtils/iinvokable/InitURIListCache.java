package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.uriListCache.HighPriorityURIListCache;
import com.volante.paymentHub.cacheUtils.uriListCache.NormalPriorityURIListCache;


public class InitURIListCache implements IInvokable {

    static boolean initialized = false;

    public Object run (Object[] args, TransformContext cxt) throws TransformException {
        assertSyntax(args);

        synchronized (InitURIListCache.class) {
        	if (!initialized) {
                HighPriorityURIListCache.init();
                NormalPriorityURIListCache.init();
            }
        	initialized = true;
        }
		return new Object();
	}


    private void assertSyntax (Object[] args) throws TransformException {
        if ((args.length != 0))
            throw new TransformException ("InitURIListCache needs no arguments");
    }

}
