package com.volantetech.services.engine.cacheutils.cachers;

import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.DataObjectSectionImpl;
import com.tplus.transform.runtime.TransformException;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Sivaranjani on 3/8/17.
 */
public class ReferenceDataCacher implements PaymentEngineCacher {

    private static ReferenceDataCacher referencedataCacher = new ReferenceDataCacher();
    private static ConcurrentHashMap<String, DataObjectSection> cacheMap = new ConcurrentHashMap<String, DataObjectSection>();

    private ReferenceDataCacher() {
    }

    public static ReferenceDataCacher getInstance()

    {
        return referencedataCacher;
    }


    public DataObjectSection getCache(String cacheName) {
        synchronized (cacheMap) {
           // System.out.println(" Before Adding "+size(cacheMap));
            return cacheMap.get(cacheName);
        }
    }

    public void setCache(String cacheName, DataObjectSection value) {
        synchronized (cacheMap) {
           /* System.out.println(
                    "Object of type '" + cacheMap.getClass() + "' has size of "
                            + InstrumentationAgent.getObjectSize(cacheMap) + " bytes.  Before ");*/
            DataObjectSection coll = (DataObjectSection) value.clone();

            cacheMap.put(cacheName, coll);
          /*  System.out.println(
                    "Object of type '" + cacheMap.getClass() + "' has size of "
                            + InstrumentationAgent.getObjectSize(cacheMap) + " bytes. After");
*/
           // System.out.println(" after Adding "+size(cacheMap));
        }
    }

    public void clearCache(String cacheName) {
        synchronized (cacheMap) {
            DataObjectSection clearedCache = new DataObjectSectionImpl();
            setCache(cacheName, clearedCache);
        }
    }


    public int getLength(String cacheName) throws TransformException {
        return getCache(cacheName).getElementCount();
    }

   /* public static int size(Object object) {
        ByteArrayOutputStream baos=null;
        try{
          //  System.out.println("Index Size: " + map.size());
            if (object == null)
                return -1;
             baos=new ByteArrayOutputStream();
            ObjectOutputStream oos=new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.flush();
            oos.close();
         //   System.out.println(" Data Size : " + baos.size());

        }catch(IOException e){
            e.printStackTrace();
        }
        return baos.size();
    }*/
}
