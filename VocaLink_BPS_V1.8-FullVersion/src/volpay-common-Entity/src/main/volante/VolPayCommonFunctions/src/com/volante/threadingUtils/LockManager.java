package com.volante.threadingUtils;

import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import java.util.concurrent.locks.ReentrantLock;

public class LockManager {

	static String LOCK_PROPERTY_NAME = "LOCK";

	public static void blockObject (DataObject obj) {
		ReentrantLock lock = getOrCreateLockObject (obj);
		if (!lock.isHeldByCurrentThread())
			lock.lock();
	}

	public static void unblockObject (DataObject obj) {
		ReentrantLock lock = getOrCreateLockObject (obj);
		if (lock.isHeldByCurrentThread())
			lock.unlock();
	}


	private static ReentrantLock getOrCreateLockObject (DataObject obj) {
		ReentrantLock lock;
		synchronized (obj) {
			lock = (ReentrantLock) obj.getProperty (LOCK_PROPERTY_NAME);
			if (lock == null) {
				lock = new ReentrantLock();
				obj.setProperty (LOCK_PROPERTY_NAME, lock);
			}
		}
		
		return lock;
	}

}
