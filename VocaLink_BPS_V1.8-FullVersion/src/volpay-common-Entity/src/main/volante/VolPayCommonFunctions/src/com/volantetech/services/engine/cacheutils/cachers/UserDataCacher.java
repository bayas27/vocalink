package com.volantetech.services.engine.cacheutils.cachers;

import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.DataObjectSectionImpl;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.util.ObjectInputStreamWithLoader;


import java.io.*;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Sivaranjani on 3/8/17.
 */
public class UserDataCacher implements PaymentEngineCacher {

    private static UserDataCacher userDataCacher;
    private static ConcurrentHashMap<String, DataObjectSection> cacheMap = new ConcurrentHashMap<String, DataObjectSection>();
    private static String cacheBackupDir;
    private static StringList userDataCacheNames;

    private UserDataCacher() {
    }

    public static UserDataCacher getInstance()

    {
        if (userDataCacher == null)
            userDataCacher = new UserDataCacher();
        return userDataCacher;
    }


    public DataObjectSection getCache(String cacheName) {
        synchronized (cacheMap) {
            return cacheMap.get(cacheName);
        }
    }

    public void setCache(String cacheName, DataObjectSection value) throws TransformException {
        throw new TransformException(" not supported for cache " + cacheName);
    }

    public void clearCache(String cacheName) throws TransformException {
        throw new TransformException(" not supported for cache " + cacheName);
    }


    public void addToCache(String cacheName, DataObject elm) throws TransformException {
        DataObjectSection curCache = getCache(cacheName);
        curCache.add(elm);
        backupOnDrive(cacheName, curCache);
    }

    public static boolean isUserDataCache(String cacheName) {


        return  userDataCacheNames!=null && userDataCacheNames.contains(cacheName);
    }


    public int getLength(String cacheName) throws TransformException {

        return getCache(cacheName).getElementCount();
    }

    public DataObjectSection flushCache(String cacheName) throws TransformException {
        DataObjectSection collLocal = null;
        DataObjectSection curCache = getCache(cacheName);
        if (curCache != null) {
            collLocal = (DataObjectSection) curCache.clone();
            curCache.clear();
        } else
            collLocal = new DataObjectSectionImpl();


        removeBackupOnDrive(cacheName);
        return collLocal;
    }


    private void backupOnDrive(String cacheName, DataObjectSection coll) throws TransformException {
        OutputStream file = null;
        BufferedOutputStream buffer = null;
        ObjectOutputStream output = null;
        try {
            file = new FileOutputStream(getBackupFile(cacheName));
            buffer = new BufferedOutputStream(file);
            output = new ObjectOutputStream(buffer);
            output.writeObject(coll);
        } catch (IOException ex) {
            throw new TransformException("Unable to backup.", ex);
        } finally {
            try {
                if (output != null)
                    output.close();
                if (buffer != null) {
                    buffer.flush();
                    buffer.close();
                }
                if (file != null)
                    file.close();
            } catch (IOException ex) {

            }
        }
    }

    private void removeBackupOnDrive(String cacheName) throws TransformException {
        File file = getBackupFile(cacheName);
        file.delete();
    }

    private static File getBackupFile(String cacheName) throws TransformException {
        final boolean cacheBackupDirLocationPresent = cacheBackupDir != null;
        if (cacheBackupDirLocationPresent) {
            String cacheBackupPath = cacheBackupDir + File.separator + cacheName + ".ser";
            final File backupFile = new File(cacheBackupDir);
            backupFile.mkdirs();
            return new File(cacheBackupPath);
        } else
            throw new TransformException("PaymentDataCacher not intialized.");
    }

    public static void initUserDataCacher(StringList cacheNames, String cacheBackupDir) throws TransformException {

        if ((cacheBackupDir == null) || (cacheBackupDir.length() == 0))
            throw new TransformException("Invalid cache backup directory provided - " + cacheBackupDir);

        UserDataCacher.cacheBackupDir = setUpCacheBackUpDir(cacheBackupDir);

        for (int i = 0; i < cacheNames.size(); i++) {
            final String cacheName = cacheNames.getValue(i);
            DataObjectSection restoredCache = restoreBackupOnDrive(cacheName);
            synchronized (cacheMap) {
                cacheMap.put(cacheName, restoredCache);
            }
           // VPECacherUtil.addNewCacheEntry(cacheName, allUserDataCache);
        }
        userDataCacheNames=cacheNames;
    }

    private static String setUpCacheBackUpDir(String cacheBackupDir) {
        StringBuilder toRet = new StringBuilder();
        try {
            Class WorkDirectoryIntializer = Class.forName("com.volantetech.volante.services.ApplicationStartupListener");
            Method method1 = WorkDirectoryIntializer.getDeclaredMethod("getWorkDir");
            String workdir = (String) method1.invoke(method1);

            toRet.append(workdir);
            toRet.append(File.separator);

            Class CacheInitiator = Class.forName("com.volantetech.volante.services.ApplicationStartupListener");
            Method method = CacheInitiator.getDeclaredMethod("getApplicationName");
            String appName = (String) method.invoke(method);

            toRet.append(appName);
            toRet.append(File.separator);

            toRet.append(cacheBackupDir);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return toRet.toString();
    }

    private static DataObjectSection restoreBackupOnDrive(String cacheName) throws TransformException {
        final String errMsg = "Unable to restore backup.";
        FileInputStream file = null;
        ObjectInput input = null;

        DataObjectSection retSec = new DataObjectSectionImpl();

        try {
            file = new FileInputStream(getBackupFile(cacheName));
            // The following way ensures that the audit obj is loaded. If we use normal ObjectInputStream, class not found exception
            input = new ObjectInputStreamWithLoader(file, UserDataCacher.class.getClassLoader());
            retSec = (DataObjectSection) input.readObject();
        } catch (EOFException ex) {

        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
            throw new TransformException(errMsg, ex);
        } catch (ClassNotFoundException ex) {
            throw new TransformException(errMsg, ex);
        } finally {
            if (file != null) {
                try {
                    file.close();
                    if (input != null)
                        input.close();
                } catch (IOException ex) {

                }
            }
        }
        return retSec;
    }

}
