
package com.volante.strategy;
/**
 * Created by DV15 on 9/21/2016.
 */
public class StrategyHelper {
    private GetNextElementStrategy strategy;

    public static int executeStrategy(String name, int range, String operation)
    {
        int value = 0;
        if (operation.equals("random")) {
            value = new RandomizeStrategy().doOperation(name,range);
        } else if(operation.equals("round-robin")){
            value = new RoundRobinStrategy().doOperation(name,range);
        }
        //System.out.println("Value = " + value);
        return value;
    }
}
