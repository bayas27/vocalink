package com.volante.vpe.vpeFlows.config;

/**
 * Created by partha on 10/26/2015.
 */
public class VPEFlowConfig {

    String flowName;
    String poolName;
    long repeatInterval;

    public VPEFlowConfig(String flowName, String poolName, long repeatInterval) {
        this.flowName = flowName;
        this.poolName = poolName;
        this.repeatInterval = repeatInterval;
    }

    public String getFlowName() {
        return flowName;
    }

    public String getPoolName() {
        return poolName;
    }

    public long getRepeatInterval() {
        return repeatInterval;
    }

    public String toString() {
        return "PoolName: " + poolName + ", FlowName: " + flowName + ", RepeatInterval: " + repeatInterval;
    }
}
