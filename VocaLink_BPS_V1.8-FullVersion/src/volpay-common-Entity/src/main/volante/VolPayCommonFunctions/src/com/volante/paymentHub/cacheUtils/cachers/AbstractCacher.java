package com.volante.paymentHub.cacheUtils.cachers;

import com.tplus.transform.runtime.TransformException;

/**
 * Created with IntelliJ IDEA.
 * User: Partha
 * Date: 12/23/14
 * Time: 2:05 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractCacher implements VPECacher {
    protected  String cacheName;

    public AbstractCacher (String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheName () throws TransformException {
        return cacheName;
    }
    
    public boolean clearCache() throws TransformException {
    	return false;
    }
    
}
