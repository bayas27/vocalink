package com.volante.paymentHub.cacheUtils.uriListCache;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by partha on 11/5/2015.
 */
public class NormalPriorityURIListCache  {

    static ConcurrentLinkedQueue uriNormalPriorityCache;
	
    public static void init() {
        uriNormalPriorityCache = new ConcurrentLinkedQueue();
    }

    public static void append (String uri) {
    	////System.out.println("Appending uri to normal priority cache - " + uri);
    	// code change --- to be reviewed if (uri != null ){
    	if(uriNormalPriorityCache == null)
    		init();
    		
    	if (uri != null ){
	        uriNormalPriorityCache.add (uri);
        }
    }

    public static String getTop() {
        String uri = null;
        if (uriNormalPriorityCache != null && !uriNormalPriorityCache.isEmpty()) {
            uri = (String) uriNormalPriorityCache.remove();
            ////System.out.println("Getting uri from normal priority cache - " + uri);
        }
        //else
            ////System.out.println("Top uri in normal priority cache is null");

        return uri;
    }
}
