package com.volante.strategy;

import java.util.HashMap;


/**
 * Created by DV15 on 9/21/2016.
 */
public class RoundRobinStrategy implements GetNextElementStrategy {
    static HashMap<String, Integer> stratergyPool = new HashMap();
    // static HashMap<String,Integer> rrEntry = new HashMap<String,Integer>();

    public int doOperation(String name, int count) {
        if (count < 1) {
            throw new RuntimeException("Not able to select endpoint using roundrobin strategy : count -> " + count);
        }
        int toReturn;
        if (count == 1) {
            toReturn = 1;
        } else {
            synchronized (stratergyPool) {
                toReturn = 0;
                if (stratergyPool.containsKey(name)) {
                    toReturn = stratergyPool.get(name);
                    if (toReturn > count) {
                        initialize(name, count);
                    }
                } else {
                    initialize(name, count);
                }
                if (toReturn < count) {
                    ++toReturn;
                } else {
                    toReturn = 1;
                }
            }
            if (toReturn < 1) {
                throw new RuntimeException("Not able to select endpoint using roundrobin strategy : selected-index -> " + toReturn);
            }
            stratergyPool.put(name, toReturn);

        }
        return toReturn;
    }

    public void initialize(String name, int count) {
        //rrEntry.put(name,count);
        stratergyPool.put(name, 0);
    }

    /*public void remove(String name,int count) {
        int range = rrEntry.get(name);
        range -= count;
        rrEntry.remove(name);
        rrEntry.put(name,range);
    }

    public void add(String name, int count) {
        int range = rrEntry.get(name);
        range += count;
        rrEntry.remove(name);
        rrEntry.put(name,range);
    }*/
}
