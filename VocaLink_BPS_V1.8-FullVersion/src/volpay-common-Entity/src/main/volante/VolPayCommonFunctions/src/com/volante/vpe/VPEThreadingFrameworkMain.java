package com.volante.vpe; /**
 * Created by Partha on 15-10-2015.
 */

import com.volante.vpe.vpeFlows.VPEFlowExecutor;
import com.volante.vpe.vpeFlows.config.VPEFlowConfigManager;
import com.volante.vpe.vpeThreadPool.*;
import com.volante.vpe.vpeThreadPool.config.VPEThreadPoolConfigManager;

import java.io.InputStream;
import java.util.GregorianCalendar;
import java.util.Scanner;


public class VPEThreadingFrameworkMain implements Runnable {

    String threadPoolConfigFileName;
    String flowConfigFileName;


    public VPEThreadingFrameworkMain(String threadPoolConfigFileName, String flowConfigFileName) {
        this.threadPoolConfigFileName = threadPoolConfigFileName;
        this.flowConfigFileName = flowConfigFileName;
    }

    public void init () throws Exception {
        InputStream threadPoolConfigFileStream = this.getClass().getClassLoader().getResourceAsStream (threadPoolConfigFileName);
        InputStream flowConfigFileStream = this.getClass().getClassLoader().getResourceAsStream (flowConfigFileName);
        VPEThreadPoolConfigManager.init (threadPoolConfigFileStream);
        VPEFlowConfigManager.init (flowConfigFileStream);
    }

    public void run() {
        try {
            init ();
            VPEThreadPoolManager vpeThreadPoolManager = new VPEThreadPoolManagerSimple();
            VPEFlowExecutor.init (vpeThreadPoolManager);
            VPEFlowExecutor.runScheduledFlows();
        } catch (Exception e) {
            //System.out.println ("Unable to instantiate VPE Threading Framework Main.\n" + e.getMessage());
        }
    }


    public static void main (String args[]) {

        VPEThreadingFrameworkMain vpeThreadingFrameworkMain = new VPEThreadingFrameworkMain (args[0], args[1]);
        vpeThreadingFrameworkMain.run();

        Scanner scan = new Scanner(System.in);
        String myLine = scan.nextLine();
        if (myLine != null)
            try {
                VPEFlowExecutor.stopScheduledFlows();
            }
            catch (Exception e) {
                //System.out.println("Unable to shutdown scheduled processes.\n" + e.getMessage());
            }

    }

}
