package com.volante.vpe.vpeFlows.config;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by partha on 10/26/2015.
 */
public class VPEFlowConfigManager {

    static List vpeScheduledFlowConfigs;
    static List vpeOtherFlowConfigs;


    public static void init (InputStream flowConfigStream) throws Exception {
        vpeScheduledFlowConfigs = new ArrayList();
        vpeOtherFlowConfigs = new ArrayList();
        VPEFlowConfigReader.readVPEFlowConfigs (flowConfigStream);
    }

    public static List getVpeScheduledFlowConfigs() {
        return vpeScheduledFlowConfigs;
    }

    public static void addToScheduledFlowConfig (VPEFlowConfig vpeFlowConfig) {
        vpeScheduledFlowConfigs.add (vpeFlowConfig);
    }

    public static void addToOtherFlowConfig (VPEFlowConfig vpeFlowConfig) {
        vpeOtherFlowConfigs.add (vpeFlowConfig);
    }

    public List getVPEFlowConfigsForCategory (String category) {
        List vpeFlowConfigsForCategory = new ArrayList();

        getVPEFlowConfigsForCategory (category, vpeScheduledFlowConfigs, vpeFlowConfigsForCategory);
        getVPEFlowConfigsForCategory (category, vpeOtherFlowConfigs, vpeFlowConfigsForCategory);

        return vpeFlowConfigsForCategory;
    }

    private void getVPEFlowConfigsForCategory (String category, List vpeFlowConfigs,  List vpeFlowConfigsForCategory) {
        for (int i=0; i< vpeFlowConfigs.size(); i++) {
            VPEFlowConfig vpeFlowConfig = (VPEFlowConfig) vpeFlowConfigs.get(i);
            if (vpeFlowConfig.getPoolName().equalsIgnoreCase (category))
                vpeFlowConfigsForCategory.add (vpeFlowConfig);
        }
    }


    private static void dumpConfigs() {
        dumpConfigs (vpeScheduledFlowConfigs);
        dumpConfigs (vpeOtherFlowConfigs);
    }

    private static void dumpConfigs (List vpeFlowConfigs) {
        for (int i=0; i< vpeFlowConfigs.size(); i++) {
            VPEFlowConfig vpeFlowConfig = (VPEFlowConfig) vpeFlowConfigs.get(i);
            //System.out.println (vpeFlowConfig.toString());
        }
    }


    public static void main (String args[]) throws Exception {
        InputStream flowConfigFileStream = VPEFlowConfigManager.class.getClassLoader().getResourceAsStream (args[0]);
        VPEFlowConfigManager.init (flowConfigFileStream);
        VPEFlowConfigManager.dumpConfigs();
    }

}
