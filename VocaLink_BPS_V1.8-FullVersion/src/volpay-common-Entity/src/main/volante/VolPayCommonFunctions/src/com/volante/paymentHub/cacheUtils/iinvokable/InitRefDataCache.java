package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.collection.StringList;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;


public class InitRefDataCache implements IInvokable {

    static boolean initialized = false;

    public Object run (Object[] args, TransformContext cxt) throws TransformException {
        assertSyntax(args);

        final StringList cacheNames = (StringList) args[0];
        synchronized (InitRefDataCache.class) {
        	if (!initialized)
        		VPECacheManager.initRefDataCache (cacheNames);
        	initialized = true;
        }
		return new Object();
	}


    private void assertSyntax (Object[] args) throws TransformException {
        if ((args.length != 1) || !(args[0] instanceof StringList))
            throw new TransformException ("Init ref data cache needs to be invoked with 1 argument - Cache names: String list");
    }

}
