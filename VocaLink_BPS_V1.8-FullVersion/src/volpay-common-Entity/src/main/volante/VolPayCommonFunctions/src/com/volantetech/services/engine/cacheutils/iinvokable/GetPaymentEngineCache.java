package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.PaymentEngineCacher;
import com.volantetech.services.engine.cacheutils.cachers.ReferenceDataCacher;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */
public class GetPaymentEngineCache implements IInvokable {
	/**
	 * To get all the objects in a cache
	 * @param args cache name
	 * @param cxt
	 * @return object in a cache
	 * @throws TransformException
     */

    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if ((args.length != 1) || !(args[0] instanceof String)) {
			throw new TransformException("Get Cache needs to be invoked with exactly one argument (Cache Name - type: String)");
		}

        final String cacheName = (String) args[0];

		PaymentEngineCacher cacher=null;

		if(UserDataCacher.isUserDataCache(cacheName))
		{
			cacher=UserDataCacher.getInstance();
		}
		else {
			cacher= ReferenceDataCacher.getInstance();
		}

		Object cache = null;

		synchronized (cacher) {
			cache = cacher.getCache (cacheName);
		}

		return cache;
	}
}
