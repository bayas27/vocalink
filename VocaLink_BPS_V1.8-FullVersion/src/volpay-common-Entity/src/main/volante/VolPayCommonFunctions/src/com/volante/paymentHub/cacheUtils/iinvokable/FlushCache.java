package com.volante.paymentHub.cacheUtils.iinvokable;

import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.handler.*;
import com.volante.paymentHub.cacheUtils.cachers.VPECacheManager;
import com.volante.paymentHub.cacheUtils.cachers.VPECacher;
import com.volante.paymentHub.cacheUtils.cachers.VPECacherUtil;


public class FlushCache implements IInvokable {
	com.tplus.transform.util.log.Log log = (com.tplus.transform.util.log.Log)com.tplus.transform.runtime.log.LogFactory.getRuntimeLog( "SetCache" );
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		if (args.length != 1)
			throw new TransformException ("Get cache should be invoked with only one argument - cache name");

        final String cacheName = (String) args[0];
        ////System.out.println ("Cache to be flushed:" + cacheName);
        VPECacher cacher = VPECacheManager.getCacher(cacheName);
		DataObjectSection cache = null;
		////System.out.println ("FlushCache (" + cacheName + "):" + System.identityHashCode(cacher) +
		//	", ID:" + Thread.currentThread().getId());
		//synchronized (VPECacheManager.class) {
		synchronized (cacher) {
			log.debug("CACHE_MANAGER: Flushing cache for " + cacheName);
			cache = cacher.flushCache ();
		}
		return cache;
	}
}
