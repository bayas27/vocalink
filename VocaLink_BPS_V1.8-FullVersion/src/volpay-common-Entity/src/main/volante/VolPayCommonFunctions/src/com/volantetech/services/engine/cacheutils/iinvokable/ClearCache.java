package com.volantetech.services.engine.cacheutils.iinvokable;

import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.handler.IInvokable;
import com.volantetech.services.engine.cacheutils.cachers.PaymentEngineCacher;
import com.volantetech.services.engine.cacheutils.cachers.ReferenceDataCacher;
import com.volantetech.services.engine.cacheutils.cachers.UserDataCacher;

/**
 * Created by Sivaranjani on 5/9/17.
 */

public class ClearCache implements IInvokable {
    /**
     * clear all elements from cache
     * @param args cache name
     * @param cxt
     * @return
     * @throws TransformException
     */
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
        if (args.length != 1)
			throw new TransformException ("Get cache should be invoked with only one argument - cache name");

        final String cacheName = (String) args[0];
        boolean cacheClearStatus = false;
        PaymentEngineCacher cacher=null;

        if(UserDataCacher.isUserDataCache(cacheName))
        {
            cacher=UserDataCacher.getInstance();
        }
        else {
            cacher= ReferenceDataCacher.getInstance();
        }

		synchronized (cacher) {
			cacher.clearCache(cacheName);
		}
        return new Object();
   }

}
