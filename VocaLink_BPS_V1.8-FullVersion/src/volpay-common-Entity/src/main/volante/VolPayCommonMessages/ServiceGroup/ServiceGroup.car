<?xml version="1.0" encoding="UTF-8" ?>
<cartridge xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="5.3.0" name="ServiceGroup" type="Cartridge">
	<version>1.0</version>
	<private>
		<summary>
			<service name="ServiceGroup" version="1.0" type="Internal Message">
			</service>
			<service name="ServiceGroupPM" version="1.0" type="Persistence Manager">
			</service>
			<function name="ForeignKeyValidatorForString"/>
			<function name="ForeignKeyValidatorForIntegers"/>
			<function name="ForeignKeyValidatorForDate"/>
			<function name="ForeignKeyValidatorForMasterEntry"/>
			<function name="ReferenceDataStatusValidater"/>
			<function name="ForeignKeyValidatorForStringList"/>
			<function name="LoadEntityByEntityName"/>
			<function name="LoadEntityByColumnFiltering"/>
			<function name="GetStringColumnByColumnFiltering"/>
			<function name="GetIntegerColumnByColumnFiltering"/>
			<function name="checkFieldFound"/>
		</summary>
	</private>
	<references type="References">
		<reference>
			<type>Cartridge</type>
			<relative-path>..\..\VolPayReferenceDataUtil\VolPayReferenceDataUtil.car</relative-path>
			<absolute-path>D:\GitProjects\VolPay-2.x\volpay-common-Entity\src\main\volante\VolPayReferenceDataUtil\VolPayReferenceDataUtil.car</absolute-path>
		</reference>
	</references>
	<internalmessage name="ServiceGroup" type="InternalMessage">
		<version>1.0</version>
		<normalizedformat>
			<xpath-namespaces>
				<namespace value="http://www.w3.org/2005/xpath-functions" prefix="fn"/>
				<namespace value="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
				<namespace value="java:com.tplus.transform.runtime.swift.SwiftXPathFunctions" prefix="swift"/>
			</xpath-namespaces>
			<fields>
				<field xsi:type="InternalMessageFieldType">
					<name>ServiceGroupCode</name>
					<type>String</type>
					<description><![CDATA[The 4 char ID assigned to the Service Group.]]></description>
					<notnull>true</notnull>
					<length>36</length>
				</field>
				<field xsi:type="InternalMessageFieldType">
					<name>ServiceGroupName</name>
					<type>String</type>
					<description><![CDATA[Name of the Service Group (max. 30 characters).]]></description>
					<notnull>false</notnull>
					<length>100</length>
				</field>
				<field xsi:type="InternalMessageFieldType">
					<name>ServiceGroupDiscription</name>
					<type>String</type>
					<description><![CDATA[Description of the service group (Max. 150 characters)]]></description>
					<notnull>false</notnull>
					<length>1000</length>
				</field>
				<field xsi:type="InternalMessageFieldType">
					<name>Status</name>
					<type>String</type>
					<description><![CDATA["ACTIVE - Office with this status can process payments.
SUSPENDED - This office is not available for selection for any payments received by the system. Office can be made active or deleted from this state by users.
CREATED - Office with this status is created in the system but yet to be approved and made Active.
WAITINGFORAPPROVAL - Office with this status will go for approval.
APPROVED - office with this status is approved but not yet active (due to future effetive date )
FORREVISION - Needs to go back to the user for revision and resubmission for approval.
REJECTED - no further revision is possible.
DELETED - Office with this status is deleted from system and can not be activated again. It will be kept in the system record for audit purpose as well as for data integrity purpose as this would have been associated with payments those were processed before deleting."]]></description>
					<notnull>true</notnull>
					<length>-1</length>
				</field>
				<field xsi:type="InternalMessageFieldType">
					<name>EffectiveFromDate</name>
					<type>DateOnly</type>
					<description><![CDATA[This will be the date from which the office will start processing the payments or will become ACTIVE]]></description>
					<notnull>true</notnull>
				</field>
				<field xsi:type="InternalMessageFieldType">
					<name>EffectiveTillDate</name>
					<type>DateOnly</type>
					<description><![CDATA[This will be the date from which the office will start processing the payments or will become ACTIVE]]></description>
					<notnull>false</notnull>
				</field>
			</fields>
		</normalizedformat>
		<validationrules name="Default" type="ValidationRules">
			<fieldvalidation>
				<name>E1</name>
				<fieldname>ServiceGroupCode</fieldname>
				<rule type="Mandatory"></rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E1</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E2</name>
				<fieldname>Status</fieldname>
				<rule type="Mandatory"></rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E2</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E3</name>
				<fieldname>EffectiveFromDate</fieldname>
				<rule type="Mandatory"></rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E3</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E4</name>
				<fieldname>ServiceGroupCode</fieldname>
				<rule type="MaxLength">36</rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E4</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E5</name>
				<fieldname>ServiceGroupName</fieldname>
				<rule type="MaxLength">100</rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E5</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E6</name>
				<fieldname>ServiceGroupDiscription</fieldname>
				<rule type="MaxLength">1000</rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E6</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E7</name>
				<fieldname>Status</fieldname>
				<rule type="MaxLength">50</rule>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E7</error-code>
			</fieldvalidation>
			<fieldvalidation>
				<name>E8</name>
				<fieldname>Status</fieldname>
				<formula><![CDATA[ReferenceDataStatusValidater(EffectiveFromDate, EffectiveTillDate, $value)]]></formula>
				<severity>error</severity>
				<cascade>true</cascade>
				<error-code>E8</error-code>
				<actionmessage><![CDATA["Invalid configuration of EffectiveFromDate,EffectiveTillDate and Status"]]></actionmessage>
			</fieldvalidation>
		</validationrules>
		<PersistenceManager type="PersistenceManager">
			<databaseformat>
				<table>SERVICEGROUP</table>
				<fields>
					<field xsi:type="DatabaseColumn">
						<name>ServiceGroup_PK</name>
						<type>String</type>
						<description><![CDATA[Auto generated primary key]]></description>
						<sqltype>VARCHAR</sqltype>
						<length>36</length>
						<primarykey>true</primarykey>
						<autogenerate>true</autogenerate>
						<autogenerate-formula>UUID()</autogenerate-formula>
					</field>
					<field xsi:type="DatabaseColumn">
						<name>ServiceGroupCode</name>
						<type>String</type>
						<description><![CDATA[The 4 char ID assigned to the Service Group.]]></description>
						<sqltype>VARCHAR</sqltype>
						<length>36</length>
					</field>
					<field xsi:type="DatabaseColumn">
						<name>ServiceGroupName</name>
						<type>String</type>
						<description><![CDATA[Name of the Service Group (max. 30 characters).]]></description>
						<sqltype>VARCHAR</sqltype>
						<notnull>false</notnull>
						<length>100</length>
					</field>
					<field xsi:type="DatabaseColumn">
						<name>ServiceGroupDiscription</name>
						<type>String</type>
						<description><![CDATA[Description of the service group (Max. 150 characters)]]></description>
						<sqltype>VARCHAR</sqltype>
						<notnull>false</notnull>
						<length>1000</length>
					</field>
					<field xsi:type="DatabaseColumn">
						<name>Status</name>
						<type>String</type>
						<description><![CDATA["ACTIVE - Office with this status can process payments.
SUSPENDED - This office is not available for selection for any payments received by the system. Office can be made active or deleted from this state by users.
CREATED - Office with this status is created in the system but yet to be approved and made Active.
WAITINGFORAPPROVAL - Office with this status will go for approval.
APPROVED - office with this status is approved but not yet active (due to future effetive date )
FORREVISION - Needs to go back to the user for revision and resubmission for approval.
REJECTED - no further revision is possible.
DELETED - Office with this status is deleted from system and can not be activated again. It will be kept in the system record for audit purpose as well as for data integrity purpose as this would have been associated with payments those were processed before deleting."]]></description>
						<sqltype>VARCHAR</sqltype>
						<length>100</length>
					</field>
					<field xsi:type="DatabaseColumn">
						<name>EffectiveFromDate</name>
						<type>DateOnly</type>
						<description><![CDATA[This will be the date from which the office will start processing the payments or will become ACTIVE]]></description>
						<sqltype>DATE</sqltype>
					</field>
					<field xsi:type="DatabaseColumn">
						<name>EffectiveTillDate</name>
						<type>DateOnly</type>
						<description><![CDATA[This will be the date from which the office will start processing the payments or will become ACTIVE]]></description>
						<sqltype>DATE</sqltype>
						<notnull>false</notnull>
					</field>
				</fields>
			</databaseformat>
			<queries>
			</queries>
			<mappingrules>
				<mappings>
					<mapping type="one2one">
						<source-field>ServiceGroupCode</source-field>
						<destination-field>ServiceGroupCode</destination-field>
					</mapping>
					<mapping type="one2one">
						<source-field>ServiceGroupName</source-field>
						<destination-field>ServiceGroupName</destination-field>
					</mapping>
					<mapping type="one2one">
						<source-field>ServiceGroupDiscription</source-field>
						<destination-field>ServiceGroupDiscription</destination-field>
					</mapping>
					<mapping type="one2one">
						<source-field>Status</source-field>
						<destination-field>Status</destination-field>
					</mapping>
					<mapping type="one2one">
						<source-field>EffectiveFromDate</source-field>
						<destination-field>EffectiveFromDate</destination-field>
					</mapping>
					<mapping type="one2one">
						<source-field>EffectiveTillDate</source-field>
						<destination-field>EffectiveTillDate</destination-field>
					</mapping>
				</mappings>
			</mappingrules>
			<elementproperties>
				<propertymap>
					<property name="codegen.java.properties">
						<value>
							<propertymap type="com.tplus.transform.design.PropertiesDef">
								<property name="Data Source" value="transformdb"/>
								<property name="Unique Key Table" value="UniqueKeyGenTable"/>
							</propertymap>
						</value>
					</property>
				</propertymap>
			</elementproperties>
		</PersistenceManager>
		<WebFormDesigner name="Default" type="WebFormDesigner">
			<webformoptions>
				<defaultlabelstyle>DefaultLabelStyle</defaultlabelstyle>
				<defaultlabelcolumnstyle>DefaultLabelColumnStyle</defaultlabelcolumnstyle>
				<defaultrendererstyle>DefaultRendererStyle</defaultrendererstyle>
				<defaultrenderercolumnstyle>DefaultRendererColumnStyle</defaultrenderercolumnstyle>
				<columns>1</columns>
				<labelposition>1</labelposition>
				<indentsubfields>1</indentsubfields>
				<emit-css>true</emit-css>
				<collapsible>Yes</collapsible>
				<instance-separator>Yes</instance-separator>
				<defaultsectionlabelstyle>DefaultSectionStyle</defaultsectionlabelstyle>
				<defaultsectionbackgroundstyle>DefaultSectionColumnStyle</defaultsectionbackgroundstyle>
				<pagetitlestyle>PageTitleStyle</pagetitlestyle>
				<showpagetitle>true</showpagetitle>
				<showpagetitleprefix>true</showpagetitleprefix>
				<formtitle>ServiceGroup</formtitle>
				<formtitlestyle>FormTitleStyle</formtitlestyle>
				<formtablestyle>FormTableStyle</formtablestyle>
				<formwidthinpixels>true</formwidthinpixels>
				<formwidth>800</formwidth>
				<cell-padding>3</cell-padding>
				<cell-spacing>0</cell-spacing>
				<border-size>0</border-size>
				<styles>
					<style>
						<name>DefaultLabelStyle</name>
						<customtext><![CDATA[    font-family: Tahoma, Verdana, Arial, sans-serif;
    padding-left: 6px;
    font-weight: normal;
    font-size: 12px;
    margin:0px;]]></customtext>
					</style>
					<style>
						<name>DefaultLabelColumnStyle</name>
						<customtext><![CDATA[    padding-left: 6px;
    padding-right: 4px;
    border: #a6b3c6 1px solid;
]]></customtext>
					</style>
					<style>
						<name>DefaultRendererStyle</name>
						<customtext><![CDATA[    font-family: Tahoma, Verdana, Arial, sans-serif;
    font-size: 12px;
    margin-top: 3px;
    margin-bottom: 3px;
    background-color: #E7EFFC;
    border: 1px solid #7F9DB9;
    font-weight: normal;
]]></customtext>
					</style>
					<style>
						<name>DefaultRendererColumnStyle</name>
						<customtext><![CDATA[    padding-left: 6px;
    padding-right: 4px;
    padding-top: 2px;
    padding-bottom: 2px;
    border: #a6b3c6 1px solid;
]]></customtext>
					</style>
					<style>
						<name>DefaultSectionStyle</name>
						<customtext><![CDATA[    font-family: Tahoma, Verdana, Arial, sans-serif;
    padding-left: 6px;
    font-weight: bold;
    font-size: 12px;
    margin:0px;]]></customtext>
					</style>
					<style>
						<name>DefaultSectionColumnStyle</name>
						<customtext><![CDATA[    background-color: #ECE9D8;
    padding: 4px;
    border: #a6b3c6 1px solid;
]]></customtext>
					</style>
					<style>
						<name>PageTitleStyle</name>
						<customtext><![CDATA[    font-family: Tahoma, Verdana, Arial, sans-serif;
    font-weight: bold;
    font-size: 12px;
    color: black;
    background-color: #ECE9D8;
    padding: 4px;
    border: #a6b3c6 1px solid;
    margin:0px;]]></customtext>
					</style>
					<style>
						<name>FormTitleStyle</name>
						<customtext><![CDATA[    font-family: Tahoma, Verdana, Arial, sans-serif;
    font-size: 12px;
    font-weight: bold;
    background-color: #FFFFFF;
    padding-left: 6px;
    padding-right: 4px;
    border: #a6b3c6 1px solid;
    margin:0px;]]></customtext>
					</style>
					<style>
						<name>FormTableStyle</name>
						<customtext><![CDATA[    border-collapse: collapse;
    border: 1px solid #A6B3C6;]]></customtext>
					</style>
				</styles>
				<customlookups>
					<customlookup>
						<name>Add Section</name>
						<image>true</image>
						<button>false</button>
						<value>images/add.gif</value>
						<help>Add Element</help>
					</customlookup>
					<customlookup>
						<name>Remove Section</name>
						<image>true</image>
						<button>false</button>
						<value>images/subtract.gif</value>
						<help>Remove Last Element</help>
					</customlookup>
					<customlookup>
						<name>Next Section</name>
						<image>true</image>
						<button>false</button>
						<value>images/next.gif</value>
						<help>Next Instance</help>
					</customlookup>
					<customlookup>
						<name>Previous Section</name>
						<image>true</image>
						<button>false</button>
						<value>images/previous.gif</value>
						<help>Previous Instance</help>
					</customlookup>
					<customlookup>
						<name>DateLookUp</name>
						<image>true</image>
						<button>false</button>
						<value>images/calendar.gif</value>
						<link>javascript:newWindow("${FormName}", "${FieldName}", "${Format}")</link>
					</customlookup>
				</customlookups>
				<images>
					<image>
						<name>Expand</name>
						<filename>images/expandnode.gif</filename>
					</image>
					<image>
						<name>Collapse</name>
						<filename>images/collapsenode.gif</filename>
					</image>
				</images>
			</webformoptions>
			<webformuiformat>
				<fields>
					<field>
						<name>ServiceGroupCode</name>
						<type>String</type>
						<label><![CDATA[Service Group Code]]></label>
						<visible>true</visible>
						<enabled>true</enabled>
						<notnull>false</notnull>
						<labelposition>0</labelposition>
						<rowspan>1</rowspan>
						<columnspan>1</columnspan>
						<newrow>false</newrow>
						<renderer xsi:type="TextField" type="TextField">
							<type>TextField</type>
							<width>36</width>
							<customattributes>
								<property name="WebFormExcerptView" value="true">
								</property>
							</customattributes>
						</renderer>
					</field>
					<field>
						<name>ServiceGroupName</name>
						<type>String</type>
						<label><![CDATA[Service Group Name]]></label>
						<visible>true</visible>
						<enabled>true</enabled>
						<notnull>true</notnull>
						<labelposition>0</labelposition>
						<rowspan>1</rowspan>
						<columnspan>1</columnspan>
						<newrow>false</newrow>
						<renderer xsi:type="TextField" type="TextField">
							<type>TextField</type>
							<width>100</width>
							<customattributes>
								<property name="WebFormExcerptView" value="true">
								</property>
							</customattributes>
						</renderer>
					</field>
					<field>
						<name>ServiceGroupDiscription</name>
						<type>String</type>
						<label><![CDATA[Service Group Description]]></label>
						<visible>true</visible>
						<enabled>true</enabled>
						<notnull>true</notnull>
						<labelposition>0</labelposition>
						<rowspan>1</rowspan>
						<columnspan>1</columnspan>
						<newrow>false</newrow>
						<renderer xsi:type="TextArea" type="TextArea">
							<type>TextArea</type>
							<width>70</width>
							<rows>14</rows>
						</renderer>
					</field>
					<field>
						<name>Status</name>
						<type>String</type>
						<label><![CDATA[Status]]></label>
						<visible>true</visible>
						<enabled>true</enabled>
						<notnull>false</notnull>
						<labelposition>0</labelposition>
						<rowspan>1</rowspan>
						<columnspan>1</columnspan>
						<newrow>false</newrow>
						<renderer xsi:type="Choice" type="Choice">
							<type>Choice</type>
							<choicerenderer>ComboChoiceRenderer</choicerenderer>
							<displayvalue>ACTIVE</displayvalue>
							<actualvalue>ACTIVE</actualvalue>
							<displayvalue>SUSPENDED</displayvalue>
							<actualvalue>SUSPENDED</actualvalue>
							<customattributes>
								<property name="WebFormExcerptView" value="true">
								</property>
							</customattributes>
						</renderer>
					</field>
					<field>
						<name>EffectiveFromDate</name>
						<type>DateOnly</type>
						<label><![CDATA[Effective From Date]]></label>
						<visible>true</visible>
						<enabled>true</enabled>
						<notnull>false</notnull>
						<labelposition>0</labelposition>
						<rowspan>1</rowspan>
						<columnspan>1</columnspan>
						<newrow>false</newrow>
						<dateformat>yyyy-MM-dd</dateformat>
						<renderer xsi:type="TextField" type="TextField">
							<type>TextField</type>
							<width>8</width>
							<customlookupname>DateLookUp</customlookupname>
							<customattributes>
								<property name="WebFormExcerptView" value="true">
								</property>
							</customattributes>
						</renderer>
					</field>
					<field>
						<name>EffectiveTillDate</name>
						<type>DateOnly</type>
						<label><![CDATA[Effective Till Date]]></label>
						<visible>true</visible>
						<enabled>true</enabled>
						<notnull>true</notnull>
						<labelposition>0</labelposition>
						<rowspan>1</rowspan>
						<columnspan>1</columnspan>
						<newrow>false</newrow>
						<dateformat>yyyy-MM-dd</dateformat>
						<renderer xsi:type="TextField" type="TextField">
							<type>TextField</type>
							<width>8</width>
							<customlookupname>DateLookUp</customlookupname>
						</renderer>
					</field>
				</fields>
			</webformuiformat>
		</WebFormDesigner>
		<elementproperties>
			<propertymap>
				<property name="codegen.java.properties">
					<value>
						<propertymap type="com.tplus.transform.design.PropertiesDef">
							<property name="Java Package Name" value="com.transform.internal.%e"/>
							<property name="Jar Name" value="%c.jar"/>
							<property name="Manifest Entries" value="transformrt.jar"/>
							<property name="Data Source" value=""/>
						</propertymap>
					</value>
				</property>
			</propertymap>
		</elementproperties>
	</internalmessage>
	<elementproperties>
		<propertymap>
			<property name="CARTRIDGE_INFO">
				<value>
					<propertymap type="com.tplus.transform.design.CartridgeInfo">
						<property name="GeneratedFiles">
							<value>
								<propertymap>
								</propertymap>
							</value>
						</property>
						<property name="cartridgefilename" value="D:\GitProjects\VolPay-2.x\volpay-common-Entity\src\main\volante\VolPayCommonMessages\ServiceGroup\ServiceGroup.car"/>
					</propertymap>
				</value>
			</property>
			<property name="DOCUMENT_PROPERTIES">
				<value>
					<propertymap>
						<property name="Title" value="ServiceGroup"/>
						<property name="Author" value="vs019"/>
						<property name="Creation Time" value="Wed Sep 21 11:37:48 IST 2016"/>
						<property name="Client Id" value="varadarajmudradi_ch@volantetech.net"/>
						<property name="Last Author" value="System2"/>
						<property name="Last Save Time" value="Tue Feb 13 18:52:45 IST 2018"/>
						<property name="Designer Build" value="1635"/>
						<property name="Revision Number" value="48"/>
					</propertymap>
				</value>
			</property>
			<property name="codegen.java.options">
				<value>
					<propertymap type="com.tplus.transform.design.codegen.java.JavaCodeGeneratorOptions">
						<property name="isJBoss" value="true"/>
						<property name="resource-references">
							<value>
								<propertylist>
								</propertylist>
							</value>
						</property>
						<property name="isWAR" value="false"/>
						<property name="isOSGI" value="true"/>
						<property name="isGenerateALSB" value="false"/>
						<property name="isEJB" value="true"/>
						<property name="osgi-options">
							<value>
								<propertymap>
									<property name="include-dependencies" value="false"/>
								</propertymap>
							</value>
						</property>
						<property name="isOrion" value="true"/>
						<property name="isWebSphere" value="true"/>
						<property name="isVolante" value="true"/>
						<property name="isRemote" value="true"/>
						<property name="isLocal" value="true"/>
						<property name="isWebLogic" value="true"/>
						<property name="isGeronimo" value="true"/>
						<property name="isGenerateJNDILinks" value="true"/>
						<property name="isWLEVS" value="false"/>
						<property name="isGenerateEAR" value="false"/>
					</propertymap>
				</value>
			</property>
			<property name="codegen.java.properties">
				<value>
					<propertymap type="com.tplus.transform.design.PropertiesDef">
						<property name="Java Package Name" value="com.transform.cartridge.%c"/>
						<property name="Max Class Length" value="150"/>
					</propertymap>
				</value>
			</property>
			<property name="codegen.java.bindings">
				<value>
					<propertymap type="com.tplus.transform.design.codegen.DefaultLanguageMapping">
					</propertymap>
				</value>
			</property>
			<property name="codegen.java.general">
				<value>
					<propertymap>
					</propertymap>
				</value>
			</property>
		</propertymap>
	</elementproperties>
	<workspace>
		<selected-element root="D:\GitProjects\VolPay-2.x\volpay-common-Entity\src\main\volante\VolPayCommonMessages\ServiceGroup\ServiceGroup.car">ServiceGroup/ServiceGroup/Persistence Designer/Database Table Design</selected-element>
	</workspace>
</cartridge>

