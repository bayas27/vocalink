/**
 * Created by Ramakrishnan on 23/3/18.
 */

package com.volante.services.dialect;
public class DSDialectCache {
     private static DSDialectCache dsDialectCache;
     private static String dsDialect;
     private DSDialectCache(){}
     public static DSDialectCache getInstance(){
         if(dsDialectCache ==null){
             dsDialectCache =new DSDialectCache();
         }
         return dsDialectCache;
     }
     public void putDialect(String dialect){
         dsDialect=dialect;
     }
     public String getDsDialect(){
         return dsDialect;
     }

}
