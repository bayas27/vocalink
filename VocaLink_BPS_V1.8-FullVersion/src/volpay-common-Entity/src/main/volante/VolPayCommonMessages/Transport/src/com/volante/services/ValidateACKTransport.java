package com.volante.services;
import com.tplus.transform.runtime.*;
import com.tplus.transform.runtime.collection.*;
import java.io.IOException;

public class ValidateACKTransport {

    static MessageFlow messageFlow;
    public static MessageFlow getMessageFlow()  throws javax.naming.NamingException {
        // look up message flow and cache
        if(messageFlow == null) {
			// Get the lookup context for the current environment
			LookupContext lcxt = LookupContextFactory.getLookupContext();

			// Lookup message flow (defined in the cartridge)
			messageFlow = lcxt.lookupMessageFlow("ValidateACKTransport");
        }
        return messageFlow;
    }

    public static boolean process(String referenceCode) {
		boolean result = false;
	       try {
	        MessageFlow messageFlow = getMessageFlow();

			// Create a TransformContext. We have no special properties to set in the context.
			TransformContext cxt = new TransformContextImpl();

	      	// Prepare the input for the message flow.
			
	      	Object[] messageFlowArgs = new Object[] {  referenceCode,  };

	      	// Execute the message flow.
	      	Object[] output = messageFlow.run(messageFlowArgs, cxt);

			// Use output
            result = (Boolean)output[0];

	    }
	    catch(TransformException e) {
	      System.err.println(e.toXMLString());
	    }
	    catch(javax.naming.NamingException e) {
	      e.printStackTrace();
	    }
	    catch(java.rmi.RemoteException e) {
	      e.printStackTrace();
	    }
	    catch(java.io.IOException e) {
	      e.printStackTrace();
	    }
	    return result;
	}
}