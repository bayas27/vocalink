import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import com.volante.component.server.jdbc.ManagedConnectionPool;
import com.tplus.transform.util.sql.connection.ConnectionInfo;
import javax.naming.NamingException;
import com.tplus.transform.util.sql.connection.ConnectionPool;
import java.sql.Connection;
public class GetDSDialectForTransp implements IInvokable {
static com.tplus.transform.util.log.Log logger = com.tplus.transform.util.log.LogFactory.getLog(GetDSDialectForTransp.class);
    public Object run(Object[] args, TransformContext cxt) throws TransformException {
		String DSName=null;
	if(args.length == 1)
		DSName = (String)args[0];
	ManagedConnectionPool obj  = null;
	ConnectionPool connectionPool = null;
	String[] dialects = {"hsql", "oracle", "mysql", "db2", "postgres"};
	String dialect = null;
	Connection con = null;
    boolean newConnection=false;
	try {
	LookupContext lcxt = LookupContextFactory.getLookupContext();
		
	connectionPool = LookupContextFactory.getLookupContext().lookupDataSource(DSName);
	ConnectionInfo info = connectionPool.getConnectionInfo();
		
		//  obj = (ManagedConnectionPool)lcxt.lookup( DSName );
	//ConnectionInfo info = obj.getConnectionInfo();
	dialect=info.getProperty("dialect");
	if(dialect == null){
	if(con==null){
	con = connectionPool.getConnection();
	newConnection=true;
	}
	
	String databaseProductName = con.getMetaData().getDatabaseProductName();
	if (databaseProductName != null) {
			databaseProductName = databaseProductName.toLowerCase();
			for (int i = 0; i < dialects.length; ++i) {
			    if (databaseProductName.contains(dialects[i])) {
				dialect= DBDialect.valueOf(dialects[i]).getName();
				break;
			    }
			}
			//Special Case - MSSQL
			if (databaseProductName.contains("microsoft") && databaseProductName.contains("sql") && databaseProductName.contains("server")) {
			    dialect = DBDialect.valueOf("mssql").getName();
			}
	
	}
	} 
	}
		
		catch (NamingException e) {
			e.printStackTrace();
	    }
		catch(Exception e)
		{
			e.printStackTrace();
			logger.debug("the Error in GetDSDialect is :"+e.getMessage());
		}
	  try{
	  if(newConnection)
	  connectionPool.releaseConnection(con);
	  }
	  catch(Exception e){
	  e.printStackTrace();
	  }
      return dialect;
  }
}
