package com.volantetech.volante.services.databasefunction;


import java.rmi.RemoteException;
import javax.naming.NamingException;
import com.tplus.transform.runtime.formula.DataObjectFunctions;
import com.tplus.transform.runtime.DataObject;
import com.tplus.transform.runtime.DataObjectSection;
import com.tplus.transform.runtime.TransformContext;
import com.tplus.transform.runtime.TransformException;
import com.tplus.transform.runtime.formula.DatabaseFunctions;
import com.tplus.transform.runtime.handler.IInvokable;
import com.tplus.transform.runtime.log.LogFactory;
import com.tplus.transform.util.log.Log;


public class DBSecQueryCustom{

    public static boolean run(DataObject curTransportObj,String sqlStatement, String param1, String param2) throws TransformException {
    	Log log = LogFactory.getRuntimeLog();
        Object[] sql_parmas_1 = new Object[]{param1, param2};
        String sectionName = "Condition";

        String curTransportCode = (String)curTransportObj.getField("TransportCode");
        int curTransportSectionSize = (int)curTransportObj.getSection(sectionName).size();
        boolean isdefaultTransportPresent= false;
        DataObject defaultTransportObj = null;

        try {
            //Check if the transport is empty
            if(curTransportSectionSize > 0)
            {
                return true;
            }
            else {
                DataObjectSection transportDataObjectSection = DatabaseFunctions.dbSQLQueryMessage("Transport", sqlStatement, sql_parmas_1);
                for (int i = 0; i < transportDataObjectSection.size(); i++) {
                    DataObject transportObj = transportDataObjectSection.getElement(i);
                    if(transportObj.getSection(sectionName).size() == 0) {
                        isdefaultTransportPresent = true;
                        defaultTransportObj = transportObj;
                        break;
                    }
                }
                if(isdefaultTransportPresent) {
                    if(defaultTransportObj.getField("TransportCode").equals(curTransportCode))
                    {
                       return true;
                    }
                    return false;
                }
                else {
                        return true;
                }

            }
        }
        catch (NamingException ne)
        {
            return false;
        }
        catch (RemoteException re)
        {
            return false;
        }
    }
}