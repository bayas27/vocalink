package com.volante.services.dialect;

import com.tplus.transform.lang.ExceptionUtil;
import com.tplus.transform.runtime.handler.*;
import com.tplus.transform.runtime.*;
import com.volante.component.server.jdbc.ManagedConnectionPool;
import com.tplus.transform.util.sql.connection.ConnectionInfo;
import javax.naming.NamingException;
import com.tplus.transform.util.sql.connection.ConnectionPool;
import java.sql.Connection;
import java.sql.SQLException;

public class GetDSDialectForTransp implements IInvokable {
	static com.tplus.transform.util.log.Log logger = com.tplus.transform.util.log.LogFactory.getLog(GetDSDialectForTransp.class);
	public Object run(Object[] args, TransformContext cxt) throws TransformException {
		String DSName=null;
		if(args.length == 1)
			DSName = (String)args[0];
		ManagedConnectionPool obj  = null;
		ConnectionPool connectionPool = null;
		String dialect = null;
		String dsDialectFromCache=null;
		try {
			LookupContext lcxt = LookupContextFactory.getLookupContext();
			connectionPool = LookupContextFactory.getLookupContext().lookupDataSource(DSName);
			ConnectionInfo info = connectionPool.getConnectionInfo();
			dialect=info.getProperty("dialect");
			if(dialect == null){
				dsDialectFromCache= DSDialectCache.getInstance().getDsDialect();
				if(dsDialectFromCache==null){
					dsDialectFromCache=autoGuessDSDialect(connectionPool);
					DSDialectCache.getInstance().putDialect(dsDialectFromCache);
					dialect=dsDialectFromCache;
				}
				else{
			
					dialect=dsDialectFromCache;
				}
			}
		}

		catch (NamingException e) {
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//logger.debug("the Error in GetDSDialect is :"+e.getMessage());
			logger.debug("the Error in GetDSDialect is : [Exception: " + e.toString() + " ]" + ", Root Cause: "+e.getCause() + " Stack Trace:"+ ExceptionUtil.toTraceString(e));
		}

		return dialect;
	}

	private String autoGuessDSDialect(ConnectionPool connectionPool) {
		String dialectFromConnection = null;
		Connection con = null;
		String[] dialects = {"hsql", "oracle", "mysql", "db2", "postgres"};
		try {
			con = connectionPool.getConnection();

			String databaseProductName = null;

			databaseProductName = con.getMetaData().getDatabaseProductName();
			if (databaseProductName != null) {
				databaseProductName = databaseProductName.toLowerCase();
				for (int i = 0; i < dialects.length; ++i) {
					if (databaseProductName.contains(dialects[i])) {
						dialectFromConnection = DBDialect.valueOf(dialects[i]).getName();
						break;
					}
				}
				//Special Case - MSSQL
				if (databaseProductName.contains("microsoft") && databaseProductName.contains("sql") && databaseProductName.contains("server")) {
					dialectFromConnection = DBDialect.valueOf("mssql").getName();
				}


			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				connectionPool.releaseConnection(con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return dialectFromConnection;
	}
}
