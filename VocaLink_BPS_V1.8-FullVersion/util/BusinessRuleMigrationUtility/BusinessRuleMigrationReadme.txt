Read me:

1)This is a migration utility to convert the Old Business Rule Structure to new one.

2)If the old rule uses deprecated functions or fields, then the business rule will be suspended and the error will be logged.

3)This utility must be executed,before processing the payment.


How to Run:

Linux:

1)Open Terminal. 

2)use sh command to run the RuleStructureMigrationUtil.sh


Windows:

Use the cmd command to run the RuleStrucutreMigrationUtil.cmd
