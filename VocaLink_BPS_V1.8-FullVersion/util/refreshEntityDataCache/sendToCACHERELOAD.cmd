@echo off
echo Sending Data to CACHERELOAD
Set FILENAME=%1
call setenv.cmd
java -classpath %CLASSPATH% com.tplus.transform.runtime.external.queueutils.MQSendFile -config prop\CACHERELOAD.send.properties %FILENAME%
pause