echo Sending Data to CACHERELOAD
export FILENAME="$1"
export CLASSPATH=.:../../VolPayHubRT/VolPayHub/WEB-INF/lib/*:runtimeRT/lib/*:
java -classpath "$CLASSPATH" com.tplus.transform.runtime.external.queueutils.MQSendFile -config prop/CACHERELOAD.send.properties "$FILENAME"
